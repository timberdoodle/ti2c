/*
| A filepath trace.
*/
def.extend = 'list@ti2c:Trace/Step/Key';

import { Self as Plan    } from '{ti2c:Path/Plan}';
import { Self as StepKey } from '{ti2c:Trace/Step/Key}';

/*
| Returns a path with a string chunk (possibly containing '/' ) appended.
| The result is a dir path.
*/
def.proto.addChunkD =
	function( str )
{
	const parts = str.split( '/' );
	const plen = parts.length;

	let path = this;

	for( let p = 0; p < plen; p++ )
	{
		path = path.dir( parts[ p ] );
	}

	return path;
};

/*
| Returns a path with a string chunk (possibly containing '/' ) appended.
| The result is a file path.
*/
def.proto.addChunkF =
	function( str )
{
	const parts = str.split( '/' );
	const plen = parts.length;

	let path = this;

	for( let p = 0; p < plen - 1; p++ )
	{
		path = path.dir( parts[ p ] );
	}

	return path.file( parts[ plen - 1 ] );
};

/*
| A path of absolute root.
*/
def.staticLazy.aRoot = ( ) =>
	Self.Elements(
		StepKey.create(
			'key', '/',
			'name', 'root',
			'plan', Plan.root
		)
	);

/*
| Turns the trace into a string (for debugging).
*/
def.lazy.asString =
	function( )
{
	let s = '';
	for( let step of this )
	{
		s += step.key;
		if( step.name === 'dir' ) s += '/';
	}
	return s;
};

/*
| Returns a path with a dir appended.
*/
def.proto.d =
def.proto.dir =
	function( name )
{
	const last = this.last;
	const lplan = last.plan;
	let sub = lplan.subs.get( 'dir' );
	if( typeof( sub ) === 'function' ) sub = sub( );

/**/if( CHECK && !sub ) throw new Error( this.name, name );

	// removes ending slash
	if( name.endsWith( '/' ) ) name = name.substr( 0, name.length - 1 );

	// name must not have another slash
	if( name.indexOf( '/' ) >= 0 ) throw new Error( );

	// shouldn't happen
	if( !sub.key ) throw new Error( );

	return(
		this.append(
			StepKey.create(
				'key', name,
				'name', 'dir',
				'plan', sub,
			)
		)
	);
};

/*
| The extension of file paths.
*/
def.lazy.ext =
	function( )
{
	const last = this.last;

	if( last.name !== 'file') return undefined;

	const filename = last.key;
	const iod = filename.lastIndexOf( '.' );

	if( iod < 0 ) return undefined;

	return filename.substr( iod + 1 );
};

/*
| Returns a path with a file appended.
*/
def.proto.f =
def.proto.file =
	function( name )
{
	const last = this.last;
	const lplan = last.plan;

	let sub = lplan.subs.get( 'dir' );
	if( typeof( sub ) === 'function' ) sub = sub( );

/**/if( CHECK && !sub ) throw new Error( this.name, name );

	// removes ending slash
	if( name.endsWith( '/' ) ) name = name.substr( 0, name.length - 1 );

	// name must not have another slash
	if( name.indexOf( '/' ) >= 0 ) throw new Error( );

	// shouldn't happen
	if( !sub.key ) throw new Error( );

	return(
		this.append(
			StepKey.create(
				'key', name,
				'name', 'file',
				'plan', sub,
			)
		)
	);
};

/*
| Creates the path trace from a string.
*/
def.static.FromString =
	function( s )
{
	let p;
	if( s.substr( 0, 2 ) === './' )
	{
		p = Self.rRoot;
		s = s.substr( 2 );
	}
	else if( s[ 0 ] === '/' )
	{
		p = Self.aRoot;
		s = s.substr( 1 );
	}
	else
	{
		throw new Error( );
	}

	const ar = s.split( '/' );
	const alen = ar.length;
	for( let a = 0; a < alen - 1; a++ )
	{
		p = p.dir( ar[ a ] + '/' );
	}

	const last = ar[ alen - 1 ];
	if( last !== '' )
	{
		p = p.file( ar[ alen - 1 ] );
	}
	return p;
};

/*
| True if this path is absolute.
*/
def.lazy.isAbsolute =
	function( )
{
	return this.get( 0 ).key === '/';
};

/*
| True if this path is relative.
*/
def.lazy.isRelative =
	function( )
{
	return this.get( 0 ).key === './';
};

/*
| Returns the parent path.
*/
def.lazy.p =
def.lazy.parent =
	function( )
{
	return this._isRParents ? this.dir( '..' ) : this.back;
};

/*
| Resolves a relative path on an absolute dir.
*/
def.proto.resolve =
	function( dir )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 1 ) throw new Error( );
/**/	if( dir.ti2ctype !== Self ) throw new Error( );
/**/	if( !dir.isAbsolute ) throw new Error( );
/**/	if( dir.last.name !== 'dir') throw new Error( );
/**/	if( !this.isRelative ) throw new Error( );
/**/}

	for( let a = 1, al = this.length - 1; a < al; a++ )
	{
		dir = dir.dir( this.get( a ).key );
	}

	return dir.file( this.last.key );
};

/*
| True if this path starts with another path.
*/
def.proto.startsWith =
	function( path )
{
/**/if( CHECK && path.ti2ctype !== Self ) throw new Error( );

	const plen = path.length;
	const tlen = this.length;

	if( plen > tlen ) return false;

	for( let a = 0; a < plen; a++ )
	{
		if( path.get( a ) !== this.get( a ) ) return false;
	}

	return true;
};

/*
| Appends a string to the path.
*/
def.proto.string =
	function( s )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 1 ) throw new Error( );
/**/	if( typeof( s ) !== 'string' ) throw new Error( );
/**/}

	let path = this;
	const pa = s.split( '/' );
	let a = 0, alen = pa.length - 1;
	for( ; a < alen; a++ )
	{
		const e = pa[ a ];
		if( e === '' ) continue;
		path = path.dir( e );
	}

	if( alen >= 0 )
	{
		const e = pa[ alen ];
		if( e !== '' ) path = path.file( e );
	}

	return path;
};

/*
| A path of a relative root.
*/
def.staticLazy.rRoot = ( ) =>
	Self.Elements(
		StepKey.create(
			'key', './',
			'name', 'root',
			'plan', Plan.root
		)
	);

/*
| Returns a path with a step back.
*/
def.lazy.back =
	function( )
{
	const len = this.length;
	if( len <= 1 ) throw new Error( );
	return this.slice( 0, len - 2 );
};

/*
| True if this is a relative path that has '../' components only.
*/
def.lazy._isRParents =
	function( )
{
	const len = this.length;
	if( this.get( 0 ).key !== './' ) return false;
	for( let a = 1; a < len; a++ )
	{
		if( this.get( a ).key !== '../' ) return false;
	}
	return true;
};
