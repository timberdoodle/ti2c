/*
| Plan of filesystem paths.
*/
def.abstract = true;

import { Self as Plan      } from '{ti2c:Plan/Self}';
import { Self as GroupSubs } from '{group@(function,ti2c:Plan/Piece)}';

/*
| A dir.
*/
def.staticLazy.dir = ( ) =>
	Plan.Build( {
		key: true,
		name: 'dir',
		subs:
			GroupSubs.Table( {
				dir: ( ) => Self.dir,
				file: ( ) => Self.file
			} ),
	} );

/*
| A file.
*/
def.staticLazy.file = ( ) =>
	Plan.Build( {
		key: true,
		name: 'file',
	} );

/*
| '/' or './'
*/
def.staticLazy.root = ( ) =>
	Plan.Build( {
		key: true,
		name: 'root',
		subs:
			GroupSubs.Table( {
				dir: ( ) => Self.dir,
				file: ( ) => Self.file
			} ),
	} );
