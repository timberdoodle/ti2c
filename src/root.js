/*
| ti2c interface.
*/

// the ti2c module.
// FIXME dont do global
const ti2c = global.ti2c = { };

export { ti2c as default };

// global pass flag for creators
global.pass = Object.freeze( { } );

{
	// silences the experimental vm warnings
	const originalEmit = process.emit;
	process.emit =
		( name, data, ...args ) =>
	{
		if(
			name === 'warning'
			&& typeof data === 'object'
			&& data.name === 'ExperimentalWarning'
		)
		{
			return false;
		}
		else
		{
			return originalEmit.apply( process, [ name, data, ...args ] );
		}
	};
}

const core = await import( './core/self.js' );

/*
| Bootstrapping.
*/
const ending = 'src/root.js';
const filename = import.meta.url;
if( !filename.endsWith( ending ) ) throw new Error( );
const rootDir = filename.substring( 7, filename.length - ending.length );
const PackageManager = await core.init( rootDir );

/*
| Returns a list of types of all entries.
*/
ti2c.getAllEntryTypes =
	function( )
{
	return PackageManager.getAllEntryTypes( );
};

/*
| Returns an entry by path.
*/
ti2c.getEntry =
	function( type )
{
	return PackageManager.getEntry( type );
};

/*
| Returns a package by name.
*/
ti2c.getPackage =
	function( name )
{
	return PackageManager.getPackage( name );
};

/*
| Registers a ti2c package.
|
| ~args: register arguments:
|    name    [STRING]: name of the package
|    meta    [OBJECT]: meta object
|    source  [STRING]: source dir of the package
|    relPath [STRING]: relative path of the caller
|                      (used to determine rootDir)
|    codegen [STRING]: optional
|                      generated code will be placed here
|                      at any runtime only one codegen dir
|                      must be present
|
| ~return: the package spec.
*/
ti2c.register =
	async function( ...args )
{
	let name, meta, filename, source, relPath, codegen;

	for( let a = 0, alen = args.length; a < alen; a += 2 )
	{
		const val = args[ a + 1 ];
		switch( args[ a ] )
		{
			case 'codegen':
				if( typeof( val ) !== 'string' )
				{
					throw new Error( 'invalid "codegen"' );
				}
				if( !val.endsWith( '/' ) ) throw new Error( '"codegen" dir must end with "/"' );
				codegen = val;
				continue;

			case 'name':
				if( typeof( val ) !== 'string' || val === '' )
				{
					throw new Error( 'package "name" missing' );
				}
				name = val;
				continue;

			case 'meta':
				if( !val.url ) throw new Error( 'invalid "meta"' );
				meta = val;
				filename = meta.url;
				if( !filename.startsWith( 'file://' ) ) throw new Error( 'invalid "meta"' );
				filename = filename.substr( 7 );
				continue;

			case 'relPath':
				if( typeof( val ) !== 'string' || val === '' )
				{
					throw new Error( '"invalid "relPath"' );
				}
				relPath = val;
				continue;

			case 'source':
				if( typeof( val ) !== 'string' ) throw new Error( '"source" dir missing' );
				if( !val.endsWith( '/' ) ) throw new Error( '"source" dir must end with "/"' );
				source = val;
				continue;

			default:
				throw new Error( 'unknown argument: ' + args[ a ] );
		}
	}

	if( !filename.endsWith( relPath + '.js' ) )
	{
		throw new Error( '"relPath" does not end with current filename' );
	}

	const srcADir = filename.substr( 0, filename.length - relPath.length - 3 );
	if( !srcADir.endsWith( source ) )
	{
		throw new Error( '"source" dir not a part of current filename' );
	}

	const rootDir = srcADir.substr( 0, srcADir.length - source.length );

	return(
		await PackageManager.addPackage(
			rootDir,
			srcADir,
			name,
			codegen,
		)
	);
};
