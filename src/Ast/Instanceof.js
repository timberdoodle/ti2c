/*
| Ast; test if left is an instance of right.
*/
def.extend = 'Ast/Base/DualOp';

def.proto.operand = 'instanceof';
