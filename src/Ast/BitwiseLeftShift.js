/*
| Ast; bitwise left shift operation.
*/
def.extend = 'Ast/Base/DualOp';

def.proto.operand = '<<';
