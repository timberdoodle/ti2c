/*
| Ast; a try statement.
|
| TODO add finallyStatement.
*/
def.extend = 'Ast/Base/Self';

def.attributes =
{
	// the catch statement
	catchStatement: { type: 'Ast/Block' },

	// the exception var
	exceptionVar: { type: 'Ast/Var' },

	// the tried statement
	tryStatement: { type: 'Ast/Block' },
};

/*
| Custom inspect.
*/
def.proto._inspect =
	function( recurse )
{
	return(
		'try{ '
		+ recurse( this.tryStatement )
		+ '} catch( ' + recurse( this.exceptionVar )
		+ ' ) { ' + recurse( this.catchStatement ) + ' }'
	);
};
