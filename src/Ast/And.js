/*
| Ast; logical and operation.
*/
def.extend = 'Ast/Base/DualOp';

def.proto.operand = '&&';
