/*
| Ast; continues current loop.
*/
def.extend = 'Ast/Base/Self';

def.singleton = true;

/*
| Custom inspect.
*/
def.proto._inspect = function( recurse ) { return 'continue'; };
