/*
| Ast; post decrement (x-- operator)
*/
def.extend = 'Ast/Base/PostOp';

def.proto.operand = '--';
