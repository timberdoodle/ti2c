/*
| Tests for equality.
*/
def.extend = 'Ast/Base/DualOp';

def.proto.operand = '===';
