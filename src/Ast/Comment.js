/*
| Ast; a comment.
*/
def.extend = 'Ast/Base/Self';

def.attributes =
{
	lines: { type: 'list@string' },
};

import { Self as ListString } from '{list@string}';

/*
| Creates the comment by an array of lines.
*/
def.static.Array =
	function( lines )
{
	return Self.create( 'lines', ListString.Array( lines ) );
};

/*
| Custom inspect.
*/
def.proto._inspect =
	function( recurse )
{
	let result = '';
	for( let line of this.lines ) result += '/* ' + line + ' */';
	return result;
};
