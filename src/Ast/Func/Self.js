/*
| Ast; a function.
*/
def.extend = 'Ast/Base/Self';

def.attributes =
{
	// arguments
	args:
	{
		type: 'list@Ast/Func/Arg',
		defaultValue: 'import("list@Ast/Func/Arg").Empty',
	},

	// true if defined async
	isAsync: { type: 'boolean', defaultValue: 'false' },

	// true if a generator
	isGenerator: { type: 'boolean', defaultValue: 'false' },

	// function body
	body: { type: [ 'undefined', 'Ast/Block' ] },

	// function name
	// undefined in case of unnamed inline functions.
	name: { type: [ 'undefined', 'string' ] },
};

import { Self as AstFuncArg } from '{ti2c:Ast/Func/Arg}';

/*
| Convenience shortcut.
|
| ~name: of the argument
| ~comment: for the argument
| ~rest: '...' or undefined.
|
| ~return: the function with an argument appended.
*/
def.proto.arg =
	function( name, comment, rest )
{
/**/if( CHECK && rest && rest !== '...' ) throw new Error( );
	const arg = AstFuncArg.create( 'name', name, 'comment', comment, 'rest', !!rest );
	return this.create( 'args', this.args.append( arg ) );
};

/*
| Custom inspect.
*/
def.proto._inspect =
	function( recurse )
{
	let text = 'function( ';
	const args = this.args;
	if( args.length === 0 )
	{
		text += ')';
	}
	else
	{
		let first = true;
		for( let arg of args )
		{
			if( first ) first = false; else text += ', ';
			text += recurse( arg );
		}
	}
	return text + recurse( this.body );
};
