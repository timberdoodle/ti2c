/*
| Ast; a function argument.
*/
def.extend = 'Ast/Base/Self';

def.attributes =
{
	// argument comment
	comment: { type: [ 'undefined', 'string' ] },

	// argument name
	name: { type: [ 'undefined', 'string' ] },

	// true if it's a ...arg
	rest: { type: 'boolean', defaultValue: 'false' },
};

/*
| Custom inspect.
*/
def.proto._inspect = function( recurse ) { return this.name; };
