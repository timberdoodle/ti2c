/*
| Ast; bitwise and assign operation.
*/
def.extend = 'Ast/Base/DualOp';

def.proto.operand = '&=';
