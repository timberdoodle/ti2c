/*
| Ast, a mathematical division.
*/
def.extend = 'Ast/Base/DualOp';

def.proto.operand = '/';
