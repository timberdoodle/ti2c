/*
| ast; array literals.
*/
def.extend = 'Ast/Base/Self';

def.attributes =
{
	entries: { type: 'list@<ti2c:Ast/Type/Expr' },
};

import { Self as ListEntries } from '{list@<ti2c:Ast/Type/Expr}';

/*
| Appends an entry.
*/
def.proto.append =
	function( entry )
{
	return Self.create( 'entries', this.entries.append( entry ) );
};

/*
| Creates the array literal.
*/
def.static.Array =
	function( entries )
{
	return Self.create( 'entries', ListEntries.Array( entries ) );
};

/*
| Creates the array literal.
*/
def.static.Elements =
	function( ...entries )
{
	return Self.create( 'entries', ListEntries.Array( entries ) );
};

/*
| Creates an empty declaration.
*/
def.staticLazy.Empty =
	function( )
{
	return Self.create( 'entries', ListEntries.Empty );
};

/*
| Custom inspect.
*/
def.proto._inspect =
	function( recurse )
{
	const entries = this.entries;

	if( entries.length === 0 ) return '[ ]';

	let result = '[';
	let first = true;
	for( let arg of this )
	{
		if( first ) first = false; else result += ', ';
		result += recurse( arg );
	}
	return result + ' ]';
};
