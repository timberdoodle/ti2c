/*
| ast negation (unary minus)
*/
def.extend = 'Ast/Base/PreOp';

def.proto.operand = '-';
