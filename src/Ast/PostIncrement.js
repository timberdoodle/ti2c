/*
| Ast; a post increment (x++).
*/
def.extend = 'Ast/Base/PostOp';

def.proto.operand = '++';
