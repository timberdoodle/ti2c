/*
| ast, tests for difference.
*/
def.extend = 'Ast/Base/DualOp';

def.proto.operand = '!==';
