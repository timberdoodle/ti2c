/*
| Various shorthands for abstract syntax trees.
*/
def.abstract = true;

import { Self as And            } from '{ti2c:Ast/And}';
import { Self as ArrayLiteral   } from '{ti2c:Ast/ArrayLiteral}';
import { Self as Assign         } from '{ti2c:Ast/Assign}';
import { Self as AstNumber      } from '{ti2c:Ast/Number}';
import { Self as AstString      } from '{ti2c:Ast/String}';
import { Self as Block          } from '{ti2c:Ast/Block}';
import { Self as Boolean        } from '{ti2c:Ast/Boolean}';
import { Self as Break          } from '{ti2c:Ast/Break}';
import { Self as Call           } from '{ti2c:Ast/Call}';
import { Self as Check          } from '{ti2c:Ast/Check}';
import { Self as Comma          } from '{ti2c:Ast/Comma}';
import { Self as Comment        } from '{ti2c:Ast/Comment}';
import { Self as Condition      } from '{ti2c:Ast/Condition}';
import { Self as Const          } from '{ti2c:Ast/Const}';
import { Self as Continue       } from '{ti2c:Ast/Continue}';
import { Self as Declaration    } from '{ti2c:Ast/Declaration}';
import { Self as Delete         } from '{ti2c:Ast/Delete}';
import { Self as Differs        } from '{ti2c:Ast/Differs}';
import { Self as Dot            } from '{ti2c:Ast/Dot}';
import { Self as Equals         } from '{ti2c:Ast/Equals}';
import { Self as For            } from '{ti2c:Ast/For}';
import { Self as ForIn          } from '{ti2c:Ast/ForIn}';
import { Self as ForOf          } from '{ti2c:Ast/ForOf}';
import { Self as Func           } from '{ti2c:Ast/Func/Self}';
import { Self as GreaterThan    } from '{ti2c:Ast/GreaterThan}';
import { Self as If             } from '{ti2c:Ast/If}';
import { Self as Import         } from '{ti2c:Ast/Import}';
import { Self as InstanceOf     } from '{ti2c:Ast/Instanceof}';
import { Self as LessThan       } from '{ti2c:Ast/LessThan}';
import { Self as Let            } from '{ti2c:Ast/Let}';
import { Self as ListString     } from '{list@string}';
import { Self as Member         } from '{ti2c:Ast/Member}';
import { Self as Multiply       } from '{ti2c:Ast/Multiply}';
import { Self as MultiplyAssign } from '{ti2c:Ast/MultiplyAssign}';
import { Self as New            } from '{ti2c:Ast/New}';
import { Self as Not            } from '{ti2c:Ast/Not}';
import { Self as Null           } from '{ti2c:Ast/Null}';
import { Self as ObjLiteral     } from '{ti2c:Ast/ObjLiteral}';
import { Self as Or             } from '{ti2c:Ast/Or}';
import { Self as Plus           } from '{ti2c:Ast/Plus}';
import { Self as PlusAssign     } from '{ti2c:Ast/PlusAssign}';
import { Self as PreIncrement   } from '{ti2c:Ast/PreIncrement}';
import { Self as Return         } from '{ti2c:Ast/Return}';
import { Self as Sep            } from '{ti2c:Ast/Sep}';
import { Self as Switch         } from '{ti2c:Ast/Switch}';
import { Self as Throw          } from '{ti2c:Ast/Throw}';
import { Self as Typeof         } from '{ti2c:Ast/Typeof}';
import { Self as Undefined      } from '{ti2c:Ast/Undefined}';
import { Self as Var            } from '{ti2c:Ast/Var}';
import { Self as VarDec         } from '{ti2c:Ast/VarDec}';
import { Self as While          } from '{ti2c:Ast/While}';
import { Self as Yield          } from '{ti2c:Ast/Yield}';
import { Self as Parser         } from '{ti2c:Parser/Self}';

/*
| Ensures argument becomes a block.
*/
const ensureBlock =
	function( arg )
{
	if( arg.ti2ctype === Block ) return arg;
	return Block.Empty.append( Parser.statement( arg ) );
};

/*
| Returns the block with a parsed statement appended.
*/
def.static.$ =
	function( ...args )
{
	return Parser.parseArray( args, 'statement' );
};

/*
| Shorthand for creating ands.
*/
def.static.and =
	function( left, right /* or more */ )
{
	if( arguments.length > 2 )
	{
		const args = Array.prototype.slice.call( arguments );
		args.splice(
			0, 2,
			And.create(
				'left', Parser.expr( left ),
				'right', Parser.expr( right ),
			)
		);
		return Self.and.apply( this, args );
	}
	return And.create( 'left', Parser.expr( left ), 'right', Parser.expr( right ) );
};

/*
| Shorthand for creating blocks.
*/
def.staticLazy.array = ( ) =>
	ArrayLiteral.Empty;

/*
| Shorthand for creating assignments.
*/
def.static.assign =
	function( left, right )
{
	return(
		Assign.create(
			'left', Parser.expr( left ),
			'right', Parser.expr( right )
		)
	);
};

/*
| Shorthand for creating blocks.
*/
def.staticLazy.block =
	( ) =>
	Block.Empty;

/*
| Shorthand for ast break.
*/
def.staticLazy.break =
	( ) =>
	Break.create( );

/*
| Shorthand for creating calls.
|
| ~func: the function to call
| ~..args: arguments of the call
*/
def.static.call =
	function( func, ...args )
{
	let call = Call.create( 'func', Parser.expr( func ) );
	for( let arg of args )
	{
		call = call.arg( arg );
	}

	return call;
};

/*
| Shorthand for creating Ast check blocks.
*/
def.static.check =
	function( arg )
{
	const Ast = Parser.statement.apply( Parser, arguments );
	return Check.create( 'block', ensureBlock( Ast ) );
};

/*
| Shorthand for creating comma operators
*/
def.static.comma =
	function( left, right /* or more */ )
{
	if( arguments.length > 2 )
	{
		const args = Array.prototype.slice.call( arguments );
		args.splice(
			0, 2,
			Comma.create(
				'left', Parser.expr( left ),
				'right', Parser.expr( right )
			)
		);
		return Self.comma.apply( this, args );
	}
	return(
		Comma.create(
			'left', Parser.expr( left ),
			'right', Parser.expr( right )
		)
	);
};

/*
| Shorthand for creating comments
*/
def.static.comment =
	function( ...args )
{
	return Comment.create( 'lines', ListString.Array( args ) );
};

/*
| Shorthand for creating conditions.
*/
def.static.condition =
	function( condition, then, elsewise )
{
	return(
		Condition.create(
			'condition', Parser.expr( condition ),
			'then', Parser.expr( then ),
			'elsewise', Parser.expr( elsewise )
		)
	);
};

/*
| Shorthand for let variable declerations.
|
| ~name:   variable name
| ~assign: variable assignment
*/
def.static.const =
	function( name, assign )
{
	return(
		Const.Elements(
			Declaration.create( 'name', name, 'assign', assign && Parser.expr( assign ) ),
		)
	);
};

/*
| Shorthand for Ast continue.
*/
def.staticLazy.continue = ( ) => Continue.singleton;

/*
| Shorthand for creating differs.
*/
def.static.differs =
	function( left, right )
{
	return Differs.create( 'left', Parser.expr( left ), 'right', Parser.expr( right ) );
};

/*
| Shorthand for creating delete calls.
*/
def.static.delete =
	function( expr )
{
	return Delete.create( 'expr', Parser.expr( expr ) );
};

/*
| Shorthand for creating dots.
*/
def.static.dot = ( expr, member ) => Dot.create( 'expr', expr, 'member', member );

/*
| Shorthand for creating equals.
*/
def.static.equals =
	function( left, right )
{
	return Equals.create( 'left', Parser.expr( left ), 'right', Parser.expr( right ) );
};

/*
| Forward general expression parsing.
*/
def.static.expr =
	function( )
{
	return Parser.expr.apply( Parser, arguments );
};

/*
| Shorthand for 'false' literals.
*/
def.staticLazy.false = ( ) => Boolean.create( 'boolean', false );

/*
| Shorthand for Ast code that throws a fail.
*/
def.static.fail =
	function( message )
{
	let call = Call.create( 'func', Var.create( 'name', 'Error' ) );
	if( message )
	{
		if( typeof( message ) === 'string' ) message = AstString.create( 'string', message );
		call = call.arg( message );
	}
	return Throw.create( 'expr', New.create( 'call', call ) );
};

/*
| Shorthand for creating less-than comparisons.
*/
def.static.lessThan =
	function( left, right )
{
	return LessThan.create( 'left', Parser.expr( left ), 'right', Parser.expr( right ) );
};

/*
| Shorthand for let variable declerations.
|
| name:   variable name
| assign: variable assignment
*/
def.static.let =
	function( name, assign )
{
	return(
		Let.Elements(
			Declaration.create( 'name', name, 'assign', assign && Parser.expr( assign ) )
		)
	);
};

/*
| Shorthand for creating greater-than comparisons.
*/
def.static.greaterThan =
	function( left, right )
{
	return GreaterThan.create( 'left', Parser.expr( left ), 'right', Parser.expr( right ) );
};

/*
| Shorthand for creating ifs.
*/
def.static.if =
	function( condition, then, elsewise )
{
	return(
		If.create(
			'condition', Parser.expr( condition ),
			'then', ensureBlock( then ),
			'elsewise', elsewise && ensureBlock( elsewise )
		)
	);
};

/*
| Shorthand for creating for loops.
*/
def.static.for =
	function( init, condition, iterate, block )
{
	return(
		For.create(
			'init', Parser.statement( init ),
			'condition', Parser.expr( condition ),
			'iterate', Parser.expr( iterate ),
			'block', ensureBlock( block )
		)
	);
};

/*
| Shorthand for creating for-in loops.
*/
def.static.forIn =
	function( variable, object, block )
{
	return(
		ForIn.create(
			'variable', Parser.expr( variable ),
			'letVar', false,
			'object', Parser.expr( object ),
			'block', ensureBlock( block )
		)
	);
};

/*
| Shorthand for creating for-in loops with 'let' for variable.
*/
def.static.forInLet =
	function( variable, object, block )
{
	return(
		ForIn.create(
			'variable', Parser.expr( variable ),
			'letVar', true,
			'object', Parser.expr( object ),
			'block', ensureBlock( block )
		)
	);
};

/*
| Shorthand for creating for-of loops.
*/
def.static.forOf =
	function( variable, object, block )
{
	return(
		ForOf.create(
			'variable', Parser.expr( variable ),
			'letVar', false,
			'object', Parser.expr( object ),
			'block', ensureBlock( block )
		)
	);
};

/*
| Shorthand for creating for-of loops with 'let' for variable.
*/
def.static.forOfLet =
	function( variable, object, block )
{
	return(
		ForOf.create(
			'variable', Parser.expr( variable ),
			'letVar', true,
			'object', Parser.expr( object ),
			'block', ensureBlock( block )
		)
	);
};

/*
| Shorthand for creating functions.
*/
def.static.func =
	function( arg )
{
	return Func.create( 'body', ensureBlock( arg ) );
};

/*
| Shorthand for creating generator functions.
*/
def.static.generator =
	function( arg )
{
	return(
		Func.create(
			'body', ensureBlock( arg ),
			'isGenerator', true
		)
	);
};

/*
| Shorthand for creating instanceof expressions.
*/
def.static.import = ( exportName, aliasName, moduleName ) =>
	Import.create(
		'aliasName', aliasName,
		'exportName', exportName,
		'moduleName', moduleName
	);

/*
| Shorthand for creating instanceof expressions.
*/
def.static.instanceof = ( left, right ) =>
	InstanceOf.create( 'left', Parser.expr( left ), 'right', Parser.expr( right ) );

/*
| Shorthand for creating members.
*/
def.static.member =
	function( expr, member )
{
	return Member.create( 'expr', expr, 'member', member );
};

/*
| Shorthand for creating multiplies.
*/
def.static.multiply =
	function( left, right /* or more */ )
{
	if( arguments.length > 2 )
	{
		const args = Array.prototype.slice.call( arguments );
		args.splice(
			0, 2,
			Multiply.create( 'left', Parser.expr( left ), 'right', Parser.expr( right ) )
		);
		return Self.multiply.apply( this, args );
	}
	return Multiply.create( 'left', Parser.expr( left ), 'right', Parser.expr( right ) );
};

/*
| Shorthand for creating multiply-assignments.
*/
def.static.multiplyAssign =
	function( left, right )
{
	return MultiplyAssign.create( 'left', Parser.expr( left ), 'right', Parser.expr( right ) );
};

/*
| Shorthand for creating new calls.
*/
def.static.new = ( call ) =>
	New.create( 'call', call );

/*
| Shorthand for creating negations.
*/
def.static.not = ( expr ) =>
	Not.create( 'expr', Parser.expr( expr ) );

/*
| Shorthand for Ast nulls.
*/
def.staticLazy.null = ( ) =>
	Null.singleton;

/*
| Shorthand for creating number literals.
*/
def.static.number = ( number ) =>
	AstNumber.create( 'number', '' + number );

/*
| Shorthand for creating object literals.
*/
def.staticLazy.objLiteral = ( ) =>
	ObjLiteral.Empty;

/*
| Shorthand for creating ors.
*/
def.static.or =
	function( left, right /* or more */ )
{
	if( arguments.length > 2 )
	{
		const args = Array.prototype.slice.call( arguments );
		args.splice( 0, 2,
			Or.create( 'left', Parser.expr( left ), 'right', Parser.expr( right ) )
		);
		return Self.or.apply( this, args );
	}
	return Or.create( 'left', Parser.expr( left ), 'right', Parser.expr( right ) );
};

/*
| Shorthand for creating pluses.
*/
def.static.plus =
	function( left, right /* or more */ )
{
	if( arguments.length > 2 )
	{
		const args = Array.prototype.slice.call( arguments );
		args.splice(
			0, 2,
			Plus.create( 'left', Parser.expr( left ), 'right', Parser.expr( right ) )
		);
		return Self.plus.apply( this, args );
	}
	return Plus.create( 'left', Parser.expr( left ), 'right', Parser.expr( right ) );
};

/*
| Shorthand for creating plus-assignments.
*/
def.static.plusAssign =
	( left, right ) =>
	PlusAssign.create( 'left', Parser.expr( left ), 'right', Parser.expr( right ) );

/*
| Shorthand for creating pre-increments.
*/
def.static.preIncrement =
	function( expr )
{
	expr = Parser.expr( expr );
	return PreIncrement.create( 'expr', expr );
};

/*
| Shorthand for creating a return statement
*/
def.static.return = ( expr ) =>
	Return.create( 'expr', Parser.expr( expr ) );

/*
| Shorthand for Ast separations.
*/
def.staticLazy.sep = ( ) =>
	Sep.singleton;

/*
| Shorthand for creating string literals.
*/
def.static.string = ( string ) =>
	AstString.create( 'string', string );

/*
| Shorthand for creating switch statements.
*/
def.static.switch = ( statement ) =>
	Switch.create( 'statement', Parser.statement( statement ) );

/*
| Shorthand for 'true' literals.
*/
def.staticLazy.true = ( ) =>
	Boolean.create( 'boolean', true );

/*
| Shorthand for creating typeofs.
*/
def.static.typeof = ( expr ) =>
	Typeof.create( 'expr', Parser.expr( expr ) );

/*
| Shorthand for 'undefined'
*/
def.staticLazy.undefined = ( ) =>
	Undefined.singleton;

/*
| Shorthand for creating variable uses.
*/
def.static.var = ( name ) =>
	Var.create( 'name', name );

/*
| Shorthand for variable declerations.
|
| name:   variable name
| assign: variable assignment
*/
def.static.varDec =
	function( name, assign )
{
	return(
		VarDec.create(
			'name', name,
			'assign', arguments.length > 1 ? Parser.expr( assign ) : undefined
		)
	);
};

/*
| Shorthand for creating for loops.
*/
def.static.while =
	function( condition, body )
{
	return(
		While.create(
			'condition', Parser.expr( condition ),
			'body', ensureBlock( body )
		)
	);
};

/*
| Shorthand for creating yield keywords.
*/
def.static.yield = ( expr ) =>
	Yield.create( 'expr', Parser.expr( expr ) );
