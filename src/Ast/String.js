/*
| Ast; a string literal.
*/
def.extend = 'Ast/Base/Self';

def.attributes =
{
	// the string value
	string: { type: 'string' },

	// token of the value
	token: { type: [ 'undefined', 'Lexer/Token' ] },
};

/*
| Walks the ast tree depth-first, pre-order
| creating a transformed copy.
| ~transform: a function to be called for all walked nodes.
*/
def.proto.walk =
	function( transform )
{
	return transform( this );
};

/*
| Custom inspect.
*/
def.proto._inspect =
	function( recurse )
{
	return '"' + this.string + '"';
};
