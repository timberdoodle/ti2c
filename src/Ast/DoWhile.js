/*
| Ast; a do while loop.
*/
def.extend = 'Ast/Base/Self';

def.attributes =
{
	// the looped body
	body: { type: 'Ast/Block' },

	// the while condition
	condition: { type: [ '< Ast/Type/Expr' ] },
};

/*
| Custom inspect.
*/
def.proto._inspect =
	function( recurse )
{
	return(
		'do{ '
		+ recurse( this.body )
		+ ' } while( '
		+ recurse( this.condition )
		+ ' ) '
	);
};
