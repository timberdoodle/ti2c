/*
| Ast; a yield keyword.
*/
def.extend = 'Ast/Base/Self';

def.attributes =
{
	// the expression to yield
	expr: { type: [ '< Ast/Type/Expr' ] },

	// token of the operator
	token: { type: [ 'undefined', 'Lexer/Token' ] },
};

/*
| Custom inspect.
*/
def.proto._inspect =
	function( recurse )
{
	return 'yield ( ' + recurse( this.expr ) + ' )';
};
