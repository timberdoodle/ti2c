/*
| Ast; ?. operator.
| Optionally gets a member of a table specified by a literal.
*/
def.extend = 'Ast/Base/Self';

def.attributes =
{
	// the expression to get the member of
	expr: { type: [ '< Ast/Type/Expr' ] },

	// the members name
	member: { type: 'string' },
};

import { Self as Member } from '{ti2c:Ast/Member}';

/*
| Walks the ast tree depth-first, pre-order
| creating a transformed copy.
|
| ~transform: a function to be called for all walked nodes.
*/
def.proto.walk =
	function( transform )
{
	const expr = this.expr.walk( transform );
	return transform( this.create( 'expr', expr ) );
};

/*
| Creates a dot member access of a dot.
|
| ~member: member string
*/
def.proto.dot =
	function( member )
{
	return Self.create( 'expr', this, 'member', member );
};

/*
| Creates a generic member access of a variable.
|
| ~member: member expression
*/
def.proto.member =
	function( member )
{
	return Member.create( 'expr', this, 'member', member );
};

/*
| Custom inspect.
*/
def.proto._inspect =
	function( recurse )
{
	return '( ' + recurse( this.expr ) + ' )?.' + this.member;
};
