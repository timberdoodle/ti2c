/*
| Ast; bitwise right shift operation.
*/
def.extend = 'Ast/Base/DualOp';

def.proto.operand = '>>';
