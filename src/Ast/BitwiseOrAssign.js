/*
| Ast; a bitwise or assign.
*/
def.extend = 'Ast/Base/DualOp';

def.proto.operand = '|=';
