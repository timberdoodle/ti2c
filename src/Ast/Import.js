/*
| Ast; an esm import statement;
*/
def.extend = 'Ast/Base/Self';

def.attributes =
{
	// alias to import the module as.
	aliasName: { type: 'string' },

	// 'default' or 'Self'
	exportName: { type: 'string' },

	// module name, e.g. '{Client/Root}'
	moduleName: { type: 'string' },
};

/*
| Custom inspect.
*/
def.proto._inspect =
	function( recurse )
{
	return(
		'import { '
		+ this.exportName
		+ ' as '
		+ this.aliasName
		+ ' } from \''
		+ this.moduleName
		+ '\';'
	);
};
