/*
| Ast; a typeof of an expression.
*/
def.extend = 'Ast/Base/Self';

def.attributes =
{
	// the expression to get the type of
	expr: { type: [ '< Ast/Type/Expr' ] },

	// token of the operator
	token: { type: [ 'undefined', 'Lexer/Token' ] },
};

/*
| Custom inspect.
*/
def.proto._inspect =
	function( recurse )
{
	return 'typeof( ' + recurse( this.expr ) + ' )';
};
