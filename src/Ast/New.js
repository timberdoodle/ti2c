/*
| Ast; a new call.
*/
def.extend = 'Ast/Base/Self';

def.attributes =
{
	// the constructor call
	call: { type: 'Ast/Call' }
};

/*
| Custom inspect.
*/
def.proto._inspect = function( recurse ) { return 'new ( ' + recurse( this.call ) + ' )'; };
