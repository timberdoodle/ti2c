/*
| Ast; optional checks in abstract syntax trees.
*/
def.extend = 'Ast/Base/Self';

def.attributes =
{
	// the code block
	block: { type: 'Ast/Block' },
};

/*
| Custom inspect.
*/
def.proto._inspect =
	function( recurse )
{
	let result = 'if( CHECK ) { ';
	result += recurse( this.block ) + '; ';
	return result + '} ';
};
