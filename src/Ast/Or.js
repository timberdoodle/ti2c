/*
| Ast; a logical or.
*/
def.extend = 'Ast/Base/DualOp';

def.proto.operand = '||';
