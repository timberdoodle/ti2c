/*
| Ast, minus assignment ( -= )
*/
def.extend = 'Ast/Base/DualOp';

def.proto.operand = '-=';
