/*
| Ast; a % operation.
*/
def.extend = 'Ast/Base/DualOp';

def.proto.operand = '%';
