/*
| Ast; conditional expressions.
| The questionmark-semicolon operator.
*/
def.extend = 'Ast/Base/Self';

def.attributes =
{
	condition: { type: [ '< Ast/Type/Expr' ] },

	then: { type: [ '< Ast/Type/Expr' ] },

	elsewise: { type: [ '< Ast/Type/Expr' ] },
};

/*
| Creates a condition with the elsewise expression set.
*/
def.proto.setElsewise =
	function( expr )
{
	return this.create( 'elsewise', expr );
};

/*
| Walks the ast tree depth-first, pre-order
| creating a transformed copy.
|
| ~transform: a function to be called for all walked nodes.
*/
def.proto.walk =
	function( transform )
{
	const condition = this.condition.walk( transform );
	const then = this.then.walk( transform );
	const elsewise = this.elsewise.walk( transform );
	return(
		transform(
			this.create(
				'condition', condition,
				'then', then,
				'elsewise', elsewise
			)
		)
	);
};

/*
| Custom inspect.
*/
def.proto._inspect =
	function( recurse )
{
	return(
		'( ' + recurse( this.condition ) + ' )'
		+ ' ? ( ' + recurse( this.then ) + ' )'
		+ ' : ( ' + recurse( this.elsewise ) + ' )'
	);
};
