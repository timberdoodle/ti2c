/*
| Ast, divide assignment ( /= )
*/
def.extend = 'Ast/Base/DualOp';

def.proto.operand = '/=';
