/*
| A mathematical addition
| or a string concation.
*/
def.extend = 'Ast/Base/DualOp';

def.proto.operand = '+';
