/*
| Ast; a number literal.
*/
def.extend = 'Ast/Base/Self';

def.attributes =
{
	// the number value
	number: { type: 'string' },

	// token of the value
	token: { type: [ 'undefined', 'Lexer/Token' ] },
};

/*
| Custom inspect.
*/
def.proto._inspect =
	function( recurse )
{
	return '' + this.number;
};
