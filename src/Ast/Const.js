/*
| Ast; constant variable declarations.
*/
def.extend = 'Ast/Base/Self';

def.attributes =
{
	declarations: { type: 'list@(ti2c:Ast/Declaration,ti2c:Ast/DestructDecl)' },
};

import { Self as ListDecls } from '{list@(ti2c:Ast/Declaration,ti2c:Ast/DestructDecl)}';

/*
| Appends an declaration.
*/
def.proto.append =
	function( decl )
{
	return Self.create( 'declarations', this.declarations.append( decl ) );
};

/*
| Creates the let decleration.
*/
def.static.Elements =
	function( ...decls )
{
	return Self.create( 'declarations', ListDecls.Array( decls ) );
};

/*
| Creates an empty declaration.
*/
def.staticLazy.Empty =
	function( )
{
	return Self.create( 'declarations', ListDecls.Empty );
};

/*
| A trivial declaration.
| If this is not trivial returns undefined.
*/
def.lazy.trivial =
	function( )
{
	const decl = this.declarations;
	if( decl.length !== 1 ) return undefined;
	return decl.get( 0 );
};

/*
| Custom inspect.
*/
def.proto._inspect =
	function( recurse )
{
	let result = 'const ';
	let first = true;
	for( let e of this )
	{
		if( first ) first = false; else result += ', ';
		result += recurse( e );
	}
	return result;
};
