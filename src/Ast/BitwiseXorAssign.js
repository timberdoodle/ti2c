/*
| Ast; an xor assignment ( ^= )
*/
def.extend = 'Ast/Base/DualOp';

def.proto.operand = '^=';
