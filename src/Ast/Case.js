/*
| Case statements in abstract syntax trees.
*/
def.extend = 'Ast/Base/Self';

def.attributes =
{
	// the statement
	block: { type: 'Ast/Block' },

	// the values of the case
	values:
	{
		type: 'list@<Ast/Type/Expr',
		defaultValue: 'import("list@<Ast/Type/Expr").Empty',
	},
};

/*
| Custom inspect.
*/
def.proto._inspect =
	function( recurse )
{
	let result = '';
	for( let e of this.values )
	{
		result += 'case ' + recurse( e ) + ': ';
	}
	return result + recurse( this.block );
};
