/*
| A mathematical substraction
*/
def.extend = 'Ast/Base/DualOp';

def.proto.operand = '-';
