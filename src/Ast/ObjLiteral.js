/*
| Ast; object literal.
*/
def.extend = 'Ast/Base/Self';

def.attributes =
{
	pairs: { type: 'twig@<Ast/Type/Expr' },
};

import { Self as Parser   } from '{ti2c:Parser/Self}';
import { Self as TwigExpr } from '{twig@<ti2c:Ast/Type/Expr}';

/*
| Returns an object literal with a key-expr pair added.
*/
def.proto.add =
	function( key, ...parseables )
{
	return(
		this.create(
			'pairs',
				this.pairs.create(
					'twig:add', key, Parser.parseArray( parseables, 'expr' )
				)
		)
	);
};

/*
| An empty object literal.
*/
def.staticLazy.Empty =
	function( )
{
	return Self.create( 'pairs', TwigExpr.Empty );
};

/*
| Walks the ast tree depth-first, pre-order
| creating a transformed copy.
|
| ~transform: a function to be called for all walked nodes.
*/
def.proto.walk =
	function( transform )
{
	// TODO actually walk through the objects
	return transform( this );
};

/*
| Custom inspect.
*/
def.proto._inspect =
	function( recurse )
{
	const pairs = this.pairs;
	if( pairs.length === 0 ) return '{ }';
	let result = '{ ';
	let first = true;
	for( let key of pairs.keys )
	{
		if( first ) first = false; else result += ', ';
		const arg = pairs.get( key );
		result += key + ' : ' + recurse( arg );
	}
	return result + ' }';
};
