/*
| Ast; return statement.
*/
def.extend = 'Ast/Base/Self';

def.attributes =
{
	// the expression to return
	expr: { type: [ 'undefined', '< Ast/Type/Expr' ] },
};

/*
| Custom inspect.
*/
def.proto._inspect =
	function( recurse )
{
	return 'return ( ' + recurse( this.expr ) + ' )';
};
