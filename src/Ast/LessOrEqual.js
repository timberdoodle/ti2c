/*
| Ast; <= operator.
*/
def.extend = 'Ast/Base/DualOp';

def.proto.operand = '<=';
