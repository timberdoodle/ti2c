/*
| ast; spread operator.
*/
def.extend = 'Ast/Base/PreOp';

def.proto.operand = '...';
