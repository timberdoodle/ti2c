/*
| Ast; a mathematical multiplication.
*/
def.extend = 'Ast/Base/DualOp';

def.proto.operand = '*';
