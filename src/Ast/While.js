/*
| Ast; a while loop.
*/
def.extend = 'Ast/Base/Self';

def.attributes =
{
	// the looped body
	body: { type: 'Ast/Block' },

	// the while condition
	condition: { type: [ '< Ast/Type/Expr' ] },
};

/*
| Custom inspect.
*/
def.proto._inspect =
	function( recurse )
{
	return(
		'while( '
		+ recurse( this.condition )
		+ ' ) ' + recurse( this.body )
		+ ' '
	);
};
