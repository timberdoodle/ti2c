/*
| Ast; a post increment (x++).
*/
def.extend = 'Ast/Base/Self';

def.attributes =
{
	// the expression to post-increment
	expr: { type: [ '< Ast/Type/Expr' ] },

	// token of the operator
	token: { type: [ 'undefined', 'Lexer/Token' ] },
};

/*
| Custom inspect.
*/
def.proto._inspect =
	function( recurse )
{
	return '( ' + recurse( this.expr ) + ' )' + this.operand;
};
