/*
| Base of all ast nodes.
*/
def.abstract = true;

import util from 'util';

/*
| Custom inspect.
*/
def.inspect =
	function( depth, opts )
{
	let postfix, result;

	if( !opts.ast )
	{
		result = 'ast{ ';
		postfix = ' }';
		opts = ti2c.copy( opts );
		opts.ast = true;
	}
	else
	{
		result = postfix = '';
	}

	// Calls the specific custom inspect.
	result += this._inspect( ( o ) => util.inspect( o, opts ) );
	return result + postfix;
};

/*
| Trap compatibility for parser.
*/
def.proto.type = undefined;

/*
| Default walk, this is an end node.
| ~transform: a function to be called for all walked nodes.
*/
def.proto.walk =
	function( transform )
{
	return transform( this );
};
