/*
| ast pre mono op.
*/
def.extend = 'Ast/Base/Self';

def.attributes =
{
	// the expression to negate
	expr: { type: [ '< Ast/Type/Expr' ] },

	// token of the operator
	token: { type: [ 'undefined', 'Lexer/Token' ] },
};

/*
| Custom inspect.
*/
def.proto._inspect =
	function( recurse )
{
	return this.operand + '( ' + recurse( this.expr ) + ' )';
};
