/*
| Base of dualisitic operations.
*/
def.extend = 'Ast/Base/Self';

def.attributes =
{
	// left side expression
	left: { type: [ '< Ast/Type/Expr' ] },

	// right side expression
	right: { type: [ '< Ast/Type/Expr' ] },

	// token of the operator
	token: { type: [ 'undefined', 'Lexer/Token' ] },
};

/*
| Walks the ast tree depth-first, pre-order
| creating a transformed copy.
|
| ~transform: a function to be called for all walked nodes.
*/
def.proto.walk =
	function( transform )
{
	const left = this.left.walk( transform );
	const right = this.right.walk( transform );
	return transform( this.create( 'left', left, 'right', right ) );
};

/*
| Custom inspect.
*/
def.proto._inspect =
	function( recurse )
{
	return(
		'( ' + recurse( this.left ) + ' ) '
		+ this.operand
		+ ' ( ' + recurse( this.right ) + ' )'
	);
};
