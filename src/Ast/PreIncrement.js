/*
| Ast, a pre increment (++x operator).
*/
def.extend = 'Ast/Base/PreOp';

def.proto.operand = '++';
