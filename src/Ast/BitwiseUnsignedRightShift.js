/*
| Ast; bitwise unsigned right shift operation.
*/
def.extend = 'Ast/Base/DualOp';

def.proto.operand = '>>>';
