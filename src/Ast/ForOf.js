/*
| Ast; a for-of loop
*/
def.extend = 'Ast/Base/Self';

def.attributes =
{
	// the loop variable
	variable: { type: 'Ast/Var' },

	// true if the for loop variable has a let
	letVar: { type: 'boolean' },

	// the object expression to iterate over
	object: { type: [ '< Ast/Type/Expr' ] },

	// the loop block
	block: { type: 'Ast/Block' },
};

/*
| Custom inspect.
*/
def.proto._inspect =
	function( recurse )
{
	return(
		'for( '
		+ ( this.letVar ? 'let ' : '' ) + recurse( this.variable )
		+ ' of ( ' + recurse( this.object ) + ' ) )'
		+ recurse( this.block ) + ' '
	);
};
