/*
| Ast; a code block.
*/
def.extend = 'Ast/Base/Self';

def.attributes =
{
	statements: { type: 'list@(<Ast/Type/Statement,<Ast/Type/Expr)' },
};

import { Self as Ast            } from '{ti2c:Ast/Self}';
import { Self as Comment        } from '{ti2c:Ast/Comment}';
import { Self as ListStatements } from '{list@(<ti2c:Ast/Type/Statement,<ti2c:Ast/Type/Expr)}';
import { Self as Parser         } from '{ti2c:Parser/Self}';
import { Self as Return         } from '{ti2c:Ast/Return}';

/*
| Returns the block with a parsed statement appended.
*/
def.proto.$ =
	function( ...args )
{
	const ast = Parser.parseArray( args, 'statement' );

	if( ast === undefined ) return this;

	return(
		this.create(
			'statements',
				ast.ti2ctype === Self
				? this.statements.appendList( ast.statements )
				: this.statements.append( ast )
		)
	);
};

/*
| Appends a statement.
*/
def.proto.append =
	function( statement )
{
	return this.create( 'statements', this.statements.append( statement ) );
};

/*
| Creates a block by an array of statements.
*/
def.static.Array =
	function( array )
{
	return this.create( 'statements', ListStatements.Array( array ) );
};

/*
| Returns the block with an assignment appended.
*/
def.proto.assign =
	function( left, right )
{
	return this.append( Ast.assign( left, right ) );
};

/*
| Returns the block with a break statement appended.
*/
def.lazy.break =
	function( )
{
	return this.append( Ast.break );
};

/*
| Recreates the block with a call appended.
| ~func
| ~ars
*/
def.proto.call = function( ...args )
{
	return this.append( Ast.call.apply( Ast, args ) );
};

/*
| Returns the block with a check appended.
| ~block
*/
def.proto.check =
	function( ...args )
{
	return this.append( Ast.check.apply( Ast, args ) );
};

/*
| Returns the block with a comment appended.
*/
def.proto.comment =
	function( ...args )
{
	let header = args[ 0 ];
	// arguments have to be a list of strings otherwise
	if( header.ti2ctype !== Comment ) header = Comment.Array( args );
	return this.append( header );
};

/*
| Returns the block with a const decleration appended.
| ~name,
| ~assign
*/
def.proto.const =
	function( ...args )
{
	return this.append( Ast.const.apply( Ast, args ) );
};

/*
| Returns the block with a continue statement appended.
*/
def.lazy.continue =
	function( )
{
	return this.append( Ast.continue );
};

/*
| Returns the block with a delete statement appended.
*/
def.proto.delete =
	function( expr )
{
	return this.append( Ast.delete( expr ) );
};

/*
| An empty block.
*/
def.staticLazy.Empty =
	function( )
{
	return Self.create( 'statements', ListStatements.Empty );
};

/*
| Returns the block with an if appended.
*/
def.proto.if =
	function( condition, then, elsewise )
{
	return this.append( Ast.if( condition, then, elsewise ) );
};

/*
| Returns the block with an import appended.
*/
def.proto.import =
	function( exportName, aliasName, moduleName )
{
	return this.append( Ast.import( exportName, aliasName, moduleName ) );
};

/*
| Returns the block with a error throwing appended.
*/
def.proto.fail =
	function( message )
{
	return this.append( Ast.fail( message ) );
};

/*
| Returns the block with a classical for loop appended.
*/
def.proto.for =
	function( init, condition, iterate, block )
{
	return this.append( Ast.for( init, condition, iterate, block ) );
};

/*
| Returns the block with a for-in loop appended.
*/
def.proto.forIn =
	function( variable, object, block )
{
	return this.append( Ast.forIn( variable, object, block ) );
};

/*
| Returns the block with a for-in loop appended.
*/
def.proto.forInLet =
	function( variable, object, block )
{
	return this.append( Ast.forInLet( variable, object, block ) );
};

/*
| Returns the block with a for-of loop appended.
*/
def.proto.forOf =
	function( variable, object, block )
{
	return this.append( Ast.forOf( variable, object, block ) );
};

/*
| Returns the block with a for-of loop appended.
*/
def.proto.forOfLet =
	function( variable, object, block )
{
	return this.append( Ast.forOfLet( variable, object, block ) );
};

/*
| Returns the block with a variable decleration appended.
| ~name   variable name
| ~assign variable assignment
*/
def.proto.let =
	function( ...args )
{
	return this.append( Ast.let.apply( Ast, args ) );
};

/*
| Shorthand for creating new calls.
*/
def.proto.new =
	function( call )
{
	return this.append( Ast.new( call ) );
};

/*
| Returns the block with a plus-assignment appended.
*/
def.proto.plusAssign =
	function( left, right )
{
	return this.append( Ast.plusAssign( left, right ) );
};

/*
| Returns the block with a term appended.
*/
def.proto.return =
	function( expr )
{
	return(
		expr.ti2ctype !== Return
		? Ast.return( expr )
		: this.append( expr )
	);
};

/*
| Separator. (just a new line for formatter)
*/
def.lazy.sep =
	function( )
{
	return this.append( Ast.sep );
};

/*
| Returns the block with a variable decleration appended.
*/
def.proto.varDec =
	function( name, assign )
{
	return this.append( Ast.varDec( name, assign ) );
};

/*
| Returns the block with a classical for loop appended.
*/
def.proto.while =
	function( condition, body )
{
	return this.append( Ast.while( condition, body ) );
};

/*
| Returns the block with a yield keyword appended.
*/
def.proto.yield =
	function( expr )
{
	return this.append( Ast.yield( expr ) );
};

/*
| Custom inspect.
*/
def.proto._inspect =
	function( recurse )
{
	let result = '{ ';
	for( let s of this.statements )
	{
		result += recurse( s ) + '; ';
	}
	return result + '} ';
};
