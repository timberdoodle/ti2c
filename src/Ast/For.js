/*
| Ast; a for loop.
*/
def.extend = 'Ast/Base/Self';

def.attributes =
{
	// the initialization
	init: { type: [ '< Ast/Type/Expr', 'Ast/Let', 'undefined' ] },

	// the continue condition
	condition: { type: [ '< Ast/Type/Expr', 'undefined' ] },

	// the iteration expression
	iterate: { type: [ '< Ast/Type/Expr', 'undefined' ] },

	// the loop block
	block: { type: 'Ast/Block' }
};

/*
| Custom inspect.
*/
def.proto._inspect =
	function( recurse )
{
	return(
		'for( '
		+ recurse( this.init ) + ' ; '
		+ recurse( this.condition ) + ' ; '
		+ recurse( this.iterate ) + ' ) '
		+ recurse( this.block ) + ' '
	);
};
