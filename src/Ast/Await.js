/*
| Ast; an await call.
*/
def.extend = 'Ast/Base/Self';

def.attributes =
{
	// the expression to delete
	expr: { type: [ '< Ast/Type/Expr' ] },

	// token of the operator
	token: { type: [ 'undefined', 'Lexer/Token' ] },
};

/*
| Custom inspect.
*/
def.proto._inspect =
	function( recurse )
{
	return 'await ( ' + recurse( this.expr ) + ' )';
};
