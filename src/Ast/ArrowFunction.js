/*
| Ast; a => function.
*/
def.extend = 'Ast/Base/Self';

def.attributes =
{
	// arguments
	args: { type: 'list@string' },

	// function body
	body: { type: [ 'undefined', 'Ast/Block', '< Ast/Type/Expr' ] },
};

/*
| Custom inspect.
*/
def.proto._inspect =
	function( recurse )
{
	let text = '( ';
	const args = this.args;
	if( args.length === 0 )
	{
		text += ')';
	}
	else
	{
		let first = true;
		for( let arg of this.args )
		{
			if( first ) first = false; else text += ', ';
			text += arg;
		}
	}
	return text + recurse( this.body );
};
