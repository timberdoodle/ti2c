/*
| Ast; throw statement.
*/
def.extend = 'Ast/Base/Self';

def.attributes =
{
	// the expression to throw
	expr: { type: [ '< Ast/Type/Expr' ] },
};

/*
| Custom inspect.
*/
def.proto._inspect =
	function( recurse )
{
	return 'throw ( ' + recurse( this.expr ) + ' )';
};
