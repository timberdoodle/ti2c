/*
| Ast; an assignment.
*/
def.extend = 'Ast/Base/DualOp';

def.proto.operand = '=';
