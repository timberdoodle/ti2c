/*
| Ast; a pre decrement (--x operator).
*/
def.extend = 'Ast/Base/PreOp';

def.proto.operand = '--';
