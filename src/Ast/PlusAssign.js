/*
| Ast; plus assignment ( += )
*/
def.extend = 'Ast/Base/DualOp';

def.proto.operand = '+=';
