/*
| Ast; a nullish coalescing operator (??)
*/
def.extend = 'Ast/Base/DualOp';

def.proto.operand = '??';
