/*
| Ast; if statement.
*/
def.extend = 'Ast/Base/Self';

def.attributes =
{
	condition: { type: [ '< Ast/Type/Expr' ] },

	then: { type: 'Ast/Block' },

	elsewise: { type: [ 'undefined', 'Ast/Block' ] }
};

import { Self as AstBlock } from '{ti2c:Ast/Block}';
import { Self as Parser   } from '{ti2c:Parser/Self}';

/*
| Creates an if with the elsewise block set.
|
| ~args: parseables
*/
def.proto.setElsewise =
	function( ...args )
{
	let elsewise = Parser.parseArray( args, 'statement' );
	if( elsewise.ti2ctype !== AstBlock ) elsewise = AstBlock.Empty.append( elsewise );
	return this.create( 'elsewise', elsewise );
};

/*
| Custom inspect.
*/
def.proto._inspect =
	function( recurse )
{
	return(
		'if( '
		+ recurse( this.condition )
		+ ' ) ' + recurse( this.then )
		+ ( this.elsewise ? 'else ' + recurse( this.elsewise ) : '' )
		+ ' '
	);
};
