/*
| Ast; bitwise not expression.
*/
def.extend = 'Ast/Base/PreOp';

def.proto.operand = '~';
