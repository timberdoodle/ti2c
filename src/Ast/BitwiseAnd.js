/*
| Ast; bitwise and operation.
*/
def.extend = 'Ast/Base/DualOp';

def.proto.operand = '&';
