/*
| Ast; the [ ] operator.
*/
def.extend = 'Ast/Base/Self';

def.attributes =
{
	// the expression to get the member of
	expr: { type: [ '< Ast/Type/Expr' ] },

	// the member expression
	member: { type: [ '< Ast/Type/Expr' ] },
};

import { Self as Dot } from '{ti2c:Ast/Dot}';

/*
| Creates a dot member access of a dot.
| ~member: member string
*/
def.proto.dot =
	function( member )
{
	return Dot.create( 'expr', this, 'member', member );
};

/*
| Walks the ast tree depth-first, pre-order
| creating a transformed copy.
| ~transform: a function to be called for all walked nodes.
*/
def.proto.walk =
	function( transform )
{
	const expr = this.expr.walk( transform );
	return transform( this.create( 'expr', expr ) );
};

/*
| Custom inspect.
*/
def.proto._inspect =
	function( recurse )
{
	return '( ' + recurse( this.expr ) + ' )' + '[ ' + recurse( this.member ) + ' ]';
};
