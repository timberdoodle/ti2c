/*
| Ast; a logical xor.
*/
def.extend = 'Ast/Base/DualOp';

def.proto.operand = '^';
