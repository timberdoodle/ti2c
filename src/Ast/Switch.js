/*
| Ast; switch statements.
*/
def.extend = 'Ast/Base/Self';

def.attributes =
{
	// the statement expression
	statement: { type: [ '< /Ast/Type/Expr' ] },

	// cases
	cases: { type: 'list@Ast/Case', defaultValue: 'import("list@Ast/Case").Empty' },

	// the default block
	defaultCase: { type: [ 'undefined', 'Ast/Block' ] },
};

import { Self as AstBlock } from '{ti2c:Ast/Block}';
import { Self as AstCase  } from '{ti2c:Ast/Case}';
import { Self as ListExpr } from '{list@<ti2c:Ast/Type/Expr}';
import { Self as Parser   } from '{ti2c:Parser/Self}';

/*
| Shortcut for appending a case to this switch.
| ~coc: case_or_condition
*/
def.proto.case =
	function( coc, ...parseables )
{
	let _case;
	if( coc.ti2ctype !== AstCase )
	{
		let block = Parser.parseArray( parseables, 'statement' );
		if( block.ti2ctype !== AstBlock ) block = AstBlock.Empty.append( block );
		const value = Parser.statement( coc );
		_case = AstCase.create( 'block', block, 'values', ListExpr.Elements( value ) );
	}
	else
	{
		_case = coc;
	}
	return this.create( 'cases', this.cases.append( _case ) );
};

/*
| Shortcut for setting the default case.
*/
def.proto.default =
	function( ...parseables )
{
	let block = Parser.parseArray( parseables, 'statement' );
	if( block.ti2ctype !== AstBlock ) block = AstBlock.Empty.append( block );
	return this.create( 'defaultCase', block );
};

/*
| Custom inspect.
*/
def.proto._inspect =
	function( recurse )
{
	let txt = 'switch( ' + recurse( this.statement ) + ') {';
	const cases = this.cases;
	for( let c of cases ) txt += recurse( c );
	const defaultCase = this.defaultCase;
	if( defaultCase ) txt += recurse( defaultCase );
	return txt += '}';
};
