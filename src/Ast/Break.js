/*
| Ast; break statement.
*/
def.extend = 'Ast/Base/Self';

def.attributes =
{
	// token of the break statement
	token: { type: [ 'undefined', 'Lexer/Token' ] },
};

/*
| Custom inspect.
*/
def.proto._inspect =
	function( recurse )
{
	return 'break';
};
