/*
| Ast; a bitwise or.
*/
def.extend = 'Ast/Base/DualOp';

def.proto.operand = '|';
