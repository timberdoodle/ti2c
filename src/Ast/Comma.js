/*
| Ast; a comma operator.
*/
def.extend = 'Ast/Base/DualOp';
def.proto.operand = ',';
