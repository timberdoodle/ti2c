/*
| A variable reference to be generated.
*/
def.extend = 'Ast/Base/Self';

def.attributes =
{
	// the variable name
	name: { type: 'string' },

	token: { type: [ 'undefined', 'Lexer/Token' ] },
};

import { Self as Dot    } from '{ti2c:Ast/Dot}';
import { Self as Member } from '{ti2c:Ast/Member}';
import { Self as Parser } from '{ti2c:Parser/Self}';

/*
| Exta checking.
*/
def.proto._check =
	function( )
{
	const regex = /^([a-zA-Z_$])([a-zA-Z0-9_$])*$/;

	if( !regex.test( this.name ) )
	{
		throw new Error( 'invalid variable name "' + this.name + '"' );
	}

	switch( this.name )
	{
		case 'true' :
		case 'false' :
			throw new Error( 'var must not be a literal' );
	}
};


/*
| Creates a dot member access of a variable.
*/
def.proto.dot =
	function( member )
{
	return Dot.create( 'expr', this, 'member', member );
};

/*
| Creates a generic member access of a variable.
|
| ~args: parseables.
*/
def.proto.member =
	function( ...args )
{
	return(
		Member.create(
			'expr', this,
			'member', Parser.parseArray( args, 'expr' )
		)
	);
};

/*
| Custom inspect.
*/
def.inspect =
	function( depth, opts )
{
	let postfix;
	let result;
	if( !opts.ast )
	{
		result = 'ast{ ';
		postfix = ' }';
		opts = ti2c.copy( opts );
		opts.ast = true;
	}
	else result = postfix = '';
	result += this.name;
	return result + postfix;
};

/*
| Walks the ast tree depth-first, pre-order
| creating a transformed copy.
| ~transform: a function to be called for all walked nodes.
*/
def.proto.walk =
	function( transform )
{
	return transform( this );
};
