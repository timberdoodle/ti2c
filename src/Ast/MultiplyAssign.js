/*
| Ast; multiply assignment ( *= ).
*/
def.extend = 'Ast/Base/DualOp';

def.proto.operand = '*=';
