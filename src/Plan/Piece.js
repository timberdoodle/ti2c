/*
| Plan piece.
*/
def.attributes =
{
	// true if this plan piece has an at
	at: { type: 'boolean', defaultValue: 'false' },

	// true if this plan piece has a key
	key: { type: 'boolean', defaultValue: 'false' },

	// name of the plan piece
	name: { type: 'string' },

	// sub traces
	subs: { type: [ 'undefined', 'group@(function,ti2c:Plan/Piece)' ] },

	// types for this plan piece
	types: { type: [ 'undefined', 'group@protean' ] },
};

def.json = true;
