/*
| A plan Traces need to follow.
| This is used for controlling and structuring code using traces.
*/
def.abstract = true;

import { Self as Piece     } from '{ti2c:Plan/Piece}';
import { Self as GroupSubs } from '{group@(function,ti2c:Plan/Piece)}';
import { Self as Types     } from '{group@protean}';

/*
| Builds the types group.
*/
function buildTypes( types )
{
	const g = { };
	for( let e of types )
	{
		g[ e.$type ] = e;
	}
	return Types.Table( g );
}

/*
| Helper to build a sub piece
|
| ~o: user object to build from.
| ~name: name of the plan piece.
*/
function buildSubPiece( o, name )
{
	let subs = o.subs;
	let types = o.types;
	const at = o.at || false;
	const key = o.key || false;

	if( subs && !subs.ti2ctype ) subs = buildSubs( subs );
	if( types && !types.ti2ctype ) types = buildTypes( types );

	return(
		Piece.create(
			'at', at,
			'key', key,
			'name', name,
			'subs', subs,
			'types', types,
		)
	);
}

/*
| Helper to build subs.
*/
function buildSubs( subs )
{
	for( let name in subs )
	{
		const s = subs[ name ];
		if( typeof( s ) === 'function' || s.ti2ctype ) continue;
		subs[ name ] = buildSubPiece( s, name );
	}
	return GroupSubs.Table( subs );
}

/*
| Builds a plan.
*/
def.static.Build =
	function( o )
{
	const name = o.name;
	let subs = o.subs;
	let types = o.types;
	const at = o.at || false;
	const key = o.key || false;

	if( subs && !subs.ti2ctype ) subs = buildSubs( subs );
	if( types && !types.ti2ctype ) types = buildTypes( types );

	return(
		Piece.create(
			'at', at,
			'key', key,
			'name', name,
			'subs', subs,
			'types', types,
		)
	);
};
