/*
| An unknown ti2c class or collection.
*/
def.singleton = true;

/*
| This is something something ti2c.
*/
def.proto.isTi2c = true;

/*
| This is not a primitive.
*/
def.proto.isPrimitive = false;

/*
| This is not a ti2c collection.
| I don't know.
*/
//def.proto.isTi2cCollection = false;
