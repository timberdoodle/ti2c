/*
| A ti2c group as type.
*/
def.extend = 'Type/Ti2c/Collection/Base';

def.proto.collectionName = 'group';
