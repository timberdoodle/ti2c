/*
| Base for ti2c collection types.
*/
def.abstract = true;

def.attributes =
{
	// the collection is these item types
	typeItems: { type: 'Type/Set' },
};

/*
| This is not a primitive.
*/
def.proto.isPrimitive = false;

/*
| This is a ti2c thing.
*/
def.proto.isTi2c = true;

/*
| This is a ti2c collection.
*/
def.proto.isTi2cCollection = true;

/*
| The specifier as string.
*/
def.lazy.asString =
	function( )
{
	const typeItems = this.typeItems;
	let str = this.collectionName + '@';
	if( typeItems.size === 1 )
	{
		return str + typeItems.trivial.asString;
	}
	else
	{
		str += '(';
		let first = true;
		for( let ti of typeItems )
		{
			if( first ) first = false; else str += ',';
			str += ti.asString;
		}
		return str + ')';
	}
};

/*
| The specifier as string.
*/
def.lazy.asFileString =
	function( )
{
	const typeItems = this.typeItems;
	let str = this.collectionName + '@';
	if( typeItems.size === 1 )
	{
		return str + typeItems.trivial.asFileString;
	}
	else
	{
		str += '(';
		let first = true;
		for( let ti of typeItems )
		{
			if( first ) first = false; else str += '__';
			str += ti.asFileString;
		}
		return str + ')';
	}
};

/*
| The type as string for a ti2c-web resource.
*/
def.lazy.asResourceString =
	function( )
{
	return this.asFileString;
};

/*
| The ti2ctype as imported var.
*/
def.lazy.varname =
	function( )
{
	const typeItems = this.typeItems;
	let name = 'tt__' + this.collectionName;
	if( typeItems.size === 1 )
	{
		return '_$Q$_' + name + typeItems.trivial.varname;
	}
	else
	{
		name += '$QC$_';
		let first = true;
		for( let ti of typeItems )
		{
			if( first ) first = false; else name += '__';
			name += ti.varname;
		}
		return name + '_$D$';
	}
};
