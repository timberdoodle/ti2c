/*
| A ti2c set as type.
*/
def.extend = 'Type/Ti2c/Collection/Base';

def.proto.collectionName = 'set';
