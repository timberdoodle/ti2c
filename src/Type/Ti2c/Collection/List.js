/*
| A ti2c list as type.
*/
def.extend = 'Type/Ti2c/Collection/Base';

def.proto.collectionName = 'list';
