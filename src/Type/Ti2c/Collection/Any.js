/*
| Generic interface for all ti2c collection types.
*/
def.abstract = true;

import { Self as SetType       } from '{ti2c:Type/Set}';
import { Self as TypeAny       } from '{ti2c:Type/Any}';
import { Self as TypeInFile    } from '{ti2c:Type/InFile}';
import { Self as TypeTi2cGroup } from '{ti2c:Type/Ti2c/Collection/Group}';
import { Self as TypeTi2cList  } from '{ti2c:Type/Ti2c/Collection/List}';
import { Self as TypeTi2cSet   } from '{ti2c:Type/Ti2c/Collection/Set}';
import { Self as TypeTi2cTwig  } from '{ti2c:Type/Ti2c/Collection/Twig}';

/*
| Create the type from a string specifier.
|
| ~str:         type string to create from
| ~basePkgName: base package name
*/
def.static.FromString =
	function( str, manager, basePkgName )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 3 ) throw new Error( );
/**/	if( typeof( str ) !== 'string' ) throw new Error( );
/**/	if( basePkgName !== undefined && typeof( basePkgName ) !== 'string' ) throw new Error( );
/**/}

	const ioat = str.indexOf( '@' );
	const strCol = str.substr( 0, ioat ).trim( );
	let strRest = str.substr( ioat + 1 ).trim( );

	let typeItems;
	if( strRest[ 0 ] === '(' )
	{
		if( strRest[ strRest.length - 1 ] !== ')' ) throw new Error( );
		const strList = strRest.substring( 1, strRest.length - 1 );

		// FIXME does not handle multilevels yet.
		const strSplit = strList.split( ',' );
		const setT = new Set( );
		for( let si of strSplit )
		{
			si = si.trim( );
			if( si[ 0 ] === '<' )
			{
				const subType = TypeInFile.FromString( si, manager, basePkgName );
				setT.add( subType );
			}
			else
			{
				const subType = TypeAny.FromString( si, manager, basePkgName );
				setT.add( subType );
			}
		}
		typeItems = SetType.create( 'set:init', setT );
	}
	else
	{
		if( strRest[ 0 ] === '<' )
		{
			const subType = TypeInFile.FromString( strRest, manager, basePkgName );
			typeItems = SetType.Elements( subType );
		}
		else
		{
			const subType = TypeAny.FromString( strRest, manager, basePkgName );
			typeItems = SetType.Elements( subType );
		}
	}

	switch( strCol )
	{
		case 'group': return TypeTi2cGroup.create( 'typeItems', typeItems );
		case 'list':  return TypeTi2cList.create( 'typeItems', typeItems );
		case 'set':   return TypeTi2cSet.create( 'typeItems', typeItems );
		case 'twig':  return TypeTi2cTwig.create( 'typeItems', typeItems );
		default: throw new Error( 'invalid collection type (before @): ' + strCol );
	}
};
