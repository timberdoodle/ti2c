/*
| Generic interface for all ti2c types.
*/
def.abstract = true;

import { Self as TypeTi2cCollectionAny } from '{ti2c:Type/Ti2c/Collection/Any}';
import { Self as TypeTi2cClass         } from '{ti2c:Type/Ti2c/Class}';

/*
| Create the type from a string specifier.
|
| ~str:         type string to create from
| ~basePkgName: base package name
*/
def.static.FromString =
	function( str, manager, basePkgName )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 3 ) throw new Error( );
/**/	if( typeof( str ) !== 'string' ) throw new Error( );
/**/	if( basePkgName !== undefined && typeof( basePkgName ) !== 'string' ) throw new Error( );
/**/}

	return(
		str.indexOf( '@' ) >= 0
		? TypeTi2cCollectionAny.FromString( str, manager, basePkgName )
		: TypeTi2cClass.FromString( str, manager, basePkgName )
	);
};
