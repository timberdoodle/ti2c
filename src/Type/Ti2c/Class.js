/*
| A ti2c class as type.
*/
def.attributes =
{
	// package name
	pkgName: { type: 'string' },

	// the entry name
	entryName: { type: 'string' },
};

/*
| The type as string.
*/
def.lazy.asString =
	function( )
{
	return this.pkgName + ':' + this.entryName;
};

/*
| The type as string for a filename (for codegen).
*/
def.lazy.asFileString =
	function( )
{
	return this.pkgName + '-' + this.entryName.replace( /\//g, '-' );
};

/*
| The type as string for a ti2c-web resource.
*/
def.lazy.asResourceString =
	function( )
{
	return this.pkgName + '/' + this.entryName.replace( /\//g, '-' );
};

/*
| Create the type from a string.
|
| ~string:      string to create from
| ~basePkgName: base package name
*/
def.static.FromString =
	function( string, manager, basePkgName )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 3 ) throw new Error( );
/**/	if( typeof( string ) !== 'string' ) throw new Error( );
/**/	if( basePkgName !== undefined && typeof( basePkgName ) !== 'string' ) throw new Error( );
/**/}

	let pkgName, entryName;

	const ios = string.indexOf( ':' );
	if( ios >= 0 )
	{
		pkgName = string.substring( 0, ios );
		entryName = string.substring( ios + 1 );
	}
	else
	{
		if( !basePkgName ) throw new Error( );
		pkgName = basePkgName;
		entryName = string;
	}

	const pkg = manager.getPackage( pkgName );
	if( !pkg ) throw new Error( 'no package "' + pkgName + '"' );

	const exp = pkg.exports?.classes.get( entryName );

	if( exp ) entryName = exp;

	return(
		Self.create(
			'pkgName', pkgName,
			'entryName', entryName,
		)
	);
};

/*
| This is not a primitive.
*/
def.proto.isPrimitive = false;

/*
| This is a ti2c thing.
*/
def.proto.isTi2c = true;

/*
| This is not a ti2c collection.
*/
def.proto.isTi2cCollection = false;

/*
| Shortcut.
*/
def.static.PkgName =
	function( pkgName, entryName )
{
	return Self.create( 'pkgName', pkgName, 'entryName', entryName );
};

/*
| The ti2ctype as imported var.
*/
def.lazy.varname =
	function( )
{
	return(
		'tt__'
		+ this.pkgName.replace( /-/g, '_' )
		+ '__'
		+ this.entryName.replace( /\//g, '_' )
	);
};
