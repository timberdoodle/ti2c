/*
| A '<' link to a type set in .txt file.
*/
def.attributes =
{
	// package name
	pkgName: { type: 'string' },

	// the file name
	fileName: { type: 'string' },
};

import * as file from '../core/file';
import { Self as SetType } from '{ti2c:Type/Set}';
import { Self as TypeAny } from '{ti2c:Type/Any}';

/*
| The specifier as string.
*/
def.lazy.asString =
	function( )
{
	return '<' + this.pkgName + ':' + this.fileName;
};

/*
| The type as string for a filename (for codegen).
*/
def.lazy.asFileString =
	function( )
{
	return '^' + this.pkgName + '-' + this.fileName.replace( /\//g, '-' );
};

/*
| Create the inFile type from a string.
|
| ~string:      string to create from
| ~manager:     the package manager to user
| ~basePkgName: base package name
*/
def.static.FromString =
	function( string, manager, basePkgName )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 3 ) throw new Error( );
/**/	if( typeof( string ) !== 'string' ) throw new Error( );
/**/	if( basePkgName !== undefined && typeof( basePkgName ) !== 'string' ) throw new Error( );
/**/}

	let pkgName, fileName;

	if( string[ 0 ] !== '<' ) throw new Error( );
	string = string.substring( 1 ).trim( );

	const ios = string.indexOf( ':' );
	if( ios >= 0 )
	{
		pkgName = string.substring( 0, ios );
		fileName = string.substring( ios + 1 );
	}
	else
	{
		if( !basePkgName ) throw new Error( );
		pkgName = basePkgName;
		fileName = string;
	}

	const pkg = manager.getPackage( pkgName );
	const exp = pkg.exports?.typeSets.get( fileName );

	if( exp ) fileName = exp;

	return(
		Self.create(
			'pkgName', pkgName,
			'fileName', fileName,
		)
	);
};

/*
| This is not a primitive.
*/
def.proto.isPrimitive = false;

/*
| This is not a ti2c thing.
*/
def.proto.isTi2c = false;

/*
| This is not a ti2c collection.
*/
def.proto.isTi2cCollection = false;

/*
| Reads the file.
*/
def.lazyFunc.read =
	async function( manager )
{
	const pkgName = this.pkgName;
	const pkg = manager.getPackage( pkgName );

	const data = await file.read( pkg.srcDir.asString + this.fileName + '.txt' );

	const lines = data.split( '\n' );

	const set = new Set( );
	for( let line of lines )
	{
		line = line.trim( );
		if( line === '' || line[ 0 ] === '#' ) continue;

		if( line[ 0 ] === '<' )
		{
			set.add( Self.FromString( line, manager, pkgName ) );
		}
		else
		{
			set.add( TypeAny.FromString( line, manager, pkgName ) );
		}
	}

	return SetType.create( 'set:init', set );
};

/*
| The ti2ctype as imported var.
*/
def.lazy.varname =
	function( )
{
	return(
		'tt__$Z$_'
		+ this.pkgName.replace( /-/g, '_' )
		+ '__'
		+ this.fileName.replace( /\//g, '_' )
	);
};
