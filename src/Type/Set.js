/*
| A set of types.
*/
def.extend = 'set@<ti2c:Type/Types';

import { Self as Parser            } from '{ti2c:Parser/Self}';
import { Self as TypeAny           } from '{ti2c:Type/Any}';
import { Self as TypeInFile        } from '{ti2c:Type/InFile}';
import { Self as TypeTi2cArbitrary } from '{ti2c:Type/Ti2c/Arbitrary}';

/*
| Adds a type or another type set to this.
*/
def.proto.addX =
	function( tx )
{
	return(
		tx.ti2ctype === Self
		? this.addSet( tx )
		: this.add( tx )
	);
};

/*
| Reads in all InFile types.
*/
def.lazyFunc.expand =
	async function( manager )
{
	if( !this.hasInFile ) return this;

	const set = new Set( );
	for( let type of this )
	{
		if( type.ti2ctype === TypeInFile )
		{
			let setEx = await type.read( manager );

			while( setEx.hasInFile )
			{
				setEx = await setEx.expand( manager );
			}

			for( let t of setEx ) set.add( t );
		}
		else
		{
			set.add( type );
		}
	}
	return Self.create( 'set:init', set );
};

/*
| True if at least one type is an InFile
*/
def.lazy.hasInFile =
	function( )
{
	for( let type of this )
	{
		if( type.ti2ctype === TypeInFile ) return true;
	}
	return false;
};

/*
| True if at least one type is a primitive.
*/
def.lazy.hasPrimitiveType =
	function( )
{
	for( let type of this )
	{
		if( type.ti2ctype === Self )
		{
			if( type.hasPrimitiveType ) return true;
			else continue;
		}
		if( type.isPrimitive ) return true;
	}
	return false;
};

/*
| True if at least one type is a ti2c class.
*/
def.lazy.hasTi2cType =
	function( )
{
	for( let type of this )
	{
		if( type.isTi2c ) return true;
	}
	return false;
};

/*
| Filtes only ti2c types.
*/
def.lazy.filterTi2cTypes =
	function( )
{
	const s = new Set( );
	for( let t of this )
	{
		if( t.isTi2c && t !== TypeTi2cArbitrary.singleton ) s.add( t );
	}
	return this.create( 'set:init', s );
};

/*
| Returns the jsonfy code for a 'val' having this type set
*/
def.lazyFunc.jsonfyCode =
	function( val )
{
	if( this.hasTi2cType )
	{
		if( this.hasPrimitiveType )
		{
			// mixed
			return Parser.expr(
				'typeof(', val, ') === "object"',
				'?', val, '.jsonfy( iSpacing )',
				': JSON.stringify(', val, ')'
			);
		}
		else
		{
			// ti2c types only
			return Parser.expr( val, '.jsonfy( iSpacing )' );
		}
	}
	else
	{
		// primitives only
		return Parser.expr( 'JSON.stringify(', val, ')' );
	}
};

/*
| Creates a type set from a specifier(string) or an array of specifiers.
|
| ~specX:       specifier or array of specifiers
| ~basePkgName: package name to base local types on
*/
def.static.SpecifierX =
	async function( specX, manager, basePkgName )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 3 ) throw new Error( );
/**/	if( typeof( basePkgName ) !== 'string' ) throw new Error( );
/**/}

	const types = new Set( );

	// TOOD make this more elegantly
	if( !Array.isArray( specX ) ) specX = [ specX ];

	for( let entry of specX )
	{
		if( entry[ 0 ] === '<' )
		{
			const tInFi = TypeInFile.FromString( entry, manager, basePkgName );
			const setInFi = await tInFi.read( manager );
			for( let e of setInFi ) types.add( e );
		}
		else
		{
			types.add( TypeAny.FromString( entry, manager, basePkgName ) );
		}
	}

	return Self.create( 'set:init', types );
};

