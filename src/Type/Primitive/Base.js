/*
| Base for primitive types.
*/
def.abstract = true;

/*
| This is a primitive.
*/
def.proto.isPrimitive = true;

/*
| This is not a ti2c thing
*/
def.proto.isTi2c = false;

/*
| This is not a ti2c collection.
*/
def.proto.isTi2cCollection = false;
