/*
| Referencing a boolean type.
*/
def.extend = 'Type/Primitive/Base';
def.singleton = true;

def.proto.asString =
def.proto.asFileString =
def.proto.varname =
	'boolean';
