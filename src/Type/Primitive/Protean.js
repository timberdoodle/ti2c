/*
| Referencing the non-type "protean" for any mutable
| objects.
*/
def.extend = 'Type/Primitive/Base';
def.singleton = true;

def.proto.asString =
def.proto.asFileString =
def.proto.varname =
	'protean';
