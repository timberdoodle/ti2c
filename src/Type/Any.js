/*
| Generic interface for all types.
*/
def.abstract = true;

import { Self as TypeArbitrary } from '{ti2c:Type/Ti2c/Arbitrary}';
import { Self as TypeBoolean   } from '{ti2c:Type/Primitive/Boolean}';
import { Self as TypeDate      } from '{ti2c:Type/Primitive/Date}';
import { Self as TypeFunction  } from '{ti2c:Type/Primitive/Function}';
import { Self as TypeInteger   } from '{ti2c:Type/Primitive/Integer}';
import { Self as TypeNull      } from '{ti2c:Type/Primitive/Null}';
import { Self as TypeNumber    } from '{ti2c:Type/Primitive/Number}';
import { Self as TypeProtean   } from '{ti2c:Type/Primitive/Protean}';
import { Self as TypeString    } from '{ti2c:Type/Primitive/String}';
import { Self as TypeTi2cAny   } from '{ti2c:Type/Ti2c/Any}';
import { Self as TypeUndefined } from '{ti2c:Type/Primitive/Undefined}';

/*
| Create the type from a string specifier.
|
| ~str:         type string to create from
| ~manager:     package manager to use
| ~basePkgName: base package name
*/
def.static.FromString =
	function( str, manager, basePkgName )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 3 ) throw new Error( );
/**/	if( typeof( str ) !== 'string' ) throw new Error( );
/**/	if( basePkgName !== undefined && typeof( basePkgName ) !== 'string' ) throw new Error( );
/**/}

	switch( str )
	{
		case 'arbitrary': return TypeArbitrary.singleton;
		case 'boolean':   return TypeBoolean.singleton;
		case 'date':      return TypeDate.singleton;
		case 'integer':   return TypeInteger.singleton;
		case 'function':  return TypeFunction.singleton;
		case 'null':      return TypeNull.singleton;
		case 'number':    return TypeNumber.singleton;
		case 'protean':   return TypeProtean.singleton;
		case 'string':    return TypeString.singleton;
		case 'undefined': return TypeUndefined.singleton;
		default:          return TypeTi2cAny.FromString( str, manager, basePkgName );
	}
};
