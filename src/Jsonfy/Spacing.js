/*
| A jsonfy spacing state.
*/
def.attributes =
{
	// step string
	step: { type: 'string' },

	// current level
	// it's numbers of levels times the step string
	level: { type: 'string' },
};

/*
| Increases level by 1.
*/
def.lazy.Inc1 =
	function( )
{
	return this.create( 'level', this.level + this.step );
};

/*
| Increases level by 2.
*/
def.lazy.Inc2 =
	function( )
{
	return this.create( 'level', this.level + this.step + this.step );
};

/*
| Starts a spacing with level 1.
*/
def.staticLazyFunc.Start1 =
	function( spacing )
{
	return Self.create( 'level', spacing, 'step', spacing );
};

/*
| Starts a spacing with level 2.
*/
def.staticLazyFunc.Start2 =
	function( spacing )
{
	return Self.create( 'level', spacing + spacing, 'step', spacing );
};
