/*
| Parses a file.
*/
Error.stackTraceLimit = Infinity;
global.CHECK = true;

import { listing } from '../core/listing.js';

await import( '../root.js' );

const pkg = await ti2c.register( 'ti2c', import.meta, 'src/', 'FileParser/Start', 'codegen/' );
const Root = await pkg.import( 'FileParser/Root' );
await Root.init( listing );
