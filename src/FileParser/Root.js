/*
| Runs the file parser tool.
*/
def.abstract = true;

import fs from 'node:fs/promises';

import { Self as Formatter } from '{Format/Self}';
import { Self as Parser    } from '{Parser/Self}';

const exceptions = new Set( [ ] );

/*
| Parses all .js files in a directory.
| Recurses into subdirs.
*/
def.static.dir =
	async function( dirname )
{
	const dir = await fs.readdir( dirname, { encoding: 'utf8', withFileTypes: true } );

	for( let entry of dir )
	{
		const subname = dirname + '/' + entry.name;
		if( entry.isDirectory( ) )
		{
			Self.dir( subname );
			continue;
		}
		if( !subname.endsWith( '.js' ) ) continue;
		if( exceptions.has( subname ) ) continue;
		console.log( '--- ' + subname + ' ---' );
		const text = ( await fs.readFile( subname ) ) + '';
		const tree = Parser.block( text );
		console.log( Formatter.format( tree ) );
	}
};

/*
| Entry point.
*/
def.static.init =
	async function( listing )
{
	if( process.argv.length < 3 )
	{
		console.log( 'Filename to parse missing.' );
		return;
	}

	if( process.argv[ 2 ] === '--listing' )
	{
		for( let filename of listing )
		{
			console.log( filename );
			const text = ( await fs.readFile( 'src/' + filename ) ) + '';
			const tree = Parser.block( text );
			console.log( Formatter.format( tree ) );
		}
		return;
	}

	if( process.argv[ 2 ] === '--dir' )
	{
		if( process.argv.length < 4 )
		{
			console.log( 'Dir to scan missing.' );
			return;
		}
		await Self.dir( process.argv[ 3 ] );
		return;
	}

	const filename = process.argv[ 2 ];
	const text = ( await fs.readFile( filename ) ) + '';

	const tree = Parser.block( text );
	console.log( Formatter.format( tree ) );
};
