/*
| Formating context.
*/
def.attributes =
{
	// true if within optional CHECK code
	check: { type: 'boolean', defaultValue: 'false' },

	// the indentation
	indent: { type: 'integer', defaultValue: '0' },

	// true if to be formated inline
	inline: { type: 'boolean', defaultValue: 'false' },
};

/*
| Tabbing format.
*/
const tabFormat = '\t';

/*
| Returns a check context.
*/
def.lazy.Check =
	function( )
{
	return this.create( 'check', true );
};

/*
| Decreases the indentation.
*/
def.lazy.Dec =
	function( )
{
	if( this.indent <= 0 ) return this;
	const dec = this.create( 'indent', this.indent - 1 );
	ti2c.aheadValue( dec, 'Inc', this );
	return dec;
};

/*
| Increases the indentation.
*/
def.lazy.Inc =
	function( )
{
	return this.create( 'indent', this.indent + 1 );
};

/*
| Creates a seperation/tab.
| When in inline it is just a space
| When not, it's a new line and a tab indentation
*/
def.lazy.sep =
	function( )
{
	let indent = this.indent;
	let tab = '';

	if( this.check )
	{
		indent--;
		tab += '/**/';
	}

	for( let a = 0; a < indent; a++ )
	{
		tab += tabFormat;
	}

	return this.inline ? ' ' : ( '\n' + tab );
};

/*
| Sets the context to be inline.
*/
def.lazy.Inline =
	function( )
{
	return this.create( 'inline', true );
};
