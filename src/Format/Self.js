/*
| Formats an abstract syntax tree into a .js file.
*/
def.attributes =
{
	// filename used for source node
	// if undefined no source map is generated
	mapSourceName: { type: [ 'undefined', 'string' ] },
};

const MaxLineWidth = 98;

import { Self as And                       } from '{ti2c:Ast/And}';
import { Self as ArrayLiteral              } from '{ti2c:Ast/ArrayLiteral}';
import { Self as ArrowFunction             } from '{ti2c:Ast/ArrowFunction}';
import { Self as Assign                    } from '{ti2c:Ast/Assign}';
import { Self as AstBoolean                } from '{ti2c:Ast/Boolean}';
import { Self as AstString                 } from '{ti2c:Ast/String}';
import { Self as Await                     } from '{ti2c:Ast/Await}';
import { Self as BitwiseAnd                } from '{ti2c:Ast/BitwiseAnd}';
import { Self as BitwiseAndAssign          } from '{ti2c:Ast/BitwiseAndAssign}';
import { Self as BitwiseLeftShift          } from '{ti2c:Ast/BitwiseLeftShift}';
import { Self as BitwiseNot                } from '{ti2c:Ast/BitwiseNot}';
import { Self as BitwiseOr                 } from '{ti2c:Ast/BitwiseOr}';
import { Self as BitwiseOrAssign           } from '{ti2c:Ast/BitwiseOrAssign}';
import { Self as BitwiseRightShift         } from '{ti2c:Ast/BitwiseRightShift}';
import { Self as BitwiseUnsignedRightShift } from '{ti2c:Ast/BitwiseUnsignedRightShift}';
import { Self as BitwiseXor                } from '{ti2c:Ast/BitwiseXor}';
import { Self as BitwiseXorAssign          } from '{ti2c:Ast/BitwiseXorAssign}';
import { Self as Block                     } from '{ti2c:Ast/Block}';
import { Self as Break                     } from '{ti2c:Ast/Break}';
import { Self as Call                      } from '{ti2c:Ast/Call}';
import { Self as Check                     } from '{ti2c:Ast/Check}';
import { Self as Comma                     } from '{ti2c:Ast/Comma}';
import { Self as Comment                   } from '{ti2c:Ast/Comment}';
import { Self as Condition                 } from '{ti2c:Ast/Condition}';
import { Self as ConditionalDot            } from '{ti2c:Ast/ConditionalDot}';
import { Self as Const                     } from '{ti2c:Ast/Const}';
import { Self as Context                   } from '{ti2c:Format/Context}';
import { Self as Continue                  } from '{ti2c:Ast/Continue}';
import { Self as Declaration               } from '{ti2c:Ast/Declaration}';
import { Self as Delete                    } from '{ti2c:Ast/Delete}';
import { Self as DestructDecl              } from '{ti2c:Ast/DestructDecl}';
import { Self as Differs                   } from '{ti2c:Ast/Differs}';
import { Self as Divide                    } from '{ti2c:Ast/Divide}';
import { Self as DivideAssign              } from '{ti2c:Ast/DivideAssign}';
import { Self as DoWhile                   } from '{ti2c:Ast/DoWhile}';
import { Self as Dot                       } from '{ti2c:Ast/Dot}';
import { Self as Equals                    } from '{ti2c:Ast/Equals}';
import { Self as For                       } from '{ti2c:Ast/For}';
import { Self as ForIn                     } from '{ti2c:Ast/ForIn}';
import { Self as ForOf                     } from '{ti2c:Ast/ForOf}';
import { Self as Func                      } from '{ti2c:Ast/Func/Self}';
import { Self as GreaterOrEqual            } from '{ti2c:Ast/GreaterOrEqual}';
import { Self as GreaterThan               } from '{ti2c:Ast/GreaterThan}';
import { Self as If                        } from '{ti2c:Ast/If}';
import { Self as Import                    } from '{ti2c:Ast/Import}';
import { Self as Instanceof                } from '{ti2c:Ast/Instanceof}';
import { Self as LessOrEqual               } from '{ti2c:Ast/LessOrEqual}';
import { Self as LessThan                  } from '{ti2c:Ast/LessThan}';
import { Self as Let                       } from '{ti2c:Ast/Let}';
import { Self as Member                    } from '{ti2c:Ast/Member}';
import { Self as Minus                     } from '{ti2c:Ast/Minus}';
import { Self as MinusAssign               } from '{ti2c:Ast/MinusAssign}';
import { Self as Multiply                  } from '{ti2c:Ast/Multiply}';
import { Self as MultiplyAssign            } from '{ti2c:Ast/MultiplyAssign}';
import { Self as Negate                    } from '{ti2c:Ast/Negate}';
import { Self as New                       } from '{ti2c:Ast/New}';
import { Self as Not                       } from '{ti2c:Ast/Not}';
import { Self as Null                      } from '{ti2c:Ast/Null}';
import { Self as NullishCoalescence        } from '{ti2c:Ast/NullishCoalescence}';
import { Self as AstNumber                 } from '{ti2c:Ast/Number}';
import { Self as ObjLiteral                } from '{ti2c:Ast/ObjLiteral}';
import { Self as Or                        } from '{ti2c:Ast/Or}';
import { Self as Plus                      } from '{ti2c:Ast/Plus}';
import { Self as PlusAssign                } from '{ti2c:Ast/PlusAssign}';
import { Self as PostDecrement             } from '{ti2c:Ast/PostDecrement}';
import { Self as PostIncrement             } from '{ti2c:Ast/PostIncrement}';
import { Self as PreDecrement              } from '{ti2c:Ast/PreDecrement}';
import { Self as PreIncrement              } from '{ti2c:Ast/PreIncrement}';
import { Self as Regex                     } from '{ti2c:Ast/Regex}';
import { Self as Remainder                 } from '{ti2c:Ast/Remainder}';
import { Self as RemainderAssign           } from '{ti2c:Ast/RemainderAssign}';
import { Self as Return                    } from '{ti2c:Ast/Return}';
import { Self as Sep                       } from '{ti2c:Ast/Sep}';
import { Self as SMapList                  } from '{ti2c:SMap/List}';
import { Self as SMapNode                  } from '{ti2c:SMap/Node}';
import { Self as Spread                    } from '{ti2c:Ast/Spread}';
import { Self as Switch                    } from '{ti2c:Ast/Switch}';
import { Self as Throw                     } from '{ti2c:Ast/Throw}';
import { Self as Try                       } from '{ti2c:Ast/Try}';
import { Self as Typeof                    } from '{ti2c:Ast/Typeof}';
import { Self as Undefined                 } from '{ti2c:Ast/Undefined}';
import { Self as Var                       } from '{ti2c:Ast/Var}';
import { Self as VarDec                    } from '{ti2c:Ast/VarDec}';
import { Self as While                     } from '{ti2c:Ast/While}';
import { Self as Yield                     } from '{ti2c:Ast/Yield}';

/*
| Returns "'" if a string needs quotes
*/
function _needsQuotes( str )
{
	return(
		str.match( /^[a-zA-Z_$][a-zA-Z_$0-9]*$/ )
		? ''
		: '\''
	);
}

/*
| Returns true if 'line' fits in a line.
*/
const fits = ( context, line ) =>
{
	if( !line ) return false;
	return line.string.length + context.indent * 4 <= MaxLineWidth;
};

/*
| Formats a block or statement to code.
|
| ~bos: block or statement
*/
def.static.format =
	function( bos )
{
	const context = Context.create( );
	const formatter = Self.create( );

	const chunk =
		bos.ti2ctype === Block
		? formatter._block( context, bos, true )
		: formatter._statement( context, bos );

	const code = chunk.string;

	return(
		code.endsWith( '\n' )
		? code
		: code + '\n'
	);
};

/*
| Formats a block or statement to code filling a sourceMap
|
| ~result: mutable(!) protean of code to generate.
| ~bos: block or statement
*/
def.static.formatWithSourceMap =
	function( bos, mapSourceName, result )
{
	const context = Context.create( );
	const formatter = Self.create( 'mapSourceName', mapSourceName );

	let chunk =
		bos.ti2ctype === Block
		? formatter._block( context, bos, true )
		: formatter._statement( context, bos );

	if( !chunk.string.endsWith( '\n' ) )
	{
		chunk = chunk.append( '\n' );
	}

	chunk.codeWithSourceMap( result, mapSourceName );
};

/*
| Formats an array literal.
|
| FUTURE format also inline
*/
def.proto._arrayLiteral =
	function( context, expr )
{
/**/if( CHECK && expr.ti2ctype !== ArrayLiteral ) throw new Error( );

	const entries = expr.entries;
	if( entries.length === 0 ) return '[ ]';

	const ci = context.Inc;
	const chunk = [ '[', ci.sep ] ;
	let first = true;
	for( let e of entries )
	{
		if( first ) { first = false; }
		else { chunk.push( ',', ci.sep ); }

		chunk.push( this._expression( ci, e, ArrayLiteral ) );
	}
	chunk.push( context.sep, ']' );
	return SMapList.Array( chunk );
};

/*
| Formats an arrow function.
*/
def.proto._arrowFunction =
	function( context, afunc )
{
	let chunk;
	const args = afunc.args;
	if( args.length === 0 )
	{
		chunk = [ '( )' ];
	}
	else
	{
		let first = true;
		chunk = [ '( ' ];
		for( let arg of args )
		{
			if( first ) first = false;
			else chunk.push( ', ' );

			chunk.push( arg );
		}
		chunk.push( ' )' );
	}
	chunk.push( ' => ' );
	const body = afunc.body;

	if( body.ti2ctype === Block )
	{
		chunk.push( this._block( context, body ) );
	}
	else
	{
		chunk.push( '( ', this._expression( context, body ), ' )' );
	}

	return SMapList.Array( chunk );
};

/*
| Formats an assignment (=,+=,-=,*=,/=).
*/
def.proto._assign =
	function( context, assign )
{
	const rcontext = assign.right.ti2ctype === Assign ? context : context.Inc;

	return(
		SMapList.Elements(
			this._expression( context, assign.left, assign.ti2ctype ),
			' ',
			assign.operand,
			rcontext.sep,
			this._expression( rcontext, assign.right, assign.ti2ctype ),
		)
	);
};

/*
| Formats an await expression.
*/
def.proto._await =
	function( context, expr )
{
/**/if( CHECK && expr.ti2ctype !== Await ) throw new Error( );

	return(
		SMapList.Elements(
			'await ',
			this._expression( context, expr.expr, Delete ),
		)
	);
};

/*
| Formats a block.
|
| ~context:    the context to format in
| ~block:      the block to format to
| ~noBrackets: omit brackets
*/
def.proto._block =
	function( context, block, noBrackets )
{
/**/if( CHECK && block.ti2ctype !== Block ) throw new Error( );

	let blockContext;
	let chunk = [ ];

	if( !noBrackets )
	{
		blockContext = context.Inc;
		chunk.push( '{' );
	}
	else
	{
		blockContext = context;
	}

	const statements = block.statements;
	for( let a = 0, alen = statements.length; a < alen; a++ )
	{
		const sa = a + 1 < alen ? statements.get( a + 1 ) : undefined;
		const sb = a > 0 ? statements.get( a - 1 ) : undefined;
		const s = statements.get( a );

		let sc = s.ti2ctype !== Check ? blockContext : blockContext.Check;
		if( chunk.length > 0 ) chunk.push( sc.sep );

		let line;
		if( !sc.inline ) line = this._statement( sc.Inline, s, sb, sa );
		if( !fits( sc, line ) ) line = this._statement( sc, s, sb, sa );

		chunk.push( line );
	}

	if( !noBrackets )
	{
		chunk.push( context.sep, '}' );
	}

	return SMapList.Array( chunk );
};

/*
| Formats a boolean literal use.
*/
def.proto._boolean =
	function( context, expr )
{
	return this._node( expr.token, expr.boolean ? 'true' : 'false' );
};

/*
| Formats a break statement.
*/
def.proto._break =
	function( context, statement )
{
	return this._node( statement.token, 'break' );
};

/*
| Formats a call.
*/
def.proto._call =
	function( context, call, snuggle )
{
/**/if( CHECK && call.ti2ctype !== Call ) throw new Error( );

	let chunk = this._expression( snuggle ? context.Inline : context, call.func, Call );
	const args = call.args;
	if( args.length === 0 )
	{
		return chunk.append( '( )' );
	}

	const subContext = context.Inc;
	chunk = [ chunk ];
	chunk.push( '(', subContext.sep );
	let first = true;
	for( let arg of args )
	{
		if( !first ) chunk.push( ',', subContext.sep ); else first = false;
		chunk.push( this._expression( subContext, arg ) );
	}
	chunk.push( context.sep, ')' );
	return SMapList.Array( chunk );
};

/*
| Formats a conditional checking code.
*/
def.proto._formatCheck =
	function( context, check )
{
/**/if( CHECK && !context.check ) throw new Error( );

	return(
		SMapList.Elements(
			'if( CHECK )',
			context.sep,
			this._block( context, check.block )
		)
	);
};

/*
| Formats a comma operator.
*/
def.proto._comma =
	function( context, expr )
{
/**/if( CHECK && expr.ti2ctype !== Comma ) throw new Error( );

	return(
		SMapList.Elements(
			this._expression( context, expr.left, Comma ),
			',',
			context.sep,
			this._expression( context, expr.right, Comma ),
		)
	);
};

/*
| Formats a comment.
*/
def.proto._comment =
	function( context, comment, prev )
{
	if( context.inline ) return false;

	const chunk = [ ];
	if( prev ) chunk.push( context.sep );

	chunk.push( '/*' );
	for( let line of comment.lines )
	{
		if( line === '' ) chunk.push( context.sep, '|' );
		else chunk.push( context.sep, '| ', line );
	}
	chunk.push( context.sep, '*/' );
	return SMapList.Array( chunk );
};

/*
| Formats a condition expression.
| The ? : thingy.
*/
def.proto._condition =
	function( context, expr )
{
/**/if( CHECK && expr.ti2ctype !== Condition ) throw new Error( );

	return(
		SMapList.Elements(
			this._expression( context, expr.condition, Condition ),
			context.sep,
			'? ',
			this._expression( context.Inline, expr.then, Condition ),
			context.sep,
			': ',
			this._expression( context.Inline, expr.elsewise, Condition ),
		)
	);
};

/*
| Formats a conditional dot.
*/
def.proto._conditionalDot =
	function( context, expr )
{
/**/if( CHECK && expr.ti2ctype !== ConditionalDot ) throw new Error( );

	return(
		SMapList.Elements(
			this._expression( context, expr.expr, ConditionalDot ),
			'?.',
			expr.member
		)
	);
};

/*
| Formats a continue statement.
*/
def.proto._continue =
	function( context, statement )
{
	return this._node( statement.token, 'continue' );
};

/*
| Formats let/constant a variable declaration.
*/
def.proto._declare =
	function( context, expr )
{
	const chunk = [ ];
	switch( expr.ti2ctype )
	{
		case Const:
			chunk.push( 'const ' );
			break;

		case Let:
			chunk.push( 'let ' );
			break;

		default: throw new Error( );
	}

	let first = true;
	for( let decl of expr.declarations )
	{
		if( first )
		{
			first = false;
		}
		else
		{
			chunk.push( ',', context.sep );
		}

		switch( decl.ti2ctype )
		{
			case Declaration:
				chunk.push( decl.name );
				break;

			case DestructDecl:
				chunk.push( this._objLiteral( context, decl.literal ) );
				break;

			default:
				throw new Error( );
		}

		if( decl.assign )
		{
			let subContext = context;
			if( decl.assign.ti2ctype !== Assign ) subContext = context.Inc;

			chunk.push( ' =', subContext.sep, this._expression( subContext, decl.assign ) );
		}
	}

	return SMapList.Array( chunk );
};

/*
| Formats a delete expression.
*/
def.proto._delete =
	function( context, expr )
{
/**/if( CHECK && expr.ti2ctype !== Delete ) throw new Error( );

	return(
		SMapList.Elements(
			'delete ',
			this._expression( context, expr.expr, Delete )
		)
	);
};

/*
| Formats a do while loop.
*/
def.proto._doWhile =
	function( context, expr )
{
	const ci = context.Inc;
	return(
		SMapList.Elements(
			'do',
			context.sep, this._statement( context, expr.body ),
			ci.sep, 'while (',
			this._expression( ci.Inline, expr.condition ),
			context.sep, ')',
		)
	);
};

/*
| Formats a dualistic operation
*/
def.proto._dualOp =
	function( context, expr )
{
	const token = expr.token;
	return(
		SMapList.Elements(
			this._expression( context, expr.left, expr.ti2ctype, 'left' ),
			context.sep,
			this._node( token, expr.operand ),
			' ',
			this._expression( context, expr.right, expr.ti2ctype, 'right' ),
		)
	);
};

/*
| Formats a dot.
*/
def.proto._dot =
	function( context, expr )
{
/**/if( CHECK && expr.ti2ctype !== Dot ) throw new Error( );

	return(
		SMapList.Elements(
			this._expression( context, expr.expr, Dot ),
			this._node( expr.tokenDot, '.' ),
			this._node( expr.tokenMember, expr.member ),
		)
	);
};

/*
| Formats an expression.
|
| ~context:   context to be formated in
| ~expr:      the expression to format
| ~pti2ctype: ti2ctype of parenting expression, may be undefined
| ~side:      if defined left or right hand side
*/
def.proto._expression =
	function( context, expr, pti2ctype, side )
{
	const exop = Self._exOpTable.get( expr.ti2ctype );
	if( exop === undefined ) throw new Error( );

	let pexop;
	if( pti2ctype )
	{
		pexop = Self._exOpTable.get( pti2ctype );
		if( pexop === undefined ) throw new Error( );
	}

	if( !side ) side = 'left';

	let bracket;
	if( pexop !== undefined )
	{
		bracket = exop.prec <= pexop.prec;
		if( side === 'left' && exop.prec === pexop.prec )
		{
			bracket = false;
		}
	}
	else
	{
		bracket = false;
	}

	let chunk;
	if( !context.inline )
	{
		chunk = this._expression2( context.Inline, expr, bracket );
	}

	if( !fits( context, chunk ) )
	{
		chunk = this._expression2( context, expr, bracket );
	}

	return SMapList.Array( chunk );
};

/*
| Helper to _expression
*/
def.proto._expression2 =
	function( context, expr, bracket )
{
	const formatter = Self._exprFormatter.get( expr.ti2ctype );
	let cinc = bracket ? context.Inc : context;

	const chunk = [ ];
	if( bracket ) chunk.push( '(', cinc.sep );

	chunk.push( this[ formatter ]( cinc, expr ) );

	if( bracket ) chunk.push( context.sep, ')' );

	return SMapList.Array( chunk );
};

/*
| Table of all expression formatters.
*/
def.staticLazy._exprFormatter = ( ) =>
	new Map( [
		[ And,                       '_dualOp'         ],
		[ ArrayLiteral,              '_arrayLiteral'   ],
		[ ArrowFunction,             '_arrowFunction'  ],
		[ Assign,                    '_assign'         ],
		[ AstBoolean,                '_boolean'        ],
		[ AstString,                 '_string'         ],
		[ Await,                     '_await'          ],
		[ BitwiseAnd,                '_dualOp'         ],
		[ BitwiseAndAssign,          '_assign'         ],
		[ BitwiseLeftShift,          '_dualOp'         ],
		[ BitwiseNot,                '_preOp'          ],
		[ BitwiseOr,                 '_dualOp'         ],
		[ BitwiseOrAssign,           '_assign'         ],
		[ BitwiseRightShift,         '_dualOp'         ],
		[ BitwiseUnsignedRightShift, '_dualOp'         ],
		[ BitwiseXor,                '_dualOp'         ],
		[ BitwiseXorAssign,          '_assign'         ],
		[ Call,                      '_call'           ],
		[ Comma,                     '_comma'          ],
		[ Condition,                 '_condition'      ],
		[ ConditionalDot,            '_conditionalDot' ],
		[ Const,                     '_declare'        ],
		[ Delete,                    '_delete'         ],
		[ Differs,                   '_dualOp'         ],
		[ Divide,                    '_dualOp'         ],
		[ DivideAssign,              '_assign'         ],
		[ Dot,                       '_dot'            ],
		[ Equals,                    '_dualOp'         ],
		[ Func,                      '_func'           ],
		[ GreaterOrEqual,            '_dualOp'         ],
		[ GreaterThan,               '_dualOp'         ],
		[ Instanceof,                '_instanceof'     ],
		[ LessOrEqual,               '_dualOp'         ],
		[ LessThan,                  '_dualOp'         ],
		[ Let,                       '_declare'        ],
		[ Member,                    '_member'         ],
		[ Minus,                     '_dualOp'         ],
		[ MinusAssign,               '_assign'         ],
		[ Multiply,                  '_dualOp'         ],
		[ MultiplyAssign,            '_assign'         ],
		[ Negate,                    '_preOp'          ],
		[ New,                       '_new'            ],
		[ Not,                       '_preOp'          ],
		[ Null,                      '_null'           ],
		[ NullishCoalescence,        '_dualOp'         ],
		[ AstNumber,                 '_number'         ],
		[ ObjLiteral,                '_objLiteral'     ],
		[ Or,                        '_dualOp'         ],
		[ Plus,                      '_dualOp'         ],
		[ PlusAssign,                '_assign'         ],
		[ PostDecrement,             '_postOp'         ],
		[ PostIncrement,             '_postOp'         ],
		[ PreDecrement,              '_preOp'          ],
		[ PreIncrement,              '_preOp'          ],
		[ Regex,                     '_regex'          ],
		[ Remainder,                 '_dualOp'         ],
		[ RemainderAssign,           '_assign'         ],
		[ Spread,                    '_preOp'          ],
		[ Typeof,                    '_typeof'         ],
		[ Undefined,                 '_undefined'      ],
		[ Var,                       '_var'            ],
		[ Yield,                     '_yield'          ],
	] );


/*
| Formats a classical for loop.
*/
def.proto._for =
	function( context, expr )
{
	const ci = context.Inc;

	const cil = ci.Inline;
	const init =
		expr.init
		? this._expression( cil, expr.init )
		: '';

	const condition =
		expr.condition
		? this._expression( cil, expr.condition )
		: '';

	const iterate =
		expr.iterate
		? this._expression( cil, expr.iterate )
		: '';

	let chunk;
	if( !ci.inline )
	{
		chunk =
		[
			'for(',
			cil.sep, init, ';', cil.sep,
			condition, ';', cil.sep,
			iterate, cil.sep, ')',
		];
	}

	if( !chunk || !fits( context, SMapList.Array( chunk.slice( ) ) ) )
	{
		chunk =
		[
			'for(',
			ci.sep, init, ';', ci.sep,
			condition, ';', ci.sep,
			iterate, context.sep, ')',
		];
	}

	chunk.push( context.sep, this._block( context, expr.block ) );

	return SMapList.Array( chunk );
};

/*
| Formats a for-in/for-of loop.
*/
def.proto._forInOf =
	function( context, expr )
{
	const iof = expr.ti2ctype === ForIn ? 'in' : 'of';

	return(
		SMapList.Elements(
			'for( ',
			( expr.letVar ? 'let ' : '' ),
			expr.variable.name,
			' ', iof, ' ',
			this._expression( context.Inline, expr.object, Instanceof ),
			' )',
			context.sep, this._block( context, expr.block ),
		)
	);
};

/*
| Formats a function.
*/
def.proto._func =
	function( context, func )
{
	let chunk = [ ];

	if( func.isAsync ) chunk.push( 'async ' );
	chunk.push( 'function' );
	if( func.isGenerator ) chunk.push( '*' );
	if( func.name !== undefined ) chunk.push( ' ', func.name );

	const args = func.args;
	if( args.length === 0 )
	{
		chunk.push( '( )' );
	}
	else
	{
		if( context.inline )
		{
			chunk.push( this._funcArgs( context, func ) );
		}
		else
		{
			// if not in inline mode, tries if an inlined header fits
			const fai = this._funcArgs( context.Inline, func );
			const chunkInline = chunk.slice( );
			chunkInline.push( fai );
			const lineInline = SMapList.Array( chunkInline );

			if( fits( context, lineInline ) )
			{
				chunk.push( fai );
			}
			else
			{
				chunk.push( this._funcArgs( context, func ) );
			}
		}
	}

	// formats the body one indentation decremented
	const dc = context.Dec;
	chunk.push( dc.sep, this._block( dc, func.body ) );

	return SMapList.Array( chunk );
};

/*
| Formts arguments of a function (or generator).
*/
def.proto._funcArgs =
	function( context, func )
{
	const args = func.args;
	const chunk = [ '(' ];
	for( let a = 0, alen = args.length; a < alen; a++ )
	{
		const arg = args.get( a );
		const name = arg.name;
		if( !name ) throw new Error( );
		chunk.push( context.Inc.sep );
		if( arg.rest ) chunk.push( '...' );
		if( arg.name ) chunk.push( arg.name );
		if( a + 1 < alen ) chunk.push( ',' );
	}
	chunk.push( context.sep, ')' );

	return SMapList.Array( chunk );
};

/*
| Formats an if statement.
*/
def.proto._if =
	function( context, statement )
{
/**/if( CHECK && statement.ti2ctype !== If ) throw new Error( );

	const cond = statement.condition;
	const ci = context.Inc;
	let condition;
	if( !context.inline )
	{
		const expr = this._expression( ci.Inline, cond );
		if( expr )
		{
			condition = SMapList.Elements( 'if( ', expr, ' )' );
		}
	}

	if( !fits( context, condition ) )
	{
		condition =
			SMapList.Elements(
				'if(', ci.sep,
				this._expression( ci, cond ),
				context.sep, ')'
			);
	}

	let then =
		SMapList.Elements(
			context.sep,
			this._block( context, statement.then ),
		);

	if( statement.elsewise )
	{
		const elsewise =
			SMapList.Elements(
				context.sep, 'else',
				context.sep, this._block( context, statement.elsewise ),
			);

		return SMapList.Elements( condition, then, elsewise );
	}
	else
	{
		return SMapList.Elements( condition, then );
	}
};

/*
| Formats an import statement.
*/
def.proto._import =
	function( context, statement )
{
/**/if( CHECK && statement.ti2ctype !== Import ) throw new Error( );

	const aliasName = statement.aliasName;
	const exportName = statement.exportName;
	const moduleName = statement.moduleName;

	return(
		SMapList.Elements(
			'import { ',
			exportName,
			' as ',
			aliasName,
			' } from \'',
			moduleName,
			'\'',
		)
	);
};

/*
| Formats an instanceof expression.
*/
def.proto._instanceof =
	function( context, expr )
{
/**/if( CHECK && expr.ti2ctype !== Instanceof ) throw new Error( );

	return(
		SMapList.Elements(
			this._expression( context, expr.left, Instanceof ),
			context.sep, 'instanceof', context.sep,
			this._expression( context, expr.right, Instanceof ),
		)
	);
};

/*
| Formats a member.
*/
def.proto._member =
	function( context, expr )
{
/**/if( CHECK && expr.ti2ctype !== Member ) throw new Error( );

	return(
		SMapList.Elements(
			this._expression( context, expr.expr, Member ),
			'[', context.sep,
			this._expression( context.Inc, expr.member ),
			context.sep, ']',
		)
	);
};

/*
| Formats a new expression.
*/
def.proto._new =
	function( context, expr )
{
/**/if( CHECK && expr.ti2ctype !== New ) throw new Error( );

	return(
		SMapList.Elements(
			'new ', this._call( context, expr.call, true )
		)
	);
};

/*
| Formats a null.
*/
def.proto._null =
	function( context, expr )
{
	return this._node( expr.token, 'null' );
};

/*
| Formats a number literal use.
*/
def.proto._number =
	function( context, expr )
{
	return this._node( expr.token, '' + expr.number );
};

/*
| Formats an object literal.
| FUTURE format also inline
*/
def.proto._objLiteral =
	function( context, expr )
{
/**/if( CHECK && expr.ti2ctype !== ObjLiteral ) throw new Error( );

	const pairs = expr.pairs;
	if( pairs.length === 0 ) return '{ }';

	const ci = context.Inc;
	const cii = ci.Inc;

	const chunk = [ '{' ];
	for( let a = 0, alen = pairs.length; a < alen; a++ )
	{
		const key = pairs.getKey( a );
		const q = _needsQuotes( key );
		chunk.push( ci.sep, q, key, q );
		const sexpr = pairs.get( key );

		if( sexpr )
		{
			chunk.push( ':', cii.sep );
			// FUTURE, precTable.Objliteral
			chunk.push( this._expression( cii, sexpr ) );
		}

		if( a + 1 < alen ) chunk.push( ',' );
	}

	chunk.push( context.sep, '}' );

	return SMapList.Array( chunk );
};

/*
| Formats a post dec- or increment.
*/
def.proto._postOp =
	function( context, expr )
{
	return(
		SMapList.Elements(
			this._expression( context, expr.expr, expr.ti2ctype ),
			this._node( expr.token, expr.operand ),
		)
	);
};

/*
| Expression operator table.
*/
def.staticLazy._exOpTable = ( ) =>
	new Map(
	[
		[ Const,                     { prec: 100              } ],
		[ ArrayLiteral,              { prec: 100              } ],
		[ AstBoolean,                { prec: 100              } ],
		[ AstNumber,                 { prec: 100              } ],
		[ AstString,                 { prec: 100              } ],
		[ Let,                       { prec: 100              } ],
		[ Null,                      { prec: 100              } ],
		[ Undefined,                 { prec: 100              } ],
		[ ObjLiteral,                { prec: 100              } ],
		[ Regex,                     { prec: 100              } ],
		[ Var,                       { prec: 100              } ],
		[ ArrowFunction,             { prec:  99              } ],
		[ Member,                    { prec:  18, asso: 'n/a' } ],
		[ ConditionalDot,            { prec:  18, asso: 'rtl' } ],
		[ Dot,                       { prec:  18, asso: 'rtl' } ],
		[ Call,                      { prec:  18, asso: 'n/a' } ],
		[ New,                       { prec:  17, asso: 'n/a' } ],
		[ Func,                      { prec:  16              } ], // ???
		[ PostDecrement,             { prec:  16              } ],
		[ PostIncrement,             { prec:  16              } ],
		[ BitwiseNot,                { prec:  15              } ],
		[ Delete,                    { prec:  15              } ],
		[ Negate,                    { prec:  15              } ],
		[ Not,                       { prec:  15              } ],
		[ PreDecrement,              { prec:  15              } ],
		[ PreIncrement,              { prec:  15              } ],
		[ Typeof,                    { prec:  15              } ],
		[ Await,                     { prec:  15              } ],
		[ Divide,                    { prec:  13              } ],
		[ Multiply,                  { prec:  13              } ],
		[ Remainder,                 { prec:  13              } ],
		[ Minus,                     { prec:  12              } ],
		[ Plus,                      { prec:  12              } ],
		[ BitwiseLeftShift,          { prec:  11              } ],
		[ BitwiseUnsignedRightShift, { prec:  11              } ],
		[ BitwiseRightShift,         { prec:  11              } ],
		[ GreaterOrEqual,            { prec:  10              } ],
		[ GreaterThan,               { prec:  10              } ],
		[ Instanceof,                { prec:  10              } ],
		[ LessOrEqual,               { prec:  10              } ],
		[ LessThan,                  { prec:  10              } ],
		[ Differs,                   { prec:   9              } ],
		[ Equals,                    { prec:   9              } ],
		[ BitwiseAnd,                { prec:   8              } ],
		[ BitwiseXor,                { prec:   7              } ],
		[ BitwiseOr,                 { prec:   6              } ],
		[ And,                       { prec:   5              } ],
		[ Or,                        { prec:   4              } ],
		[ NullishCoalescence,        { prec:   4              } ],
		[ Condition,                 { prec:   3              } ],
		[ Assign,                    { prec:   2              } ],
		[ BitwiseAndAssign,          { prec:   2              } ],
		[ BitwiseOrAssign,           { prec:   2              } ],
		[ BitwiseXorAssign,          { prec:   2              } ],
		[ DivideAssign,              { prec:   2              } ],
		[ MinusAssign,               { prec:   2              } ],
		[ MultiplyAssign,            { prec:   2              } ],
		[ PlusAssign,                { prec:   2              } ],
		[ RemainderAssign,           { prec:   2              } ],
		[ Yield,                     { prec:   2              } ],
		[ Spread,                    { prec:   2, asso: 'n/a' } ],
		[ Comma,                     { prec:   1              } ],
	] );


/*
| Formats a monoistic operator.
*/
def.proto._preOp =
	function( context, expr )
{
	return(
		SMapList.Elements(
			this._node( expr.token, expr.operand ),
			this._expression( context, expr.expr, expr.ti2ctype ),
		)
	);
};

/*
| Formats a regular expression.
*/
def.proto._regex =
	function( context, expr )
{
	return(
		SMapList.Elements(
			'/',
			expr.string,
			'/',
			expr.flags
		)
	);
};

/*
| Formats a return statement.
*/
def.proto._return =
	function( context, statement )
{
/**/if( CHECK && statement.ti2ctype !== Return ) throw new Error( );

	const ci = context.Inc;
	const expr = statement.expr;

	if( !expr ) return SMapList.Elements( 'return' );

	if( context.inline )
	{
		return(
			SMapList.Elements(
				'return ',
				this._expression( ci, statement.expr ),
			)
		);
	}
	else
	{
		return(
			SMapList.Elements(
				'return(',
				ci.sep, this._expression( ci, statement.expr ),
				context.sep, ')',
			)
		);
	}
};

/*
| Formats a separator.
*/
def.proto._sep =
	function( context )
{
	return SMapList.Empty;
};

/*
| Formats a statement.
|
| ~context: context to be formated in
| ~statement: the statement to be formated
| ~prev: the previous statement (or undefined)
| ~next: the next statement (or undefined)
*/
def.proto._statement =
	function( context, statement, prev, next )
{
	let chunk;
	switch( statement.ti2ctype )
	{
		case Block:    chunk = this._block( context, statement, false );  break;
		case Break:    chunk = this._break( context, statement );         break;
		case Check:    chunk = this._formatCheck( context, statement );   break;
		case Comment:  chunk = this._comment( context, statement, prev ); break;
		case Const:    chunk = this._declare( context, statement );       break;
		case Continue: chunk = this._continue( context, statement );      break;
		case DoWhile:  chunk = this._doWhile( context, statement );       break;
		case For:      chunk = this._for( context, statement );           break;
		case ForIn:    chunk = this._forInOf( context, statement );       break;
		case ForOf:    chunk = this._forInOf( context, statement );       break;
		case If:       chunk = this._if( context, statement );            break;
		case Import:   chunk = this._import( context, statement );        break;
		case Let:      chunk = this._declare( context, statement );       break;
		case Return:   chunk = this._return( context, statement );        break;
		case Sep:      chunk = this._sep( context );                      break;
		case Switch:   chunk = this._switch( context, statement );        break;
		case Throw:    chunk = this._throw( context, statement );         break;
		case Try:      chunk = this._try( context, statement );           break;
		case VarDec:   chunk = this._varDec( context, statement, prev );  break;
		case While:    chunk = this._while( context, statement );         break;
		default:       chunk = this._expression( context, statement );    break;
	}

	if( chunk === false ) return false;

	switch( statement.ti2ctype )
	{
		case And:
		case Assign:
		case AstString:
		case AstBoolean:
		case Await:
		case BitwiseAnd:
		case BitwiseAndAssign:
		case BitwiseOr:
		case BitwiseOrAssign:
		case BitwiseXor:
		case BitwiseXorAssign:
		case Break:
		case Call:
		case Const:
		case Continue:
		case ConditionalDot:
		case Comma:
		case Delete:
		case Divide:
		case DivideAssign:
		case Dot:
		case GreaterOrEqual:
		case GreaterThan:
		case Import:
		case LessOrEqual:
		case LessThan:
		case Let:
		case Member:
		case Minus:
		case MinusAssign:
		case Multiply:
		case MultiplyAssign:
		case Negate:
		case New:
		case Not:
		case AstNumber:
		case Plus:
		case PlusAssign:
		case PostDecrement:
		case PostIncrement:
		case PreDecrement:
		case PreIncrement:
		case Regex:
		case Remainder:
		case RemainderAssign:
		case Return:
		case Spread:
		case Throw:
		case Var:
		case Yield:
			return(
				chunk.ti2ctype === SMapList
				? chunk.append( ';' )
				: SMapList.Elements( chunk, ';' )
			);

		case Block:
		case Check:
		case Comment:
		case DoWhile:
		case For:
		case ForIn:
		case ForOf:
		case Func:
		case If:
		case Sep:
		case Switch:
		case Try:
		case While:
			return chunk;

		case VarDec:
		{
			const vs = next?.ti2ctype === VarDec ? ',' : ';';
			return chunk.append( vs, '\n' );
		}

		default: throw new Error( statement.__TI2C_NAME__ );
	}
};

/*
| Formats a string literal.
*/
def.proto._string =
	function( context, expr )
{
	return(
		this._node(
			expr.token,
			'\'' + expr.string.replaceAll( '\'', '\\\'' ) + '\'',
		)
	);
};

/*
| Formats a switch statement
*/
def.proto._switch =
	function( context, _switch )
{
	const caseContext = context.Inc;
	const chunk =
	[
		'switch( ',
		this._expression( context.Inline, _switch.statement ),
		' )', context.sep, '{',
	];

	const cases = _switch.cases;
	for( let c of cases )
	{
		const values = c.values;
		for( let v of values )
		{
			chunk.push(
				caseContext.sep, 'case ',
				this._expression( caseContext.Inline, v ), ':',
			);
		}
		chunk.push( this._block( caseContext.Inc, c.block, true ) );
	}

	if( _switch.defaultCase )
	{
		chunk.push(
			caseContext.sep, 'default :',
			this._block( caseContext.Inc, _switch.defaultCase, true ),
		);
	}

	chunk.push( context.sep, '}' );

	return SMapList.Array( chunk );
};

/*
| Formats a throw statement.
*/
def.proto._throw =
	function( context, statement )
{
/**/if( CHECK && statement.ti2ctype !== Throw ) throw new Error( );

	const ci = context.Inc;
	return(
		SMapList.Elements(
			'throw (', ci.sep,
			this._expression( context.Inc, statement.expr ),
			ci.sep, ')',
		)
	);
};

/*
| Formats a try statement.
*/
def.proto._try =
	function( context, expr )
{
	const ci = context.Inc;
	return(
		SMapList.Elements(
			'try',
			context.sep, this._statement( ci, expr.tryStatement ),
			ci.sep, 'catch( ', expr.exceptionVar.name, ' )',
			context.sep, this._statement( ci, expr.catchStatement ),
		)
	);
};


/*
| Formats a typeof expression.
*/
def.proto._typeof =
	function( context, expr )
{
/**/if( CHECK && expr.ti2ctype !== Typeof ) throw new Error( );

	return(
		SMapList.Elements(
			'typeof(', context.sep,
			this._expression( context.Inc, expr.expr, Typeof ),
			context.sep, ')',
		)
	);
};

/*
| Formats an undefined.
*/
def.proto._undefined =
	function( context, expr )
{
	return this._node( expr.token, 'undefined' );
};

/*
| Formats a variable use.
*/
def.proto._var =
	function( context, expr )
{
	return this._node( expr.token, expr.name );
};

/*
| Formats a variable declaration.
*/
def.proto._varDec =
	function( context, varDec, prev )
{
	// true when this defines a function
	let isFunc = false;
	const chunk = [ ];

	if( varDec.assign )
	{
		if( varDec.assign.ti2ctype === Func ) isFunc = true;
		else if(
			varDec.assign.ti2ctype === Assign
			&& varDec.assign.right.ti2ctype === Func
		)
		{
			// TODO allow arbitrary amount of assignments
			isFunc = true;
		}
	}

	if( !isFunc )
	{
		if( !prev || prev.ti2ctype !== VarDec )
		{
			if( !context.inline ) chunk.push( context.sep, 'var', '\n' );
			else chunk.push( 'var ' );
		}

		context = context.Inc; // TODO?
		chunk.push( context.sep );
		chunk.push( varDec.name );
	}
	else
	{
		// functions are not combined in VarDecs
		chunk.push( context.sep, 'var ', varDec.name );
	}

	if( varDec.assign )
	{
		chunk.push( ' =', context.sep );
		if( varDec.assign.ti2ctype !== Assign ) context = context.Inc;
		chunk.push( this._expression( context, varDec.assign ) );
	}

	return SMapList.Array( chunk );
};

/*
| Formats a while loop.
*/
def.proto._while =
	function( context, expr )
{
	const ci = context.Inc;
	return(
		SMapList.Elements(
			'while(',
			ci.sep,
			this._expression( ci.Inline, expr.condition ),
			context.sep, ')',
			context.sep, this._statement( context, expr.body ),
		)
	);
};

/*
| Formats a yield operator.
*/
def.proto._yield =
	function( context, expr )
{
	const ci = context.Inc;
	if( context.inline )
	{
		return(
			SMapList.Elements(
				'yield ',
				this._expression( ci, expr.expr, expr.ti2ctype ),
			)
		);
	}
	else
	{
		return(
			SMapList.Elements(
				'yield(',
				ci.sep, this._expression( ci, expr.expr, expr.ti2ctype ),
				context.sep, ')',
			)
		);
	}
};

/*
| If generating with sourcemaps, creates a SourceMap node
| Otherwise name is returned as string.
*/
def.proto._node =
	function( token, name )
{
	if( this.mapSourceName && token )
	{
		return(
			SMapNode.create(
				'name', name,
				'token', token,
			)
		);
	}
	else
	{
		return name;
	}
};
