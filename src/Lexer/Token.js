/*
| A lexer token.
*/
def.attributes =
{
	// position of the token in the line
	column: { type: 'integer' },

	// line of the token
	line: { type: 'string' },

	// line number of the token
	lineNr: { type: 'integer' },

	// the original token
	name: { type: 'string' },

	// the token type
	type: { type: 'string' },

	// the token value
	value: { type: [ 'undefined', 'number', 'string' ] }
};

/*
| Shows an error at the token.
*/
def.proto.error =
	function( message )
{
	console.error( 'Error in line ' + this.lineNr + ':' );
	console.error( this.line );
	let sp = '';
	const column = this.column;
	for( let c = 0; c < column; c++ ) sp += ' ';
	console.error( sp + '^' );
	console.error( sp + message );
	console.error( );
	throw new Error( );
};

/*
| Shortcut to create a token of type t with value v.
|
| ~line:   line
| ~lineNr: line number
| ~column: position in line
| ~type:   type
*/
def.static.LLCT =
	function( line, lineNr, column, type )
{
	return(
		Self.create(
			'line', line,
			'lineNr', lineNr,
			'column', column,
			'name', type,
			'type', type,
			'value', undefined,
		)
	);
};

/*
| Shortcut to create a token of type t with value v.
|
| ~line:   line
| ~lineNr: line number
| ~column: position in line
| ~type:   type
| ~value:  value maybe undefined
*/
def.static.LLCTV =
	function( line, lineNr, column, type, value )
{
	return(
		Self.create(
			'line', line,
			'lineNr', lineNr,
			'name', value,
			'column', column,
			'type', type,
			'value', value,
		)
	);
};

/*
| Shortcut to create a token of type t with value v.
|
| ~line:   line
| ~lineNr: line number
| ~column: position in line
| ~type:   type
| ~value:  value maybe undefined
| ~name:   name, original token
*/
def.static.LLCTVN =
	function( line, lineNr, column, type, value, name )
{
	return(
		Self.create(
			'line', line,
			'lineNr', lineNr,
			'name', name,
			'column', column,
			'type', type,
			'value', value,
		)
	);
};

/*
| Allowed token types.
*/
def.staticLazy._allowedTokens = ( ) =>
	new Set( [
		'=',
		'.',
		',',
		'+',
		'-',
		'*',
		'/',
		'<',
		'>',
		'?',
		'^',
		'~',
		':',
		';',
		'!',
		'[',
		']',
		'(',
		')',
		'{',
		'}',
		'&',
		'|',
		'%',
		'++',
		'--',
		'+=',
		'-=',
		'*=',
		'/=',
		'^=',
		'%=',
		'|=',
		'??',
		'&=',
		'<=',
		'>=',
		'=>',
		'||',
		'&&',
		'?.',
		'<<',
		'>>',
		'>>>',
		'===',
		'!==',
		'...',
		'async',
		'await',
		'break',
		'case',
		'catch',
		'const',
		// FIXME continue
		'default',
		'delete',
		'do',
		'else',
		'export',
		'false',
		'finally',
		'for',
		'function',
		'identifier',
		'if',
		'import',
		'in',
		'instanceof',
		'let',
		'new',
		'null',
		'number',
		'of',
		'regex',
		'regex-flags',
		'return',
		'string',
		'switch',
		'throw',
		'true',
		'try',
		'typeof',
		'undefined',
		'yield',
		'while',
	] );

/*
| Allow keywords.
*/
def.staticLazy.keywords = ( ) =>
	new Set( [
		'async',
		'await',
		'break',
		'case',
		'catch',
		'const',
		// FIXME continue
		'delete',
		'default',
		'do',
		'else',
		'export',
		'false',
		'finally',
		'for',
		'function',
		'if',
		'import',
		'in',
		'instanceof',
		'let',
		'new',
		'null',
		'of',
		'return',
		'switch',
		'throw',
		'true',
		'try',
		'typeof',
		'undefined',
		'while',
		'yield',
	] );

/*
| Forces a keyword token to a identifier.
*/
def.lazy.forceIdentifier =
	function( )
{
	if( !Self.keywords.has( this.type ) ) return this;
	return this.create( 'type', 'identifier', 'value', this.type );
};

/*
| Exta checking
*/
def.proto._check =
	function( )
{
/**/if( CHECK )
/**/{
/**/	if( !Self._allowedTokens.has( this.type ) ) throw new Error( );
/**/}
};
