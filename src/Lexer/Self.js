/*
| The javascript lexer turns a string into a series of tokens.
*/
def.abstract = true;

import { Self as ListToken } from '{list@ti2c:Lexer/Token}';
import { Self as Token     } from '{ti2c:Lexer/Token}';

/*
| Tokenizes a javascript string.
|
| ~str: string to tokenize.
|
| ~return: a list of tokens.
*/
def.static.tokenize =
	function( str )
{
	if( typeof( str ) !== 'string' ) throw new Error( );

	const tokens = [ ];
	let line = Self._getLine( str, 0 );
	let lineNr = 1;
	let column = 0;

	for( let a = 0, alen = str.length; a < alen; a++, column++ )
	{
		const ch = str[ a ];

		if( ch === '\n' )
		{
			line = Self._getLine( str, a + 1 );
			lineNr++;
			column = -1;
			continue;
		}

		// skips whitespaces
		if( ch === ' ' ) continue;

		// tabs
		if( ch === '\t' ) { column += 7; continue; }

		// a keyword or an identifier
		if( ch.match( /[a-zA-Z_$]/ ) )
		{
			const tpos = column;
			let value = ch;
			while( a + 1 < alen && str[ a + 1 ].match( /[a-zA-Z0-9_$]/ ) )
			{
				value += str[ ++a ];
				column++;
			}

			if( Token.keywords.has( value ) )
			{
				tokens.push( Token.LLCT( line, lineNr, tpos, value ) );
			}
			else
			{
				tokens.push( Token.LLCTV( line, lineNr, tpos, 'identifier', value ) );
			}
			continue;
		}

		// a number
		if( ch.match(/[0-9]/ ) )
		{
			let value = ch;
			while( a + 1 < alen )
			{
				if( str[ a + 1 ] === 'e' && str[ a + 2 ] === '-' )
				{
					// for example 1e-3, FIXME might be an issue for 0x2e-0x33
					value += str[ ++a ];
					value += str[ ++a ];
					column+=2;
				}
				else if( str[ a + 1 ].match( /[0-9x.A-Fa-f]/ ) )
				{
					value += str[ ++a ];
					column++;
				}
				else
				{
					break;
				}
			}

			tokens.push( Token.LLCTV( line, lineNr, column, 'number', value ) );
			continue;
		}

		switch( ch )
		{
			case ':':
			case ';':
			case ',':
			case '(':
			case ')':
			case '[':
			case ']':
			case '{':
			case '}':
			case '~':
			{
				tokens.push( Token.LLCT( line, lineNr, column, ch ) );
				continue;
			}

			case '"':
			{
				// a string literal
				let value = '';
				const tpos = column;
				a++; column++;
				if( a >= alen ) throw new Error( '" missing' );
				while( str[ a ] !== '"' )
				{
					const ch2 = str[ a ];
					if( ch2 === '\\' && a < alen && str[ a + 1 ] === '"' )
					{
						value += '"';
						a += 2; column += 2;
					}
					else if( ch2 === '\\' && a < alen && str[ a + 1 ] === '\\' )
					{
						value += '\\';
						a += 2; column += 2;
					}
					else
					{
						value += ch2;
						a++; column++;
					}
					if( a >= alen ) throw new Error( '" missing' );
				}
				tokens.push(
					Token.LLCTVN(
						line, lineNr, tpos, 'string', value, '"' + value + '"'
					)
				);
				continue;
			}

			case '\'':
			{
				// a string literal
				const tpos = column;
				let value = '';
				a++; column++;
				if( a >= alen ) throw new Error( '\' missing' );
				while( str[ a ] !== '\'' )
				{
					const ch2 = str[ a ];
					if( ch2 === '\\' && a < alen && str[ a + 1 ] === '\'' )
					{
						value += '\'';
						a += 2; column += 2;
					}
					else if( ch2 === '\\' && a < alen && str[ a + 1 ] === '\\' )
					{
						value += '\\';
						a += 2; column += 2;
					}
					else
					{
						value += ch2;
						a++; column++;
					}

					if( a >= alen ) throw new Error( '\' missing' );
				}
				tokens.push(
					Token.LLCTVN( line, lineNr, tpos, 'string', value, '\'' + value + '\'' )
				);
				continue;
			}

			case '<':
			{
				if( a + 1 < alen && str[ a + 1 ] === '<' )
				{
					tokens.push( Token.LLCT( line, lineNr, column, '<<' ) );
					a++; column++;
				}
				else if( a + 1 < alen && str[ a + 1 ] === '=' )
				{
					tokens.push( Token.LLCT( line, lineNr, column, '<=' ) );
					a++; column++;
				}
				else
				{
					tokens.push( Token.LLCT( line, lineNr, column, '<' ) );
				}
				continue;
			}

			case '>':
			{
				if( a + 2 < alen && str[ a + 1 ] === '>' && str[ a + 2 ] === '>' )
				{
					tokens.push( Token.LLCT( line, lineNr, column, '>>>' ) );
					a += 2; column += 2;
				}
				else if( a + 1 < alen && str[ a + 1 ] === '>' )
				{
					tokens.push( Token.LLCT( line, lineNr, column, '>>' ) );
					a++; column++;
				}
				else if( a + 1 < alen && str[ a + 1 ] === '=' )
				{
					tokens.push( Token.LLCT( line, lineNr, column, '>=' ) );
					a++; column++;
				}
				else
				{
					tokens.push( Token.LLCT( line, lineNr, column, '>' ) );
				}
				continue;
			}

			case '?':
			{
				if( a + 1 < alen && str[ a + 1 ] === '.' )
				{
					tokens.push( Token.LLCT( line, lineNr, column, '?.' ) );
					a++; column++;
				}
				else if( a + 1 < alen && str[ a + 1 ] === '?' )
				{
					tokens.push( Token.LLCT( line, lineNr, column, '??' ) );
					a++; column++;
				}
				else
				{
					tokens.push( Token.LLCT( line, lineNr, column, '?' ) );
				}
				continue;
			}

			case '.':
			{
				if( a + 2 < alen && str[ a + 1 ] === '.' && str[ a + 2 ] === '.' )
				{
					tokens.push( Token.LLCT( line, lineNr, column, '...' ) );
					a += 2; column += 2;
				}
				else
				{
					tokens.push( Token.LLCT( line, lineNr, column, ch ) );
				}
				continue;
			}

			case '=':
			{
				if( a + 1 < alen && str[ a + 1 ] === '=' )
				{
					if( a + 2 < alen && str[ a + 2 ] === '=' )
					{
						tokens.push( Token.LLCT( line, lineNr, column, '===' ) );
						a += 2; column += 2;
						continue;
					}
					else throw new Error( 'Use === instead of ==');
				}
				else if( a + 1 < alen && str[ a + 1 ] === '>' )
				{
					tokens.push( Token.LLCT( line, lineNr, column, '=>' ) );
					a++; column++;
				}
				else
				{
					tokens.push( Token.LLCT( line, lineNr, column, '=' ) );
				}
				continue;
			}

			case '!':
			{
				if( a + 1 < alen && str[ a + 1 ] === '=' )
				{
					if( a + 2 < alen && str[ a + 2 ] === '=' )
					{
						tokens.push( Token.LLCT( line, lineNr, column, '!==' ) );
						a += 2; column += 2;
						continue;
					}
					else throw new Error( 'Use !== instead of !=');
				}
				else
				{
					tokens.push( Token.LLCT( line, lineNr, column, '!' ) );
				}
				continue;
			}

			case '+':
			{
				if( a + 1 < alen && str[ a + 1 ] === '+' )
				{
					tokens.push( Token.LLCT( line, lineNr, column, '++' ) );
					a++; column++;
				}
				else if( a + 1 < alen && str[ a + 1 ] === '=' )
				{
					tokens.push( Token.LLCT( line, lineNr, column, '+=' ) );
					a++; column++;
				}
				else
				{
					tokens.push( Token.LLCT( line, lineNr, column, '+' ) );
				}
				continue;
			}

			case '-':
			{
				if( a + 1 < alen && str[ a + 1 ] === '-' )
				{
					tokens.push( Token.LLCT( line, lineNr, column, '--' ) );
					a++; column++;
				}
				else if( a + 1 < alen && str[ a + 1 ] === '=' )
				{
					tokens.push( Token.LLCT( line, lineNr, column, '-=' ) );
					a++; column++;
				}
				else
				{
					tokens.push( Token.LLCT( line, lineNr, column, '-' ) );
				}
				continue;
			}

			case '*':
			{
				if( a + 1 < alen && str[ a + 1 ] === '=' )
				{
					tokens.push( Token.LLCT( line, lineNr, column, '*=' ) );
					a++; column++;
				}
				else
				{
					tokens.push( Token.LLCT( line, lineNr, column, '*' ) );
				}
				continue;
			}

			case '^':
			{
				if( a + 1 < alen && str[ a + 1 ] === '=' )
				{
					tokens.push( Token.LLCT( line, lineNr, column, '^=' ) );
					a++; column++;
				}
				else
				{
					tokens.push( Token.LLCT( line, lineNr, column, '^' ) );
				}
				continue;
			}

			case '%':
			{
				if( a + 1 < alen && str[ a + 1 ] === '=' )
				{
					tokens.push( Token.LLCT( line, lineNr, column, '%=' ) );
					a++; column++;
				}
				else
				{
					tokens.push( Token.LLCT( line, lineNr, column, '%' ) );
				}
				continue;
			}

			case '/':
			{
				if( a + 1 < alen && str[ a + 1 ] === '/' )
				{
					// double slash comment
					a++;
					while( a + 1 < alen && str[ a ] !== '\n' ){ a++; }
					lineNr++;
					line = Self._getLine( str, a + 1 );
					column = -1;
				}
				else if( a + 1 < alen && str[ a + 1 ] === '*' )
				{
					// c-style comment
					a++; column++;
					while( a + 1 < alen && ( str[ a ] !== '*' || str[ a + 1 ] !== '/' ) )
					{
						column++;
						if( str[ a ] === '\n' )
						{
							line = Self._getLine( str, a + 1 );
							lineNr++;
							column = 0;
						}
						a++;
					}
					a++; column++;
				}
				else if(
					tokens.length > 0
					&& Self._beforeRegex.has( tokens[ tokens.length - 1 ].type )
				)
				{
					// it is a regular expression
					let regex = '';
					a++; column++;
					let bracketLevel = 0;
					while( a + 1 < alen && ( str[ a ] !== '/' || bracketLevel > 0 ) )
					{
						// TODO handle backslash slash
						const ch2 = str[ a ];
						regex += ch2;
						switch( ch2 )
						{
							case '[': bracketLevel++; break;
							case ']': bracketLevel--; break;
						}
						column++; a++;
					}
					tokens.push( Token.LLCTV( line, lineNr, column, 'regex', regex ) );
					let flags = '';
					while( a + 1 < alen && Self._regexFlags.has( str[ a + 1 ] ) )
					{
						flags += str[ a + 1 ];
						column++; a++;
					}
					if( flags !== '' )
					{
						tokens.push( Token.LLCTV( line, lineNr, column, 'regex-flags', flags ) );
					}
				}
				else if( a + 1 < alen && str[ a + 1 ] === '=' )
				{
					tokens.push( Token.LLCT( line, lineNr, column, '/=' ) );
					a++; column++;
				}
				else
				{
					tokens.push( Token.LLCT( line, lineNr, column, '/' ) );
				}
				continue;
			}

			case '|':
			{
				if( a + 1 < alen && str[ a + 1 ] === '|' )
				{
					tokens.push( Token.LLCT( line, lineNr, column, '||' ) );
					a++; column++;
				}
				else if( a + 1 < alen && str[ a + 1 ] === '=' )
				{
					tokens.push( Token.LLCT( line, lineNr, column, '|=' ) );
					a++; column++;
				}
				else
				{
					tokens.push( Token.LLCT( line, lineNr, column, '|' ) );
				}
				continue;
			}

			case '&':
			{
				if( a + 1 < alen && str[ a + 1 ] === '&' )
				{
					tokens.push( Token.LLCT( line, lineNr, column, '&&' ) );
					a++; column++;
				}
				else if( a + 1 < alen && str[ a + 1 ] === '=' )
				{
					tokens.push( Token.LLCT( line, lineNr, column, '&=' ) );
					a++; column++;
				}
				else
				{
					tokens.push( Token.LLCT( line, lineNr, column, '&' ) );
				}
				continue;
			}

			default:
			{
				console.error( 'Error in line ' + lineNr + ':' );
				console.error( line );
				let sp = '';
				for( let b = 1; b < column; b++ ) sp += ' ';
				console.error( sp + '^' );
				console.error( sp + 'lexer error' );
				console.error( );
				throw new Error( );
			}
		}
	}

	return ListToken.Array( tokens );
};

/*
| Tokens after which a '/' is supposed to be a regelar expression.
*/
def.staticLazy._beforeRegex = ( ) =>
	new Set( [ '=', ',', '(', '=>', 'return' ] );

/*
| Regular expression flags
*/
def.staticLazy._regexFlags = ( ) =>
	new Set( [ 'd', 'g', 'i', 'm', 's', 'u', 'y' ] );

/*
| Returns the line starting at p.
*/
def.static._getLine =
	function( str, p )
{
	let slen = str.length;
	let p2 = p;
	while( p2 < slen && str[ p2 ] !== '\n' ) p2++;
	return str.substring( p, p2 );
};
