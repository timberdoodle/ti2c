/*
| Runs the codeob generator for the ti2c package itself.
*/
def.singleton = true;

import * as file      from '../core/file';
import vm             from 'node:vm';
import { argv, exit } from 'node:process';

import { Self as EntryClass     } from '{ti2c:Package/Entry/Class/Self}';
import { Self as EntryGroup     } from '{ti2c:Package/Entry/Collection/Group}';
import { Self as EntryList      } from '{ti2c:Package/Entry/Collection/List}';
import { Self as EntrySet       } from '{ti2c:Package/Entry/Collection/Set}';
import { Self as EntryTwig      } from '{ti2c:Package/Entry/Collection/Twig}';
import { Self as Formatter      } from '{Format/Self}';
import { Self as Package        } from '{Package/Self}';
import { Self as Path           } from '{Path/Self}';
import { Self as TypeTi2cAny    } from '{Type/Ti2c/Any}';
import { Self as TypeTi2cClass  } from '{Type/Ti2c/Class}';
import { Self as TypeTi2cGroup  } from '{Type/Ti2c/Collection/Group}';
import { Self as TypeTi2cList   } from '{Type/Ti2c/Collection/List}';
import { Self as TypeTi2cSet    } from '{Type/Ti2c/Collection/Set}';
import { Self as TypeTi2cTwig   } from '{Type/Ti2c/Collection/Twig}';

/*
| The build target ti2c package.
*/
let _pkgTrg;

/*
| Entries to target, keys are name, values are entries
*/
let _entries = new Map( );

/*
| The listing as set.
*/
//let _listingSet;

/*
| Path to target.
*/
let _pathTrg;
let _codeGenDir;

/*
| PackageManager simulator in ouroboros mode.
*/
def.proto.getPackage =
	function( name )
{
	if( name !== 'ti2c' ) throw new Error( );
	return _pkgTrg;
};

/*
| PackageManager simulator in ouroboros mode.
*/
def.proto.getEntry =
	function( type )
{
	return _entries.get( type );
};

/*
| Ouroboros linker.
| Simulates ti2c core in ouroboros mode.
*/
async function obLinker( entryCore, specifier, referencingModule )
{
	if( specifier === '((ti2c::loop))' )
	{
		// provides the ti2c loop back interface
		const exportNames = [ 'def', 'pass', 'Self', 'ti2c' ];
		const loop =
			new vm.SyntheticModule(
				exportNames,
				( ) =>
				{
					loop.setExport( 'def', entryCore.def );
					loop.setExport( 'pass', { } );
					loop.setExport( 'Self', { } );
					loop.setExport( 'ti2c', { } );
				},
				{
					context: referencingModule.context
				}
			);
		return loop;
	}

	// otherwise just give something dummy
	const exportNames = [ 'argv', 'default', 'exit', 'listing', 'Self' ];
	const dummy =
		new vm.SyntheticModule(
			exportNames,
			( ) =>
			{
				for( let key of exportNames )
				{
					dummy.setExport( key, { } );
				}
			},
			{
				identifier: specifier,
				context: referencingModule.context,
			}
		);
	return dummy;
}

/*
| Runs the oroboros builder.
*/
def.static.run =
	async function( )
{
	if( argv.length !== 3 )
	{
		console.error( 'Usage: ' + argv[ 0 ] + ' ' + argv[ 1 ] + ' [ti2c root dir]' );
		exit( -1 );
	}

	const dirTrg = argv[ 2 ];
	if( dirTrg[ dirTrg.length - 1 ] !== '/' )
	{
		console.error( argv[ 2 ] + ' does not end with a \'/\'' );
		exit( -1 );
	}
	_pathTrg = Path.FromString( dirTrg );
	_codeGenDir = _pathTrg.d( 'codeob' );

	_pkgTrg =
		Package.create(
			'exports', undefined,
			'name', 'ti2c',
			'rootDir', _pathTrg,
			'srcDir', _pathTrg.d( 'src' ),
		);

	const listing = ( await import( dirTrg + 'src/core/listing.js' ) ).listing;
	// tests if the listing is alphabetically
	for( let a = 0, alen = listing.length - 1; a < alen; a++ )
	{
		if( listing[ a ].toLowerCase( ) >= listing[ a + 1 ].toLowerCase( ) )
		{
			throw new Error(
				'listing not in order: '
				+ listing[ a ] + ' >= ' + listing[ a + 1 ]
			);
		}
	}

	//_listingSet = new Set( listing );

	for( let name of listing )
	{
		const type = TypeTi2cAny.FromString( name, Self.singleton, 'ti2c' );
		await load( type );
	}

	if( listing.length !== _entries.size ) throw new Error( );

	for( let name of listing )
	{
		const type = TypeTi2cAny.FromString( name, Self.singleton, 'ti2c' );
		const entry = _entries.get( type );
		console.log( '  generating ' + entry.aPathCodeGen.asString );
		const ast = await entry.ast;
		const output = Formatter.format( ast );
		await file.write( entry.aPathCodeGen.asString, output );
	}

	console.log( '  done' );
};

/*
| Loads an entry.
*/
async function load( type )
{
	console.log( '  loading ' + type.asString );
	switch( type.ti2ctype )
	{
		case TypeTi2cClass: return await loadClass( type );
		case TypeTi2cGroup: return await loadGroup( type );
		case TypeTi2cList:  return await loadList( type );
		case TypeTi2cSet:   return await loadSet( type );
		case TypeTi2cTwig:  return await loadTwig( type );
		default: throw new Error( );
	}
}

/*
| Loads an entry class.
*/
async function loadClass( type )
{
	const pathIn = _pathTrg.d( 'src' ).addChunkF( type.entryName + '.js' );
	const code = await file.read( pathIn.asString );
	const input = 'import { def, pass, Self, ti2c } from "((ti2c::loop))";' + code;
	const stm =
		new vm.SourceTextModule(
			input,
			{
				identifier: pathIn.asString,
				context: vm.createContext( { CHECK: true } ),
			}
		);
	const entryCore =
	{
		def:
		{
			lazy: { },
			lazyFunc: { },
			proto: { },
			static: { },
			staticLazy: { },
			staticLazyFunc: { },
		},
	};

	await stm.link( obLinker.bind( undefined, entryCore ) );
	await stm.evaluate( );

	const nameExtend = entryCore.def.extend;
	let entryExtend;
	if( nameExtend )
	{
		entryExtend = _entries.get( nameExtend );
		if( !entryExtend )
		{
			// generates the extended entry first
			// they need to be in the listing tough

			// FIXME clean up ti2c: resolving in init phase
			// if( !_listingSet.has( 'ti2c:' + nameExtend ) ) throw new Error( );

			const typeExtend = TypeTi2cAny.FromString( nameExtend, Self.singleton, 'ti2c' );
			entryExtend = await load( typeExtend );
		}
	}

	const entry =
		await EntryClass.Define(
			entryCore,
			type,
			entryExtend,
			_codeGenDir,
			Self.singleton,
		);
	_entries.set( type, entry );
	return entry;
}

/*
| Loads an entry group.
*/
async function loadGroup( type )
{
	const entry =
		await EntryGroup.Define(
			{ },
			type,
			_codeGenDir,
			Self.singleton,
		);
	_entries.set( type, entry );
	return entry;
}

/*
| Loads an entry list.
*/
async function loadList( type )
{
	const entry =
		await EntryList.Define(
			{ },
			type,
			_codeGenDir,
			Self.singleton,
		);
	_entries.set( type, entry );
	return entry;
}

/*
| Loads an entry set.
*/
async function loadSet( type )
{
	const entry =
		await EntrySet.Define(
			{ },
			type,
			_codeGenDir,
			Self.singleton,
		);
	_entries.set( type, entry );
	return entry;
}

/*
| Loads an entry twig.
*/
async function loadTwig( type )
{
	const entry =
		await EntryTwig.Define(
			{ },
			type,
			_codeGenDir,
			Self.singleton,
		);
	_entries.set( type, entry );
	return entry;
}
