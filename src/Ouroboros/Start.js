/*
| Runs the ti2c-code generator for the ti2c package itself.
*/
Error.stackTraceLimit = Infinity;
global.CHECK = true;

await import( '../root.js' );
const pkg =
	await ti2c.register(
		'name',    'ti2c',
		'meta',    import.meta,
		'source',  'src/',
		'relPath', 'Ouroboros/Start',
		'codegen', 'codegen/'
	);
const Root = await pkg.import( 'Ouroboros/Root' );
await Root.run( );
