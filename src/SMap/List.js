/*
| A source map list.
*/
def.extend = 'list@(string,ti2c:SMap/List,ti2c:SMap/Node)';

// FIXME remove
def.json = true;
def.fromJsonArgs = [ ];

import { Self as SMapNode } from '{ti2c:SMap/Node}';

/*
| The source map as string.
*/
def.lazy.string =
	function( )
{
	let s = '';
	for( let e of this )
	{
		switch( e.ti2ctype )
		{
			case Self:
			case SMapNode:
				s += e.string;
				break;

			default:
/**/			if( CHECK && typeof( e ) !== 'string' ) throw new Error( );
				s += e;
				break;
		}
	}

	return s;
};

/*
| Turns the node list into code with a source map
|
| ~result: a mutable(!) protean where the map is created
| ~sourceFilename: name to associate these tokens with.
*/
def.proto.codeWithSourceMap =
	function( result, sourceFilename )
{
	for( let e of this )
	{
		switch( e.ti2ctype )
		{
			case Self:
				e.codeWithSourceMap( result, sourceFilename );
				break;

			case SMapNode:
				result.addNode( e, sourceFilename );
				break;

			default:
				if( CHECK && typeof( e ) !== 'string' ) throw new Error( );
				result.addString( e );
		}
	}
};

