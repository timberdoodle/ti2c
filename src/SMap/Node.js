/*
| A source map node.
*/
def.attributes =
{
	// the original token
	token: { type: 'Lexer/Token' },

	// generated name
	name: { type: [ 'undefined', 'string' ] },
};

// FIXME remove
def.json = true;

/*
| The generated string
*/
def.lazy.string =
	function( )
{
	return this.name || this.token.name;
};
