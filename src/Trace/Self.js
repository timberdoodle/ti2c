/*
| A trace.
*/
def.attributes =
{
	_list: { type: [ 'list@<Trace/Step/Types' ] },
};

def.json = true;
def.fromJsonArgs = [ 'plan' ];

import { Self as ListStep      } from '{list@<ti2c:Trace/Step/Types}';
import { Self as PlanPiece     } from '{ti2c:Plan/Piece}';
import { Self as StepAt        } from '{ti2c:Trace/Step/At}';
import { Self as StepKey       } from '{ti2c:Trace/Step/Key}';
import { Self as StepPlain     } from '{ti2c:Trace/Step/Plain}';

/*
| Adds a step.
*/
def.proto.add =
	function( name, arg )
{
	const list = this._list;
	const last = list.last;
	const lplan = last.plan;

	let sub = lplan.subs.get( name );
/**/if( CHECK && !sub ) throw new Error( );

	if( typeof( sub  ) === 'function' ) sub = sub( );

/**/if( CHECK && !sub ) throw new Error( );

	let step;
	if( typeof( arg ) === 'number' )
	{
/**/	if( CHECK && !sub.at ) throw new Error( );
		step = StepAt.create( 'at', arg, 'name', name, 'plan', sub );
	}
	else if( typeof( arg ) === 'string' )
	{
/**/	if( CHECK && !sub.key ) throw new Error( );
		step = StepKey.create( 'key', arg, 'name', name, 'plan', sub );
	}
	else
	{
/**/	if( CHECK && ( sub.at || sub.key ) ) throw new Error( );
		step = StepPlain.create( 'name', name, 'plan', sub );
	}

	const t = this.create( '_list', list.append( step ) );
	ti2c.aheadValue( t, 'back', this );
	return t;
};

/*
| Adds a step object.
*/
def.proto.addStep =
	function( step )
{
	const last = this.last;
	const lplan = last.plan;
	const name = step.name;

	let sub = lplan.subs.get( name );

/**/if( CHECK && !sub ) throw new Error( );

	if( typeof( sub  ) === 'function' ) sub = sub( );

/**/if( CHECK && !sub ) throw new Error( );

	let astep;
	let t;

	if( sub.at )
	{
		if( step.ti2ctype !== StepAt )
		{
			throw new Error( );
		}

		astep = StepAt.create( 'at', step.at, 'name', name, 'plan', sub );
	}
	else if( sub.key )
	{
		if( step.ti2ctype !== StepKey )
		{
			throw new Error( );
		}

		astep = StepKey.create( 'key', step.key, 'name', name, 'plan', sub );
	}
	else
	{
		if( step.ti2ctype !== StepPlain )
		{
			throw new Error( );
		}

		astep = StepPlain.create( 'name', name, 'plan', sub );
	}

	t = this.create( '_list', this._list.append( astep ) );
	ti2c.aheadValue( t, 'back', this );
	return t;
};

/*
| Turns the trace into a string (for debugging).
*/
def.lazy.asString =
	function( )
{
	const list = this._list;
	let str = 'trace: ';
	let first = true;
	for( let step of list )
	{
		if( !first )
		{
			str += '->';
		}
		else
		{
			first = false;
		}
		str += step.asString;
	}
	return str;
};

/*
| Returns a trace with a step back.
*/
def.lazy.back =
	function( )
{
	const list = this._list;
	const len = list.length;
	if( len <= 1 ) throw new Error( );
	return this.create( '_list', list.remove( len - 1 ) );
};

/*
| traces from the end backwards until the step with 'name' is found.
|
| ~name: step name to trace.
*/
def.lazyFunc.backward =
	function( name )
{
	let t = this;
	for( ;; )
	{
		if( t.last.name === name )
		{
			return t;
		}

		if( t.length === 1 )
		{
			return undefined;
		}

		t = t.back;
	}
};

/*
| Removes the one entry from the front of trace.
*/
def.lazy.chop =
	function( )
{
	const s1 = this.get( 1 );
	const p1 = s1.plan;

	let t;
	if( p1.at )
	{
		t = Self.root( p1, s1.at );
	}
	else if( p1.key )
	{
		t = Self.root( p1, s1.key );
	}
	else
	{
		t = Self.root( p1 );
	}

	for( let a = 2, alen = this.length; a < alen; a++ )
	{
		const step = this.get( a );
		t = t.addStep( step );
	}

	return t;
};

/*
| Returns the length this trace has in common with another
| as seen from the beginning.
*/
def.proto.commonLength =
	function( trace )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 1 ) throw new Error( );
/**/	if( trace.ti2ctype !== Self ) throw new Error( );
/**/}

	const tlist = this._list;
	const olist = trace._list;
	const tlen = this.length;
	const olen = trace.length;
	const clen = Math.min( tlen, olen );

	let a;
	for( a = 0; a < clen; a++ )
	{
		if( tlist.get( a ) !== olist.get( a ) )
		{
			break;
		}
	}
	return a;
};

/*
| First step.
*/
def.lazy.first =
	function( )
{
	return this._list.first;
};

/*
| traces from the start forwards until the step with 'name' is found.
|
| ~name: step name to trace.
*/
def.lazyFunc.forward =
	function( name )
{
	const list = this._list;
	for( let a = 0, len = list.length; a < len; a++ )
	{
		if( list.get( a ).name === name )
		{
			return this.head( a  + 1 );
		}
	}

	return undefined;
};

/*
| Custom json creator.
*/
def.static.FromJson =
	function( json, plan )
{
/**/if( CHECK && arguments.length !== 2 ) throw new Error( );

	if( json.$type !== 'ti2c:Trace/Self' ) throw new Error( );

	const jtrace = json.trace;

	if( !Array.isArray( jtrace ) ) throw new Error( );

	let rStep = jtrace[ 0 ];
	let trace;
	let p;

	if( rStep[ 0 ] === '#' || rStep[ 0 ] === '>' )
	{
		p = 2;
		trace = Self.root( plan, jtrace[ 1 ] );
	}
	else
	{
		p = 1;
		trace = Self.root( plan );
	}

	for( let len = jtrace.length; p < len; p++ )
	{
		const name = jtrace[ p ];
		const n = name.charAt( 0 );

		if( n !== '>' && n !== '#' )
		{
			trace = trace.add( name );
		}
		else
		{
			trace = trace.add( name.slice( 1 ), jtrace[ ++p ] );
		}
	}

	return trace;
};

/*
| Grafts a new leaf on a tree.
*/
def.proto.graft =
	function( tree, val )
{
	if( this.length === 1 )
	{
		return val;
	}

	const tback = this.back;
	let sub = tback.pick( tree );
	sub = this.last.graft( sub, val );

	return tback.graft( tree, sub );
};

/*
| Gets a step.
*/
def.proto.get =
	function( a )
{
	return this._list.get( a );
};

/*
| Returns true if this trace has 'trace' as parent.
*/
def.proto.hasTrace =
	function( trace )
{
	let t = this;
	for( ;; )
	{
		if( t === trace )
		{
			return true;
		}

		if( t.length === 1 )
		{
			return false;
		}

		t = t.back;
	}
};

/*
| Returns the front part of the trace
| up to len;
*/
def.lazyFunc.head =
	function( len )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 1 ) throw new Error( );
/**/	if( typeof( len ) !== 'number' ) throw new Error( );
/**/	if( len > this.length ) throw new Error( );
/**/}

	let t = this;
	for( let a = this.length; a > len ; a-- )
	{
		t = t.back;
	}
	return t;
};

/*
| Increments the last at step.
*/
def.lazyFunc.incLastAt =
	function( at )
{
	let last = this.last;
	if( last.at === undefined ) throw new Error( );
	last = last.create( 'at', last.at + at );
	return this.back.addStep( last );
};

/*
| True if the traces are for the same list
| and this at is before
*/
def.lazyFunc.lastAtIsBefore =
	function( other )
{
	const lastThis = this.last;
	// this needs to end with an at
	if( lastThis.at === undefined ) throw new Error( );

	if( !other ) return false;
	if( this.back !== other.back ) return false;

	return lastThis.at < other.last.at;
};

/*
| Custom jsonfy converter.
*/
def.lazyFunc.jsonfy =
	function( space )
{
	let r = '{"$type":"ti2c:Trace/Self","trace":[';
	const list = this._list;
	for( let a = 0, alen = list.length; a < alen; a++ )
	{
		if( a > 0 ) r+= ',';
		r += list.get( a ).jsonfyStep;
	}
	return r + ']}';
};

/*
| Last step.
*/
def.lazy.last =
	function( )
{
	return this._list.last;
};

/*
| Length of the trace.
*/
def.lazy.length =
	function( )
{
	return this._list.length;
};

/*
| Picks the traced leaf.
*/
def.proto.pick =
	function( tree )
{
	const list = this._list;

	for( let a = 1, alen = list.length; a < alen; a++ )
	{
		tree = list.get( a ).pick( tree );
	}

	return tree;
};

/*
| Picks the traced leaf.
|
| ~tree:  tree to pick
| ~start: start of subtrace to pick from
| ~end:   end of subtrace to pick from (if undefined until end of trace)
*/
def.proto.pickSlice =
	function( tree, start, end )
{
/**/if( CHECK && ( arguments.length < 2 || arguments.length > 3 ) ) throw new Error( );

	const list = this._list;
	const len = list.length;

	if( end === undefined )
	{
		end = len;
	}

/**/if( CHECK )
/**/{
/**/	if( start >= end ) throw new Error( );
/**/	if( start > len ) throw new Error( );
/**/	if( end > len ) throw new Error( );
/**/}

	// step zero doesn't do anything
	if( start === 0 ) start = 1;

	for( let a = start; a < end; a++ )
	{
		const step = list.get( a );
		tree = step.pick( tree );
	}

	return tree;
};

/*
| Starts a new trace root.
|
| ~plan:  plan of the trace.
| ~arg:   optionally 'at' or 'key'
*/
def.static.root =
	function( plan, arg )
{
/**/if( CHECK && plan.ti2ctype !== PlanPiece ) throw new Error( );

	const name = plan.name;

	switch( typeof( arg ) )
	{
		case 'undefined':
			return(
				Self.create(
					'_list',
						ListStep.Elements(
							StepPlain.create(
								'plan', plan,
								'name', name,
							)
						)
				)
			);

		case 'number':
			return(
				Self.create(
					'_list',
						ListStep.Elements(
							StepAt.create(
								'at', arg,
								'plan', plan,
								'name', name,
							)
						)
				)
			);

		case 'string':
			return(
				Self.create(
					'_list',
						ListStep.Elements(
							StepKey.create(
								'key', arg,
								'plan', plan,
								'name', name,
							)
						)
				)
			);

		default:
			throw new Error( );
	}
};

/*
| Replaces a step.
|
| ~pos:  step position to replace.
| ~name: replace step with this name.
| ~arg:  replace at/key with this value (or undefined if plain)
*/
def.proto.set =
	function( pos, name, arg )
{
	const list = this._list;
	let step = list.get( pos );

/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 && arguments.length !== 3 ) throw new Error( );
/**/	if( step.name !== name ) throw new Error( );
/**/}

	const sub = step.plan;
	if( sub.at )
	{
		step = StepAt.create( 'at', arg, 'name', name, 'plan', sub );
	}
	else if( sub.key )
	{
		step = StepKey.create( 'key', arg, 'name', name, 'plan', sub );
	}
	else
	{
		step = StepPlain.create( 'name', name, 'plan', sub );

		if( arg !== undefined )
		{
			throw new Error( );
		}
	}

	return this.create( '_list', list.set( pos, step ) );
};

/*
| Replaces a step object
|
| ~pos:  step position to replace.
| ~step: replace step with this name.
*/
def.proto.setStep =
	function( pos, step )
{
	const list = this._list;
	let stepOld = list.get( pos );
	const name = step.name;

/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 && arguments.length !== 3 ) throw new Error( );
/**/	if( step.name !== name ) throw new Error( );
/**/}

	let sub = stepOld.plan;

/**/if( CHECK && !sub ) throw new Error( );

	if( typeof( sub ) === 'function' )
	{
		sub = sub( );
	}

/**/if( CHECK && !sub ) throw new Error( );

	if( sub.at )
	{
		step = StepAt.create( 'at', step.at, 'name', name, 'plan', sub );
	}
	else if( sub.key )
	{
		step = StepKey.create( 'key', step.key, 'name', name, 'plan', sub );
	}
	else
	{
		step = StepPlain.create( 'name', name, 'plan', sub );
	}

	return this.create( '_list', list.set( pos, step ) );
};

/*
| Extra checking.
*/
def.proto._check =
	function( )
{
/**/if( CHECK && this._list.length < 1 ) throw new Error( );
};
