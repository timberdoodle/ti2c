/*
| A plain trace step.
*/
def.extend = 'Trace/Step/Base';

def.attributes =
{
	// name of the trace
	name: { type: 'string' },

	// plan piece
	plan: { type: 'Plan/Piece' },
};

/*
| The trace step as string (for debugging).
*/
def.lazy.asString =
	function( )
{
	return this.name;
};

/*
| Grafts a new leaf on a tree.
| In case of a root trace returns the leaf.
*/
def.proto.graft =
	function( tree, val )
{
	return tree.create( this.name, val );
};

/*
| Help for custom jsonfy converter.
*/
def.lazy.jsonfyStep =
	function( )
{
	return '"' + this.name + '"';
};

/*
| Help for custom json converter.
*/
def.lazy.jsonStep =
	function( )
{
	return [ this.name ];
};

/*
| Picks the traced leaf.
*/
def.proto.pick =
	function( tree )
{
	return tree[ this.name ];
};
