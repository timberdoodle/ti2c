/*
| A trace step with 'key' value.
*/
def.extend = 'Trace/Step/Base';

def.attributes =
{
	// the key
	key: { type: 'string' },

	// name of the trace
	name: { type: 'string' },

	// plan piece
	plan: { type: 'Plan/Piece' },
};

/*
| The trace step as string (for debugging).
*/
def.lazy.asString =
	function( )
{
	return this.name + '(' + this.key + ')';
};

/*
| Grafts a new leaf on a tree.
| In case of a root trace returns the leaf.
*/
def.proto.graft =
	function( tree, val )
{
	const name = this.name;
	return tree.create( name, tree[ name ].set( this.key, val ) );
};

/*
| Help for custom jsonfy converter.
*/
def.lazy.jsonfyStep =
	function( )
{
	return '">' + this.name + '","'  + this.key + '"';
};

/*
| Help for custom json converter.
*/
def.lazy.jsonStep =
	function( )
{
	return [ '>' + this.name, this.key ];
};

/*
| Picks the traced leaf.
*/
def.proto.pick =
	function( tree )
{
	return tree[ this.name ].get( this.key );
};
