/*
| The base of all traces.
*/
def.abstract = true;

/*
| Helper for custom fromJson creators.
|
| ~json:    object to create from.
| ~plan:    trace plan
| ~varname: name of variable to create ('prev' or 'val' )
*/
def.proto.fromJsonVar =
	function( json, plan, varname )
{
/**/if( CHECK && arguments.length !== 3 ) throw new Error( );

	const types = this.plan.types;
	let v = json[ varname ];

	if( v === undefined ) return undefined;

	if( types )
	{
		const tt = types.get( v.$type );

		if( !tt ) throw new Error( 'fromJsonVar, unknown type: ' + v.$type );

		if( tt.$fromJsonArgs[ 0 ] === 'plan' )
		{
			return tt.FromJson( v, plan );
		}
		else
		{
			return tt.FromJson( v );
		}
	}
	else
	{
		const tv = typeof( v );
		if( tv === 'object' || tv === 'array' ) throw new Error( );
	}

	return v;
};

