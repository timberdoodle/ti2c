/*
| A trace step with 'at' number.
*/
def.extend = 'Trace/Step/Base';

def.attributes =
{
	// the at value
	at: { type: 'number' },

	// name of the trace
	name: { type: 'string' },

	// plan piece
	plan: { type: 'Plan/Piece' },
};

/*
| The trace step as string (for debugging).
*/
def.lazy.asString =
	function( )
{
	return this.name + '(' + this.at + ')';
};

/*
| Grafts a new leaf on a tree.
| In case of a root trace returns the leaf.
*/
def.proto.graft =
	function( tree, val )
{
	const name = this.name;
	return tree.create( name, tree[ name ].set( this.at, val ) );
};

/*
| Help for custom jsonfy converter.
*/
def.lazy.jsonfyStep =
	function( )
{
	return '"#' + this.name + '",'  + this.at;
};

/*
| Help for custom json converter.
*/
def.lazy.jsonStep =
	function( )
{
	return [ '#' + this.name, this.at ];
};

/*
| Picks the traced leaf.
*/
def.proto.pick =
	function( tree )
{
	return tree[ this.name ].get( this.at );
};
