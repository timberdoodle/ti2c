/*
| General ti2c stuff used in node and browser.
*/

/*
| A lazy value is computed and fixated before it is needed.
|
| obj: object to ahead the lazy value for
| name: name to ahead
| value: value to ahead
*/
ti2c.aheadValue =
	function( obj, name, value )
{
/**/if( CHECK && value === undefined ) throw new Error( );

	// FUTURE CHECK if value is already set.

	return( obj.__lazy[ name ] = value );
};

/*
| A function taking a key and no side effects
| is computed for a value and fixated before it is needed.
|
| obj:   object to ahead for
| name:  property to ahead
| key:   function ( key ) value to ahead
| value: value ( result ) to ahead
*/
ti2c.aheadFunction =
	function( obj, name, key,  value )
{
/**/if( CHECK )
/**/{
/**/	if( name === undefined ) throw new Error( );
/**/	if( key === undefined ) throw new Error( );
/**/	if( value === undefined ) throw new Error( );
/**/}

	let c = obj.__lazy[ name ];
	if( !c ) c = obj.__lazy[ name ] = new Map( );
	c.set( key, value );
	return value;
};

/*
| Copies one object (not deep!)
|
| Also doesn't do hasOwnProperty checking since that one
| is only to be used on vanilla objects.
|
| ~obj: the object to copy from
*/
ti2c.copy =
	function( obj )
{
	const c = { };
	for( let k in obj ) c[ k ] = obj[ k ];
	return c;
};

/*
| Tests if the object has a lazy value set.
*/
ti2c.hasLazyValueSet =
	function( obj, name )
{
	return obj.__lazy[ name ] !== undefined;
};

/*
| A trap to catches all invalid ti2c object access.
*/
ti2c.trap =
	new Proxy(
		{ },
		{
			get : function( target, prop, receiver )
			{
				if( prop === '__lazy' ) return target.__lazy;
				if( prop === 'then' ) return target.then;
				if( typeof( prop ) === 'symbol' ) return target[ prop ];
				throw new Error( 'invalid property access: ' + String( prop ) );
			},
		}
	);

/*
| Common functions for typed immutables.
*/
const proto = ti2c._proto = { };

/*
| A value is computed and fixated only when needed.
*/
const lazyValue =
proto.lazyValue =
	function( proto, key, getter )
{
/**/if( CHECK )
/**/{
/**/	// there is something amiss if static and
/**/	// tim obj lazyness is used together
/**/	if( proto.__lazy ) throw new Error( );
/**/}

	Object.defineProperty(
		proto,
		key,
		{
			get : function( )
			{
				let val = this.__lazy[ key ];
				if( val !== undefined ) return val;
				//val = Object.freeze( getter.call( this ) );
				val = getter.call( this );
				return( this.__lazy[ key ] = val );
			}
		}
	);
};

/*
| A function taking a key and no side effects.
| Computed values are cached.
*/
const lazyFunction =
proto.lazyFunction =
	function( proto, name, getter )
{
/**/if( CHECK )
/**/{
/**/	// there is something amiss if static and tim obj
/**/	// lazyness is used together
/**/	if( proto.__lazy ) throw new Error( );
/**/}

	proto[ name ] =
		function( key )
	{
		let c = this.__lazy[ name ];
		if( !c ) c = this.__lazy[ name ] = new Map( );
		let v = c.get( key );
		if( v !== undefined ) return v;
		v = getter.call( this, key );
		c.set( key, v );
		return v;
	};
};

/*
| A value is computed and fixated only when needed
| but not from a tim but a static object.
*/
proto.lazyStaticValue =
	function( obj, key, getter )
{
	if( !Object.prototype.hasOwnProperty.call( obj, '__lazy' ) ) obj.__lazy = { };
	Object.defineProperty(
		obj, key,
		{
			get : function( )
			{
				let val = this.__lazy[ key ];
				if( val !== undefined ) return val;
				//val = Object.freeze( getter.call( this ) );
				val = getter.call( this );
				return( this.__lazy[ key ] = val );
			}
		}
	);
};

/*
| A function taking a key and no side effects.
| Computed values are cached. Same as for a tim, but for a static.
*/
proto.lazyStaticFunc =
	function( obj, name, getter )
{
	if( !Object.prototype.hasOwnProperty.call( obj, '__lazy' ) ) obj.__lazy = { };
	obj[ name ] =
		function( key )
	{
		let c = this.__lazy[ name ];
		if( !c ) c = this.__lazy[ name ] = new Map( );
		let v = c.get( key );
		if( v !== undefined ) return v;
		v = getter.call( this, key );
		c.set( key, v );
		return v;
	};
};

/*
| Returns true if 'obj' is an empty object.
*/
proto.isEmpty =
	function( obj )
{
	for( let dummy in obj ) return false;
	return true;
};

/*
| Returns the group with another group added,
| overwriting collisions.
*/
proto.groupAddGroup =
	function( group )
{
/**/if( CHECK && this.ti2ctype !== group.ti2ctype ) throw new Error( );

	const g = { };
	for( let k in this._group ) g[ k ] = this._group[ k ];
	for( let k in group._group ) g[ k ] = group._group[ k ];
	return this.create( 'group:init', g );
};

/*
| Gets one entry from the group.
*/
proto.groupGet =
	function( key )
{
	return this._group[ key ];
};

/*
| Returns the group with one element removed.
*/
proto.groupRemove =
	function( key )
{
	return this.create( 'group:remove', key );
};

/*
| Returns the group with one element set.
*/
proto.groupSet =
	function( key, e )
{
	return this.create( 'group:set', key, e );
};

/*
| Returns the size of the group.
*/
proto.groupSize =
	function( )
{
	return this.keys.length;
};

/*
| Returns the list with an element appended.
*/
proto.listAppend =
	function( e )
{
	return this.create( 'list:append', e );
};

/*
| Returns the list with another list appended.
*/
proto.listAppendList =
	function( list )
{
	return this.create( 'list:init', this._list.concat( list._list ) );
};

/*
| Returns an array clone of the list.
*/
proto.listClone =
	function( from, to )
{
	if( arguments.length === 0 ) return this._list.slice( );
	return this._list.slice( from, to );
};

/*
| Returns last element of the list.
*/
proto.listFirst =
	function( )
{
/**///if( CHECK && this.length === 0 ) throw new Error( );

	return this._list[ 0 ];
};

/*
| Returns one element of the list.
*/
proto.listGet =
	function( idx )
{
	if( idx < 0 ) idx += this.length;
/**/if( CHECK && ( idx < 0 || idx >= this.length ) ) throw new Error( );
	return this._list[ idx ];
};

/*
| Returns the list with one element inserted.
*/
proto.listInsert =
	function( idx, e )
{
	return this.create( 'list:insert', idx, e );
};

/*
| Returns the length of the list.
*/
proto.listLength =
	function( )
{
	return this._list.length;
};

/*
| Returns last element of the list.
*/
proto.listLast =
	function( )
{
/**///if( CHECK && this.length === 0 ) throw new Error( );

	return this._list[ this.length - 1 ];
};

/*
| Returns the list with one element removed.
*/
proto.listRemove =
	function( idx )
{
	return this.create( 'list:remove', idx );
};

/*
| Returns the list with one element set.
*/
proto.listSet =
	function( idx, e )
{
	if( idx < 0 ) idx += this.length;
/**/if( CHECK && ( idx < 0 || idx > this.length ) ) throw new Error( );
	return this.create( 'list:set', idx, e );
};


/*
| Returns a slice of the list.
*/
proto.listSlice =
	function( from, to )
{
	if( from < 0 ) from += this.length;

	if( to === undefined ) to = this.length;
	else if( to < 0 ) to += this.length;

/**/if( CHECK )
/**/{
/**/	if( from < 0 || from > this.length ) throw new Error( );
/**/	if( to < 0 || to > this.length ) throw new Error( );
/**/	if( to < from ) throw new Error( );
/**/}

	return this.create( 'list:init', this._list.slice( from, to ) );
};

/*
| Returns a list sorted by compare functions
|
| ~compare: function to compare it with.
*/
proto.listSort =
	function( compare )
{
	const a = this._list.slice( ).sort( compare );
	return this.create( 'list:init', a );
};

/*
| Returns the set with one element added.
*/
proto.setAdd =
	function( e )
{
	return this.create( 'set:add', e );
};

/*
| Returns the set with another set added.
*/
proto.setAddSet =
	function( set )
{
	let s = new Set( this._set );
	let it = set._set.keys( );
	for( let i = it.next( ); !i.done; i = it.next( ) ) s.add( i.value );
	return this.create( 'set:init', s );
};

/*
| Returns true if the set has an element.
*/
proto.setHas =
	function( e )
{
	return this._set.has( e );
};

/*
| Returns the set with one element removed.
*/
proto.setRemove =
	function( e )
{
	return this.create( 'set:remove', e );
};

/*
| Returns the size of the group.
*/
proto.setSize =
	function( )
{
	return this._set.size;
};

/*
| Returns the one and only element or the set if size != 1.
*/
proto.setTrivial =
	function( )
{
	if( this.size !== 1 ) return this;
	return this._set.keys( ).next( ).value;
};

/*
| Returns the element at rank.
*/
proto.twigAtRank =
	function( rank )
{
	return this.get( this.keys[ rank ] );
};

/*
| Returns the element by key.
*/
proto.twigGet =
	function( key )
{
	return this._twig[ key ];
};

/*
| Returns the key at a rank.
*/
proto.twigGetKey =
	function( idx )
{
	return this.keys[ idx ];
};

/*
| Returns the length of the twig.
*/
proto.twigLength =
	function( )
{
	return this.keys.length;
};

/*
| Returns the rank of the key.
|
| This means it returns the index of key in the ranks array.
*/
proto.twigRankOf =
	function( key )
{
/**/if( CHECK && typeof( key ) !== 'string' ) throw new Error( );
	return this.keys.indexOf( key );
};

/*
| Returns the twig with the element at key set.
*/
proto.twigSet =
	function( key, entry )
{
	return this.create( 'twig:set', key, entry );
};

/*
| Prepares a ti2c class to be ran.
|
| ~entry: the entry to prepare.
| ~entryExtend: the extented entry.
| ~inspect: use this inspect function (node only)
*/
ti2c._prepare =
	function( entry, entryExtend, inspect )
{
	const proto = ti2c._proto;
	const Self = entry.Self;
	const def = entry.def;

	if( entryExtend )
	{
		const edef = entryExtend.def;
		const protoMask  = { };
		const staticMask = { };

		for( let name in def.static )         staticMask[ name ] = true;
		for( let name in def.staticLazy )     staticMask[ name ] = true;
		for( let name in def.staticLazyFunc ) staticMask[ name ] = true;

		for( let name in def.lazy )     protoMask[ name ] = true;
		for( let name in def.lazyFunc ) protoMask[ name ] = true;
		for( let name in def.proto )    protoMask[ name ] = true;

		for( let name in edef.static )
		{
			if( !staticMask[ name ] ) def.static[ name ] = edef.static[ name ];
		}

		for( let name in edef.staticLazy )
		{
			if( !staticMask[ name ] ) def.staticLazy[ name ] = edef.staticLazy[ name ];
		}

		for( let name in edef.staticLazyFunc )
		{
			if( !staticMask[ name ] ) def.staticLazyFunc[ name ] = edef.staticLazyFunc[ name ];
		}

		for( let name in edef.lazy )
		{
			if( !protoMask[ name ] ) def.lazy[ name ] = edef.lazy[ name ];
		}

		for( let name in edef.lazyFunc )
		{
			if( !protoMask[ name ] ) def.lazyFunc[ name ] = edef.lazyFunc[ name ];
		}

		for( let name in edef.proto )
		{
			if( !protoMask[ name ] ) def.proto[ name ] = edef.proto[ name ];
		}

		if( edef.inspect && !def.inspect )
		{
			def.inspect = edef.inspect;
		}
	}

	// assigns statics
	for( let name in def.static ) Self[ name ] = def.static[ name ];

	// assigns lazy statics
	for( let name in def.staticLazy )
	{
		proto.lazyStaticValue( Self, name, def.staticLazy[ name ] );
	}

	// assigns lazy static funcs
	for( let name in def.staticLazyFunc )
	{
		proto.lazyStaticFunc( Self, name, def.staticLazyFunc[ name ] );
	}

	if( !def.abstract )
	{
		const prototype = Self.prototype;

		// assigns lazy values to the prototype
		for( let name in def.lazy )
		{
			lazyValue( prototype, name, def.lazy[ name ] );
		}

		// assigns lazy integer functions to the prototype
		for( let name in def.lazyFunc )
		{
			lazyFunction( prototype, name, def.lazyFunc[ name ] );
		}

		// assigns functions to the prototype
		for( let name in def.proto )
		{
			prototype[ name ] = def.proto[ name ];
		}

		// sets custom inspects
		if( inspect && def.inspect ) prototype[ inspect.custom ] = def.inspect;
	}
};

