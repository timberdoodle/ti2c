/*
| File reading wrapper.
|
| Direct node.js API is a bit convoluted here.
| Provides usable backtracs in case of errror.
*/
import fs from 'node:fs/promises';

const readOptions = Object.freeze( { encoding: 'utf8' } );

/*
| Reads 'filename' and returns it as string.
| Throws error if not existing.
*/
export async function read( filename )
{
	let data;
	try
	{
		data = await fs.readFile( filename, readOptions );
	}
	catch( e )
	{
		console.log( 'Cannot read file "' + filename + '"' );
		throw new Error( );
	}

	// converts Buffer to string
	return data + '';
}

/*
| Reads 'filename' and returns it as string.
| Returns undefined if not existing.
*/
export async function readExisting( filename )
{
	try
	{
		await fs.stat( filename );
	}
	catch( e )
	{
		if( e.code === 'ENOENT' ) return undefined;
		console.log( 'Cannot stat file "' + filename + '"' );
		throw new Error( );
	}

	let data;
	try
	{
		data = await fs.readFile( filename, readOptions );
	}
	catch( e )
	{
		console.log( 'Cannot read file "' + filename + '"' );
		throw new Error( );
	}

	// converts Buffer to string
	return data + '';
}

/*
| Writes 'data' into 'filename'.
*/
export async function write( filename, data )
{
	try
	{
		data = await fs.writeFile( filename, data );
	}
	catch( e )
	{
		console.log( 'Cannot write file "' + filename + '"' );
		throw new Error( );
	}
}

