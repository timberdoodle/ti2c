/*
| murmurHash3.js v3.0.0 (http://github.com/karanlyons/murmurHash3.js)
| A TypeScript/JavaScript implementation of MurmurHash3's hashing algorithms.
|
| Copyright (c) 2012-2020 Karan Lyons. Freely distributable under the MIT license.
|
| [Axel Kittenberger] (2021):
| translated from .ts to .js
| heaviliy restructured code to fit ti2c needs:
| * removed remainder buffer ability, instead padding stuff with zeros
| * added multiple arguments into one hash call
| * called it "timur" as it technically isn't truely murmur anymore.
*/

//const strToBuf = TextEncoder.prototype.encode.bind( new TextEncoder( ) );

const mul32 = ( m, n ) =>
	( ( ( m & 0xffff ) * n ) + ( ( ( ( m >>> 16 ) * n ) & 0xffff ) << 16 ) );

const rol32 = ( n, r ) =>
	( ( n << r ) | ( n >>> ( 32 - r ) ) );

function x86fmix32( h )
{
	h ^= h >>> 16;
	h = mul32( h, 0x85ebca6b );
	h ^= h >>> 13;
	h = mul32( h, 0xc2b2ae35 );
	return h ^ ( h >>> 16 );
}

function x86mix32( h, k )
{
	k = mul32( k, 0xcc9e2d51 );
	k = rol32( k, 15 );
	k = mul32( k, 0x1b873593 );
	h ^= k;
	h = rol32( h, 13 );
	return mul32( h, 5 ) + 0xe6546b64;
}

/*
| Cache for hashed strings.
|
| This is timed, every 'cacheSwitchTime' milliseconds the cache
| to be filled is switched (but the previous still looked up)
*/
const strCacheSwitchTime = 180000;
let strCacheTime = Date.now( );
let strCacheCur = { };
let strCachePre = { };

/*
| Hashes a string.
*/
function hashString( str )
{
	/*
	// This may look more elegent to access the bytes of string directly
	// but in reality calling TextEncoder is a lot slower.
	const buf = strToBuf( e );
	const dtv = new DataView( buf.buffer, buf.byteOffset );
	const blen = buf.byteLength;
	len += blen;
	const remainder = blen % 4;
	const bytes = blen - remainder;
	for( let i = 0; i < bytes; i += 4 ) h1 = x86mix32( h1, dtv.getUint32( i, true ) );
	if( remainder )
	{
		let k1 = 0;
		switch( remainder )
		{
			case 3: k1 |= dtv.getUint8( bytes + 2 );
			// fallthrough
			case 2: k1 |= dtv.getUint8( bytes + 1 ) << 8;
			// fallthrough
			case 1: k1 |= dtv.getUint8( bytes ) << 16;
		}
		h1 = x86mix32( h1, k1 );
	}
	return h1;
	*/

	const blen = str.length;
	len += blen;

	let h1 = strCacheCur[ str ];
	if( h1 !== undefined ) return h1;

	h1 = strCachePre[ str ];
	if( h1 !== undefined )
	{
		// in previous cache, move to current
		strCacheCur[ str ] = h1;
		return h1;
	}

	h1 = 1717212397;
	const remainder = blen % 2;
	const bytes = blen - remainder;
	for( let i = 0; i < bytes; i += 2 )
	{
		h1 = x86mix32( h1, str.charCodeAt( i ) << 16 + str.charCodeAt( i + 1 ) );
	}

	if( remainder )
	{
		h1 = x86mix32( h1, str.charCodeAt( bytes ) << 16 );
	}

	const now = Date.now( );
	if( now - strCacheTime >= strCacheSwitchTime )
	{
		// switches caches
		strCachePre = strCacheCur;
		strCacheCur = { };
		strCacheTime = now;
	}

	strCacheCur[ str ] = h1;
	return h1;
}

const numdv = new DataView( new ArrayBuffer( 16 ) );
let len;

/*
| Hashes one entity.
*/
function hashEntity( h1, e )
{
	switch( typeof( e ) )
	{
		case 'undefined':
		{
			len++;
			return x86mix32( h1, 202709291 );
		}

		case 'boolean':
		{
			len++;
			return x86mix32( h1, e ? 4255843951 : 527994763 );
		}

		case 'number':
		{
			len++;
			const ndv = numdv;
			ndv.setFloat64( 0, e, true );
			h1 = x86mix32( h1, ndv.getInt32(  0 ) );
			h1 = x86mix32( h1, ndv.getInt32(  4 ) );
			h1 = x86mix32( h1, ndv.getInt32(  8 ) );
			return x86mix32( h1, ndv.getInt32( 12 ) );
		}

		case 'function':
		{
			len++;
			// assigns a random hash value to proteans
			if( e.__hash === undefined )
			{
				e.__hash = Math.floor( Math.random( ) * 0xffffffff );
			}

			return x86mix32( h1, e.__hash );
		}

		case 'object':
		{
			len++;
			// why the hell typeof( null ) is 'object' is beyond me
			if( e === null ) return x86mix32( h1, 1795030597 );

			// assigns a random hash value to proteans
			if( e.__hash === undefined )
			{
				e.__hash = Math.floor( Math.random( ) * 0xffffffff );
			}

			return x86mix32( h1, e.__hash );
		}

		case 'string':
		{
			const hstr = hashString( e );
			return x86mix32( h1, hstr );
		}

		default: throw new Error( typeof( e ), e );
	}
}

/*
| Generates the mur hash of something iterable
|
| ~h1: start hash value
| ~iterable
*/
const hashIterable =
ti2c._hashIterable =
	function( h1, iterable )
{
	len = 0;
	for( let e of iterable )
	{
		h1 = hashEntity( h1, e );
		h1 ^= len & 0xffffffff;
		h1 = x86fmix32( h1 );
	}
	return h1;
};

/*
| Hashes a set.
|
| This is a little different to _hasIterable since sets are unsorted,
| So the part hashes are sorted to get deterministic behaviour.
*/
ti2c._hashSet =
	function( h1, set )
{
	const hashes = [ ];
	for( let e of set )
	{
		len = 0;
		hashes.push( hashEntity( 0, e ) );
	}
	hashes.sort( );
	for( let h of hashes ) h1 = x86mix32( h1, h );
	h1 ^= set.size & 0xffffffff;
	return x86fmix32( h1 );
};

/*
| Generates the mur hash of s
*/
ti2c._hashKeys =
	function( h1, keys, group )
{
	len = 0;
	for( let key of keys )
	{
		h1 = hashEntity( h1, key );
		h1 = hashEntity( h1, group[ key ] );
		h1 ^= len & 0xffffffff;
		h1 = x86fmix32( h1 );
	}
	return h1;
};

/*
| Hashes the arguments.
*/
ti2c._hashArgs =
	function( h1, ...args )
{
	return hashIterable( h1, args );
};
