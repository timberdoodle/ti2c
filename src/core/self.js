/*
| Loads the def of a ti2c.
*/
import vm   from 'node:vm';
import util from 'node:util';

// FIXME remove, use require from meta.import
import { default as nodeModule } from 'node:module';
import { listing } from './listing.js';

import * as file from './file.js';
import './common.js';
import './timur32.js';

/*
| The package manager.
| Core is in init kickstart phase if yet undefined.
*/
let _PackageManager;

/*
| The packages in core.
*/
const _packages = { };

/*
| All entries
*/
const _entries = { };

/*
| Caches all known imports.
*/
const esmImports = new Map( );

/*
| Adds an entry to the core.
*/
function addEntry( name )
{
	let entry = _entries[ name ];
	if( entry ) return entry;

	entry =
	_entries[ name ] =
	{
		def:
		{
			proto: { },
			lazy: { },
			lazyFunc: { },
			static: { },
			staticLazy: { },
			staticLazyFunc: { },
		},
		// all imports literally as strings
		// ti2c-web needs the literal versions for import maps
		// in devel mode
		importSpecifiers: new Set( ),
		// tracks the loading state of the entry
		// 0 ... nothing
		// 1 ... evaluated src file
		// 2 ... evaluated codegen file
		// 3 ... prepared the class
		loaded: 0,
		Self: { },
	};

	return entry;
}


/*
| Adds a package to the core.
|
| ~name:       name of the package.
| ~rootDir:    rootDir of the package
*/
async function addPackage( name, rootDir )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 ) throw new Error( );
/**/	if( typeof( name ) !== 'string' ) throw new Error( );
/**/	if( typeof( rootDir ) !== 'string' ) throw new Error( );
/**/}

	if( name === 'ti2c' ) return;

	if( _packages[ name ] ) throw new Error( );

	let exports = await file.readExisting( rootDir + 'exports.json' );
	if( exports )
	{
		exports = JSON.parse( exports );
	}

	_packages[ name ] =
		Object.freeze( {
			rootDir: rootDir,
			exports: exports,
		} );
}

/*
| Initializes the core.
|
| ~rootDir: ti2c's rootDir,
*/
export async function init( rootDir )
{
	if( _PackageManager ) throw new Error( );

	let exports = await file.read( rootDir + 'exports.json' );
	exports = JSON.parse( exports );

	_packages.ti2c =
		Object.freeze( {
			rootDir: rootDir,
			exports: exports,
		} );

/**/let setListing;
/**/if( CHECK ) setListing = new Set( listing );

	// loads the core entries and their generated code
	for( let name of listing )
	{
		if( name.indexOf( '@' ) < 0 )
		{
			// a class
			if( !name.startsWith( 'ti2c:' ) ) throw new Error( );

			const entry = addEntry( name );
			const entryName = name.substring( 5 );

			const filename = rootDir + 'src/' + entryName + '.js';
			await loadEntry( entry, 'ti2c', filename );

/**/		if( CHECK )
/**/		{
/**/			for( let imp of entry.importSpecifiers )
/**/			{
/**/				if( !setListing.has( imp ) )
/**/				{
/**/					throw new Error( 'import "' + imp + '" not in core/listing"' );
/**/				}
/**/			}
/**/		}

			const codeGenFilename =
				rootDir
				+ 'codeob/ti2c-'
				+ entryName.replace( /\//g, '-' ) + '.js';

			await loadEntryCode( entry, codeGenFilename );
		}
		else
		{
			// a collection
			const entry = addEntry( name );
			const codeGenFilename =
				rootDir
				+ 'codeob/'
				+ (
					name
					.replace( /[:/]/g, '-' )
					.replace( /</g, '^' )
					.replace( /,/g, '__' )
				)
				+ '.js';

			await loadEntryCode( entry, codeGenFilename );
		}
	}

	// local function to prepare an entry
	// recurses in case of extending
	function prepareInitEntry( name )
	{
		const entry = _entries[ name ];
		if( entry.loaded === 3 ) return entry;
		if( entry.loaded !== 2 ) throw new Error( );

		let eDefExtend = entry.def.extend;
		let entryExtend;

		if( eDefExtend )
		{
			if( eDefExtend.indexOf( '@' ) < 0 ) eDefExtend = 'ti2c:' + eDefExtend;
			entryExtend = prepareInitEntry( eDefExtend );
		}

		ti2c._prepare( entry, entryExtend, util.inspect );
		entry.loaded = 3;
		return entry;
	}

	// prepares the core entries
	for( let name of listing )
	{
		prepareInitEntry( name );
	}

	_PackageManager = _entries[ 'ti2c:Package/Manager' ].Self;
	await _PackageManager.init(
		rootDir,
		Object.freeze( {
			addEntry:      addEntry,
			addPackage:    addPackage,
			loadEntry:     loadEntry,
			loadEntryCode: loadEntryCode,
		} ),
		listing,
	);
	return _PackageManager;
}

/*
| For dynamic imports.
*/
async function esmImportDynamically( specifier, referencingModule )
{
	return await import( specifier );
}

/*
| Function used for esm linking.
|
| ~baseFilename:      base file name used for relative specifiers
| ~entry:             base entry doing the linking (to be bound)
| ~pkgName:           current package name
| ~specifier:         specifier to link
| ~referencingModule: handed through vm module
*/
async function esmLinker( baseFilename, entry, pkgName, specifier, referencingModule )
{
	if( specifier === '((ti2c::loop))' )
	{
		// provides the ti2c loop back interface
		const loop =
			new vm.SyntheticModule(
				[ 'def', 'pass', 'Self', 'ti2c' ],
				( ) =>
				{
					loop.setExport( 'def', entry.def );
					loop.setExport( 'pass', pass );
					loop.setExport( 'Self', entry.Self );
					loop.setExport( 'ti2c', ti2c );
				},
				{
					identifier: '((ti2c::loop))',
					context: referencingModule.context
				}
			);
		return loop;
	}

	// imports a ti2c entry
	if( specifier.startsWith( '{' ) )
	{
		let imp;
		if( specifier.startsWith( '{{' ) )
		{
			// specifier from auto generated code has to be fully in
			// resolved/normalized form already
			if( !specifier.endsWith( '}}' ) ) throw new Error( );
			imp = specifier.substring( 2, specifier.length - 2 );
		}
		else
		{
			// imports the ti2c entry
			if( !specifier.endsWith( '}' ) ) throw new Error( );

			specifier = specifier.substring( 1, specifier.length - 1 );
			if( _PackageManager )
			{
				imp = _PackageManager.TypeFromSpecifier( specifier, pkgName ).asString;
			}
			else
			{
				// in ti2c kickstart phase
				imp = specifier;
			}
		}

		if( pkgName )
		{
			// only tracks importSpecifiers for class defining entries.
			entry.importSpecifiers.add( specifier );
		}
		const iEntry = addEntry( imp );

		// cached?
		if( esmImports.has( iEntry ) )
		{
			return esmImports.get( iEntry );
		}

		const imported =
			new vm.SyntheticModule(
				[ 'default', 'Self' ],
				( ) =>
				{
					imported.setExport( 'default', iEntry.Self );
					imported.setExport( 'Self', iEntry.Self );
				},
				{
					//identifier: specifier,
					context: referencingModule.context
				}
			);
		esmImports.set( iEntry, imported );
		return imported;
	}

	// forwards traditional imports

	// FIXME, replace with following if import.meta.resolve is no longer experimental
	// await import.meta.resolve( specifier, pkg.url );
	const require = nodeModule.createRequire( baseFilename );
	const resolved = require.resolve( specifier );

	// cached?
	if( esmImports.has( resolved ) )
	{
		return esmImports.get( resolved );
	}

	const mod = await import( resolved );
	const exportNames = Object.keys( mod );
	const imported =
		new vm.SyntheticModule(
			exportNames,
			( ) =>
			{
				exportNames.forEach( key => imported.setExport( key, mod[ key ] ) );
			},
			{
				identifier: specifier,
				context: referencingModule.context
			}
		);
	esmImports.set( specifier, imported );
	return imported;
}

/*
| Loads a file defining a ti2c class.
|
| ~entry:    the entry to be loaded.
| ~pkgName:  name of the package the entry is in.
| ~filename: filename to load.
*/
async function loadEntry( entry, pkgName, filename )
{
/**/if( CHECK && arguments.length !== 3 ) throw new Error( );

	if( entry.loaded > 0 ) return;

	const code = await file.read( filename );
	const input = 'import { def, pass, Self, ti2c } from "((ti2c::loop))";' + code;
	const stm =
		new vm.SourceTextModule(
			input,
			{
				identifier: filename,
				importModuleDynamically: esmImportDynamically,
			}
		);
	await stm.link( esmLinker.bind( undefined, filename, entry, pkgName ) );
	await stm.evaluate( );
	entry.loaded = 1;
}

/*
| Loads generated code of a ti2c class.
|
| ~entry: the entry to be loaded.
*/
async function loadEntryCode( entry, filename )
{
	if( entry.loaded > 2 ) return;

	const ti2cCode = await file.read( filename );
	const input = 'import { pass, Self, ti2c } from "((ti2c::loop))";' + ti2cCode;
	const stm =
		new vm.SourceTextModule(
			input,
			{
				identifier: filename,
			}
		);
	await stm.link( esmLinker.bind( undefined, filename, entry, undefined ) );
	await stm.evaluate( );
	entry.loaded = 2;
}
