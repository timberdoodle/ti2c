/*
| The result of a walk.
*/
def.attributes =
{
	// all package names used, sorted alphabetically
	pkgNames: { type: 'list@string' },

	// list of types walked, sorted by extended stuff first
	types: { type: 'list@<ti2c:Type/Ti2c/Types' },
};
