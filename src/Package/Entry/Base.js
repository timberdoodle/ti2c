/*
| Base of ti2c entries (class and collections)
*/
def.abstract = true;

def.attributes =
{
	// absolute path to generated code file name
	aPathCodeGen: { type: 'Path/Self' },

	// manager to use getEntry from
	manager: { type: [ 'Package/Manager', 'Ouroboros/Root' ] },

	// entry core
	_coreEntry: { type: [ 'undefined', 'protean' ] },
};

import { Self as Ast } from '{ti2c:Ast/Self}';

/*
| Safe increment for hash collisions, considering max hash = 2^32
*/
def.staticLazy._iEpsilon =
	( ) =>
	Ast.number( Math.pow( 2,-21 ) );

/*
| Generates the creators interning.
*/
def.lazy._creatorIntern =
	function( )
{
	const action =
		Ast.block
		// shifts the used entry upward
		.if(
			'uhash !== undefined',
			Ast.block
			.$( 'imap.set( ihash, imap.get( uhash ) )' )
			.$( 'imap.set( uhash, wr )' )
		)
		.$(
			this.setGlobal
			? Ast.$( 'globalThis.', this.setGlobal, '= e' )
			: undefined
		)
		.$( 'return e' );

	return(
		Ast.block
		.$( this._creatorInternHash )
		// the position in imap being looked up
		.$( 'let ihash = hash' )
		// the position in imap to use
		.$( 'let uhash' )
		// last position in imap used
		.$( 'let lhash' )
		.for(
			'let wr = imap.get( hash )',
			'wr',
			Ast.$( 'wr = imap.get( ihash += ', Self._iEpsilon, ' )' ),
			Ast.block
			.$( 'const e = wr.deref( );' )
			.if(
				'e',
				Ast.block
				.$( 'lhash = ihash' )
				.$( this._creatorInternEqTest( action ) ),
				Ast.block.$( 'if( uhash === undefined ) uhash = ihash;' ),
				Ast.block.$( 'lhash = ihash' )
			)
		)
		// otherwise appends to the list
		.if(
			'uhash === undefined',
			Ast.block
			.$( 'uhash = ihash' )
			// if lhash is undefined it stays undefined
			.$( 'if( lhash < uhash ){ lhash = uhash; }' )
		)
		// and cleans the map of empty derefences
		.if(
			'lhash !== undefined',
			Ast.block
			.$( 'lhash +=', Self._iEpsilon )
			.while(
				'imap.get( lhash )',
				Ast.block
				.$( 'imap.set( lhash, undefined )' )
				.$( 'lhash +=', Self._iEpsilon )
			)
		)
	);
};


/*
| The base hash for this ti2ctype.
*/
def.lazy.hash =
	function( )
{
	return Ast.number( ti2c._hashArgs( 0, this.type.asString ) );
};

/*
| Generates the header of generated files.
*/
def.lazy._header =
	function( )
{
	return(
		Ast.block
		.comment(
			'This is an auto generated file.',
			'Editing this might be rather futile.',
		)
		.sep
	);
};

/*
| Generates the interning map and caching queue.
*/
def.lazy._iMapAndCache =
	function( )
{
	if( this.singleton ) return undefined;

	return(
		Ast.block
		.comment( 'The interning map.' )
		.$( 'let imap = new Map( )' )
		.comment( 'Caching queue.' )
		.$( 'let cSize = globalThis.TI2C_RETENTION ? 1024 : 0' )
		.$( 'let cArray = cSize ? new Array( cSize ) : undefined' )
		.$( 'let cPos = 0' )
		.comment( 'Timestamp since last turnaround' )
		.$( 'let cTurn = cSize ? Date.now( ) : undefined' )
		.comment( 'Puts an entry in the caching queue.' )
		.$( 'const cPut = ',
			Ast.func(
				Ast.block
				.$( 'cArray[ cPos ] = o;' )
				.$( 'cPos = ++cPos % cSize ' )
				.if( '!cPos',
					Ast.block
					.$( 'const now = Date.now( )' )
					.if( 'now - cTurn < globalThis.TI2C_RETENTION',
						Ast.block
						//.check(
						//	'console.log(',
						//		'"increasing cache queue for",',
						//		'Self.__TI2C_NAME__',
						//	')'
						//)
						.$( 'cPos = cSize' )
						.$( 'cSize *= 2' )
						.$( 'cArray.length = cSize' )
					)
					.$( 'cTurn = now' )
				)
			)
			.arg( 'o', 'ti2c object' )
		)
	);
};

/*
| Generates reflection code.
*/
def.lazy._reflection =
	function( )
{
	return(
		Ast.block
		.comment( 'Type reflection.' )
		.$( 'prototype.ti2ctype = Self' )
		.comment( 'Reflection for debugging.' )
		.$(
			'Self.__TI2C_NAME__ = prototype.__TI2C_NAME__ =',
			Ast.string( this.type.asString )
		)
	);
};
