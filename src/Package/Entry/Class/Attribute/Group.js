/*
| An attribute description of a ti2c class.
|
| FIXME remove?
*/
def.extend = 'group@ti2c:Package/Entry/Class/Attribute/Self';

/*
| True if at least one attribute group is to be jsonfied and has a ti2c type.
*/
def.lazy.hasSubJsonfy =
	function( )
{
	for( let attr of this )
	{
		if( !attr.json ) continue;
		if( attr.types.hasTi2cType ) return true;
	}

	return false;
};
