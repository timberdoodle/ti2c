/*
| An attribute description of a tim object.
*/
def.attributes =
{
	// default value dependencies
	defaultValueDepends: { type: 'Type/Set' },

	// default value expression
	defaultValueExpr: { type: [ 'undefined', '< Ast/Type/Expr' ] },

	// create JSON converters
	json: { type: 'boolean', defaultValue : 'false' },

	// attribute name
	name: { type: 'string' },

	// attribute types
	types: { type: 'Type/Set' },
};

import { Self as Ast         } from '{ti2c:Ast/Self}';
import { Self as AstString   } from '{ti2c:Ast/String}';
import { Self as AstVar      } from '{ti2c:Ast/Var}';
import { Self as Call        } from '{ti2c:Ast/Call}';
import { Self as TypeSet     } from '{ti2c:Type/Set}';
import { Self as TypeTi2cAny } from '{ti2c:Type/Ti2c/Any}';

/*
| The name as ast string.
*/
def.lazy.$name =
	function( )
{
	return Ast.string( this.name );
};

/*
| Creates the attribute spec from the definition.
|
| ~def:         the json definition to create from
| ~name:        name of the attribute to define
| ~manager:     package manager to use
| ~basePkgName: base packag name
*/
def.static.FromDef =
	async function( def, name, manager, basePkgName )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 4 ) throw new Error( );
/**/	if( typeof( name ) !== 'string' ) throw new Error( );
/**/	if( typeof( basePkgName ) !== 'string' ) throw new Error( );
/**/}

	const jAttr = def.attributes[ name ];
	const jType = jAttr.type;
	const types = await TypeSet.SpecifierX( jType, manager, basePkgName );

	const defaultValue = jAttr.defaultValue;
	let defaultValueExpr;
	let defaultValueDepends = TypeSet.Empty;

	if( defaultValue )
	{
		// walker to transform default value
		// replaces import( 'path' ) with the import variable
		const transformDefaultValue =
			function( node )
		{
			if( node.ti2ctype !== Call ) return node;
			const func = node.func;
			if( func.ti2ctype !== AstVar ) return node;
			if( func.name !== 'import' ) return node;

			const args = node.args;
			if( args.length !== 1 )
			{
				throw new Error( 'import in defaultValue must have one argument' );
			}

			const specifier = args.get( 0 );
			if( specifier.ti2ctype !== AstString )
			{
				throw new Error( 'import argument in defaultValue must be string' );
			}

			const type = TypeTi2cAny.FromString( specifier.string, manager, basePkgName );
			defaultValueDepends = defaultValueDepends.add( type );

			return Ast.var( type.varname );
		};

		defaultValueExpr = Ast.expr( defaultValue );
		defaultValueExpr = defaultValueExpr.walk( transformDefaultValue );
	}


	return(
		Self.create(
			'defaultValueExpr', defaultValueExpr,
			'defaultValueDepends', defaultValueDepends,
			'json', !!jAttr.json,
			'name', name,
			'types', types,
		)
	);
};

/*
| The variable name to use in generated code.
*/
def.lazy.varname =
	function( )
{
	return 'v_' + this.name.replace( /-/g, '_' );
};
