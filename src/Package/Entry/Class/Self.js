/*
| Specifications of a ti2c class.
*/
def.attributes =
{
	// true if this ti2c class cannot be instanced itself
	abstract: { type: 'boolean' },

	// absolute path to defining javascript file
	aPathDef: { type: 'Path/Self' },

	// group of alike functions
	alikes: { type: [ 'undefined', 'Package/Entry/Class/Alike/Group' ] },

	// if this is a table the allowed attributes
	attributes: { type: [ 'Package/Entry/Class/Attribute/Group' ] },

	// if true an init checker is to be called
	check: { type: 'boolean' },

	// renaming the default creator
	creator: { type: 'string' },

	// true for using a custom from JSON converter.
	customFromJson: { type: 'boolean' },

	// true for using a custom jsonfy converter.
	customJsonfy: { type: 'boolean' },

	// if set extends this ti2c class
	entryExtend: { type: [ 'undefined', '< Package/Entry/Types' ] },

	// if extending a collection than the collection enty
	extendCollection: { type: [ 'undefined', '< Package/Entry/Collection/Types' ] },

	// true if this ti2c class has lazy values
	hasLazy: { type: 'boolean' },

	// json name of the ti2c class
	jsonName: { type: [ 'undefined', 'string' ] },

	// if set, actualizes a global variable to the last created instance
	setGlobal: { type: [ 'undefined', 'string' ] },

	// true if this a singleton
	// (no attributes or group/list/set/twig, not abstract)
	singleton: { type: 'boolean' },

	// type identifier
	type: { type: 'Type/Ti2c/Class' },

	// overrides FromJson arguments
	_fromJsonArgsOverride: { type: [ 'undefined', 'list@string' ] },
};

def.extend = 'Package/Entry/Base';

import { Self as ArrayLiteral      } from '{ti2c:Ast/ArrayLiteral}';
import { Self as Ast               } from '{ti2c:Ast/Self}';
import { Self as Attribute         } from '{ti2c:Package/Entry/Class/Attribute/Self}';
import { Self as AttributeGroup    } from '{ti2c:Package/Entry/Class/Attribute/Group}';
import { Self as GroupAlike        } from '{ti2c:Package/Entry/Class/Alike/Group}';
import { Self as ListString        } from '{list@string}';
import { Self as SetString         } from '{set@string}';
import { Self as TypeArbitrary     } from '{ti2c:Type/Ti2c/Arbitrary}';
import { Self as TypeBoolean       } from '{ti2c:Type/Primitive/Boolean}';
import { Self as TypeDate          } from '{ti2c:Type/Primitive/Date}';
import { Self as TypeFunction      } from '{ti2c:Type/Primitive/Function}';
import { Self as TypeInteger       } from '{ti2c:Type/Primitive/Integer}';
import { Self as TypeNull          } from '{ti2c:Type/Primitive/Null}';
import { Self as TypeNumber        } from '{ti2c:Type/Primitive/Number}';
import { Self as TypeProtean       } from '{ti2c:Type/Primitive/Protean}';
import { Self as TypeSet           } from '{ti2c:Type/Set}';
import { Self as TypeString        } from '{ti2c:Type/Primitive/String}';
import { Self as TypeTi2cAny       } from '{ti2c:Type/Ti2c/Any}';
import { Self as TypeTi2cArbitrary } from '{ti2c:Type/Ti2c/Arbitrary}';
import { Self as TypeTi2cClass     } from '{ti2c:Type/Ti2c/Class}';
import { Self as TypeTi2cGroup     } from '{ti2c:Type/Ti2c/Collection/Group}';
import { Self as TypeTi2cList      } from '{ti2c:Type/Ti2c/Collection/List}';
import { Self as TypeTi2cSet       } from '{ti2c:Type/Ti2c/Collection/Set}';
import { Self as TypeTi2cTwig      } from '{ti2c:Type/Ti2c/Collection/Twig}';
import { Self as TypeUndefined     } from '{ti2c:Type/Primitive/Undefined}';
import { Self as Validator         } from '{ti2c:Package/Validator}';

/*
| Returns true if obj isn't empty.
*/
const isntEmpty =
	function( obj )
{
	for( let _ in obj ) return true;
	return false;
};

/*
| Executes the generator.
| Returns an ast tree.
*/
def.lazy.ast =
	function( )
{
	const exc = this.extendCollection;

	if( exc && this.abstract ) throw new Error( );

	let code = this._header;
	if( exc )
	{
		code =
			code
			.$( exc._astImports )
			.$( exc._iMapAndCache )
			.$( exc._constructor )
			.$( exc._creator );

		if( exc.hasJson && !this.customFromJson )
		{
			code = code.$( exc._fromJsonCreator );
		}
		code = code.$( this._reflection );
		code = code.$( exc._proto );
		if( exc.hasJson )
		{
			code = code.$( exc._jsonfy );
			code = code.$( exc._jsonType );
		}

	}
	else
	{
		if( this.abstract ) return code;
		code = code.$( this._astImports );
		if( this.setGlobal )
		{
			code = code.$( 'globalThis.', this.setGlobal, '= undefined' );
		}
		code = code.$( this._iMapAndCache );
		code = code.$( this._constructor );
		if( this.singleton )
		{
			code = code.$( this._singleton );
		}
		code = code.$( this._creator );
		if( this.hasJson && !this.customFromJson )
		{
			code = code.$( this._fromJsonCreator );
		}
		code = code.$( this._reflection );
		code = code.$( this._proto );
		if( this.hasJson )
		{
			code = code.$( this._jsonfy );
			code = code.$( this._jsonType );
		}
		if( this.alikes )
		{
			code = code.$( this._alikes );
		}
	}

	return code;
};

/*
| Creates an entry.
|
| ~coreEntry:   entry in the core
| ~type:        entry type
| ~entryExtend: if this entry extends another this has to that entry
| ~codeGenDir:  directory for generated directory
| ~manager:     manager to use getEntry from
*/
def.static.Define =
	async function( coreEntry, type, entryExtend, codeGenDir, manager )
{
/**/if( CHECK && arguments.length !== 5 ) throw new Error( );

	const def = coreEntry.def;
	Validator.check( def );

	const pkgName = type.pkgName;
	const entryName = type.entryName;
	let extendCollection =
		entryExtend
		? (
			entryExtend.ti2ctype === Self
			? entryExtend.extendCollection
			: entryExtend
		)
		: undefined;

	let creator = def.create ? def.create[ 0 ] : 'create';
	const abstract = !!def.abstract;
	// in case of attributes, group, list, set or twig
	// it will be turned off again
	let singleton = true;
	let attributes = AttributeGroup.create( );
	let setGlobal;

/**/if( CHECK && def.extend )
/**/{
/**/	// Sanity check, if the caller provide the right extended entry?
/**/	const typeExtend = TypeTi2cAny.FromString( def.extend, manager, pkgName );
/**/	if( typeExtend !== entryExtend.type ) throw new Error( );
/**/}

	if( def.attributes && extendCollection ) throw new Error( );

	if( def.attributes )
	{
		for( let name in def.attributes )
		{
			singleton = false;
			const attr = await Attribute.FromDef( def, name, manager, pkgName );
			attributes = attributes.set( name, attr );
		}
	}

	if( entryExtend && !extendCollection )
	{
		const exAttributes = entryExtend.attributes;
		for( let name of exAttributes.keys )
		{
			if( attributes.get( name ) ) continue;
			const attr = exAttributes.get( name );
			attributes = attributes.set( name, attr );
		}
	}

	if( def.global ) setGlobal = def.global;

	if( abstract ) singleton = false;

	// if extending a non-singleton this can't be one either
	if(
		singleton
		&& entryExtend
		&& ( extendCollection || entryExtend.attributes.size > 0 )
	) singleton = false;

	const hasLazy =
		!! ( entryExtend?.hasLazy )
		|| ( !!def.json )
		|| isntEmpty( def.lazy )
		|| isntEmpty( def.lazyFunc );

	if( singleton && !def.singleton ) throw new Error( 'this is a singleton' );
	if( !singleton && def.singleton ) throw new Error( 'this is not a singleton' );
	if( def.abstract && def.singleton ) throw new Error( 'abstract and singleton are exclusive' );
	if( singleton && def.creator ) throw new Error( 'singletons cannot have creators' );

	if( singleton ) creator = '_create';

	const customJsonfy = def.lazyFunc.jsonfy !== undefined;

	let jsonName = def.json;
	if( jsonName === true )
	{
		jsonName = type.asString;
	}

	// if extending a collection json is to be true.
	if( !jsonName && extendCollection )
	{
		jsonName = type.asString;
	}

	if( extendCollection && jsonName )
	{
		extendCollection = extendCollection.create( 'jsonName', jsonName );
	}

	const fromJsonArgsOverride =
		def.fromJsonArgs
		? ListString.Array( def.fromJsonArgs )
		: undefined;

	const aPathDef = manager.getPackage( pkgName ).srcDir.addChunkF( entryName + '.js' );
	const aPathCodeGen = codeGenDir.f( type.asFileString + '.js' );

	const alikes =
		def.alike
		? GroupAlike.Define( def.alike )
		: undefined;

	const spec =
		Self.create(
			'abstract', abstract,
			'alikes', alikes,
			'aPathCodeGen', aPathCodeGen,
			'aPathDef', aPathDef,
			'attributes', attributes,
			'check', !!def.proto._check,
			'creator', creator,
			'customFromJson', def.static.FromJson !== undefined,
			'customJsonfy', customJsonfy,
			'entryExtend', entryExtend,
			'extendCollection', extendCollection,
			'hasLazy', hasLazy,
			'jsonName', jsonName,
			'manager', manager,
			'setGlobal', setGlobal,
			'singleton', singleton,
			'type', type,
			'_coreEntry', coreEntry,
			'_fromJsonArgsOverride', fromJsonArgsOverride,
		);

	spec._validate( def );
	return spec;
};

/*
| Types of all other ti2c classes this depends on.
*/
def.lazy.dependencies =
	function( )
{
	const attributes = this.attributes;
	const pkgName = this.type.pkgName;
	const abstract = this.abstract;
	const entryExtend = this.entryExtend;

	const set = new Set( );

	if( entryExtend ) set.add( entryExtend.type );

	for( let spec of this._coreEntry.importSpecifiers )
	{
		set.add( TypeTi2cAny.FromString( spec, this.manager, pkgName ) );
	}

	if(
		this.jsonName
		&& !this.customJsonfy
		&& attributes.hasSubJsonfy
	)
	{
		set.add( TypeTi2cClass.PkgName( 'ti2c', 'Jsonfy/Spacing' ) );
	}


	if( !abstract )
	{
		for( let attr of attributes )
		{
			for( let t of attr.types.filterTi2cTypes )
			{
				set.add( t );
			}

			const advd = attr.defaultValueDepends;
			if( advd )
			{
				for( let t of advd ) set.add( t );
			}
		}
	}

	return TypeSet.create( 'set:init', set );
};

/*
| The number of classes this class extends.
*/
def.lazy.extendLevel =
	function( )
{
	const entryExtend = this.entryExtend;

	return(
		entryExtend
		? entryExtend.extendLevel + 1
		: 0
	);
};

/*
| FIXME.
*/
def.lazy.fromJsonArgs =
	function( )
{
	const fjaOv = this._fromJsonArgsOverride;
	if( fjaOv ) return fjaOv;
	if( this.extendCollection ) return this.entryExtend.fromJsonArgs;
	return ListString.Empty;
};

/*
| True if this class has jsonfy()/FromJson() handles.
| FIXME remove.
*/
def.lazy.hasJson =
	function( )
{
	return !!this.jsonName;
};

/*
| Set of all import specifieres used.
*/
def.lazy.importSpecifiers =
	function( )
{
	return SetString.create( 'set:init', this._coreEntry.importSpecifiers );
};

/*
| Generates the alike test(s).
*/
def.lazy._alikes =
	function( )
{
	const alikes = this.alikes;
	let cond;
	let result = Ast.block;

	for( let alikeName of alikes.keys )
	{
		const alike = alikes.get( alikeName );
		const ignores = alike.ignores;

		result = result.comment( 'Tests partial equality.' );

		let block =
			Ast.block
			.$( 'if( this === obj ) return true;' )
			.$( 'if( !obj ) return false;' );

		const attributes = this.attributes;
		for( let name of attributes.keys )
		{
			if( ignores.has( name ) ) continue;

			const attr = attributes.get( name );
			const ceq = Ast.$( 'this.', attr.name, ' === ', 'obj.', attr.name );
			cond =
				cond
				? Ast.$( cond, '&&', ceq )
				: ceq;
		}

		block = block.$( 'return', cond );

		result =
			result
			.assign(
				Ast.$( 'prototype.', alikeName ),
				Ast.func( block ).arg( 'obj', 'object to compare to' )
			);
	}

	return result;
};

/*
| Generates the constructor.
*/
def.lazy._constructor =
	function( )
{
	let block = Ast.block;

	if( this.hasLazy ) block = block.$( 'this.__lazy = { }' );
	if( !this.abstract ) block = block.$( 'this.__hash = hash' );

	const attributes = this.attributes;

	// assigns the variables
	for( let attr of attributes )
	{
		const assign = Ast.$( 'this.', attr.name, ' = ', attr.varname );
		block = block.append( assign );
	}

	block = block.$( 'Object.freeze( this )' );

	// FUTURE force freezing date attributes
	// calls init checker
	if( this.check )
	{
		block = block.check( 'this._check( )' );
	}

	let cf = Ast.func( block );
	for( let name of this._constructorList )
	{
		switch( name )
		{
			case 'hash':
				cf = cf.arg( 'hash', 'hash value' ); break;
			default:
				cf = cf.arg( name );
				break;
		}
	}

	block =
		Ast.block
		.comment( 'Constructor.' )
		.$( 'const Constructor =', cf )
		.comment( 'In case of checking all unknown access is to be trapped.' )
		.check(
			Ast.block
			.$( 'const Trap = ', Ast.func( Ast.block ) )
			.$( 'Trap.prototype = ti2c.trap' )
			.$( 'Constructor.prototype = new Trap( )' )
		)
		.comment( 'Constructor prototype.' )
		.$( 'const prototype = Self.prototype = Constructor.prototype' );

	if( !this.abstract )
	{
		block = block.$( 'Self.__hash = ', this.hash );
	}

	return block;
};

/*
| Arguments for the constructor.
*/
def.lazy._constructorList =
	function( )
{
	const attributes = this.attributes;
	const aKeys = attributes.keys;
	const constructorList = [ ];

	if( !this.abstract )
	{
		constructorList.push( 'hash' );
	}

	for( let key of aKeys )
	{
		constructorList.push( 'v_' + key );
	}

	return ListString.Array( constructorList );
};

/*
| Generates the creator.
*/
def.lazy._creator =
	function( )
{
	let block =
		Ast.block
		.$( this._creatorVariables )
		.$( this._creatorInheritanceReceiver )
		.$( this._creatorFreeStringsParser )
		.$( this._creatorDefaults )
		.$( this._creatorChecks( false ) )
		.$(
			!this.singleton
			? this._creatorIntern
			: Ast.$( 'const hash =', this.hash )
		)
		.$( this._creatorReturn );

	const creator = Ast.func( block ).arg( 'args', 'free strings', '...' );

	return(
		Ast.block
		.comment( 'Creates a new object.' )
		.$(
			'Self.', this.creator, ' = prototype.',
			this.creator, ' = ', creator
		)
	);
};

/*
| Generates the creators checks.
|
| ~json: if true do checks for fromJsonCreator
*/
def.lazyFunc._creatorChecks =
	function( json )
{
	let check = Ast.block;

	for( let attr of this.attributes )
	{
		if( json && !attr.json ) continue;
		if( attr.types.ti2ctype === TypeProtean ) continue;
		const tcheck = this._typeCheckFailCondition( attr.varname, attr.types );
		if( tcheck )
		{
			check = check.$( 'if(', tcheck, ') throw new Error( ); ' );
		}
	}

	return(
		json
		? check
		: ( check.statements.length > 0 ? Ast.block.check( check ) : Ast.block )
	);
};

/*
| Generates the creators default values
*/
def.lazy._creatorDefaults =
	function( )
{
	let result = Ast.block;
	for( let attr of this.attributes )
	{
		const dve = attr.defaultValueExpr;
		if( dve !== undefined && dve !== Ast.undefined )
		{
			result =
				result
				.$(
					'if( ', attr.varname, ' === undefined )',
					attr.varname, ' = ', dve, ';'
				);
		}
	}
	return result;
};

/*
| Generates the creators free strings parser.
*/
def.lazy._creatorFreeStringsParser =
	function( )
{
	// possibly doesn't have any
	if( this.attributes.size === 0 )
	{
		return undefined;
	}

	let loop = Ast.block.let( 'arg', 'args[ a + 1 ]' );
	let _switch = Ast.switch( 'args[ a ]' );

	for( let attr of this.attributes )
	{
		_switch =
			_switch
			.case( attr.$name,
				'{',
					'if( arg !== pass ) {', attr.varname, ' = arg; }',
					'break;',
				'}'
			);
	}

	_switch = _switch.default( 'throw new Error( args[ a ] )' );
	loop = loop.append( _switch );

	return(
		Ast.block
		.$( 'for( let a = 0, alen = args.length; a < alen; a += 2 )', loop )
	);
};

/*
| Generates the creators inheritance receiver.
*/
def.lazy._creatorInheritanceReceiver =
	function( )
{
	const attributes = this.attributes;
	let receiver = Ast.block;
	for( let attr of attributes )
	{
		const val = Ast.$( 'this.', attr.name );
		receiver = receiver.$( attr.varname, ' = ', val );
	}

	if( receiver === Ast.block )
	{
		return undefined;
	}

	let result = Ast.$( 'if ( this !== Self )', receiver, ';' );
	return result;
};

/*
| Generates the equality test for creator interning.
*/
def.lazyFunc._creatorInternEqTest =
	function( action )
{
	// equality testing
	const list = [ ];
	for( let arg of this.attributes )
	{
		list.push( Ast.$( arg.varname, ' === ', 'e.', arg.name ) );
	}

	if( list.length === 0 ) throw new Error( );

	const eqTest =
		list.length === 1
		? list[ 0 ]
		: Ast.and.apply( undefined, list );

	return Ast.if( eqTest, action );
};

/*
| Generates the hash for creator interning.
*/
def.lazy._creatorInternHash =
	function( )
{
	// hash making
	let call = Ast.$( 'ti2c._hashArgs(', this.hash, ')' );
	for( let attr of this.attributes )
	{
		call = call.arg( Ast.var( attr.varname ) );
	}
	return Ast.block.$( 'const hash =', call );
};

/*
| Generates the creators return statement
*/
def.lazy._creatorReturn =
	function( )
{
	if( this.singleton )
	{
		return(
			Ast.block
			.$(
				'if( !_singleton ) _singleton = new Constructor('
				+ ( this.abstract ? ' ' : ' hash ' )
				+ ');'
			)
			.$( 'return _singleton' )
		);
	}

	let call = Ast.$( 'Constructor( )' );
	for( let argName of this._constructorList )
	{
		call = call.arg( argName );
	}

	let block =
		Ast.block
		.$( 'const newti2c = new', call, ';' );

	if( !this.abstract )
	{
		block = block.$( 'imap.set( uhash, new WeakRef( newti2c ) )' );
	}

	const setGlobal = this.setGlobal;
	if( setGlobal )
	{
		block = block.$( 'globalThis.', setGlobal, '= newti2c' );
	}

	block = block.if( 'cSize', 'cPut( newti2c )' );
	return block.$( 'return newti2c' );
};

/*
| Generates the creators variable list.
*/
def.lazy._creatorVariables =
	function( )
{
/**/if( CHECK && arguments.length !== 0 ) throw new Error( );
	const varlist = [ ];
	for( let attr of this.attributes )
	{
		varlist.push( attr.varname );
	}
	varlist.sort( );
	let result = Ast.block;
	for( let varname of varlist )
	{
		result = result.let( varname );
	}
	return result;
};

/*
| Generates the fromJsonCreator's variable list.
*/
def.lazy._fromJsonCreatorVariables =
	function( )
{
	const varlist = [ ];
	for( let attr of this.attributes )
	{
		varlist.push( attr.varname );
	}
	varlist.sort( );
	let result = Ast.block;
	for( let varname of varlist )
	{
		result = result.let( varname );
	}
	return result;
};

/*
| Generates a fromJsonCreator's json parser for one attribute
*/
def.proto._fromJsonCreatorAttributeParser =
	function( attr )
{
	// handles attributes that can have only one type
	switch( attr.types.ti2ctype )
	{
		case TypeBoolean:
		case TypeInteger:
		case TypeNumber:
		case TypeString:
			return Ast.$( attr.varname, ' = arg' );

		case TypeTi2cClass:
		{
			const ja = attr.types.entry.fromJsonArgs;
			let call = Ast.$( attr.types.var, '.FromJson( arg )' );
			for( let a of ja )
			{
				call = call.arg( a );
			}
			return Ast.$( attr.varname, ' = ', call );
		}
	}

	// the code switch
	let cSwitch;
	// primitive checks
	const pcs = [ ];
	// table of already generated Json types
	for( let type of attr.types )
	{
		switch( type.ti2ctype )
		{
			case TypeBoolean:
				pcs.push( 'typeof( arg ) === "boolean"' );
				break;

			case TypeNull:
				pcs.push( 'arg === null' );
				break;

			case TypeNumber:
			case TypeInteger:
				pcs.push( 'typeof( arg ) === "number"' );
				break;

			case TypeString:
				pcs.push( 'typeof( arg ) === "string"' );
				break;

			case TypeUndefined:
				pcs.push( 'arg === undefined' );
				break;

			case TypeTi2cClass:
			case TypeTi2cGroup:
			case TypeTi2cList:
			case TypeTi2cSet:
			case TypeTi2cTwig:
			{
				if( !cSwitch )
				{
					cSwitch =
						Ast
						.switch( 'arg.$type' )
						.default( 'throw new Error( );' );
				}

				let jsonName = this.manager.getEntry( type ).jsonName;
				if( !jsonName )
				{
					throw new Error(
						'"' + this.manager.getEntry( type ).aPathDef.asString + '"'
						+ ' expected to have a def.json'
					);
				}

				let call = Ast.$( type.varname, '.FromJson( arg )' );
				const ja = this.manager.getEntry( type ).fromJsonArgs;
				for( let a of ja )
				{
					call = call.arg( a );
				}

				cSwitch =
					cSwitch
					.case(
						Ast.string( jsonName ),
						Ast.block
						.$( attr.varname, ' = ', call )
						.break
					);
				break;
			}

			default: throw new Error( type.ti2ctype.__TI2C_NAME__ );
		}
	}

	const pcsl = pcs.length;
	if( pcsl === 0 )
	{
		return cSwitch;
	}

	let cond = Ast.expr( pcs[ 0 ] );
	for( let a = 1; a < pcsl; a++ )
	{
		cond = Ast.or( cond, Ast.expr( pcs[ a ] ) );
	}

	const pcif = Ast.$( 'if( ', cond, ' )', attr.varname, ' = arg;' );

	if( !cSwitch ) return pcif;

	return pcif.setElsewise( cSwitch );
};

/*
| Generates the fromJsonCreator's json parser.
*/
def.proto._fromJsonCreatorParser =
	function( jsonList )
{
	const attributes = this.attributes;
	// the switch
	let nameSwitch =
		Ast.switch( 'name' )
		.case(
			'"$type"',
			Ast.block
			.$( 'if( arg !==', Ast.string( this.jsonName ), ') throw new Error( );' )
			.break
		);

	for( let name of jsonList )
	{
		if( Self._fromJsonCreatorParserIgnores[ name ] ) continue;
		const attr = attributes.get( name );
		nameSwitch =
			nameSwitch
			.case(
				Ast.string( attr.name ),
				Ast.block
				.$( this._fromJsonCreatorAttributeParser( attr ) )
				.break
			);
	}

	return(
		Ast.block
		.$( 'for( let name in json )',
			Ast.block
			.$( 'const arg = json[ name ]' )
			.append( nameSwitch )
		)
	);
};

/*
| Ignored attributes in genFromJsonCreatorParser
*/
def.staticLazy._fromJsonCreatorParserIgnores =
	( ) => ( {
		group: true,
		keys: true,
		list: true,
		set: true,
		twig: true
	} );

/*
| Generates the fromJsonCreator's return statement.
*/
def.lazy._fromJsonCreatorReturn =
	function( )
{
	if( this.singleton ) return Ast.$( 'return Self.singleton' );

	let call = Ast.$( 'Constructor( )' );

	for( let name of this._constructorList )
	{
		switch( name )
		{
			case 'hash':
				call = call.arg( 'hash' );
				break;

			case 'groupDup':
			case 'listDup':
			case 'twigDup':
				call = call.arg( 'true' );
				break;

			case 'group':
			case 'keys':
			case 'list':
			case 'set':
			case 'twig':
				call = call.arg( name );
				break;

			default:
				call = call.arg( name );
				break;
		}
	}

	let block = Ast.block.$( 'const newtim = new', call );
	if( !this.abstract )
	{
		block = block.$( 'imap.set( uhash, new WeakRef( newtim ) )' );
	}
	return block.$( 'return newtim' );
};

/*
| Generates the fromJsonCreator.
*/
def.lazy._fromJsonCreator =
	function( )
{
	// all attributes expected from json
	const jsonList = [ ];
	for( let attr of this.attributes )
	{
		if( attr.json ) jsonList.push( attr.name );
	}

	jsonList.sort( );

	// function contents
	let block =
		Ast.block
		.check(
			'if( arguments.length !== '
			+ ( this.fromJsonArgs.length + 1 )
			+ ' ) throw new Error( );'
		)
		.$( this._fromJsonCreatorVariables )
		.$( this._fromJsonCreatorParser( jsonList ) )
		.$( this._creatorDefaults )
		.$( this._creatorChecks( true ) )
		.$(
			!this.singleton
			? this._creatorIntern
			: undefined
		)
		.$( this._fromJsonCreatorReturn );

	let func = Ast.func( block ).arg( 'json' );
	for( let a of this.fromJsonArgs )
	{
		func = func.arg( a );
	}

	return(
		Ast.block
		.comment( 'Creates a new object from json.' )
		.$( 'Self.FromJson =', func )
	);
};

/*
| Generates the json type identifier.
*/
def.lazy._jsonType =
	function( )
{
	const fja = this.fromJsonArgs;

	let entries = [ ];
	for( let a of fja )
	{
		entries.push( Ast.string( a ) );
	}
	const astFja = ArrayLiteral.Array( entries );

	return(
		Ast.block
		.comment( 'Json type identifier' )
		.$( 'Self.$type = prototype.$type =', Ast.string( this.jsonName ) )
		.$( 'Self.$fromJsonArgs = prototype.$fromJsonArgs =', astFja )
	);
};

/*
| Generates the proto stuff.
*/
def.lazy._proto =
	function( )
{
	// FIXME remove
	return Ast.block;
};


/*
| Generates code for setting the prototype
| lazy value named 'name' to 'func'.
|
| ~name: lazy value name as string
| ~func: lazy value function name
*/
def.proto._protoLazyValueSet =
	function( name, func )
{
	return(
		Ast.block
		.$( 'ti2c._proto.lazyValue( prototype,', Ast.string( name ), ', ti2c._proto.', func, ')' )
	);
};

/*
| Generates code for setting the prototype
| entry 'key' to 'value'
*/
def.proto._protoSet =
	function( key, value )
{
	return Ast.$( 'prototype.', key, ' = ti2c._proto.', value );
};

/*
| Generates the imports code.
*/
def.lazy._astImports =
	function( )
{
	if( this.abstract ) return Ast.block;

	const attributes = this.attributes;
	let imports = TypeSet.create( );

	if( attributes )
	{
		for( let attr of attributes )
		{
			imports = imports.addX( attr.types );
			imports = imports.addSet( attr.types.filterTi2cTypes );
			const advd = attr.defaultValueDepends;
			if( advd ) imports = imports.addSet( advd );
		}
	}

	// FIXME make this a lazy value if jsonfy is used
	if(
		this.jsonName
		&& !this.customJsonfy
		&& attributes.hasSubJsonfy
	)
	{
		imports = imports.add( TypeTi2cClass.PkgName( 'ti2c', 'Jsonfy/Spacing' ) );
	}

	let block = Ast.block;
	for( let type of imports )
	{
		if( type.isTi2c && type !== TypeTi2cArbitrary.singleton )
		{
			block = block.import( 'Self', type.varname, '{{' + type.asString + '}}' );
		}
	}
	return block;
};

/*
| Generates the singleton decleration.
*/
def.lazy._singleton =
	function( )
{
	return(
		Ast.block
		.comment( 'Singleton.' )
		.let( '_singleton' )
		.$(
			'ti2c._proto.lazyStaticValue( Self, "singleton", ',
			Ast.func( 'return Self._create( );' ),
			')'
		)
	);
};

/*
| Generates a type check of a non set variable.
| It is true if the variable fails the check.
|
| ~varname: name of the variable
| ~type:    type to check for
*/
def.proto._singleTypeCheckFailCondition =
	function( varname, type )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 ) throw new Error( );
/**/	if( typeof( varname ) !== 'string' ) throw new Error( );
/**/}

	switch( type.ti2ctype )
	{
		case TypeArbitrary:
			return Ast.$( varname, '?.ti2ctype === undefined' );

		case TypeBoolean:
			return Ast.$( 'typeof( ', varname, ' ) !== "boolean"' );

		case TypeDate:
			return Ast.$( '!(', varname, 'instanceof Date )' );

		case TypeFunction:
			return Ast.$( 'typeof( ', varname, ' ) !== "function"' );

		case TypeInteger:
			return Ast.$(
				Ast.$( 'typeof( ', varname, ' ) !== "number"' ),
				'|| Number.isNaN( ', varname, ' )',
				'|| Math.floor( ', varname, ' ) !== ', varname
			);

		case TypeNull:
			return Ast.$( varname, '!== null' );

		case TypeNumber:
			return Ast.$(
				'typeof( ', varname, ' ) !== "number"',
				'|| Number.isNaN( ', varname, ' )'
			);

		case TypeProtean:
			return Ast.$( 'typeof( ', varname, ' ) !== "object"' );

		case TypeString:
			return Ast.$( 'typeof( ', varname, ' ) !== "string"' );

		case TypeTi2cClass:
		case TypeTi2cGroup:
		case TypeTi2cList:
		case TypeTi2cSet:
		case TypeTi2cTwig:
			return Ast.$( varname, '.ti2ctype !== ', type.varname );

		case TypeUndefined:
			return Ast.$( varname, '!== undefined' );

		default: throw new Error( );
	}
	// unreachable
};

/*
| Generates the jsonfy converter.
*/
def.lazy._jsonfy =
	function( )
{
	const attributes = this.attributes;
	const jsonName = this.jsonName;

	let block = Ast.block;
	if( this.customJsonfy ) return block;

	// jsonfy code to build return value
	let hasSubJsonfy;
	let hasI2Space = false;
	hasSubJsonfy = this.attributes.hasSubJsonfy;

	let jr = Ast.block.$( 'let i0space, i1space' );
	if( hasI2Space )
	{
		jr = jr.$( 'let i2space' );
	}

	if( hasSubJsonfy )
	{
		const jsonfySpacing = TypeTi2cClass.PkgName( 'ti2c', 'Jsonfy/Spacing' );

		jr = jr
			.$( 'let iSpacing' )
			.$( 'if( spacing === undefined )',
				'{',
					'i0space = i1space = "";',
					hasI2Space ? 'i2space = "";' : undefined,
				'}',
				'else if( typeof( spacing ) === "string" )',
				'{',
					'i0space = "\\n";',
					'i1space = "\\n" + spacing;',
					hasI2Space ? 'i2space = i1space + spacing;' : undefined,
					'iSpacing =',
						jsonfySpacing.varname, '.',
						hasI2Space ? 'Start2' : 'Start1', '( spacing );',
				'}',
				'else',
				'{',
					'i0space = "\\n" + spacing.level;',
					'i1space = i0space + spacing.step;',
					hasI2Space ? 'i2space = i1space + spacing.step;' : undefined,
					'iSpacing = spacing.', hasI2Space ? 'Inc2;' : 'Inc1;',
				'}'
			);
	}
	else
	{
		jr = jr
			.$(
				'if( spacing === undefined )',
				'{',
					'i0space = i1space = "";',
					hasI2Space ? 'i2space = "";' : undefined,
				'}',
				'else if( typeof( spacing ) === "string" )',
				'{',
					'i0space = "\\n";',
					'i1space = i0space + spacing;',
					hasI2Space ? 'i2space = i1space + spacing;' : undefined,
				'}',
				'else',
				'{',
					'i0space = "\\n" + spacing.level;',
					'i1space = i0space + spacing.step;',
					hasI2Space ? 'i2space = i1space + spacing.step;' : undefined,
				'}'
			);
	}

	jr = jr.$( 'const colon = i1space !== "" ? ": " : ":"' );

	jr = jr.$(
		'let r = "{" + i1space +',
		Ast.string( '"$type"' ), '+ colon +', Ast.string( '"' + jsonName + '"' )
	);

	let keys = attributes.keys;
	let first = true;
	for( let name of keys )
	{
		const attr = attributes.get( name );
		if( !attr.json ) continue;
		if( first ){ first = false; jr = jr.$( 'let val = this.', name ); }
		else jr = jr.$( 'val = this.', name );
		const jrb =
			Ast.block
			.$( 'r += "," + i1space +', Ast.string( '"' + name + '"' ), '+ colon' )
			.$( 'r +=', attr.types.jsonfyCode( 'val' ) );

		jr =
			attr.types.has( TypeUndefined.singleton )
			? jr.$( 'if( val !== undefined )', jrb )
			: jr.$( jrb );
	}
	jr = jr.$( 'r += i0space + "}"' );
	jr = jr.$( 'return r' );
	const fjr = Ast.func( jr ).arg( 'spacing' );
	return(
		block
		.comment( 'Stable jsonfy' )
		.$( 'ti2c._proto.lazyFunction( prototype, "jsonfy", ', fjr, ')' )
	);
};

/*
| Generates a type check of a variable.
|
| ~varname: the variable name to check
| ~types:   the type or TypeSet it has to match
*/
def.proto._typeCheckFailCondition =
	function( varname, types )
{
/**/if( CHECK && arguments.length !== 2 ) throw new Error( );

	if( types.ti2ctype !== TypeSet ) return this._singleTypeCheckFailCondition( varname, types );
	if( types.size === 1 ) return this._singleTypeCheckFailCondition( varname, types.trivial );
	const condArray = [ ];

	// first do the primitives
	for( let type of types )
	{
		if( type.ti2ctype === TypeTi2cClass ) continue;
		const cond = this._singleTypeCheckFailCondition( varname, type );
		if( cond )
		{
			condArray.push( cond );
		}
	}

	// then do the ti2c types
	for( let type of types )
	{
		if( type.ti2ctype !== TypeTi2cClass ) continue;
		const cond = this._singleTypeCheckFailCondition( varname, type );
		if( cond )
		{
			condArray.push( cond );
		}
	}

	switch( condArray.length )
	{
		case 0:  return undefined;
		case 1:  return condArray[ 0 ];
		default: return Ast.and.apply( undefined, condArray );
	}
};

/*
| Throws an error if something doesn't seem right
| with the spec. This part of the validation is done
| after the spec is created.
|
| ~def: the definition
*/
def.proto._validate =
	function( def )
{
	const attr = def.attributes;
	if( attr )
	{
		for( let name in attr )
		{
			if( Self._validateAttributeBlocklist[ name ] )
			{
				throw new Error( 'attribute must not be named "' + name + '"' );
			}
		}
	}

	if(
		def.proto.jsonfy !== undefined
		|| def.static.jsonfy !== undefined
		|| def.staticLazy.jsonfy !== undefined
		|| def.lazy.jsonfy !== undefined
	) throw new Error( '"jsonfy" must be lazyFunc' );

	if(
		def.proto.FromJson !== undefined
		|| def.lazy.FromJson !== undefined
		|| def.staticLazy.FromJson !== undefined
		|| def.lazyFunc.FromJson !== undefined
	) throw new Error( '"FromJson" must be static' );
};

/*
| Attributes must not be named like these.
*/
def.static._validateAttributeBlocklist =
	Object.freeze( { create: true } );

