/*
| Specifications of a group of alike functions.
*/
def.extend = 'group@ti2c:Package/Entry/Class/Alike/Self';

import { Self as Alike } from '{ti2c:Package/Entry/Class/Alike/Self}';

/*
| Defines a group of alike functions.
|
| ~protean: protean of the definition.
*/
def.static.Define =
	function( protean )
{
	const keys = Object.keys( protean );
	const group = { };

	for( let key of keys )
	{
		group[ key ] = Alike.Define( protean[ key ] );
	}

	return Self.Table( group );
};
