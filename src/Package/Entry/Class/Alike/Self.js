/*
| Specifications of an alike functions.
*/
def.attributes =
{
	// true if this ti2c class cannot be instanced itself
	ignores: { type: 'set@string' },
};

import { Self as SetString } from '{set@string}';

/*
| Defines the alike function
|
| ~protean: protean of the definition
*/
def.static.Define =
	function( protean )
{
	return(
		Self.create(
			'ignores', SetString.create( 'set:init', new Set( Object.keys( protean.ignores ) ) )
		)
	);
};
