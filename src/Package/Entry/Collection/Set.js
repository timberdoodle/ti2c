/*
| Specifications of a ti2c set collection.
*/
def.attributes =
{
	// type identifier
	type: { type: 'Type/Ti2c/Collection/Set' },
};

def.extend = 'Package/Entry/Collection/Base';

import { Self as Ast           } from '{ti2c:Ast/Self}';
import { Self as ListString    } from '{list@string}';
import { Self as TypeBoolean   } from '{ti2c:Type/Primitive/Boolean}';
import { Self as TypeNull      } from '{ti2c:Type/Primitive/Null}';
import { Self as TypeNumber    } from '{ti2c:Type/Primitive/Number}';
import { Self as TypeSet       } from '{ti2c:Type/Set}';
import { Self as TypeString    } from '{ti2c:Type/Primitive/String}';
import { Self as TypeUndefined } from '{ti2c:Type/Primitive/Undefined}';
import { Self as TypeTi2cClass } from '{ti2c:Type/Ti2c/Class}';
import { Self as TypeTi2cList  } from '{ti2c:Type/Ti2c/Collection/List}';

/*
| Creates an entry.
|
| ~coreEntry:  entry in the core
| ~type:       entry type
| ~codeGenDir: directory for generated directory
*/
def.static.Define =
	async function( coreEntry, type, codeGenDir, manager )
{
	const aPathCodeGen = codeGenDir.f( type.asFileString + '.js' );
	const typeItemsExpanded = await type.typeItems.expand( manager );

	return(
		Self.create(
			'aPathCodeGen', aPathCodeGen,
			'jsonName', type.asString,
			'manager', manager,
			'type', type,
			'typeItemsExpanded', typeItemsExpanded,
			'_coreEntry', coreEntry,
		)
	);
};

/*
| Generates the constructor.
*/
def.lazy._constructor =
	function( )
{
	let block =
		Ast.block
		.$( 'this.__lazy = { }' )
		.$( 'this.__hash = hash' )
		.$( 'this._set = set' )
		.$( 'Object.freeze( this )' )
		.$( 'Object.freeze( set )' );

	let cf = Ast.func( block );
	for( let name of this._constructorList )
	{
		switch( name )
		{
			case 'hash': cf = cf.arg( 'hash', 'hash value' ); break;
			case 'set': cf = cf.arg( 'set', 'set' ); break;
			default: throw new Error( );
		}
	}

	return(
		Ast.block
		.comment( 'Constructor.' )
		.$( 'const Constructor =', cf )
		.comment( 'In case of checking all unknown access is to be trapped.' )
		.check(
			Ast.block
			.$( 'const Trap = ', Ast.func( Ast.block ) )
			.$( 'Trap.prototype = ti2c.trap' )
			.$( 'Constructor.prototype = new Trap( )' )
		)
		.comment( 'Constructor prototype.' )
		.$( 'const prototype = Self.prototype = Constructor.prototype' )
		.$( 'Self.__hash = ', this.hash )
	);
};

/*
| Arguments for the constructor.
*/
def.lazy._constructorList =
	function( )
{
	return ListString.Elements( 'hash', 'set' );
};

/*
| Generates the creators checks.
|
| ~json: do checks for fromJsonCreator
*/
def.lazyFunc._creatorChecks =
	function( json )
{
	let check = Ast.block;
	check =
		check
		.$(
			'for( let v of set )',
			Ast.block
			.$( 'if( ',
				this._typeCheckFailCondition( Ast.$( 'v' ), this.typeItemsExpanded ),
				') throw new Error( );'
			)
		);

	return(
		json
		? check
		: ( check.statements.length > 0 ? Ast.block.check( check ) : Ast.block )
	);
};

/*
| Generates the creators free strings parser.
*/
def.lazy._creatorFreeStringsParser =
	function( )
{
	let loop = Ast.block.let( 'arg', 'args[ a + 1 ]' );
	let _switch = Ast.switch( 'args[ a ]' );

	const dupCheck =
		Ast.$( '{ if( !setDup ) { set = new Set( set ); setDup = true; } }' );

	_switch =
		_switch
		.case( '"set:add"', dupCheck.$( 'set.add( arg, args[ a + 1 ] )' ).break )
		.case(
			'"set:init"',
			Ast.block
			.check( 'if( !( arg instanceof Set ) ) throw new Error( );' )
			.$( 'set = arg' )
			.$( 'setDup = true' )
			.break
		)
		.case( '"set:remove"', dupCheck.$( 'set.delete( arg )' ).break );

	_switch = _switch.default( 'throw new Error( args[ a ] )' );
	loop = loop.append( _switch );

	return(
		Ast.block
		.$( 'for( let a = 0, alen = args.length; a < alen; a += 2 )', loop )
	);
};

/*
| Generates the creators inheritance receiver.
*/
def.lazy._creatorInheritanceReceiver =
	function( )
{
	let receiver = Ast.block;
	receiver =
		receiver
		.$( 'set = this._set' )
		.$( 'setDup = false' );

	let result = Ast.$( 'if ( this !== Self )', receiver, ';' );

	return result.setElsewise( '{ set = new Set( ); setDup = true; }' );
};

/*
| Generates the equality test for creator interning.
|
| ~action: ast action to do
*/
def.lazyFunc._creatorInternEqTest =
	function( action )
{
	return(
		Ast.block
		.$( 'const eset = e._set' )
		.if( 'set.size !== eset.size', 'continue' )
		.$( 'let eq = true' )
		.forOfLet(
			'e', 'set',
			Ast.block
			.if( '!eset.has( e )', Ast.block.$( 'eq = false' ).$( 'break' ) )
		)
		.if( 'eq', action )
	);
};

/*
| Generates the hash for creator interning.
*/
def.lazy._creatorInternHash =
	function( )
{
	// for a set hash values need to be sorted since there is no order
	// in the set.
	return Ast.$( 'const hash = ti2c._hashSet(', this.hash, ', set )' );
};

/*
| Generates the creators variable list.
*/
def.lazy._creatorVariables =
	function( )
{
	const varlist = [ ];

	varlist.push( 'set', 'setDup' );

	varlist.sort( );
	let result = Ast.block;
	for( let varname of varlist )
	{
		result = result.let( varname );
	}
	return result;
};

/*
| Generates the fromJsonCreator's variable list.
*/
def.lazy._fromJsonCreatorVariables =
	function( )
{
	const varlist = [ ];
	varlist.push( 'jset', 'set' );
	varlist.sort( );
	let result = Ast.block;
	for( let varname of varlist )
	{
		result = result.let( varname );
	}
	return result;
};

/*
| Generates the fromJsonCreator's json parser.
*/
def.proto._fromJsonCreatorParser =
	function( jsonList )
{
	const aSwitch =
		Ast.switch( 'name' )
		.case(
			'"$type"',
			Ast.block
			.$( 'if( arg !== ', Ast.string( this.jsonName ), ') throw new Error( );' )
			.break
		)
		.case( '"set"', Ast.block.$( 'jset = arg' ).break )
		.default( 'throw new Error( )' );

	return(
		Ast.block
		.$( 'for( let name in json )',
			Ast.block
			.$( 'const arg = json[ name ]' )
			.$( aSwitch )
		)
	);
};

/*
| Generates the fromJsonCreator's set processing.
*/
def.lazy._fromJsonCreatorProcessing =
	function( )
{
	const typeItems = this.typeItemsExpanded;

	let loopBody = Ast.block;

	const result =
		Ast.block
		.$( 'if( !jset ) throw new Error( );' )
		.$( 'set = new Set( )' );

	let loopSwitch =
		Ast.switch( 'e.$type' )
		.default( 'throw new Error( )' );

	for( let type of typeItems )
	{
		switch( type )
		{
			case TypeNull.singleton:
				loopBody = loopBody.$( 'if( e === null ) { set.add( null ); continue; }' );
				continue;
			case TypeUndefined.singleton:
				loopBody =
					loopBody.$( 'if( e === undefined ) { set.add( undefined ); continue; }' );
				continue;
			case TypeBoolean.singleton:
				loopBody =
					loopBody.$( 'if( typeof( e ) === "boolean" ) { set.add( e ); continue; }' );
				continue;
			case TypeNumber.singleton:
				loopBody =
					loopBody.$( 'if( typeof( e ) === "number" ) { set.add( e ); continue; }' );
				continue;
			case TypeString.singleton:
				loopBody =
					loopBody.$( 'if( typeof( e ) === "string" ) { set.add( e ); continue; }' );
				continue;
		}

		const jsonName = this.manager.getEntry( type ).jsonName;
		if( !jsonName ) throw new Error( );

		const ja = this.manager.getEntry( type ).fromJsonArgs;
		let call = Ast.$( Ast.var( type.varname ), '.FromJson( e )' );
		for( let a of ja )
		{
			call = call.arg( a );
		}

		loopSwitch =
			loopSwitch
			.case(
				Ast.string( jsonName ),
				Ast.block.$( 'set.add( ', call, ')' ).break
			);
	}

	loopBody =
		loopSwitch.cases.length > 0
		? loopBody.$( loopSwitch )
		: loopBody.$( 'throw new Error( )' );

	return result.$( 'for( let e of jset )', loopBody, ';' );

	/*
	// FUTURE dirty workaround
	if(
		typeItems.size !== 1
		|| (
			typeItems.trivial.ti2ctype !== TypeNumber
			&& typeItems.trivial.ti2ctype !== TypeString
		)
	)
	{
		// sets json conversation not yet fully implemented
		return undefined;
	}

	return(
		Ast.block
		.$( 'if( !jset ) throw new Error( );' )
		.$( 'set = new Set( jset )' )
	);
	*/
};

/*
| Generates the proto stuff.
*/
def.lazy._proto =
	function( )
{
	let block =
		Ast.block
		.comment( 'Returns the set with one element added.' )
		.$( this._protoSet( 'add', 'setAdd' ) )
		.comment( 'Returns the set with another set added.' )
		.$( this._protoSet( 'addSet', 'setAddSet' ) )
		.comment( 'Returns a clone primitive.' )
		.$( 'prototype.clone = ', Ast.func( 'return new Set( this._set );' ) )
		.comment( 'Returns true if the set has an element.' )
		.$( this._protoSet( 'has', 'setHas' ) )
		.comment( 'Returns the set with one element removed.' )
		.$( this._protoSet( 'remove', 'setRemove' ) )
		.comment( 'Returns the size of the set.' )
		.$( this._protoLazyValueSet( 'size', 'setSize' ) )
		.comment( 'Returns the one and only element or the set if size != 1.' )
		.$( this._protoLazyValueSet( 'trivial', 'setTrivial' ) )
		.comment( 'Forwards the iterator.' )
		.$(
			'prototype[ Symbol.iterator ] =',
			Ast.func( 'return this._set[ Symbol.iterator ]( )' ),
		)
		.comment( 'Creates the set with direct elements.' )
		.$( 'Self.Elements =',
			Ast.func( 'return Self.create( "set:init", new Set( arguments ) );' ),
		)
		.comment( 'Creates an empty set of this type.' )
		.$( 'ti2c._proto.lazyStaticValue( Self, "Empty", ',
			Ast.func( 'return Self.create( );' ),
			')',
		);

	// adds special shortcuts for set@string
	const items = this.typeItemsExpanded;
	if(
		items.size === 1
		&& items.trivial.ti2ctype === TypeString
	)
	{
		const typeList =
			TypeTi2cList.create( 'typeItems', TypeSet.Elements( TypeString.singleton ) );

		block =
			block
			.comment( 'Turns the set into a sorted list.' )
			.$( 'ti2c._proto.lazyValue( prototype, "list", ',
				Ast.func(
					Ast.block
					.$( 'const a = Array.from( this._set )' )
					.$( 'a.sort( )' )
					.$( 'return ', typeList.varname ,'.Array( a )' )
				),
				')',
			);
	}

	return block;
};

/*
| Generates the jsonfy converter.
*/
def.lazy._jsonfy =
	function( )
{
	let block = Ast.block;

	// jsonfy code to build return value
	const hasSubJsonfy = this.typeItemsExpanded.hasTi2cType;
	const hasI2Space = true;

	let jr = Ast.block.$( 'let i0space, i1space' );
	if( hasI2Space ) jr = jr.$( 'let i2space' );
	if( hasSubJsonfy )
	{
		const jsonfySpacing = TypeTi2cClass.PkgName( 'ti2c', 'Jsonfy/Spacing' );
		jr = jr
			.$( 'let iSpacing' )
			.$( 'if( spacing === undefined )',
				'{',
					'i0space = i1space = "";',
					hasI2Space ? 'i2space = "";' : undefined,
				'}',
				'else if( typeof( spacing ) === "string" )',
				'{',
					'i0space = "\\n";',
					'i1space = "\\n" + spacing;',
					hasI2Space ? 'i2space = i1space + spacing;' : undefined,
					'iSpacing =',
						Ast.var( jsonfySpacing.varname ), '.',
						hasI2Space ? 'Start2' : 'Start1', '( spacing );',
				'}',
				'else',
				'{',
					'i0space = "\\n" + spacing.level;',
					'i1space = i0space + spacing.step;',
					hasI2Space ? 'i2space = i1space + spacing.step;' : undefined,
					'iSpacing = spacing.', hasI2Space ? 'Inc2;' : 'Inc1;',
				'}'
			);
	}
	else
	{
		jr = jr
			.$(
				'if( spacing === undefined )',
				'{',
					'i0space = i1space = "";',
					hasI2Space ? 'i2space = "";' : undefined,
				'}',
				'else if( typeof( spacing ) === "string" )',
				'{',
					'i0space = "\\n";',
					'i1space = i0space + spacing;',
					hasI2Space ? 'i2space = i1space + spacing;' : undefined,
				'}',
				'else',
				'{',
					'i0space = "\\n" + spacing.level;',
					'i1space = i0space + spacing.step;',
					hasI2Space ? 'i2space = i1space + spacing.step;' : undefined,
				'}'
			);
	}
	jr = jr.$( 'const colon = i1space !== "" ? ": " : ":"' );

	jr = jr
		.$(
			'let r = "{" + i1space +',
			Ast.string( '"$type"' ), '+ colon +', Ast.string( '"' + this.jsonName + '",' ),
			'+ i1space +', Ast.string( '"set"' ),
			'+ colon;'
		)
		.$( 'if( this.size === 0 ) { return r + "[ ]" + i0space + "}" }' )
		.$( 'const set = this._set;' )
		.$( 'const jstrs = [ ];' )
		.forOfLet( 'val', 'set',
			Ast.block
			.$( 'jstrs.push( ', this.typeItemsExpanded.jsonfyCode( 'val' ), ')' )
		)
		.$( 'jstrs.sort( );' )
		.$( 'r += "[" + i2space;' )
		.$( 'r += jstrs.join( "," +', hasI2Space ? 'i2space' : 'i1space', ');' )
		.$( 'r += i1space + "]" + i0space + "}";' );

	jr = jr.$( 'return r' );
	const fjr = Ast.func( jr ).arg( 'spacing' );
	return(
		block
		.comment( 'Stable jsonfy' )
		.$( 'ti2c._proto.lazyFunction( prototype, "jsonfy", ', fjr, ')' )
	);
};
