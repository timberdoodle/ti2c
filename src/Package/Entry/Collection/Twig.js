/*
| Specifications of a ti2c twig collection.
*/
def.attributes =
{
	// type identifier
	type: { type: 'Type/Ti2c/Collection/Twig' },
};

def.extend = 'Package/Entry/Collection/Base';

import { Self as Ast            } from '{ti2c:Ast/Self}';
import { Self as ListString     } from '{list@string}';
import { Self as TypeTi2cClass  } from '{ti2c:Type/Ti2c/Class}';

/*
| Creates an entry.
|
| ~coreEntry:  entry in the core
| ~type:       entry type
| ~codeGenDir: directory for generated directory
*/
def.static.Define =
	async function( coreEntry, type, codeGenDir, manager )
{
	const aPathCodeGen = codeGenDir.f( type.asFileString + '.js' );
	const typeItemsExpanded = await type.typeItems.expand( manager );

	return(
		Self.create(
			'aPathCodeGen', aPathCodeGen,
			'jsonName', type.asString,
			'manager', manager,
			'type', type,
			'typeItemsExpanded', typeItemsExpanded,
			'_coreEntry', coreEntry,
		)
	);
};

/*
| Generates the constructor.
*/
def.lazy._constructor =
	function( )
{
	let block =
		Ast.block
		.$( 'this.__lazy = { }' )
		.$( 'this.__hash = hash' )
		.$( 'this._twig = twig' )
		.$( 'this.keys = keys' )
		.$( 'Object.freeze( this )' )
		.$( 'Object.freeze( twig )' )
		.$( 'Object.freeze( keys )' );

	let cf = Ast.func( block );
	for( let name of this._constructorList )
	{
		switch( name )
		{
			case 'hash': cf = cf.arg( 'hash', 'hash value' ); break;
			case 'keys': cf = cf.arg( 'keys', 'twig key ranks' ); break;
			case 'twig': cf = cf.arg( 'twig', 'twig' ); break;
			default: throw new Error( );
		}
	}

	return(
		Ast.block
		.comment( 'Constructor.' )
		.$( 'const Constructor =', cf )
		.comment( 'In case of checking all unknown access is to be trapped.' )
		.check(
			Ast.block
			.$( 'const Trap = ', Ast.func( Ast.block ) )
			.$( 'Trap.prototype = ti2c.trap' )
			.$( 'Constructor.prototype = new Trap( )' )
		)
		.comment( 'Constructor prototype.' )
		.$( 'const prototype = Self.prototype = Constructor.prototype' )
		.$( 'Self.__hash = ', this.hash )
	);
};

/*
| Arguments for the constructor.
*/
def.lazy._constructorList =
	function( )
{
	return ListString.Elements( 'hash', 'keys', 'twig' );
};

/*
| Generates the creators checks.
|
| ~json: do checks for fromJsonCreator
*/
def.lazyFunc._creatorChecks =
	function( json )
{
	// FUTURE check if ranks and twig keys match
	let check =
		Ast.block
		.$(
			'for( let key of keys )',
			Ast.block
			.$( 'const o = twig[ key ]' )
			.$( 'if( ',
				this._typeCheckFailCondition( Ast.$( 'o' ), this.typeItemsExpanded ),
				') throw new Error( );'
			)
		);

	return(
		json
		? check
		: ( check.statements.length > 0 ? Ast.block.check( check ) : Ast.block )
	);
};

/*
| Generates the creators free strings parser.
*/
def.lazy._creatorFreeStringsParser =
	function( )
{
	let loop = Ast.block.let( 'arg', 'args[ a + 1 ]' );
	let _switch = Ast.switch( 'args[ a ]' );

	let dupCheck =
		Ast.$(
			'{ if( twigDup !== true )',
			'{',
			'  twig = ti2c.copy( twig );',
			'  keys = keys.slice( );',
			'  twigDup = true;',
			'} }'
		);

	_switch =
		_switch
		.case(
			'"twig:add"',
			dupCheck
			.$( 'key = arg' )
			.$( 'arg = args[ ++a + 1 ]' )
			.$( 'if( twig[ key ] !== undefined ) throw new Error( );' )
			.$( 'twig[ key ] = arg' )
			.$( 'keys.push( key )' )
			.break
		)
		.case(
			'"twig:init"',
			Ast.block
			.$( 'twigDup = true' )
			.$( 'twig = arg' )
			.$( 'keys = args[ ++a + 1 ]' )
			.check(
				Ast.block
				.$( 'if( !Array.isArray( keys ) ) throw new Error( );' )
				.$(
					'if( Object.keys( twig ).length !== keys.length ) throw new Error( );'
				)
				.$( 'for( let key of keys )',
					Ast.block
					.$( 'if( twig[ key ] === undefined ) throw new Error( );' )
				)
			)
			.break
		)
		.case(
			'"twig:insert"',
			dupCheck
			.$( 'key = arg' )
			.$( 'rank = args[ a + 2 ]' )
			.$( 'arg = args[ a +  3 ]' )
			.$( 'a += 2' )
			.$( 'if( twig[ key ] !== undefined ) throw new Error( );' )
			.$( 'if( rank < 0 || rank > keys.length ) throw new Error( );' )
			.$( 'twig[ key ] = arg' )
			.$( 'keys.splice( rank, 0, key )' )
			.break
		)
		.case(
			'"twig:remove"',
			dupCheck
			.$( 'if( twig[ arg ] === undefined ) throw new Error( );' )
			.$( 'delete twig[ arg ]' )
			.$( 'keys.splice( keys.indexOf( arg ), 1 )' )
			.break
		)
		.case(
			'"twig:set+"',
			dupCheck
			.$( 'key = arg' )
			.$( 'arg = args[ ++a + 1 ]' )
			.$( 'if( twig[ key ] === undefined ) keys.push( key );' )
			.$( 'twig[ key ] = arg' )
			.break
		)
		.case(
			'"twig:set"',
			dupCheck
			.$( 'key = arg' )
			.$( 'arg = args[ ++a + 1 ]' )
			.$( 'if( twig[ key ] === undefined ) throw new Error( );' )
			.$( 'twig[ key ] = arg' )
			.break
		)
		.default( 'throw new Error( args[ a ] )' );

	loop = loop.append( _switch );

	return(
		Ast.block
		.$( 'for( let a = 0, alen = args.length; a < alen; a += 2 )', loop )
	);
};

/*
| Generates the creators inheritance receiver.
*/
def.lazy._creatorInheritanceReceiver =
	function( )
{
	let receiver = Ast.block;
	receiver =
		receiver
		.$( 'twig = this._twig' )
		.$( 'keys = this.keys' )
		.$( 'twigDup = false' );

	let result = Ast.$( 'if ( this !== Self )', receiver, ';' );

	return result.setElsewise( '{ twig = { }; keys = [ ]; twigDup = true; }' );
};

/*
| Generates the equality test for creator interning.
|
| ~action: ast action to do
*/
def.lazyFunc._creatorInternEqTest =
	function( action )
{
	return(
		Ast.block
		.$( 'const len = keys.length' )
		.$( 'const ekeys = e.keys' )
		.$( 'const etwig = e._twig' )
		.if( 'keys.length !== ekeys.length', 'continue' )
		.$( 'let eq = true' )
		.for(
			'let a = 0', 'a < len', 'a++',
			Ast.block
			.$( 'const key = keys[ a ]' )
			.if( 'key !== ekeys[ a ] || twig[ key ] !== etwig[ key ]',
				Ast.block.$( 'eq = false' ).$( 'break' )
			)
		)
		.if( 'eq', action )
	);
};

/*
| Generates the hash for creator interning.
*/
def.lazy._creatorInternHash =
	function( )
{
	return Ast.$( 'const hash = ti2c._hashKeys(', this.hash, ', keys, twig )' );
};

/*
| Generates the creators variable list.
*/
def.lazy._creatorVariables =
	function( )
{
/**/if( CHECK && arguments.length !== 0 ) throw new Error( );
	const varlist = [ ];

	varlist.push( 'key', 'keys', 'rank', 'twig', 'twigDup' );

	varlist.sort( );
	let result = Ast.block;
	for( let varname of varlist )
	{
		result = result.let( varname );
	}
	return result;
};

/*
| Generates the fromJsonCreator's variable list.
*/
def.lazy._fromJsonCreatorVariables =
	function( )
{
	const varlist = [ ];
	varlist.push( 'jval', 'jwig', 'keys', 'twig' );

	varlist.sort( );
	let result = Ast.block;
	for( let varname of varlist )
	{
		result = result.let( varname );
	}
	return result;
};

/*
| Generates the fromJsonCreator's json parser.
*/
def.proto._fromJsonCreatorParser =
	function( jsonList )
{
	// the switch
	let nameSwitch =
		Ast.switch( 'name' )
		.case(
			'"$type"',
			Ast.block
			.$( 'if( arg !== ', Ast.string( this.jsonName ), ') throw new Error( );' )
			.break
		);

	nameSwitch =
		nameSwitch
		.case( '"twig"', Ast.block.$( 'jwig = arg' ).break )
		.case( '"keys"', Ast.block.$( 'keys = arg' ).break );

	return(
		Ast.block
		.$( 'for( let name in json )',
			Ast.block
			.$( 'const arg = json[ name ]' )
			.append( nameSwitch )
		)
	);
};

/*
| Generates the fromJsonCreator's twig processing.
*/
def.lazy._fromJsonCreatorProcessing =
	function( )
{
	const items = this.typeItemsExpanded;

	let switchExpr = Ast.switch( 'jval.$type' );

	for( let twigType of items )
	{
		const jsonName = this.manager.getEntry( twigType ).jsonName;
		const ja = this.manager.getEntry( twigType ).fromJsonArgs;
		let call = Ast.$( Ast.var( twigType.varname ), '.FromJson( jval )' );
		for( let a of ja )
		{
			call = call.arg( a );
		}
		switchExpr =
			switchExpr.case(
				Ast.string( jsonName ),
				Ast.block.$( 'twig[ key ] =', call ).break
			);
	}

	// invalid twig type
	switchExpr = switchExpr.default( 'throw new Error( );' );
	const loop =
		Ast.block
		// json keys/twig mismatch
		.$( 'if( !jwig[ key ] ) throw new Error( );' )
		.$( 'jval = jwig[ key ]' )
		.append( switchExpr );

	return(
		Ast.block
		.$( 'twig = { }' )
		// ranks/twig information missing
		.$( 'if( !jwig || !keys ) throw new Error( );' )
		.$( 'for( let key of keys )', loop )
	);
};

/*
| Generates the proto stuff.
*/
def.lazy._proto =
	function( )
{
	return(
		Ast.block
		.comment( 'Returns the element at rank.' )
		.$( this._protoSet( 'atRank', 'twigAtRank' ) )
		.comment( 'Returns the element by key.' )
		.$( this._protoSet( 'get', 'twigGet' ) )
		.comment( 'Returns the key at a rank.' )
		.$( this._protoSet( 'getKey', 'twigGetKey' ) )
		.comment( 'Returns the length of the twig.')
		.$( this._protoLazyValueSet( 'length', 'twigLength' ) )
		.comment( 'Returns the rank of the key.' )
		.$( 'ti2c._proto.lazyFunction( prototype, "rankOf", ti2c._proto.twigRankOf )' )
		.comment( 'Returns the twig with the element at key set.' )
		.$( this._protoSet( 'set', 'twigSet' ) )
		.comment( 'Iterates over the twig.' )
		.$(
			'prototype[ Symbol.iterator ] =',
			Ast.generator(
				'for( let a = 0, al = this.length; a < al; a++ ) yield this.atRank( a );'
			)
		)
		.comment( 'Reverse iterates over the twig.' )
		.$(
			'prototype.reverse =',
			Ast.generator(
				'for( let a = this.length - 1; a >= 0; a-- ) yield this.atRank( a );'
			)
		)
		.comment( 'Creates an empty group of this type.' )
		.$( 'ti2c._proto.lazyStaticValue( Self, "Empty", ',
			Ast.func( 'return Self.create( );' ),
			')'
		)
		.comment( 'Grows the twig.' )
		.$( 'Self.Grow =',
			Ast.func(
				Ast.block
				.$( 'let ranks = [ ]' )
				.$( 'let twig = { }' )
				.for( 'let a = 0, alen = args.length', 'a < alen', 'a+=2',
					Ast.block
					.$( 'const key = args[ a ]' )
					.$( 'ranks.push( key )' )
					.$( 'twig[ key ] = args[ a + 1 ]' )
				)
				.$( 'return Self.create( "twig:init", twig, ranks )' )
			)
			.arg( 'args', 'key/obj pairs', '...' )
		)
	);
};

/*
| Generates the jsonfy converter.
*/
def.lazy._jsonfy =
	function( )
{
	let block = Ast.block;

	// jsonfy code to build return value
	const hasSubJsonfy = this.typeItemsExpanded.hasTi2cType;
	const hasI2Space = true;

	let jr = Ast.block.$( 'let i0space, i1space' );
	if( hasI2Space ) jr = jr.$( 'let i2space' );
	if( hasSubJsonfy )
	{
		const jsonfySpacing = TypeTi2cClass.PkgName( 'ti2c', 'Jsonfy/Spacing' );
		jr = jr
			.$( 'let iSpacing' )
			.$( 'if( spacing === undefined )',
				'{',
					'i0space = i1space = "";',
					hasI2Space ? 'i2space = "";' : undefined,
				'}',
				'else if( typeof( spacing ) === "string" )',
				'{',
					'i0space = "\\n";',
					'i1space = "\\n" + spacing;',
					hasI2Space ? 'i2space = i1space + spacing;' : undefined,
					'iSpacing =',
						Ast.var( jsonfySpacing.varname ), '.',
						hasI2Space ? 'Start2' : 'Start1', '( spacing );',
				'}',
				'else',
				'{',
					'i0space = "\\n" + spacing.level;',
					'i1space = i0space + spacing.step;',
					hasI2Space ? 'i2space = i1space + spacing.step;' : undefined,
					'iSpacing = spacing.', hasI2Space ? 'Inc2;' : 'Inc1;',
				'}'
			);
	}
	else
	{
		jr = jr
			.$(
				'if( spacing === undefined )',
				'{',
					'i0space = i1space = "";',
					hasI2Space ? 'i2space = "";' : undefined,
				'}',
				'else if( typeof( spacing ) === "string" )',
				'{',
					'i0space = "\\n";',
					'i1space = i0space + spacing;',
					hasI2Space ? 'i2space = i1space + spacing;' : undefined,
				'}',
				'else',
				'{',
					'i0space = "\\n" + spacing.level;',
					'i1space = i0space + spacing.step;',
					hasI2Space ? 'i2space = i1space + spacing.step;' : undefined,
				'}'
			);
	}
	jr = jr.$( 'const colon = i1space !== "" ? ": " : ":"' );

	jr = jr
		.$(
			'let r = "{" + i1space +',
			Ast.string( '"$type"' ), '+ colon +', Ast.string( '"' + this.jsonName + '"' )
		)
		.$( 'r+= "," + i1space +', Ast.string( '"keys"' ), '+ colon + "[";' )
		.$(	'const keys = this.keys;' )
		.$(	'const twig = this._twig;' )
		.for(
			'let a = 0, alen = keys.length', 'a < alen', 'a++',
			Ast.block
			.$( 'r += i2space + ', Ast.string( '"' ), '+ keys[ a ] +', Ast.string( '"' ) )
			.$( 'if( a + 1 < alen ) r += ",";' )
		)
		.$(	'r += i1space + "]," + i1space +', Ast.string( '"twig"' ), '+ colon + "{"' )
		.$( 'let first = true' )
		.for(
			'let a = 0, alen = keys.length', 'a < alen', 'a++',
			Ast.block
			.$( 'const key = keys[ a ]' )
			.$( 'const val = twig[ key ]' )
			.$( 'if( !first ){ r += ","; } else { first = false; }' )
			.$( 'r += i2space +', Ast.string( '"' ), '+ key +', Ast.string( '"' ), '+ colon' )
			.$(	'r +=', this.typeItemsExpanded.jsonfyCode( Ast.$( 'val' ) ) )
		)
		.$( 'r += i1space + "}" + i0space + "}";' );

	jr = jr.$( 'return r' );
	const fjr = Ast.func( jr ).arg( 'spacing' );
	return(
		block
		.comment( 'Stable jsonfy' )
		.$( 'ti2c._proto.lazyFunction( prototype, "jsonfy", ', fjr, ')' )
	);
};
