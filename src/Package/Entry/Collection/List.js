/*
| Specifications of a ti2c list collection.
*/
def.attributes =
{
	// type identifier
	type: { type: 'Type/Ti2c/Collection/List' },
};

def.extend = 'Package/Entry/Collection/Base';

import { Self as Ast            } from '{ti2c:Ast/Self}';
import { Self as ListString     } from '{list@string}';
import { Self as TypeBoolean    } from '{ti2c:Type/Primitive/Boolean}';
import { Self as TypeNull       } from '{ti2c:Type/Primitive/Null}';
import { Self as TypeNumber     } from '{ti2c:Type/Primitive/Number}';
import { Self as TypeString     } from '{ti2c:Type/Primitive/String}';
import { Self as TypeUndefined  } from '{ti2c:Type/Primitive/Undefined}';
import { Self as TypeTi2cClass  } from '{ti2c:Type/Ti2c/Class}';

/*
| Creates an entry.
|
| ~coreEntry:  entry in the core
| ~type:       entry type
| ~codeGenDir: directory for generated directory
*/
def.static.Define =
	async function( coreEntry, type, codeGenDir, manager )
{
	const aPathCodeGen = codeGenDir.f( type.asFileString + '.js' );
	const typeItemsExpanded = await type.typeItems.expand( manager );

	return(
		Self.create(
			'aPathCodeGen', aPathCodeGen,
			'jsonName', type.asString,
			'manager', manager,
			'type', type,
			'typeItemsExpanded', typeItemsExpanded,
			'_coreEntry', coreEntry,
		)
	);
};

/*
| Generates the constructor.
*/
def.lazy._constructor =
	function( )
{
	let cf =
		Ast.func(
			Ast.block
			.$( 'this.__lazy = { }' )
			.$( 'this.__hash = hash' )
			.$( 'this._list = list' )
			.$( 'Object.freeze( this )' )
			.$( 'Object.freeze( list )' )
		)
		.arg( 'hash', 'hash value' )
		.arg( 'list', 'list' );

	return(
		Ast.block
		.comment( 'Constructor.' )
		.$( 'const Constructor =', cf )
		.comment( 'In case of checking all unknown access is to be trapped.' )
		.check(
			Ast.block
			.$( 'const Trap = ', Ast.func( Ast.block ) )
			.$( 'Trap.prototype = ti2c.trap' )
			.$( 'Constructor.prototype = new Trap( )' )
		)
		.comment( 'Constructor prototype.' )
		.$( 'const prototype = Self.prototype = Constructor.prototype' )
		.$( 'Self.__hash = ', this.hash )
	);
};

/*
| Arguments for the constructor.
*/
def.lazy._constructorList =
	function( )
{
	return ListString.Elements( 'hash', 'list' );
};

/*
| Generates the creators checks.
|
| ~json: do checks for fromJsonCreator
*/
def.lazyFunc._creatorChecks =
	function( json )
{
	const check =
		Ast.block
		.$( 'for( let o of list )',
			Ast.block
			.$( 'if(',
				this._typeCheckFailCondition( Ast.$( 'o' ), this.typeItemsExpanded ),
				') throw new Error( );'
			)
		);

	return(
		json
		? check
		: ( check.statements.length > 0 ? Ast.block.check( check ) : Ast.block )
	);
};

/*
| Generates the creators free strings parser.
*/
def.lazy._creatorFreeStringsParser =
	function( )
{
	let loop = Ast.block.let( 'arg', 'args[ a + 1 ]' );
	let _switch = Ast.switch( 'args[ a ]' );

	const dupCheck =
		Ast.$( '{ if( !listDup ) { list = list.slice( ); listDup = true; } }' );

	_switch =
		_switch
		.case(
			'"list:init"',
			Ast.block
			.$(
				'if( Array.isArray( arg ) )',
				'{ list = arg; listDup = true; }',
				'else { list = arg._list; listDup = false; }'
			)
			.break
		)
		.case( '"list:append"', dupCheck.$( 'list.push( arg )' ).break )
		.case(
			'"list:insert"',
			dupCheck.$( 'list.splice( arg, 0, args[ ++a + 1 ] )' ).break
		)
		.case( '"list:remove"', dupCheck.$( 'list.splice( arg, 1 ) ' ).break )
		.case( '"list:set"', dupCheck.$( 'list[ arg ] = args[ ++a + 1 ]' ).break )
		.default( 'throw new Error( args[ a ] )' );

	loop = loop.append( _switch );

	return(
		Ast.block
		.$( 'for( let a = 0, alen = args.length; a < alen; a += 2 )', loop )
	);
};

/*
| Generates the creators inheritance receiver.
*/
def.lazy._creatorInheritanceReceiver =
	function( )
{
	const receiver =
		Ast.block
		.$( 'list = this._list' )
		.$( 'listDup = false' );

	return(
		Ast.$( 'if ( this !== Self )', receiver, ';' )
		.setElsewise( '{ list = [ ]; listDup = true; }' )
	);
};

/*
| Generates the equality test for creator interning.
|
| ~action: ast action to do
*/
def.lazyFunc._creatorInternEqTest =
	function( action )
{
	return(
		Ast.block
		.$( 'const elist = e._list' )
		.if( 'len !== elist.length', 'continue' )
		.$( 'let eq = true' )
		.for( 'let a = 0', 'a < len', 'a++',
			Ast.if( 'list[ a ] !== elist[ a ]',
				Ast.block.$( 'eq = false' ).$( 'break' )
			)
		)
		.if( 'eq', action )
	);
};

/*
| Generates the hash for creator interning.
*/
def.lazy._creatorInternHash =
	function( )
{
	return(
		Ast.block
		.$( 'const len = list.length' )
		.$( 'const hash = ti2c._hashIterable(', this.hash, ',list)' )
	);
};

/*
| Generates the creators variable list.
*/
def.lazy._creatorVariables =
	function( )
{
/**/if( CHECK && arguments.length !== 0 ) throw new Error( );

	const varlist = [ 'list', 'listDup' ];

	varlist.sort( );
	let result = Ast.block;
	for( let varname of varlist )
	{
		result = result.let( varname );
	}
	return result;
};

/*
| Generates the fromJsonCreator's variable list.
*/
def.lazy._fromJsonCreatorVariables =
	function( )
{
	const varlist = [ 'jlist', 'list' ];
	varlist.sort( );
	let result = Ast.block;
	for( let varname of varlist )
	{
		result = result.let( varname );
	}
	return result;
};

/*
| Generates the fromJsonCreator's json parser.
*/
def.proto._fromJsonCreatorParser =
	function( jsonList )
{
	const aSwitch =
		Ast.switch( 'name' )
		.case(
			'"$type"',
			Ast.block
			.$( 'if( arg !== ', Ast.string( this.jsonName ), ') throw new Error( );' )
			.break
		)
		.case( '"list"', Ast.block.$( 'jlist = arg' ).break )
		.default( 'throw new Error( )' );

	return(
		Ast.block
		.$( 'for( let name in json )',
			Ast.block
			.$( 'const arg = json[ name ]' )
			.$( aSwitch )
		)
	);
};

/*
| Generates the fromJsonCreator's list processing.
*/
def.lazy._fromJsonCreatorProcessing =
	function( )
{
	const typeItems = this.typeItemsExpanded;

	// FIXME dirty workaround
	if( typeItems.size === 1 && typeItems.trivial.ti2ctype === TypeString )
	{
		return(
			Ast.block
			.$( 'if( !jlist ) throw new Error( );' )
			.$( 'list = jlist' )
		);
	}

	let loopBody = Ast.block;

	const result =
		Ast.block
		.$( 'if( !jlist ) throw new Error( );' )
		.$( 'list = [ ]' );

	let loopSwitch =
		Ast.switch( 'jlist[ r ].$type' )
		.default( 'throw new Error( )' );

	for( let type of typeItems )
	{
		switch( type )
		{
			case TypeNull.singleton:
				loopBody = loopBody.$( 'if( jlist[ r ] === null ) { list[ r ] = null; continue; }' );
				continue;
			case TypeUndefined.singleton:
				loopBody =
					loopBody.$( 'if( jlist[ r ] === undefined ) { list[ r ] = undefined; continue; }' );
				continue;
			case TypeBoolean.singleton:
				loopBody =
					loopBody.$( 'if( typeof( jlist[ r ] ) === "boolean" ) { list[ r ] = jlist[ r ]; continue; }' );
				continue;
			case TypeNumber.singleton:
				loopBody =
					loopBody.$( 'if( typeof( jlist[ r ] ) === "number" ) { list[ r ] = jlist[ r ]; continue; }' );
				continue;
			case TypeString.singleton:
				loopBody =
					loopBody.$( 'if( typeof( jlist[ r ] ) === "string" ) { list[ r ] = jlist[ r ]; continue; }' );
				continue;
		}

		const jsonName = this.manager.getEntry( type ).jsonName;
		if( !jsonName ) throw new Error( );

		const ja = this.manager.getEntry( type ).fromJsonArgs;
		let call = Ast.$( Ast.var( type.varname ), '.FromJson( jlist[ r ] )' );
		for( let a of ja )
		{
			call = call.arg( a );
		}

		loopSwitch =
			loopSwitch
			.case(
				Ast.string( jsonName ),
				Ast.block.$( 'list[ r ] =', call ).break
			);
	}

	loopBody =
		loopSwitch.cases.length > 0
		? loopBody.$( loopSwitch )
		: loopBody.$( 'throw new Error( )' );

	return result.$( 'for( let r = 0, rlen = jlist.length; r < rlen; ++r )', loopBody, ';' );
};

/*
| Generates the proto stuff.
*/
def.lazy._proto =
	function( )
{
	let block =
		Ast.block
		.comment( 'Returns the list with an element appended.' )
		.$( this._protoSet( 'append', 'listAppend' ) )
		.comment( 'Returns the list with another list appended.' )
		.$( this._protoSet( 'appendList', 'listAppendList' ) )
		.comment( 'Returns an array clone of the list.' )
		.$( this._protoSet( 'clone', 'listClone' ) )
		.comment( 'Returns the first element of the list.')
		.$( this._protoLazyValueSet( 'first', 'listFirst' ) )
		.comment( 'Returns one element from the list.' )
		.$( this._protoSet( 'get', 'listGet' ) )
		.comment( 'Returns the list with one element inserted.' )
		.$( this._protoSet( 'insert', 'listInsert' ) )
		.comment( 'Returns the last element of the list.')
		.$( this._protoLazyValueSet( 'last', 'listLast' ) )
		.comment( 'Returns the length of the list.')
		.$( this._protoLazyValueSet( 'length', 'listLength' ) )
		.comment( 'Returns the list with one element removed.' )
		.$( this._protoSet( 'remove', 'listRemove' ) )
		.comment( 'Returns the list with one element set.' )
		.$( this._protoSet( 'set', 'listSet' ) )
		.comment( 'Returns a slice from the list.' )
		.$( this._protoSet( 'slice', 'listSlice' ) )
		.comment( 'Returns a sorted list.' )
		.$( this._protoSet( 'sort', 'listSort' ) )
		// FUTURE maybe this can be simplified?
		.comment( 'Forwards the iterator.' )
		.$(
			'prototype[ Symbol.iterator ] =',
			Ast.func( 'return this._list[ Symbol.iterator ]( )' )
		)
		.comment( 'Reverse iterates over the list.' )
		.$(
			'prototype.reverse =',
			Ast.generator(
				'for( let a = this.length - 1; a >= 0; a-- ) yield this._list[ a ];'
			)
		)
		.comment( 'Creates the list from an Array.' )
		.$( 'Self.Array =',
			Ast.func( 'return Self.create( "list:init", array );')
			.arg( 'array', 'the array' )
		)
		.comment( 'Creates the list with direct elements.' )
		.$( 'Self.Elements =',
			Ast.func(
				'return Self.create( "list:init", Array.prototype.slice.call( arguments) );'
			)
		)
		.comment( 'Creates an empty list of this type.' )
		.$( 'ti2c._proto.lazyStaticValue( Self, "Empty", ',
			Ast.func( 'return Self.create( );' ),
			')'
		);

	// adds special shortcuts for list@string
	const items = this.typeItemsExpanded;
	if(
		items.size === 1
		&& items.trivial.ti2ctype === TypeString
	)
	{
		block =
			block
			.comment( 'Joins the string list into one string' )
			.$( 'ti2c._proto.lazyFunction( prototype, "join", ',
				Ast.func( 'return this._list.join( sep )' ).arg( 'sep' ),
				')',
			);
	}

	return block;
};

/*
| Generates the jsonfy converter.
*/
def.lazy._jsonfy =
	function( )
{
	let block = Ast.block;

	// jsonfy code to build return value

	const hasSubJsonfy = this.typeItemsExpanded.hasTi2cType;
	const hasI2Space = true;

	let jr = Ast.block.$( 'let i0space, i1space' );
	if( hasI2Space )
	{
		jr = jr.$( 'let i2space' );
	}

	if( hasSubJsonfy )
	{
		const jsonfySpacing = TypeTi2cClass.PkgName( 'ti2c', 'Jsonfy/Spacing' );
		jr = jr
			.$( 'let iSpacing' )
			.$( 'if( spacing === undefined )',
				'{',
					'i0space = i1space = "";',
					hasI2Space ? 'i2space = "";' : undefined,
				'}',
				'else if( typeof( spacing ) === "string" )',
				'{',
					'i0space = "\\n";',
					'i1space = "\\n" + spacing;',
					hasI2Space ? 'i2space = i1space + spacing;' : undefined,
					'iSpacing =',
						jsonfySpacing.varname, '.',
						hasI2Space ? 'Start2' : 'Start1', '( spacing );',
				'}',
				'else',
				'{',
					'i0space = "\\n" + spacing.level;',
					'i1space = i0space + spacing.step;',
					hasI2Space ? 'i2space = i1space + spacing.step;' : undefined,
					'iSpacing = spacing.', hasI2Space ? 'Inc2;' : 'Inc1;',
				'}'
			);
	}
	else
	{
		jr = jr
			.$(
				'if( spacing === undefined )',
				'{',
					'i0space = i1space = "";',
					hasI2Space ? 'i2space = "";' : undefined,
				'}',
				'else if( typeof( spacing ) === "string" )',
				'{',
					'i0space = "\\n";',
					'i1space = i0space + spacing;',
					hasI2Space ? 'i2space = i1space + spacing;' : undefined,
				'}',
				'else',
				'{',
					'i0space = "\\n" + spacing.level;',
					'i1space = i0space + spacing.step;',
					hasI2Space ? 'i2space = i1space + spacing.step;' : undefined,
				'}'
			);
	}
	jr = jr.$( 'const colon = i1space !== "" ? ": " : ":"' );

	jr = jr
		.$(
			'let r = "{" + i1space +',
			Ast.string( '"$type"' ), '+ colon +', Ast.string( '"' + this.jsonName + '",' ),
			'+ i1space +', Ast.string( '"list"' ),
			'+ colon'
		)
		.$( 'if( this.length === 0 ) { return r + "[ ]" + i0space + "}" }' )
		.$( 'r += "[" + i2space;' )
		.$( 'let first = true;' )
		.forOfLet( 'val', 'this._list',
			Ast.block
			.$( 'if( !first ){ r += "," + i2space; } else { first = false; }' )
			.$( 'r += ', this.typeItemsExpanded.jsonfyCode( 'val' ) )
		)
		.$( 'r += i1space + "]" + i0space + "}";' );

	jr = jr.$( 'return r' );
	const fjr = Ast.func( jr ).arg( 'spacing' );

	return(
		block
		.comment( 'Stable jsonfy' )
		.$( 'ti2c._proto.lazyFunction( prototype, "jsonfy", ', fjr, ')' )
	);
};
