/*
| Specifications of a ti2c group collection.
*/
def.attributes =
{
	// type identifier
	type: { type: 'Type/Ti2c/Collection/Group' },
};

def.extend = 'Package/Entry/Collection/Base';

import { Self as Ast            } from '{ti2c:Ast/Self}';
import { Self as ListString     } from '{list@string}';
import { Self as TypeBoolean    } from '{ti2c:Type/Primitive/Boolean}';
import { Self as TypeFunction   } from '{ti2c:Type/Primitive/Function}';
import { Self as TypeNull       } from '{ti2c:Type/Primitive/Null}';
import { Self as TypeNumber     } from '{ti2c:Type/Primitive/Number}';
import { Self as TypeProtean    } from '{ti2c:Type/Primitive/Protean}';
import { Self as TypeString     } from '{ti2c:Type/Primitive/String}';
import { Self as TypeUndefined  } from '{ti2c:Type/Primitive/Undefined}';
import { Self as TypeTi2cClass  } from '{ti2c:Type/Ti2c/Class}';

/*
| Creates an entry.
|
| ~coreEntry:  entry in the core
| ~type:       entry type
| ~codeGenDir: directory for generated directory
*/
def.static.Define =
	async function( coreEntry, type, codeGenDir, manager )
{
	const aPathCodeGen = codeGenDir.f( type.asFileString + '.js' );
	const typeItemsExpanded = await type.typeItems.expand( manager );

	return(
		Self.create(
			'aPathCodeGen', aPathCodeGen,
			'jsonName', type.asString,
			'manager', manager,
			'type', type,
			'typeItemsExpanded', typeItemsExpanded,
			'_coreEntry', coreEntry,
		)
	);
};

/*
| Generates the constructor.
*/
def.lazy._constructor =
	function( )
{
	let block =
		Ast.block
		.$( 'this.__lazy = { }' )
		.$( 'this.__hash = hash' )
		.$( 'this._group = group' )
		.$( 'this.keys = keys' )
		.$( 'Object.freeze( this )' )
		.$( 'Object.freeze( group )' )
		.$( 'Object.freeze( keys )' );

	let cf = Ast.func( block );
	for( let name of this._constructorList )
	{
		switch( name )
		{
			case 'group': cf = cf.arg( 'group', 'group' ); break;
			case 'hash': cf = cf.arg( 'hash', 'hash value' ); break;
			case 'keys': cf = cf.arg( 'keys', 'twig key ranks' ); break;
			default: throw new Error( );
		}
	}

	return(
		Ast.block
		.comment( 'Constructor.' )
		.$( 'const Constructor =', cf )
		.comment( 'In case of checking all unknown access is to be trapped.' )
		.check(
			Ast.block
			.$( 'const Trap = ', Ast.func( Ast.block ) )
			.$( 'Trap.prototype = ti2c.trap' )
			.$( 'Constructor.prototype = new Trap( )' )
		)
		.comment( 'Constructor prototype.' )
		.$( 'const prototype = Self.prototype = Constructor.prototype' )
		.$( 'Self.__hash = ', this.hash )
	);
};

/*
| Arguments for the constructor.
*/
def.lazy._constructorList =
	function( )
{
	return ListString.Elements( 'hash', 'group', 'keys' );
};

/*
| Generates the creators checks.
|
| ~json: do checks for fromJsonCreator
*/
def.lazyFunc._creatorChecks =
	function( json )
{
	const check =
		Ast.block
		.$( 'for( let k in group )',
			Ast.block
			.$( 'const o = group[ k ]' )
			.$( 'if(',
				this._typeCheckFailCondition( Ast.$( 'o' ), this.typeItemsExpanded ),
				') throw new Error( );'
			)
		);

	return(
		json
		? check
		: ( check.statements.length > 0 ? Ast.block.check( check ) : Ast.block )
	);
};

/*
| Generates the creators free strings parser.
*/
def.lazy._creatorFreeStringsParser =
	function( )
{
	let loop = Ast.block.let( 'arg', 'args[ a + 1 ]' );
	let _switch = Ast.switch( 'args[ a ]' );

	let dupCheck =
		Ast.$( '{ if( !groupDup ) { group = ti2c.copy( group ); groupDup = true; } }' );

	_switch =
		_switch
		.case(
			'"group:init"',
			Ast.block
			.$( 'group = arg' )
			.$( 'groupDup = true' )
			.break
		)
		.case(
			'"group:set"',
			dupCheck.$( 'group[ arg ] = args[ ++a + 1 ]' ).break
		)
		.case(
			'"group:remove"',
			dupCheck.$( 'delete group[ arg ]' ).break
		);

	_switch = _switch.default( 'throw new Error( args[ a ] )' );
	loop = loop.append( _switch );

	return(
		Ast.block
		.$( 'for( let a = 0, alen = args.length; a < alen; a += 2 )', loop )
	);
};

/*
| Generates the creators inheritance receiver.
*/
def.lazy._creatorInheritanceReceiver =
	function( )
{
	let receiver = Ast.block;
	receiver =
		receiver
		.$( 'group = this._group' )
		.$( 'groupDup = false' );
	let result = Ast.$( 'if ( this !== Self )', receiver, ';' );

	return result.setElsewise( '{ group = { }; groupDup = true; }' );
};

/*
| Generates the equality test for creator interning.
|
| ~action: ast action to do
*/
def.lazyFunc._creatorInternEqTest =
	function( action )
{
	return(
		Ast.block
		.$( 'const ekeys = e.keys' )
		.$( 'const len = keys.length' )
		.$( 'const egroup = e._group' )
		.if( 'keys.length !== ekeys.length', 'continue' )
		.$( 'let eq = true' )
		.for(
			'let a = 0', 'a < len', 'a++',
			Ast.block
			.$( 'const key = keys[ a ]' )
			.if( 'key !== ekeys[ a ] || group[ key ] !== egroup[ key ]',
				Ast.block
				.$( 'eq = false' )
				.$( 'break' )
			)
		)
		.if( 'eq', action )
	);
};

/*
| Generates the hash for creator interning.
*/
def.lazy._creatorInternHash =
	function( )
{
	return(
		Ast.block
		.$( 'const keys = Object.freeze( Object.keys( group ).sort( ) )' )
		.$( 'const hash = ti2c._hashKeys(', this.hash, ', keys, group )' )
	);
};

/*
| Generates the creators variable list.
*/
def.lazy._creatorVariables =
	function( )
{
	const varlist = [ ];
	varlist.push( 'group', 'groupDup' );
	varlist.sort( );
	let result = Ast.block;
	for( let varname of varlist )
	{
		result = result.let( varname );
	}
	return result;
};

/*
| Generates the fromJsonCreator's variable list.
*/
def.lazy._fromJsonCreatorVariables =
	function( )
{
	const varlist = [ 'jgroup', 'group' ];

	varlist.sort( );
	let result = Ast.block;
	for( let varname of varlist )
	{
		result = result.let( varname );
	}
	return result;
};

/*
| Generates the fromJsonCreator's json parser.
*/
def.proto._fromJsonCreatorParser =
	function( jsonList )
{
	// the switch
	let nameSwitch =
		Ast.switch( 'name' )
		.case(
			'"$type"',
			Ast.block
			.$( 'if( arg !== ', Ast.string( this.jsonName ), ') throw new Error( );' )
			.break
		);

	nameSwitch = nameSwitch.case( '"group"', Ast.block.$( 'jgroup = arg' ).break );

	return(
		Ast.block
		.$( 'for( let name in json )',
			Ast.block
			.$( 'const arg = json[ name ]' )
			.append( nameSwitch )
		)
	);
};

/*
| Generates the fromJsonCreator's group processing.
*/
def.lazy._fromJsonCreatorProcessing =
	function( )
{
	const items = this.typeItemsExpanded;

	let haveNull = false;
	// FUTURE dirty workaround
	if( items.size === 1 && items.trivial.ti2ctype === TypeString )
	{
		return(
			Ast.block
			.$( 'if( !jgroup ) throw new Error( );' )
			.$( 'group = jgroup' )
		);
	}

	// FUTURE dirty workaround 2
	if( items.size === 1 && items.trivial.ti2ctype === TypeBoolean )
	{
		return(
			Ast.block
			.$( 'if( !jgroup ) throw new Error( );' )
			.$( 'group = jgroup' )
		);
	}

	const result =
		Ast.block
		.$( 'if( !jgroup ) throw new Error( );' )
		.$( 'group = { }' );

	let loopSwitch =
		Ast.switch( 'jgroup[ k ].$type' )
		.default( 'throw new Error( );' );

	// FUTURE allow more than one non-ti2c type
	let customDefault = false;
	for( let type of items )
	{
		switch( type.ti2ctype )
		{
			case TypeBoolean:
			{
				if( customDefault ) throw new Error( );
				customDefault = true;
				loopSwitch =
					loopSwitch
					.default(
						'if( typeof( jgroup[ k ] ) === "boolean" ) group[ k ] = jgroup[ k ];',
						'else throw new Error( );'
					);
				continue;
			}

			case TypeNull:
				haveNull = true;
				continue;

			case TypeNumber:
			{
				if( customDefault ) throw new Error( );
				customDefault = true;
				loopSwitch =
					loopSwitch
					.default(
						Ast.$(
							'if( typeof( jgroup[ k ] ) === "number" )',
							'{ group[ k ] = jgroup[ k ]; }',
							'else',
							'{ throw new Error( ); }'
						)
					);
				continue;
			}

			case TypeFunction:
			case TypeProtean:
				continue;

			case TypeString:
			{
				if( customDefault ) throw new Error( );
				customDefault = true;
				loopSwitch =
					loopSwitch
					.default(
						Ast.$(
							'if( typeof( jgroup[ k ] ) === "string" )',
							'{ group[ k ] = jgroup[ k ]; }',
							'else',
							'{ throw new Error( ); }'
						)
					);
				continue;
			}
		}

		const jsonName = this.manager.getEntry( type ).jsonName;
		if( !jsonName ) throw new Error( );

		const ja = this.manager.getEntry( type ).fromJsonArgs;
		let call = Ast.$( Ast.var( type.varname ), '.FromJson( jgroup[ k ] )' );
		for( let a of ja )
		{
			call = call.arg( a );
		}
		loopSwitch =
			loopSwitch.case(
				Ast.string( jsonName ),
				Ast.block.$( 'group[ k ] =', call ).break
			);
	}

	let loopBody;
	if( !haveNull )
	{
		loopBody = loopSwitch;
	}
	else
	{
		loopBody =
			Ast.block
			.if(
				'jgroup[ k ] === null',
				Ast.block
				.$( 'group[ k ] = null' )
				.continue
			)
			.$( loopSwitch );
	}

	return result.$( 'for( let k in jgroup )', loopBody, ';' );
};

/*
| Generates the proto stuff.
*/
def.lazy._proto =
	function( )
{
	return(
		Ast.block
		.comment(
			'Returns the group with another group added,',
			'overwriting collisions.'
		)
		.$( this._protoSet( 'addGroup', 'groupAddGroup' ) )
		.comment( 'Gets one element from the group.' )
		.$( this._protoSet( 'get', 'groupGet' ) )
		.comment( 'Returns the group with one element removed.' )
		.$( this._protoSet( 'remove', 'groupRemove' ) )
		.comment( 'Returns the group with one element set.' )
		.$( this._protoSet( 'set', 'groupSet' ) )
		.comment( 'Returns the size of the group.')
		.$( this._protoLazyValueSet( 'size', 'groupSize' ) )
		.comment( 'Iterates over the group by sorted keys.' )
		.$(
			'prototype[ Symbol.iterator ] =',
			Ast.generator(
				'for( let key of this.keys ) yield this.get( key );'
			)
		)
		.comment( 'Creates an empty group of this type.' )
		.$( 'ti2c._proto.lazyStaticValue( Self, "Empty", ',
			Ast.func( 'return Self.create( );' ),
			')'
		)
		.$( 'Self.Table =',
			Ast.func( 'return Self.create( "group:init", group );')
			.arg( 'group', 'the group' )
		)
	);
};

/*
| Generates the jsonfy converter.
*/
def.lazy._jsonfy =
	function( )
{
	let block = Ast.block;

	// jsonfy code to build return value
	const hasSubJsonfy = this.typeItemsExpanded.hasTi2cType;
	const hasI2Space = true;

	let jr = Ast.block.$( 'let i0space, i1space' );
	if( hasI2Space ) jr = jr.$( 'let i2space' );
	if( hasSubJsonfy )
	{
		const jsonfySpacing = TypeTi2cClass.PkgName( 'ti2c', 'Jsonfy/Spacing' );
		jr = jr
			.$( 'let iSpacing' )
			.$( 'if( spacing === undefined )',
				'{',
					'i0space = i1space = "";',
					hasI2Space ? 'i2space = "";' : undefined,
				'}',
				'else if( typeof( spacing ) === "string" )',
				'{',
					'i0space = "\\n";',
					'i1space = "\\n" + spacing;',
					hasI2Space ? 'i2space = i1space + spacing;' : undefined,
					'iSpacing =',
						Ast.var( jsonfySpacing.varname ), '.',
						hasI2Space ? 'Start2' : 'Start1', '( spacing );',
				'}',
				'else',
				'{',
					'i0space = "\\n" + spacing.level;',
					'i1space = i0space + spacing.step;',
					hasI2Space ? 'i2space = i1space + spacing.step;' : undefined,
					'iSpacing = spacing.', hasI2Space ? 'Inc2;' : 'Inc1;',
				'}'
			);
	}
	else
	{
		jr = jr
			.$(
				'if( spacing === undefined )',
				'{',
					'i0space = i1space = "";',
					hasI2Space ? 'i2space = "";' : undefined,
				'}',
				'else if( typeof( spacing ) === "string" )',
				'{',
					'i0space = "\\n";',
					'i1space = i0space + spacing;',
					hasI2Space ? 'i2space = i1space + spacing;' : undefined,
				'}',
				'else',
				'{',
					'i0space = "\\n" + spacing.level;',
					'i1space = i0space + spacing.step;',
					hasI2Space ? 'i2space = i1space + spacing.step;' : undefined,
				'}'
			);
	}
	jr = jr.$( 'const colon = i1space !== "" ? ": " : ":"' );

	jr = jr
		.$(
			'let r = "{" + i1space +',
			Ast.string( '"$type"' ), '+ colon +',
			Ast.string( '"' + this.jsonName + '",' ),
			'+ i1space +', Ast.string( '"group"' ), '+ colon'
		)
		.$( 'r += "{";' )
		.$( 'const keys = this.keys;' )
		.$( 'const group = this._group;' )
		.$( 'let first = true;' );
	const typeItems = this.typeItemsExpanded;
	// for block
	let jfb = Ast.block.$( 'const val = group[ key ]' );
	if( typeItems.has( TypeUndefined.singleton ) )
	{
		jfb = jfb.$( 'if( val === undefined ) continue;' );
	}
	jfb = jfb
		.$( 'if( !first ){ r += ","; } else { first = false; }' )
		.$( 'r += i2space + ', Ast.string( '"' ), '+ key + ', Ast.string( '"' ), '+ colon' )
		.$( 'r += ', typeItems.jsonfyCode( 'val' ) );
	jr = jr.forOfLet( 'key', 'keys', jfb );
	jr = jr.$( 'r += i1space + "}" + i0space + "}";' );

	jr = jr.$( 'return r' );
	const fjr = Ast.func( jr ).arg( 'spacing' );

	return(
		block
		.comment( 'Stable jsonfy' )
		.$( 'ti2c._proto.lazyFunction( prototype, "jsonfy", ', fjr, ')' )
	);
};
