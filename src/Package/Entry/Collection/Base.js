/*
| Specifications of a ti2c list collection.
*/
def.extend = 'Package/Entry/Base';

def.attributes =
{
	// the json name to use
	jsonName: { type: 'string' },

	// to be same as type.typeItems, but InFile Types are expanded.
	typeItemsExpanded: { type: 'Type/Set' },
};

import { Self as Ast            } from '{ti2c:Ast/Self}';
import { Self as AstVar         } from '{ti2c:Ast/Var}';
import { Self as EntryClass     } from '{ti2c:Package/Entry/Class/Self}';
import { Self as EntryGroup     } from '{ti2c:Package/Entry/Collection/Group}';
import { Self as EntryList      } from '{ti2c:Package/Entry/Collection/List}';
import { Self as EntrySet       } from '{ti2c:Package/Entry/Collection/Set}';
import { Self as EntryTwig      } from '{ti2c:Package/Entry/Collection/Twig}';
import { Self as ListString     } from '{list@string}';
import { Self as TypeArbitrary  } from '{ti2c:Type/Ti2c/Arbitrary}';
import { Self as TypeBoolean    } from '{ti2c:Type/Primitive/Boolean}';
import { Self as TypeDate       } from '{ti2c:Type/Primitive/Date}';
import { Self as TypeFunction   } from '{ti2c:Type/Primitive/Function}';
import { Self as TypeInteger    } from '{ti2c:Type/Primitive/Integer}';
import { Self as TypeNull       } from '{ti2c:Type/Primitive/Null}';
import { Self as TypeNumber     } from '{ti2c:Type/Primitive/Number}';
import { Self as TypeProtean    } from '{ti2c:Type/Primitive/Protean}';
import { Self as TypeString     } from '{ti2c:Type/Primitive/String}';
import { Self as TypeUndefined  } from '{ti2c:Type/Primitive/Undefined}';
import { Self as TypeSet        } from '{ti2c:Type/Set}';
import { Self as TypeTi2cClass  } from '{ti2c:Type/Ti2c/Class}';
import { Self as TypeTi2cGroup  } from '{ti2c:Type/Ti2c/Collection/Group}';
import { Self as TypeTi2cList   } from '{ti2c:Type/Ti2c/Collection/List}';
import { Self as TypeTi2cSet    } from '{ti2c:Type/Ti2c/Collection/Set}';
import { Self as TypeTi2cTwig   } from '{ti2c:Type/Ti2c/Collection/Twig}';

/*
| Executes the generator.
| Returns an ast tree.
*/
def.lazy.ast =
	function( )
{
	let code =
		this._header
		.$( this._astImports )
		.$( this._iMapAndCache )
		.$( this._constructor )
		.$( this._creator );

	if( this.hasJson )
	{
		code = code.$( this._fromJsonCreator );
	}

	code =
		code
		.$( this._reflection )
		.$( this._proto );

	if( this.hasJson )
	{
		code =
			code
			.$( this._jsonfy )
			.$( this._jsonType );
	}

	return code;
};

/*
| Types of all other ti2c classes this depends on.
*/
def.lazy.dependencies =
	function( )
{
	const set = new Set( );
	const items = this.typeItemsExpanded;

	for( let t of this.type.typeItems.filterTi2cTypes ) set.add( t );
	for( let t of items.filterTi2cTypes ) set.add( t );

	if(
		this.ti2ctype === EntrySet
		&& items.size === 1
		&& items.trivial.ti2ctype === TypeString
	)
	{
		set.add( TypeTi2cList.create( 'typeItems', TypeSet.Elements( TypeString.singleton ) ) );
	}

	if( items.hasTi2cType && this.hasJson )
	{
		set.add( TypeTi2cClass.PkgName( 'ti2c', 'Jsonfy/Spacing' ) );
	}

	return TypeSet.create( 'set:init', set );
};

/*
| Collections never extend another entry.
*/
def.proto.entryExtend = undefined;

/*
| The number of classes this class extends.
*/
def.proto.extendLevel = 0;

/*
| FIXME cleanup?
*/
def.lazy.fromJsonArgs =
	function( )
{
	const set = new Set( );

	for( let t of this.typeItemsExpanded )
	{
		if( !t.isTi2c ) continue;

		// FIXME does not catch A-B-A cycles
		if( t === this.type ) continue;

		const entry = this.manager.getEntry( t );
		const tfja = entry.fromJsonArgs;
		if( !tfja ) continue;
		for( let s of tfja ) set.add( s );
	}

	return ListString.Array( Array.from(set).sort() );

	//return ListString.Empty;
};

/*
| True if this collection has jsonfy()/FromJson() handles.
| FIXME remove.
*/
def.lazy.hasJson =
	function( )
{
	for( let type of this.typeItemsExpanded )
	{
		if( type.isPrimitive ) return true;

		// FIXME does not catch A-B-A cycles
		if( type === this.type ) continue;

		const entry = this.manager.getEntry( type );

		switch( entry.ti2ctype )
		{
			case EntryClass:
				if( entry.jsonName ) return true;
				break;

			case EntryGroup:
			case EntryList:
			case EntrySet:
			case EntryTwig:
				if( entry.hasJson ) return true;
				break;

			// default ignore
		}
	}

	return false;
};

/*
| Collections do not have lazy values.
*/
def.proto.hasLazy = false;

/*
| This is a collection.
*/
def.proto.isCollection = true;

/*
| Collections never have a global set.
*/
def.proto.setGlobal = false;

/*
| Collections are never singleton.
*/
def.proto.singleton = false;

/*
| Generates the creator.
*/
def.lazy._creator =
	function( )
{
	let block =
		Ast.block
		.$( this._creatorVariables )
		.$( this._creatorInheritanceReceiver )
		.$( this._creatorFreeStringsParser )
		.$( this._creatorChecks( false ) )
		.$( this._creatorIntern )
		.$( this._creatorReturn );

	const creator = Ast.func( block ).arg( 'args', 'free strings', '...' );

	return(
		Ast.block
		.comment( 'Creates a new object.' )
		.$( 'Self.create = prototype.create = ', creator )
	);
};

/*
| Generates the creators return statement
*/
def.lazy._creatorReturn =
	function( )
{
	let call = Ast.$( 'Constructor( )' );
	for( let argName of this._constructorList )
	{
		call = call.arg( argName );
	}

	return(
		Ast.block
		.$( 'const newti2c = new', call, ';' )
		.$( 'imap.set( uhash, new WeakRef( newti2c ) )' )
		.if( 'cSize', 'cPut( newti2c )' )
		.$( 'return newti2c' )
	);
};

/*
| Generates the fromJsonCreator.
*/
def.lazy._fromJsonCreator =
	function( )
{
	// all attributes expected from json
	const jsonList = [ ];

	//if( this.collectionType === 'twig' ) jsonList.push( 'twig', 'keys' );
	jsonList.sort( );

	// FIXME Remove once fromJson is fixed
	if( !this._fromJsonCreatorProcessing ) return undefined;

	// function contents
	let block =
		Ast.block
		.check(
			'if( arguments.length !== '
			+ ( this.fromJsonArgs.length + 1 )
			+ ' ) throw new Error( );'
		)
		.$( this._fromJsonCreatorVariables )
		.$( this._fromJsonCreatorParser( jsonList ) )
		.$( this._fromJsonCreatorProcessing )
		.$( this._creatorChecks( true ) )
		.$( this._creatorIntern )
		.$( this._fromJsonCreatorReturn );

	let func = Ast.func( block ).arg( 'json' );
	for( let a of this.fromJsonArgs )
	{
		func = func.arg( a );
	}

	return(
		Ast.block
		.comment( 'Creates a new object from json.' )
		.$( 'Self.FromJson =', func )
	);
};

/*
| Ignored attributes in genFromJsonCreatorParser
*/
def.staticLazy._fromJsonCreatorParserIgnores =
	( ) => ( {
		group: true,
		keys: true,
		list: true,
		set: true,
		twig: true
	} );

/*
| Generates the fromJsonCreator's return statement.
*/
def.lazy._fromJsonCreatorReturn =
	function( )
{
	let call = Ast.$( 'Constructor( )' );

	for( let name of this._constructorList )
	{
		switch( name )
		{
			case 'hash':
				call = call.arg( 'hash' );
				break;

			case 'groupDup':
			case 'listDup':
			case 'twigDup':
				call = call.arg( 'true' );
				break;

			case 'group':
			case 'keys':
			case 'list':
			case 'set':
			case 'twig':
				call = call.arg( name );
				break;

			default:
				call = call.arg( name );
				break;
		}
	}

	let block = Ast.block.$( 'const newtim = new', call );
	block = block.$( 'imap.set( uhash, new WeakRef( newtim ) )' );
	return block.$( 'return newtim' );
};

/*
| Generates the json type identifier.
*/
def.lazy._jsonType =
	function( )
{
	const fja = this.fromJsonArgs;

	let astFja = Ast.array;
	for( let a of fja )
	{
		astFja = astFja.append( Ast.string( a ) );
	}

	return(
		Ast.block
		.comment( 'Json type identifier' )
		.$( 'Self.$type = prototype.$type =', Ast.string( this.jsonName ) )
		.$( 'Self.$fromJsonArgs = prototype.$fromJsonArgs =', astFja )
	);
};

/*
| Generates code for setting the prototype
| lazy value named 'name' to 'func'.
|
| ~name: lazy value name as string
| ~func: lazy value function name
*/
def.proto._protoLazyValueSet =
	function( name, func )
{
	return(
		Ast.block
		.$(
			'ti2c._proto.lazyValue( prototype,', Ast.string( name ), ', ti2c._proto.', func, ')'
		)
	);
};

/*
| Generates code for setting the prototype
| entry 'key' to 'value'
*/
def.proto._protoSet =
	function( key, value )
{
	return Ast.$( 'prototype.', key, ' = ti2c._proto.', value );
};

/*
| Generates the imports code.
*/
def.lazy._astImports =
	function( )
{
	const items = this.typeItemsExpanded;
	let imports = TypeSet.Empty;

	for( let it of items )
	{
		if( it.isTi2c ) imports = imports.add( it );
	}

	// FIXME make this a lazy value if jsonfy is used
	if( items.hasTi2cType && this.hasJson )
	{
		imports = imports.add( TypeTi2cClass.PkgName( 'ti2c', 'Jsonfy/Spacing' ) );
	}

	if(
		this.ti2ctype === EntrySet
		&& items.size === 1
		&& items.trivial.ti2ctype === TypeString
	)
	{
		imports = imports.add( TypeTi2cList.create( 'typeItems', TypeSet.Elements( TypeString.singleton ) ) );
	}

	let block = Ast.block;
	for( let type of imports )
	{
		//if( type.ti2ctype !== TypeTi2cClass ) continue;

		block = block.import( 'Self', type.varname, '{{' + type.asString + '}}' );
	}
	return block;
};

/*
| Generates a type check of a non set variable.
| It is true if the variable fails the check.
|
| ~aVar: name of the variable
| ~type: type to check for
*/
def.proto._singleTypeCheckFailCondition =
	function( aVar, type )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 ) throw new Error( );
/**/	if( aVar.ti2ctype !== AstVar ) throw new Error( );
/**/}

	switch( type.ti2ctype )
	{
		case TypeArbitrary:
			return Ast.$( aVar, '?.ti2ctype === undefined' );

		case TypeBoolean:
			return Ast.$( 'typeof( ', aVar, ' ) !== "boolean"' );

		case TypeDate:
			return Ast.$( '!(', aVar, 'instanceof Date )' );

		case TypeFunction:
			return Ast.$( 'typeof( ', aVar, ' ) !== "function"' );

		case TypeInteger:
			return Ast.$(
				Ast.$( 'typeof( ', aVar, ' ) !== "number"' ),
				'|| Number.isNaN( ', aVar, ' )',
				'|| Math.floor( ', aVar, ' ) !== ', aVar
			);

		case TypeNull:
			return Ast.$( aVar, '!== null' );

		case TypeNumber:
			return Ast.$(
				'typeof( ', aVar, ' ) !== "number"',
				'|| Number.isNaN( ', aVar, ' )'
			);

		case TypeProtean:
			return Ast.$( 'typeof( ', aVar, ' ) !== "object"' );

		case TypeString:
			return Ast.$( 'typeof( ', aVar, ' ) !== "string"' );

		case TypeTi2cClass:
		case TypeTi2cGroup:
		case TypeTi2cList:
		case TypeTi2cSet:
		case TypeTi2cTwig:
			return Ast.$( aVar, '.ti2ctype !== ', type.varname );

		case TypeUndefined:
			return Ast.$( aVar, '!== undefined' );

		default: throw new Error( );
	}
	// unreachable
};

/*
| Generates a type check of a variable.
|
| ~aVar:  the variable to check
| ~types: the type or TypeSet it has to match
*/
def.proto._typeCheckFailCondition =
	function( aVar, types )
{
/**/if( CHECK && arguments.length !== 2 ) throw new Error( );

	if( types.ti2ctype !== TypeSet )
	{
		return this._singleTypeCheckFailCondition( aVar, types );
	}

	if( types.size === 1 )
	{
		return this._singleTypeCheckFailCondition( aVar, types.trivial );
	}

	const condArray = [ ];

	// first do the primitives
	for( let type of types )
	{
		if( !type.isPrimitive ) continue;
		const cond = this._singleTypeCheckFailCondition( aVar, type );
		if( cond )
		{
			condArray.push( cond );
		}
	}

	// then do the ti2c types
	for( let type of types )
	{
		if( !type.isTi2c ) continue;
		const cond = this._singleTypeCheckFailCondition( aVar, type );
		if( cond )
		{
			condArray.push( cond );
		}
	}

	switch( condArray.length )
	{
		case 0:  return undefined;
		case 1:  return condArray[ 0 ];
		default: return Ast.and.apply( undefined, condArray );
	}
};
