/*
| Checks if a tim definition looks ok.
|
| TODO move all to spec.validate( )
*/

def.abstract = true;

/*
| A ti2c definition may have these.
*/
const defEntries =
	Object.freeze( new Set( [
		'abstract',
		'alike',
		'attributes',
		'create',
		'extend',
		'fromJsonArgs',
		'global',
		'inspect',
		'json',
		'lazy',
		'lazyFunc',
		'proto',
		'singleton',
		'static',
		'staticLazy',
		'staticLazyFunc',
	] ) );

/*
| Checks the alike definitions.
|
| ~def: the tim definition
*/
const checkAlikes =
	function( def )
{
	// TODO currently disabled

	/*
	const alike = def.alike;
	if( !def.attributes ) throw new Error( 'there cannot be alikes without attributes' );

	for( let name in alike )
	{
		const adef = alike[ name ];
		for( let spec in adef )
		{
			if( spec !== 'ignores' )
				throw new Error( 'alike ' + name + ' has invalid specifier ' + spec );
		}

		const ignores = adef.ignores;
		if( typeof( ignores ) !== 'object' )
			throw new Error( 'alike ' + name + ' misses ignores.' );
	}
	*/
};

/*
| Checks if an attributes type is valid.
| This does not include sets, it checks a single type.
|
| ~name: attribute name
| ~type: the type specifier to check
*/
const checkAttributeSingleType =
	function( name, type )
{
	if( typeof( type ) !== 'string' )
		throw new Error( 'attribute "' + name + '" has invalid type: ' + type );

	/*
	FIXME test primitive by letter case

	if( type.indexOf( '/' ) < 0 && type.indexOf( ':' ) < 0 )
	{
		switch( type )
		{
			case 'boolean' :
			case 'date' :
			case 'function' :
			case 'integer' :
			case 'number' :
			case 'null' :
			case 'protean' :
			case 'string' :
			case 'undefined' :

				break;

			default :

				throw new Error( 'attribute "' + name + '", type has unknown primitive: ' + type );
		}
	}
	*/
};

/*
| Checks if a ti2c attribute definition looks ok.
|
| ~def: the ti2c definition
| ~name: the attribute name
*/
const checkAttribute =
	function( def, name )
{
	const attr = def.attributes[ name ];
	const type = attr.type;

	if( typeof( type ) === 'string' ) checkAttributeSingleType( name, type );
	else if( Array.isArray( type ) )
	{
		for( let t of type ) checkAttributeSingleType( name, t );
	}
	else
	{
		throw new Error( 'attribute "' + name + '" has invalid type: ' + type );
	}

	for( let key in attr )
	{
		const value = attr[ key ];

		switch( key )
		{
			case 'defaultValue' :

				if( typeof( value ) !== 'string' )
					throw new Error( 'defaultValue not a string expression' );
				break;

			case 'json' :

				if( typeof( value ) !== 'boolean' ) throw new Error( 'json flag must be boolean' );
				break;

			case 'type' : break;

			default :

				throw new Error(
					'attribute ' + '"' + name + '"' + ' has invalid specifier: '
					+ '"' + key + '"'
				);
		}
	}
};

/*
| Checks if a ti2c definition looks ok.
*/
def.static.check =
	function( def )
{
	if( !def ) throw new Error( );
	for( let name in def )
	{
		if( !defEntries.has( name ) )
		{
			throw new Error( 'invalid ti2c def parameter: ' + name );
		}
	}
	const attr = def.attributes;
	if( def.alike && ( def.group || def.list || def.set || def.twig ) )
	{
		throw new Error( 'alike may only be with tables' );
	}

	if( attr ) for( let name in attr ) checkAttribute( def, name );
	if( def.alike ) checkAlikes( def );

	if( def.abstract !== undefined && typeof( def.abstract ) !== 'boolean' )
	{
		throw new Error( 'abstract must be a boolean value' );
	}
};
