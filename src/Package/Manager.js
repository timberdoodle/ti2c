/*
| Manages all ti2c packages in currently running instance.
*/
def.singleton = true;

import * as file from '../core/file';
import fs        from 'node:fs/promises';
import util      from 'node:util';

import { Self as EntryClass    } from '{ti2c:Package/Entry/Class/Self}';
import { Self as EntryGroup    } from '{ti2c:Package/Entry/Collection/Group}';
import { Self as EntryList     } from '{ti2c:Package/Entry/Collection/List}';
import { Self as EntrySet      } from '{ti2c:Package/Entry/Collection/Set}';
import { Self as EntryTwig     } from '{ti2c:Package/Entry/Collection/Twig}';
import { Self as Exports       } from '{ti2c:Package/Exports}';
import { Self as Formatter     } from '{ti2c:Format/Self}';
import { Self as GroupPackage  } from '{group@ti2c:Package/Self}';
import { Self as ListString    } from '{list@string}';
import { Self as ListTypeTi2c  } from '{list@<ti2c:Type/Ti2c/Types}';
import { Self as Path          } from '{ti2c:Path/Self}';
import { Self as Package       } from '{ti2c:Package/Self}';
import { Self as SetTypeTi2c   } from '{set@<ti2c:Type/Ti2c/Types}';
import { Self as TypeTi2cAny   } from '{ti2c:Type/Ti2c/Any}';
import { Self as TypeTi2cClass } from '{ti2c:Type/Ti2c/Class}';
import { Self as TypeTi2cGroup } from '{ti2c:Type/Ti2c/Collection/Group}';
import { Self as TypeTi2cList  } from '{ti2c:Type/Ti2c/Collection/List}';
import { Self as TypeTi2cSet   } from '{ti2c:Type/Ti2c/Collection/Set}';
import { Self as TypeTi2cTwig  } from '{ti2c:Type/Ti2c/Collection/Twig}';
import { Self as Walk          } from '{ti2c:Package/Walk}';

/*
| Handle to core, passed on init.
*/
let _core;

/*
| The packages as twig@Package
| Not a group since they are
| ordered by root directory (for proper subdirectory containment)
*/
let _packages;

/*
| All ti2c entries, Type is key, Entry is value.
*/
const _entries = new Map( );

/*
| Generated code is put here.
*/
let _codeGenDir;

/*
| The stack of entries to be prepared.
*/
const _prepareStack = [ ];

/*
| Adds a ti2c package.
|
| ~rootDir:    absolute directory of the package root
| ~srcDir:     absolute directory of source dir
| ~name:       name of the package
| ~codeGenDir: if set puts ti2c generated code in this directory
*/
def.static.addPackage =
	async function( rootDir, srcDir, name, codeGenDir )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 4 ) throw new Error( );
/**/	if( typeof( rootDir ) !== 'string' ) throw new Error( );
/**/	if( typeof( srcDir ) !== 'string' ) throw new Error( );
/**/	if( codeGenDir !== undefined && typeof( codeGenDir ) !== 'string' ) throw new Error( );
/**/}

	rootDir = Path.FromString( rootDir );
	srcDir = Path.FromString( srcDir );

	if( codeGenDir )
	{
		if( _codeGenDir ) throw new Error( );
		_codeGenDir = rootDir.addChunkD( codeGenDir );
	}

	{
		const pkgTi2c = _packages.get( 'ti2c' );
		if( pkgTi2c && name === 'ti2c' )
		{
			// a ti2c tool is a special case registering the ti2c package
			// albeit it already being initialized.
			if( rootDir !== pkgTi2c.rootDir ) throw new Error( );
			if( srcDir !== pkgTi2c.srcDir ) throw new Error( );
			return pkgTi2c;
		}
	}

	// package name already used?
	if( _packages.get( name ) ) throw new Error( );

	await _core.addPackage( name, rootDir.asString );

	const exports = await Exports.FromFile( rootDir.f( 'exports.json' ) );
	const pkg =
		Package.create(
			'exports', exports,
			'name', name,
			'rootDir', rootDir,
			'srcDir', srcDir,
		);
	_packages = _packages.set( name, pkg );
	return pkg;
};

/*
| Generates the code of a ti2c class.
*/
def.static.generate =
	async function( entry )
{
/**/if( CHECK && arguments.length !== 1 ) throw new Error( );

	const filename = entry.aPathCodeGen.asString;
	console.log( 'ti2c-code:', filename );

	for( let td of entry.dependencies )
	{
		await Self.loadEntry( td );
	}

	const ast = entry.ast;
	await file.write( filename, Formatter.format( ast ) );
};

/*
| Gets a list of all types of all entries.
*/
def.static.getAllEntryTypes =
	function( )
{
	return ListTypeTi2c.Array( Array.from( _entries.keys( ) ) );
};

/*
| Gets an entry.
|
| ~type: type of the entry to get.
*/
def.proto.getEntry =
def.static.getEntry =
	function( type )
{
	const entry = _entries.get( type );
	if( !entry ) throw new Error( );
	return entry;
};

/*
| Returns the package by name.
*/
def.proto.getPackage =
def.static.getPackage =
	function( name )
{
	return _packages.get( name );
};

/*
| Initializes the packages.
*/
def.static.init =
	async function( rootDir, core, listing )
{
	if( _packages ) throw new Error( );

	_core = core;

	_packages = GroupPackage.Empty;
	const aPathRoot = Path.FromString( rootDir );
	const codeObDir = aPathRoot.d( 'codeob' );

	await Self.addPackage(
		rootDir,          // package root dir
		rootDir + 'src/', // package src dir
		'ti2c',           // pkgName
		undefined,        // codeGenDir
	);

	const initEntry =
		async ( name ) =>
	{
		const type = TypeTi2cAny.FromString( name, Self.singleton, undefined );
		let entry = _entries.get( type );
		if( entry ) return entry;
		const coreEntry = core.addEntry( name );

		switch( type.ti2ctype )
		{
			case TypeTi2cClass:
			{
				const entryNameExtend = coreEntry.def.extend;
				let entryExtend;
				if( entryNameExtend )
				{
					const typeExtend = TypeTi2cClass.PkgName( 'ti2c', entryNameExtend );
					entryExtend = _entries.get( typeExtend );
					if( !entryExtend )
					{
						// FIXME fix convenient resolving of ti2c: in init phase.
						entryExtend =
							entryNameExtend.indexOf( '@' ) >= 0
							? await initEntry( entryNameExtend )
							: await initEntry( 'ti2c:' + entryNameExtend );
					}
				}
				entry =
					await EntryClass.Define(
						coreEntry, type, entryExtend, codeObDir, Self.singleton
					);
				break;
			}

			case TypeTi2cGroup:
			{
				entry =
					await EntryGroup.Define(
						coreEntry, type, codeObDir, Self.singleton
					);
				break;
			}

			case TypeTi2cList:
			{
				entry =
					await EntryList.Define(
						coreEntry, type, codeObDir, Self.singleton
					);
				break;
			}

			case TypeTi2cSet:
			{
				entry =
					await EntrySet.Define(
						coreEntry, type, codeObDir, Self.singleton
					);
				break;
			}

			case TypeTi2cTwig:
			{
				entry =
					await EntryTwig.Define(
						coreEntry, type, codeObDir, Self.singleton
					);
				break;
			}

			default: throw new Error( );
		}

		_entries.set( type, entry );
		return entry;
	};

	for( let name of listing )
	{
		await initEntry( name );
	}
};

/*
| Returns the entry.
| Loads it if it isn't already loaded.
|
| ~type: type id of the ti2c class to load
| ~init: true if in init mode
*/
def.static.loadEntry =
	async function( type )
{
	let entry = _entries.get( type );
	if( entry ) return entry;

	const coreEntry = await _core.addEntry( type.asString );
	if( coreEntry.loaded === 3 ) throw new Error( );

	switch( type.ti2ctype )
	{
		case TypeTi2cClass:
		{
			const pkgName = type.pkgName;
			const entryName = type.entryName;
			const aPath = Self.getPackage( pkgName ).srcDir.addChunkF( entryName + '.js' );
			await _core.loadEntry( coreEntry, pkgName, aPath.asString );

			let entryExtend;
			if( coreEntry.def.extend )
			{
				const eType = TypeTi2cAny.FromString( coreEntry.def.extend, Self.singleton, pkgName );
				entryExtend = await Self.loadEntry( eType );
			}

			entry = await EntryClass.Define( coreEntry, type, entryExtend, _codeGenDir, Self.singleton );
			break;
		}

		case TypeTi2cGroup:
			entry = await EntryGroup.Define( coreEntry, type, _codeGenDir, Self.singleton );
			break;

		case TypeTi2cList:
			entry = await EntryList.Define( coreEntry, type, _codeGenDir, Self.singleton );
			break;

		case TypeTi2cSet:
			entry = await EntrySet.Define( coreEntry, type, _codeGenDir, Self.singleton );
			break;

		case TypeTi2cTwig:
			entry = await EntryTwig.Define( coreEntry, type, _codeGenDir, Self.singleton );
			break;

		default:
			throw new Error( type.ti2ctype.__TI2C_NAME__ );
	}

	_entries.set( type, entry );
	_prepareStack.push( entry );
	if( type.isTi2cCollection )
	{
		for( let e of entry.typeItemsExpanded )
		{
			if( e.isTi2c ) await Self.loadEntry( e );
		}
	}
	return entry;
};

/*
| Returns the entry.
| Loads it if it isn't already loaded.
|
| ~type: type id of the ti2c class to load
| ~init: true if in init mode
*/
def.static.prepareEntry =
	async function( entry )
{
	const coreEntry = entry._coreEntry;

	if( coreEntry.loaded === 3 ) throw new Error( );

	// tests if the ti2c-code is out of date or not existing
	// and creates it if so.

	const aPathCodeGen = entry.aPathCodeGen;

	switch( entry.ti2ctype )
	{
		case EntryClass:
		{
			const aPathDef = entry.aPathDef;
			let inStat, outStat;
			try
			{
				inStat = await fs.stat( aPathDef.asString );
			}
			catch( e )
			{
				console.log( 'Cannot stat "' + aPathDef.asString + '"' );
				throw new Error( );
			}

			try
			{
				outStat = await fs.stat( aPathCodeGen.asString );
			}
			catch( e )
			{
				// ignore
			}

			if( !outStat || inStat.mtime > outStat.mtime )
			{
				await Self.generate( entry );
			}
			break;
		}

		case EntryGroup:
		case EntryList:
		case EntrySet:
		case EntryTwig:
		{
			// generates the file if not present
			try
			{
				await fs.stat( aPathCodeGen.asString );
			}
			catch( e )
			{
				await Self.generate( entry );
			}
			break;
		}

		default: throw new Error( );
	}

	await _core.loadEntryCode( coreEntry, aPathCodeGen.asString );
	if( coreEntry.loaded !== 2 ) throw new Error( );
	ti2c._prepare( coreEntry, entry.entryExtend?._coreEntry, util.inspect );
	coreEntry.loaded = 3;

	//for( let spec of entry._coreEntry.importSpecifiers )
	for( let td of entry.dependencies )
	{
		const te = await Self.loadEntry( td );

		if( te._coreEntry.loaded !== 3 )
		{
			_prepareStack.push( te );
		}
	}

	return entry;
};

/*
| The core requests an import specifier to be resolved
| into a normalized string.
|
| ~specifier: the specifier to resolve
| ~basePkgName:   the base package name
*/
def.static.TypeFromSpecifier =
	function( specifier, basePkgName )
{
	return TypeTi2cAny.FromString( specifier, Self.singleton, basePkgName );
};

/*
| Prepares all loaded entries.
*/
def.static.prepareEntries =
	async function( )
{
	while( _prepareStack.length > 0 )
	{
		const entry = _prepareStack.shift( );
		if( entry._coreEntry.loaded !== 3 )
		{
			await Self.prepareEntry( entry );
		}
	}
};

/*
| Recursive helper for DependencyWalk.
|
| setTypes: the set of types being built
| entry:    entry currently been inspected
| parents:  parents already walked (because of circles)
*/
async function walkHelper( entry, setTypes, setPackageNames, parents )
{
	const type = entry.type;

	if( setTypes.has( type ) || parents.has( type ) ) return;

	if( type.ti2ctype === TypeTi2cClass )
	{
		setPackageNames.add( type.pkgName );
	}

	parents = parents.add( type );

	for( let typeDep of entry.dependencies )
	{
		const entryDep = Self.getEntry( typeDep );
		await walkHelper( entryDep, setTypes, setPackageNames, parents );
	}

	setTypes.add( type );
}

/*
| Creates the a list of all dependencies by walking an entry.
|
| The list is sorted such that extended entries come first.
|
| ~nameRoot: name of item to start the walk with
*/
def.static.walk =
	async function( nameRoot )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 1 ) throw new Error( );
/**/	if( typeof( nameRoot ) !== 'string' ) throw new Error( );
/**/}

	const typeRoot = TypeTi2cClass.FromString( nameRoot, Self.singleton, undefined );
	const entryRoot = Self.getEntry( typeRoot );

	const setTypes = new Set( );
	const setPackageNames = new Set( );
	await walkHelper( entryRoot, setTypes, setPackageNames, SetTypeTi2c.Empty );
	const listTypes = Array.from( setTypes );

	// sorts the list of types so extended stuff comes first
	listTypes.sort(
		( ta, tb ) =>
		{
			const ea = Self.getEntry( ta );
			const eb = Self.getEntry( tb );

			const ela = ea.extendLevel;
			const elb = eb.extendLevel;

			if( ela < elb ) return -1;
			if( ela > elb ) return 1;

			const isa = ta.asString;
			const isb = tb.asString;

			if( isa < isb ) return -1;
			if( isa > isb ) return 1;

			return 0;
		}
	);

	const pkgNames = Array.from( setPackageNames ).sort( );

	return(
		Walk.create(
			'pkgNames', ListString.Array( pkgNames ),
			'types', ListTypeTi2c.Array( listTypes ),
		)
	);
};
