/*
| A group of exports
*/
def.attributes =
{
	// exported classes
	classes: { type: 'group@string' },

	// exported typeSets
	typeSets: { type: 'group@string' },
};

import * as file from '../core/file';

import { Self as GroupString     } from '{group@string}';
import { Self as GroupListString } from '{group@list@string}';
import { Self as ListString      } from '{list@string}';
import { Self as Path            } from '{ti2c:Path/Self}';

/*
| Creates it from a "exports.json" file.
|
| ~path: a Path pointing to the file
|
| returns undefined if the file does not exist.
*/
def.static.FromFile =
	async function( path )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 1 ) throw new Error( );
/**/	if( path.ti2ctype !== Path ) throw new Error( );
/**/}

	const filename = path.asString;
	const text = await file.readExisting( filename );
	if( !text )
	{
		return undefined;
	}

	const group = JSON.parse( text );

	let classes = { };
	let typeSets = { };

	for( let key of Object.keys( group ) )
	{
		const val = group[ key ];
		if( key[ 0 ] === '<' )
		{
			key = key.substring( 1 );
			typeSets[ key ] = val;
		}
		else
		{
			classes[ key ] = val;
		}
	}

	return(
		Self.create(
			'classes', GroupString.Table( classes ),
			'typeSets', GroupString.Table( typeSets ),
		)
	);
};

/*
| Creates a reverse look group of the exported stuff.
*/
def.lazy.reverseClassLookupTable =
	function( )
{
	const classes = this.classes;
	const table = { };

	for( let key of classes.keys )
	{
		const val = classes.get( key );
		let list = table[ val ];
		if( !list ) list = table[ val ] = [ ];
		list.push( key );
	}

	for( let key of Object.keys( table ) )
	{
		table[ key ] = ListString.Array( table[ key ] );
	}

	return GroupListString.Table( table );
};
