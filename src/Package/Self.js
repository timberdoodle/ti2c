/*
| A ti2c package.
*/
def.attributes =
{
	// exports of the package
	exports: { type: [ 'undefined', 'Package/Exports' ] },

	// name for this tree
	name: { type: 'string' },

	// the absolute path of the root directory of the package
	rootDir: { type: 'Path/Self' },

	// source directory (relative to dir)
	srcDir: { type: 'Path/Self' },
};

import { Self as PackageManager } from '{ti2c:Package/Manager}';
import { Self as TypeTi2cClass  } from '{ti2c:Type/Ti2c/Class}';

/*
| Imports a ti2c.
|
| Interface to be used by user.
*/
def.proto.import =
	async function( specifier )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 1 ) throw new Error( );
/**/	if( typeof( specifier ) !== 'string' ) throw new Error( );
/**/}

	if( specifier.indexOf( ':' ) >= 0 ) throw new Error( );

	const type = TypeTi2cClass.PkgName( this.name, specifier );
	const entry = await PackageManager.loadEntry( type );
	await PackageManager.prepareEntries( );
	return entry._coreEntry.Self;
};
