/*
| Runs the node Read-Eval-Print-Loop for debugging ti2c.
*/
Error.stackTraceLimit = Infinity;
global.CHECK = true;
global.NODE = true;

await import( '../root.js' );
const pkg = await ti2c.register( 'ti2c', import.meta, 'src/', 'Repl/Start', 'codegen/' );
const Root = await pkg.import( 'Repl/Root' );
Root.init( );
