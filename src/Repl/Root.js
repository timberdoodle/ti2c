/*
| Runs the node Read-Eval-Print-Loop for debugging ti2c.
*/
def.abstract = true;

import util from 'node:util';
globalThis.util = util;

import repl from 'repl';

import { Self as Ast       } from '{Ast/Self}';
import { Self as Path      } from '{Path/Self}';
import { Self as Formatter } from '{Format/Self}';
import { Self as Parser    } from '{Parser/Self}';

/*
| Comfort function, inspects with infinite depth as default.
*/
globalThis.inspect = ( obj ) => globalThis.util.inspect( obj, { depth: null } );

def.static.init =
	function( )
{
	globalThis.Formatter = Formatter;
	globalThis.Parser    = Parser;
	globalThis.Path      = Path;
	globalThis.Ast       = Ast;
	globalThis.parse     = global.$ = Parser.statement;
	const formatter = globalThis.formatter = Formatter.create( );
	globalThis.format    = formatter.format;
	globalThis.$$ =
		function( arg )
	{
		console.log( formatter.format( arg ) );
	};
	const rs = repl.start( 'repl> ' );
	rs.setupHistory( '.repl-history', ( ) => { } );
};
