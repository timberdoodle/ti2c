/*
| A parser for javascript.
|
| The parser must not use Ast/Self,
| because that one is using the parser.
*/
def.abstract = true;

import { Self as And                       } from '{ti2c:Ast/And}';
import { Self as ArrayLiteral              } from '{ti2c:Ast/ArrayLiteral}';
import { Self as ArrowFunction             } from '{ti2c:Ast/ArrowFunction}';
import { Self as Assign                    } from '{ti2c:Ast/Assign}';
import { Self as AstNumber                 } from '{ti2c:Ast/Number}';
import { Self as AstString                 } from '{ti2c:Ast/String}';
import { Self as AstBoolean                } from '{ti2c:Ast/Boolean}';
import { Self as Await                     } from '{ti2c:Ast/Await}';
import { Self as BitwiseAnd                } from '{ti2c:Ast/BitwiseAnd}';
import { Self as BitwiseAndAssign          } from '{ti2c:Ast/BitwiseAndAssign}';
import { Self as BitwiseLeftShift          } from '{ti2c:Ast/BitwiseLeftShift}';
import { Self as BitwiseNot                } from '{ti2c:Ast/BitwiseNot}';
import { Self as BitwiseOr                 } from '{ti2c:Ast/BitwiseOr}';
import { Self as BitwiseOrAssign           } from '{ti2c:Ast/BitwiseOrAssign}';
import { Self as BitwiseRightShift         } from '{ti2c:Ast/BitwiseRightShift}';
import { Self as BitwiseUnsignedRightShift } from '{ti2c:Ast/BitwiseUnsignedRightShift}';
import { Self as BitwiseXor                } from '{ti2c:Ast/BitwiseXor}';
import { Self as BitwiseXorAssign          } from '{ti2c:Ast/BitwiseXorAssign}';
import { Self as Block                     } from '{ti2c:Ast/Block}';
import { Self as Break                     } from '{ti2c:Ast/Break}';
import { Self as Call                      } from '{ti2c:Ast/Call}';
import { Self as Case                      } from '{ti2c:Ast/Case}';
import { Self as Comma                     } from '{ti2c:Ast/Comma}';
import { Self as Condition                 } from '{ti2c:Ast/Condition}';
import { Self as ConditionalDot            } from '{ti2c:Ast/ConditionalDot}';
import { Self as Const                     } from '{ti2c:Ast/Const}';
import { Self as Declaration               } from '{ti2c:Ast/Declaration}';
import { Self as Delete                    } from '{ti2c:Ast/Delete}';
import { Self as DestructDecl              } from '{ti2c:Ast/DestructDecl}';
import { Self as Differs                   } from '{ti2c:Ast/Differs}';
import { Self as Divide                    } from '{ti2c:Ast/Divide}';
import { Self as DivideAssign              } from '{ti2c:Ast/DivideAssign}';
import { Self as Dot                       } from '{ti2c:Ast/Dot}';
import { Self as DoWhile                   } from '{ti2c:Ast/DoWhile}';
import { Self as Equals                    } from '{ti2c:Ast/Equals}';
import { Self as For                       } from '{ti2c:Ast/For}';
import { Self as ForIn                     } from '{ti2c:Ast/ForIn}';
import { Self as ForOf                     } from '{ti2c:Ast/ForOf}';
import { Self as Func                      } from '{ti2c:Ast/Func/Self}';
import { Self as FuncArg                   } from '{ti2c:Ast/Func/Arg}';
import { Self as GreaterOrEqual            } from '{ti2c:Ast/GreaterOrEqual}';
import { Self as GreaterThan               } from '{ti2c:Ast/GreaterThan}';
import { Self as If                        } from '{ti2c:Ast/If}';
import { Self as Import                    } from '{ti2c:Ast/Import}';
import { Self as Instanceof                } from '{ti2c:Ast/Instanceof}';
import { Self as LessOrEqual               } from '{ti2c:Ast/LessOrEqual}';
import { Self as LessThan                  } from '{ti2c:Ast/LessThan}';
import { Self as Let                       } from '{ti2c:Ast/Let}';
import { Self as Lexer                     } from '{ti2c:Lexer/Self}';
import { Self as ListCase                  } from '{list@ti2c:Ast/Case}';
import { Self as ListExpr                  } from '{list@<ti2c:Ast/Type/Expr}';
import { Self as ListFuncArg               } from '{list@ti2c:Ast/Func/Arg}';
import { Self as ListParserToken           } from '{list@<ti2c:Parser/Tokens}';
import { Self as ListString                } from '{list@string}';
import { Self as Member                    } from '{ti2c:Ast/Member}';
import { Self as Minus                     } from '{ti2c:Ast/Minus}';
import { Self as MinusAssign               } from '{ti2c:Ast/MinusAssign}';
import { Self as Multiply                  } from '{ti2c:Ast/Multiply}';
import { Self as MultiplyAssign            } from '{ti2c:Ast/MultiplyAssign}';
import { Self as Negate                    } from '{ti2c:Ast/Negate}';
import { Self as New                       } from '{ti2c:Ast/New}';
import { Self as Not                       } from '{ti2c:Ast/Not}';
import { Self as Null                      } from '{ti2c:Ast/Null}';
import { Self as NullishCoalescence        } from '{ti2c:Ast/NullishCoalescence}';
import { Self as ObjLiteral                } from '{ti2c:Ast/ObjLiteral}';
import { Self as Or                        } from '{ti2c:Ast/Or}';
import { Self as ParserHandle              } from '{ti2c:Parser/Handle}';
import { Self as Plus                      } from '{ti2c:Ast/Plus}';
import { Self as PlusAssign                } from '{ti2c:Ast/PlusAssign}';
import { Self as PostDecrement             } from '{ti2c:Ast/PostDecrement}';
import { Self as PostIncrement             } from '{ti2c:Ast/PostIncrement}';
import { Self as PreDecrement              } from '{ti2c:Ast/PreDecrement}';
import { Self as PreIncrement              } from '{ti2c:Ast/PreIncrement}';
import { Self as Regex                     } from '{ti2c:Ast/Regex}';
import { Self as Remainder                 } from '{ti2c:Ast/Remainder}';
import { Self as RemainderAssign           } from '{ti2c:Ast/RemainderAssign}';
import { Self as Return                    } from '{ti2c:Ast/Return}';
import { Self as Spread                    } from '{ti2c:Ast/Spread}';
import { Self as State                     } from '{ti2c:Parser/State}';
import { Self as Switch                    } from '{ti2c:Ast/Switch}';
import { Self as Throw                     } from '{ti2c:Ast/Throw}';
import { Self as Token                     } from '{ti2c:Lexer/Token}';
import { Self as Try                       } from '{ti2c:Ast/Try}';
import { Self as Typeof                    } from '{ti2c:Ast/Typeof}';
import { Self as Undefined                 } from '{ti2c:Ast/Undefined}';
import { Self as Var                       } from '{ti2c:Ast/Var}';
import { Self as While                     } from '{ti2c:Ast/While}';
import { Self as Yield                     } from '{ti2c:Ast/Yield}';

/*
| Parses code to create an ast tree.
|
| ~args: list of strings to parse or ast subtrees
*/
def.static.block =
	function( ...args )
{
	const tokens = Self._tokenizeArray( args );
	let block = Block.Empty;
	if( tokens.length === 0 ) return block;

	let state = State.create( 'tokens', tokens, 'pos', 0, 'ast', undefined );

	while( !state.reachedEnd )
	{
		state = Self._parseToken( state, Self._parseStatementStart );
		block = block.append( state.ast );
		if( !state.reachedEnd && state.current.type === ';' )
		{
			state = state.advance( pass );
		}
		state = state.stay( undefined );
	}

	return block;
};

/*
| Parses code to create an ast tree.
|
| ~array: the array of elements to parse
| ~start: parses at 'block', 'expression', or 'statement' level
*/
def.static.parseArray =
	function( array, start )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 ) throw new Error( );
/**/	if( !Array.isArray( array ) ) throw new Error( );
/**/}

	const tokens = Self._tokenizeArray( array );
	if( tokens.length === 0 ) return undefined;

	let state = State.create( 'tokens', tokens, 'pos', 0, 'ast', undefined );

	switch( start )
	{
		case 'expr':
			state = Self._parseToken( state, Self._parseExpressionStart );
			break;

		case 'statement':
			state = Self._parseToken( state, Self._parseStatementStart );
			break;

		default: throw new Error( );
	}

	// ignores one ending semicolon
	if( !state.reachedEnd && state.current.type === ';' ) state = state.advance( pass );
	if( !state.reachedEnd ) throw new Error( 'too many tokens' );

	return state.ast;
};

/*
| Parses code to create an ast tree.
|
| ~args: list of strings to parse or ast subtrees
*/
def.static.statement =
	function( ...args )
{
	return Self.parseArray( args, 'statement' );
};

/*
| Parses code as expression.
|
| ~args: list of strings to parse or ast subtrees.
*/
def.static.expr =
	function( ...args )
{
	return Self.parseArray( args, 'expr' );
};


/*
| Forces expr into a block.
*/
def.static._forceBlock =
	function( expr )
{
	return(
		expr.ti2ctype === Block
		? expr
		: Block.Empty.append( expr )
	);
};

/*
| Returns the handle for a state
|
| ~state: current state
| ~statement: true if a statement is allowed here
*/
def.static._getHandle =
	function( state, statement )
{
	let handle;

	if( !state.ast )
	{
		if( statement ) handle = Self._statementHandles[ state.current.type ];
		if( !handle ) handle = Self._leftHandles[ state.current.type ];
	}
	else
	{
		handle = Self._rightHandles[ state.current.type ];
	}

	if( !handle ) state.current.error( 'unexpected token' );

	return handle;
};

/*
| Handler for array literals.
|
| ~state: current parser state
| ~handle: operator handle
*/
def.static._handleArrayLiteral =
	function( state, handle )
{
/**/if( CHECK && state.ast ) throw new Error( );

	// this is an array literal
	let alit = ArrayLiteral.Empty;
	state = state.advance( undefined );
	if( state.current.type !== ']' )
	{
		// there are elements
		while( state.current.type !== ']' )
		{
			state = Self._parseToken( state, handle );
			alit = alit.append( state.ast );

			// finished array literal?
			if( state.current.type === ']' ) break;

			if( state.current.type !== ',' )
			{
				state.current.error( 'parse error' );
				throw new Error( );
			}

			state = state.advance( undefined );
		}
	}

	// advances over closing square bracket
	return state.advance( alit );
};

/*
| Handlow for arrow functions.
|
| ~state: current parser state
| ~handle: operator handle
*/
def.static._handleArrow =
	function( state, handle )
{
	// unwinds the state to be identifiers.
	let ast = state.ast;
	let args = ListString.Empty;

	if( ast === undefined )
	{
		// it's a ( ) =>
	}
	else if( ast.ti2ctype === Var )
	{
		args = args.append( ast.name );
	}
	else if( ast.ti2ctype === Comma )
	{
		while( ast.ti2ctype === Comma )
		{
			const right = ast.right;
			if( right.ti2ctype !== Var )
			{
				state.current.error( 'invalid arrow function' );
				throw new Error( );
			}
			args = args.append( right.name );
			ast = ast.left;
		}
		if( ast.ti2ctype !== Var )
		{
			state.current.error( 'invalid arrow function' );
			throw new Error( );
		}
		const args2 = args.append( ast.name );
		args = ListString.Empty;
		for( let s of args2.reverse( ) )
		{
			args = args.append( s );
		}
	}
	else
	{
		state.current.error( 'invalid arrow function' );
		throw new Error( );
	}

	// goes over the =>
	state = state.advance( undefined );

	if( state.current.type === '{' )
	{
		// it is a function body.
		state = Self._handleBlock( state );
	}
	else
	{
		// it is an expression.
		//state = Self._parseToken( state, Self._parseExpressionStart );

		// special expression start handling which should stop at a ,
		state = Self._parseToken(
			state, ParserHandle.Handler( '_handleParserError', 1.5 )
		);
	}

	const body = state.ast;
	return state.stay( ArrowFunction.create( 'args', args, 'body', body ) );
};

/*
| Handler for await operations.
|
| ~state: current parser state
| ~handle: operator handle
*/
def.static._handleAwait =
	function( state, handle )
{
	const ast = state.ast;

	if( ast ) throw new Error( 'parse error' );
	state = Self._parseToken( state.advance( undefined ), handle );
	return state.stay( Await.create( 'call', state.ast ) );
};

/*
| Handler for boolean
|
| ~state: current parser state
*/
def.static._handleBlock =
	function( state )
{
/**/if( CHECK )
/**/{
/**/	if( state.ast ) throw new Error( );
/**/	if( state.current.type !== '{' ) throw new Error( );
/**/}

	state = state.advance( pass ); // skips the '{';
	let block = Block.Empty;
	while( state.current.type !== '}' )
	{
		state = Self._parseToken( state, Self._parseStatementStart );
		block = block.append( state.ast );
		if( state.current.type === ';' )
		{
			state = state.advance( pass );
		}
		state = state.stay( undefined );
	}

	return state.advance( block );
};

/*
| Handler for boolean literals.
|
| ~state: current parser state
*/
def.static._handleBooleanLiteral =
	function( state )
{
	const current = state.current;

	let bool;
	switch( current.type )
	{
		case 'true': bool = true; break;
		case 'false': bool = false; break;
		default: throw new Error( );
	}

	return(
		state.advance(
			AstBoolean.create( 'boolean', bool, 'token', current )
		)
	);
};

/*
| Handler for break.
|
| ~state: current parser state
*/
def.static._handleBreak =
	function( state )
{
/**/if( CHECK )
/**/{
/**/	if( state.ast ) throw new Error( );
/**/	if( state.current.type !== 'break' ) throw new Error( );
/**/}

	return state.advance( Break.create( 'token', state.current ) );
};

/*
| Handler for ( ) in case of calls.
|
| ~state: current parser state
| ~handle: operator handle
*/
def.static._handleCall =
	function( state, handle )
{
	const ast = state.ast;

/**/if( CHECK && !ast ) throw new Error( );

	let call = Call.create( 'func', ast );
	state = state.advance( undefined );
	if( state.reachedEnd ) throw new Error( 'expected ")"' );

	if( state.current.type !== ')' )
	{
		// there are arguments
		for( ;; )
		{
			do
			{
				state = Self._parseToken( state, handle );
				if( state.current.type === ';' ) throw new Error( 'parse error' );
			}
			while(
				!state.reachedEnd
				&& state.current.type !== ')'
				&& state.current.type !== ','
			);

			if( state.reachedEnd ) throw new Error( 'expected ")"' );

			if( state.ast ) call = call.arg( state.ast );

			// finished call?
			if( state.current.type === ')' ) break;
			if( state.current.type === ',' )
			{
				state = state.advance( undefined );
				if( state.current.type === ')' ) break;
				continue;
			}

			state.current.error( 'parse error' );
			throw new Error( );
		}
	}

	// advances over closing bracket
	return state.advance( call );
};

/*
| Handler for ? : conditionals
|
| ~state:  current parser state
| ~handle: operator handle
*/
def.static._handleCondition =
	function( state, handle )
{
	const condition = state.ast;

/**/if( CHECK )
/**/{
/**/	if( !condition ) throw new Error( );
/**/	if( state.current.type !== '?' ) throw new Error( );
/**/}

	state = state.advance( undefined );
	if( state.reachedEnd ) throw new Error( 'parse error' );
	state = Self._parseToken( state, handle );
	const then = state.ast;
	state.expect( ':' );
	state = Self._parseToken( state.advance( undefined ), handle );

	return(
		state.stay(
			Condition.create(
				'condition', condition,
				'then', then,
				'elsewise', state.ast
			)
		)
	);
};

/*
| Handler for conditional dots.
|
| ~state:  current parser state
| ~handle: operator handle
*/
def.static._handleConditionalDot =
	function( state, handle )
{
	const ast = state.ast;
	if( !ast ) throw new Error( );
	let name = state.preview;
	if( name.type === 'delete' )
	{
		// "delete" may be used as identifier here
		name = name.create( 'type', 'identifier', 'value', 'delete' );
	}
	else if( name.type !== 'identifier' )
	{
		state.current.error( 'parse error' );
		throw new Error( );
	}

	return(
		state.create(
			'ast', ConditionalDot.create( 'expr', state.ast, 'member', name.value ),
			'pos', state.pos + 2
		)
	);
};

/*
| Handler for do (while).
|
| ~state: current parser state
*/
def.static._handleDo =
	function( state )
{
/**/if( CHECK )
/**/{
/**/	if( state.ast ) throw new Error( );
/**/	if( state.current.type !== 'do' ) throw new Error( );
/**/}

	state = state.advance( pass );
	state = Self._parseToken( state, Self._parseStatementStart );
	const body = Self._forceBlock( state.ast );
	if( state.current.type === ';' ) state = state.advance( pass );
	state.expect( 'while' );
	state = state.advance( undefined, '(' );
	state = state.advance( pass );
	state = Self._parseToken( state, Self._parseStatementStart );
	const condition = state.ast;
	state.expect( ')' );
	return state.advance( DoWhile.create( 'body', body, 'condition', condition ) );
};

/*
| Handler for dots.
|
| ~state: current parser state
| ~handle: operator handle
*/
def.static._handleDot =
	function( state, handle )
{
	const ast = state.ast;
	if( !ast ) throw new Error( );

	const tokenDot = state.current;
	const tokenMember = state.preview.forceIdentifier;

	if( tokenMember.type !== 'identifier' )
	{
		state.current.error( 'parse error' );
		throw new Error( );
	}

	const aast =
		Dot.create(
			'expr', state.ast,
			'member', tokenMember.value,
			'tokenDot', tokenDot,
			'tokenMember', tokenMember,
		);

	return(
		state.create(
			'ast', aast,
			'pos', state.pos + 2
		)
	);
};

/*
| Generic handler for dualistic operations.
|
| ~state: current parser state
| ~handl: operator handle
*/
def.static._handleDualisticOps =
	function( state, handle )
{
	const ast = state.ast;
	if( !ast ) throw new Error( 'parse error' );
	const current = state.current;
	state = Self._parseToken( state.advance( undefined ), handle );
	return(
		state.stay(
			handle.astCreator.create(
				'left', ast,
				'right', state.ast,
				'token', current,
			)
		)
	);
};

/*
| Handler for for.
|
| ~state: current parser state
*/
def.static._handleFor =
	function( state )
{
/**/if( CHECK )
/**/{
/**/	if( state.ast ) throw new Error( );
/**/	if( state.current.type !== 'for' ) throw new Error( );
/**/}

	state = state.advance( pass, '(' );
	state = state.advance( undefined );
	if( state.reachedEnd ) throw new Error( 'unexpected end of file' );

	if( state.current.type === 'let' )
	{
		state = Self._handleLet( state );
	}
	else if( state.current.type !== ';' )
	{
		state = Self._parseToken( state, Self._parseExpressionStart );
	}

	if( state.current.type === 'in' || state.current.type === 'of' )
	{
		let loopVar = state.ast;
		let letVar = false;
		const forType = state.current.type;
		if( loopVar.ti2ctype === Let )
		{
			const letEntry = loopVar.trivial;
			if( !letEntry ) throw new Error( 'invalid let in for of/in loop' );
			if( letEntry.assign ) throw new Error( 'invalid assignment in for of/in loop' );
			loopVar = Var.create( 'name', letEntry.name );
			letVar = true;
		}
		else if( loopVar.ti2ctype !== Var )
		{
			throw new Error( 'invalid loop variable' );
		}

		state = Self._parseToken( state.advance( undefined ), Self._parseExpressionStart );
		const obj = state.ast;
		state.expect( ')' );
		state = Self._parseToken( state.advance( undefined ), Self._parseStatementStart );
		const block = state.ast;

		if( !Self._noSemicolon.has( block.ti2ctype ) )
		{
			if( !state.current || state.current.type !== ';' ) throw new Error( 'expected ";"' );
			state = state.advance( pass );
		}

		switch( forType )
		{
			case 'in':
				return(
					state.stay(
						ForIn.create(
							'variable', loopVar,
							'letVar', letVar,
							'object', obj,
							'block', Self._forceBlock( block )
						)
					)
				);
			case 'of':
				return(
					state.stay(
						ForOf.create(
							'variable', loopVar,
							'letVar', letVar,
							'object', obj,
							'block', Self._forceBlock( block )
						)
					)
				);
			default: throw new Error( );
		}
	}

	if( state.current.type !== ';' ) throw new Error( '";" or "in" or "of" expected' );
	state = state.advance( pass );
	const init = state.ast;
	state = state.stay( undefined );
	if( state.current.type !== ';' )
	{
		state = Self._parseToken( state, Self._parseExpressionStart );
	}
	const condition = state.ast;
	state.expect( ';' );
	state = state.advance( undefined );
	if( state.current.type !== ')' )
	{
		state = Self._parseToken( state, Self._parseExpressionStart );
	}
	const iterate = state.ast;
	state.expect( ')' );
	state = state.advance( undefined );
	state = Self._parseToken( state, Self._parseStatementStart );
	const block = state.ast;
	if( !Self._noSemicolon.has( block.ti2ctype ) )
	{
		if( !state.current || state.current.type !== ';' ) throw new Error( 'expected ";"' );
		state = state.advance( pass );
	}
	return(
		state.stay(
			For.create(
				'init', init,
				'condition', condition,
				'iterate', iterate,
				'block', Self._forceBlock( block )
			)
		)
	);
};

/*
| Handler for function declaration (those with function names).
|
| ~state: current parser state
*/
def.static._handleFunctionDeclaration =
	function( state )
{
/**/if( CHECK && state.ast ) throw new Error( );

	let isAsync = false;
	if( state.current.type === 'async' )
	{
		isAsync = true;
		state = state.advance( pass, 'function' );
	}
	state = state.advance( pass, 'identifier' );
	const name = state.current.value;
	state = state.advance( pass, '(' );
	state = state.advance( pass );
	let args = ListFuncArg.Empty;
	while( state.current.type !== ')' )
	{
		let rest = false;
		if( state.current.type === '...' )
		{
			rest = true;
			state = state.advance( pass );
		}
		state.expect( 'identifier' );
		args = args.append( FuncArg.create( 'name', state.current.value, 'rest', rest ) );
		state = state.advance( pass, ')', ',' );
		if( state.current.type === ',' ) state = state.advance( pass );
	}
	state = state.advance( pass, '{' );
	state = state.advance( );

	let body = Block.Empty;
	while( state.current.type !== '}' )
	{
		state = state.stay( undefined );
		state = Self._parseToken( state, Self._parseStatementStart );
		body = body.append( state.ast );
		if( state.current.type === ';' ) state = state.advance( pass );
	}
	return(
		state.advance(
			Func.create(
				'args', args,
				'body', body,
				'isAsync', isAsync,
				'name', name,
			)
		)
	);
};

/*
| Handler for ( ) in case of groupings.
|
| ~state: current parser state
| ~handle:  operator handle
*/
def.static._handleGrouping =
	function( state, handle )
{
/**/if( CHECK && state.ast ) throw new Error( );

	// it may actually be ( ) => arrow function
	if( state.foresight( ')', '=>' ) )
	{
		return(
			Self._handleArrow(
				state.advance( undefined ).advance( undefined ),
				Self._rightHandles[ '=>' ]
			)
		);
	}

	state = state.advance( undefined );
	state = Self._parseToken( state, handle );
	while( state.current.type !== ')' )
	{
		state = Self._parseToken( state, handle );
		if( state.reachedEnd ) throw new Error( 'expected ")"' );
	}
	return state.advance( pass );
};

/*
| Handler for identifiers.
|
| ~state: current parser state
*/
def.static._handleIdentifier =
	function( state )
{
	const current = state.current;
	return state.advance( Var.create( 'name', current.value, 'token', current ) );
};

/*
| Handler for if.
|
| ~state: current parser state
*/
def.static._handleIf =
	function( state )
{
/**/if( CHECK )
/**/{
/**/	if( state.ast ) throw new Error( );
/**/	if( state.current.type !== 'if' ) throw new Error( );
/**/}

	state = state.advance( pass, '(' );
	state = state.advance( pass );
	state = Self._parseToken( state, Self._parseExpressionStart );
	const condition = state.ast;
	state.expect( ')' );
	state = Self._parseToken( state.advance( undefined ), Self._parseStatementStart );

	const then = state.ast;
	if( !Self._noSemicolon.has( then.ti2ctype ) )
	{
		if( !state.current || state.current.type !== ';' ) throw new Error( 'expected ";"' );
		state = state.advance( pass );
	}

	let elsewise;
	if( state.current && state.current.type === 'else' )
	{
		state = Self._parseToken( state.advance( undefined ), Self._parseStatementStart );
		elsewise = state.ast;

		if( !Self._noSemicolon.has( elsewise.ti2ctype ) )
		{
			if( !state.current || state.current.type !== ';' ) throw new Error( 'expected ";"' );
			state = state.advance( pass );
		}
	}

	return(
		state.stay(
			If.create(
				'condition', condition,
				'then', Self._forceBlock( then ),
				'elsewise', elsewise && Self._forceBlock( elsewise )
			)
		)
	);
};

/*
| Handler for import statement.
|
| ~state: current parser state
*/
def.static._handleImport =
	function( state )
{
/**/if( CHECK )
/**/{
/**/	if( state.ast ) throw new Error( );
/**/	if( state.current.type !== 'import' ) throw new Error( );
/**/}

	state = state.advance( undefined, '{' );

	state = state.advance( undefined, 'identifier' );
	const exportName = state.current.value;

	state = state.advance( undefined, 'identifier' );
	if( state.current.value !== 'as' ) state.current.error( 'unexpected token' );

	state = state.advance( undefined, 'identifier' );
	const aliasName = state.current.value;

	state = state.advance( undefined, '}' );

	state = state.advance( undefined, 'identifier' );
	if( state.current.value !== 'from' ) state.current.error( 'unexpected token' );

	state = state.advance( undefined, 'string' );
	const moduleName = state.current.value;

	return(
		state.advance(
			Import.create(
				'aliasName', aliasName,
				'exportName', exportName,
				'moduleName', moduleName,
			)
		)
	);
};

/*
| Handler for inline function
|
| ~state: current parser state
*/
def.static._handleInlineFunction =
	function( state )
{
/**/if( CHECK && state.ast ) throw new Error( );

	let isAsync = false;
	if( state.current.type === 'async' )
	{
		isAsync = true;
		state = state.advance( pass, 'function' );
	}

	state = state.advance( pass, '(', '*' );

	let isGenerator = false;
	if( state.current.type === '*' )
	{
		isGenerator = true;
		state = state.advance( pass, '(' );
	}

	state = state.advance( pass );
	let args = ListFuncArg.Empty;
	while( state.current.type !== ')' )
	{
		let rest = false;
		if( state.current.type === '...' )
		{
			rest = true;
			state = state.advance( pass );
		}

		state.expect( 'identifier' );
		args = args.append( FuncArg.create( 'name', state.current.value, 'rest', rest ) );
		state = state.advance( pass, ')', ',' );
		if( state.current.type === ',' ) state = state.advance( pass );
	}
	state = state.advance( pass, '{' );
	state = state.advance( );

	let body = Block.Empty;
	while( state.current.type !== '}' )
	{
		state = state.stay( undefined );
		state = Self._parseToken( state, Self._parseStatementStart );
		body = body.append( state.ast );
		if( state.current.type === ';' ) state = state.advance( pass );
	}

	return(
		state.advance(
			Func.create(
				'args', args,
				'body', body,
				'isAsync', isAsync,
				'isGenerator', isGenerator,
			)
		)
	);
};

/*
| Handler for inline import statement.
|
| ~state: current parser state
*/
def.static._handleInlineImport =
	function( state )
{
/**/if( CHECK )
/**/{
/**/	if( state.ast ) throw new Error( );
/**/	if( state.current.type !== 'import' ) throw new Error( );
/**/}

	return state.advance( Var.create( 'name', 'import' ) );
};

/*
| Handler for member operations.
|
| ~state:  current parser state
| ~handle: operator handle
*/
def.static._handleMember =
	function( state, handle )
{
	const ast = state.ast;
/**/if( CHECK && !ast ) throw new Error( );

	state = Self._parseToken( state.advance( undefined ), handle );

	while( state.current.type !== ']' )
	{
		state = Self._parseToken( state, handle );
		if( state.reachedEnd ) throw new Error( );
	}

	const aast =
		Member.create(
			'expr', ast,
			'member', state.ast,
		);

	return state.advance( aast );
};

/*
| Generic handler for left side mono operations.
|
| ~state:  current parser state
| ~handle: operator handle
*/
def.static._handleMonoOps =
	function( state, handle )
{
	const ast = state.ast;
	const token = state.current;
	if( ast )
	{
		// postfix (e.g. increment or decrement)
		const aast =
			handle.astCreator.create(
				'expr', state.ast,
				'token', token,
			);
		return state.advance( aast );
	}
	else
	{
		// prefix (e.g. increment or decrement)
		state = Self._parseToken( state.advance( undefined ), handle );
		const aast =
			handle.astCreator.create(
				'expr', state.ast,
				'token', token,
			);
		return state.stay( aast );
	}
};

/*
| Handler for let.
|
| ~state: current parser state
*/
def.static._handleLet =
	function( state )
{
	const type = state.current.type;

/**/if( CHECK )
/**/{
/**/	if( state.ast ) throw new Error( );
/**/	if( type !== 'const' && type !== 'let' ) throw new Error( );
/**/}

	let decl = type === 'const' ? Const.Empty : Let.Empty;
	state = state.advance( pass );
	for( ;; )
	{
		if( state.reachedEnd ) return state.stay( decl );
		if( state.current.type === ';' ) return state.stay( decl );

		let entry;
		switch( state.current.type )
		{
			case 'identifier':
			{
				let name = state.current.value;
				state = state.advance( pass );
				if( state.reachedEnd )
				{
					let entry = Declaration.create( 'name', name );
					decl = decl.append( entry );
					return state.stay( decl );
				}
				entry = Declaration.create( 'name', name );
				break;
			}
			case '{':
			{
				state = Self._handleObjectLiteral( state, Self._leftHandles[ '{' ] );
				entry = DestructDecl.create( 'literal', state.ast );
				break;
			}
			default:
				state.current.error( 'unexpected token' );
				throw new Error( );
		}

		switch( state.current.type )
		{
			case '=':
			{
				state =
					Self._parseToken( state.advance( undefined ), Self._statementHandles[ 'let' ] );
				entry = entry.create( 'assign', state.ast );
				decl = decl.append( entry );
				if( !state.reachedEnd && state.current.type === ',' ) state = state.advance( pass );
				break;
			}

			case ',':
			{
				state = state.advance( undefined );
				decl = decl.append( entry );
				break;
			}

			case ';':
			case 'of':
			case 'in':
			{
				decl = decl.append( entry );
				return state.stay( decl );
			}

			default: throw new Error( 'unexpected token', state.current.type );
		}
	}
};

/*
| Handler for new operations.
|
| ~state: current parser state
| ~handle:  operator handle
*/
def.static._handleNew =
	function( state, handle )
{
	const ast = state.ast;
	if( ast ) throw new Error( 'parse error' );
	state = Self._parseToken( state.advance( undefined ), handle );
	return state.stay( New.create( 'call', state.ast ) );
};

/*
| Handler for null.
|
| ~state: current parser state
*/
def.static._handleNull =
	function( state )
{
	return state.advance( Null.create( 'token', state.current ) );
};

/*
| Handler for numeric literals.
|
| ~state: current parser state
*/
def.static._handleNumber =
	function( state )
{
	const current = state.current;

	return(
		state.advance(
			AstNumber.create( 'number', current.value, 'token', current )
		)
	);
};

/*
| Handler for { } Object literals
|
| ~state:  current parser state
| ~handle: operator handle
*/
def.static._handleObjectLiteral =
	function( state, handle )
{
	const ast = state.ast;
	if( ast ) throw new Error( 'parse error' );

	// this is an array literal
	let olit = ObjLiteral.Empty;

	// skips the '{'
	state = state.advance( undefined, '}', 'identifier', 'string', 'default' );

	while( state.current.type !== '}' )
	{
		let name;
		switch( state.current.type )
		{
			case 'default':
				name = 'default';
				break;

			case 'identifier':
			case 'string':
				name = state.current.value;
				break;

			// FUTURE add '[' case
			default:
				throw new Error( );
		}

		state = state.advance( pass );
		if( state.current.type === ':' )
		{
			state = state.advance( pass );
			state = Self._parseToken( state, handle );
			olit = olit.add( name, state.ast );
		}
		else
		{
			olit = olit.add( name, Undefined.create( ) );
		}
		state.expect( ',', '}' );
		if( state.current.type === ',' )
		{
			state = state.advance( undefined );
		}
	}

	// advances over closing square bracket
	return state.advance( olit );
};

/*
| Generic parser error.
*/
def.static._handleParserError =
	function( )
{
	throw new Error( 'parse error' );
};

/*
| Generic pass handler.
| It just passes back up
|
| ~state: current parser state
*/
def.static._handlePass =
	function( state )
{
	return state;
};

/*
| Handler for regular expressions.
|
| ~state: current parser state
*/
def.static._handleRegex =
	function( state )
{
	let flags, string;
	if( state.preview.type === 'regex-flags' )
	{
		string = state.current.value;
		state = state.advance( pass );
		flags = state.current.value;
	}
	else
	{
		string = state.current.value;
	}
	return state.advance( Regex.create( 'string', string, 'flags', flags ) );
};

/*
| Handler for return.
|
| ~state: current parser state
*/
def.static._handleReturn =
	function( state )
{
/**/if( CHECK )
/**/{
/**/	if( state.ast ) throw new Error( );
/**/	if( state.current.type !== 'return' ) throw new Error( );
/**/}
	state = state.advance( pass );
	if( state.current.type === ';' )
	{
		return state.stay( Return.create( ) );
	}

	state = Self._parseToken( state, Self._parseExpressionStart );
	return state.stay( Return.create( 'expr', state.ast ) );
};

/*
| Handler for string literals.
|
| ~state: current parser state
*/
def.static._handleString =
	function( state )
{
	const current = state.current;
	return(
		state.advance(
			AstString.create( 'string', current.value, 'token', current )
		)
	);
};

/*
| Handler for switch.
|
| ~state: current parser state
*/
def.static._handleSwitch =
	function( state )
{
/**/if( CHECK )
/**/{
/**/	if( state.ast ) throw new Error( );
/**/	if( state.current.type !== 'switch' ) throw new Error( );
/**/}

	state = state.advance( pass, '(' );
	state = state.advance( pass );
	state = Self._parseToken( state, Self._parseExpressionStart );

	const statement = state.ast;
	if( state.current.type !== ')' ) throw new Error( 'expected ":"' );
	state = state.advance( undefined, '{' );
	state = state.advance( pass, 'case', 'default', '}' );

	let cases = ListCase.Empty;
	let defaultCase;

	while( state.current.type !== '}' )
	{
		if( state.current.type === 'default' )
		{
			state = state.advance( undefined, ':' );
			state = state.advance( pass );
			let block = Block.Empty;
			while( state.current.type !== '}' && state.current.type !== 'case' )
			{
				state = Self._parseToken( state, Self._parseStatementStart );
				block = block.append( state.ast );
				if( state.current.type === ';' ) state = state.advance( pass );
				state = state.stay( undefined );
			}
			defaultCase = block;
			continue;
		}

		let values = ListExpr.Empty;
		while( state.current.type === 'case' )
		{
			state = state.advance( undefined );
			state = Self._parseToken( state, Self._parseExpressionStart );
			state.expect( ':' );
			values = values.append( state.ast );
			state = state.advance( undefined );
		}

		let block = Block.Empty;
		while(
			state.current.type !== '}'
			&& state.current.type !== 'case'
			&& state.current.type !== 'default'
		)
		{
			state = Self._parseToken( state, Self._parseStatementStart );
			block = block.append( state.ast );
			if( state.current.type === ';' ) state = state.advance( pass );
			state = state.stay( undefined );
		}
		cases = cases.append( Case.create( 'block', block, 'values', values ) );
	}
	return(
		state.advance(
			Switch.create(
				'cases', cases,
				'defaultCase', defaultCase,
				'statement', statement,
			)
		)
	);
};

/*
| Handler for throw.
|
| ~state: current parser state
*/
def.static._handleThrow =
	function( state )
{
/**/if( CHECK )
/**/{
/**/	if( state.ast ) throw new Error( );
/**/	if( state.current.type !== 'throw' ) throw new Error( );
/**/}

	state = Self._parseToken( state.advance( pass ), Self._parseExpressionStart );
	return state.stay( Throw.create( 'expr', state.ast ) );
};

/*
| Handler for try.
|
| ~state: current parser state
*/
def.static._handleTry =
	function( state )
{
/**/if( CHECK )
/**/{
/**/	if( state.ast ) throw new Error( );
/**/	if( state.current.type !== 'try' ) throw new Error( );
/**/}

	state = state.advance( pass, '{' );
	state = Self._parseToken( state, Self._parseStatementStart );
	const tryStatement = state.ast;
	state.expect( 'catch' );
	state = state.advance( undefined, '(' );
	state = state.advance( pass, 'identifier' );
	const exceptionVar = Var.create( 'name', state.current.value );
	state = state.advance( undefined, ')' );
	state = state.advance( undefined, '{' );
	state = Self._parseToken( state, Self._parseStatementStart );
	const catchStatement = state.ast;
	return(
		state.stay(
			Try.create(
				'exceptionVar', exceptionVar,
				'catchStatement', catchStatement,
				'tryStatement', tryStatement,
			)
		)
	);
};

/*
| Handler for undefined.
|
| ~state: current parser state
*/
def.static._handleUndefined =
	function( state )
{
	return state.advance( Undefined.create( 'token', state.current ) );
};
/*
| Handler for while.
|
| ~state: current parser state
*/
def.static._handleWhile =
	function( state )
{
/**/if( CHECK )
/**/{
/**/	if( state.ast ) throw new Error( );
/**/	if( state.current.type !== 'while' ) throw new Error( );
/**/}

	state = state.advance( pass, '(' );
	state = state.advance( pass );
	state = Self._parseToken( state, Self._parseExpressionStart );
	const condition = state.ast;
	state.expect( ')' );
	state = state.advance( undefined );
	state = Self._parseToken( state, Self._parseStatementStart );

	const body = state.ast;
	if( !Self._noSemicolon.has( body.ti2ctype ) )
	{
		state.expect( ';' );
		state = state.advance( pass );
	}

	return(
		state.stay(
			While.create(
				'body', Self._forceBlock( body ),
				'condition', condition,
			)
		)
	);
};

/*
| Left token specifications for unary operants.
|
| They are consulted when the current parse buffer is *not* empty.
*/
def.staticLazy._leftHandles = ( ) =>
{
	const c = ParserHandle.Handler;
	const s = { };

	s[ 'true'  ]      = c( '_handleBooleanLiteral', 100 );
	s[ 'false' ]      = c( '_handleBooleanLiteral', 100 );
	s.identifier      = c( '_handleIdentifier',     100 );
	s.null            = c( '_handleNull',           100 );
	s.number          = c( '_handleNumber',         100 );
	s.regex           = c( '_handleRegex',          100 );
	s.string          = c( '_handleString',         100 );
	s.undefined       = c( '_handleUndefined',      100 );
	s.import          = c( '_handleInlineImport',   100 );

	s[ '('        ]   = c( '_handleGrouping',        19, 'l2r' );
	s[ 'new'      ]   = c( '_handleNew',             17, 'r2l' );
	s[ '++'       ]   = c( '_handleMonoOps',         16, 'n/a', PreIncrement );
	s[ '--'       ]   = c( '_handleMonoOps',         16, 'n/a', PreDecrement );
	s[ '~'        ]   = c( '_handleMonoOps',         15, 'r2l', BitwiseNot );
	s[ '-'        ]   = c( '_handleMonoOps',         15, 'r2l', Negate );
	s[ '!'        ]   = c( '_handleMonoOps',         15, 'r2l', Not );
	s[ 'delete'   ]   = c( '_handleMonoOps',         15, 'r2l', Delete );
	s[ 'typeof'   ]   = c( '_handleMonoOps',         15, 'r2l', Typeof );
	s[ 'await'    ]   = c( '_handleMonoOps',         15, 'r2l', Await );
	s[ 'yield'    ]   = c( '_handleMonoOps',          2, 'r2l', Yield );
	s[ '...'      ]   = c( '_handleMonoOps',          2, 'n/a', Spread );
	s[ '['        ]   = c( '_handleArrayLiteral',   1.5, 'l2r' );
	s[ '{'        ]   = c( '_handleObjectLiteral',  1.5 );
	s[ ','        ]   = c( '_handleDualisticOps',     1, 'l2r', Comma );
	s[ 'async'    ]   = c( '_handleInlineFunction',   0 );
	s[ 'function' ]   = c( '_handleInlineFunction',   0 );

	return Object.freeze( s );
};

/*
| Right token specifications for unary operants.
|
| They are consulted when the current parse buffer is empty.
*/
def.staticLazy._rightHandles = ( ) =>
{
	const c = ParserHandle.Handler;
	const s = { };

	s[ '=>'         ] = c( '_handleArrow',           20, 'l2r' );
	s[ '('          ] = c( '_handleCall',            19, 'l2r' );
	s[ '['          ] = c( '_handleMember',          18, 'l2r' );
	s[ ']'          ] = c( '_handlePass',            18 ); // 0?
	s[ '.'          ] = c( '_handleDot',             18, 'l2r' );
	s[ '?.'         ] = c( '_handleConditionalDot',  18, 'l2r' );
	s[ '++'         ] = c( '_handleMonoOps',         16, 'n/a', PostIncrement );
	s[ '--'         ] = c( '_handleMonoOps',         16, 'n/a', PostDecrement );
	s[ '*'          ] = c( '_handleDualisticOps',    13, 'l2r', Multiply );
	s[ '/'          ] = c( '_handleDualisticOps',    13, 'l2r', Divide );
	s[ '%'          ] = c( '_handleDualisticOps',    13, 'l2r', Remainder );
	s[ '+'          ] = c( '_handleDualisticOps',    12, 'l2r', Plus );
	s[ '-'          ] = c( '_handleDualisticOps',    12, 'l2r', Minus );
	s[ '<<'         ] = c( '_handleDualisticOps',    11, 'l2r', BitwiseLeftShift );
	s[ '>>'         ] = c( '_handleDualisticOps',    11, 'l2r', BitwiseRightShift );
	s[ '>>>'        ] = c( '_handleDualisticOps',    11, 'l2r', BitwiseUnsignedRightShift );
	s[ '<='         ] = c( '_handleDualisticOps',    10, 'l2r', LessOrEqual );
	s[ '<'          ] = c( '_handleDualisticOps',    10, 'l2r', LessThan );
	s[ '>='         ] = c( '_handleDualisticOps',    10, 'l2r', GreaterOrEqual );
	s[ '>'          ] = c( '_handleDualisticOps',    10, 'l2r', GreaterThan );
	s[ '==='        ] = c( '_handleDualisticOps',     9, 'l2r', Equals );
	s[ '!=='        ] = c( '_handleDualisticOps',     9, 'l2r', Differs );
	s[ 'instanceof' ] = c( '_handleDualisticOps',     9, 'l2r', Instanceof );
	s[ '&'          ] = c( '_handleDualisticOps',     8, 'l2r', BitwiseAnd );
	s[ '^'          ] = c( '_handleDualisticOps',     7, 'l2r', BitwiseXor );
	s[ '|'          ] = c( '_handleDualisticOps',     6, 'l2r', BitwiseOr );
	s[ '&&'         ] = c( '_handleDualisticOps',     5, 'l2r', And );
	s[ '||'         ] = c( '_handleDualisticOps',     4, 'l2r', Or );
	s[ '??'         ] = c( '_handleDualisticOps',     4, 'l2r', NullishCoalescence );
	s[ '?'          ] = c( '_handleCondition',        3, 'l2r' );
	s[ '='          ] = c( '_handleDualisticOps',     2, 'r2l', Assign );
	s[ '+='         ] = c( '_handleDualisticOps',     2, 'r2l', PlusAssign );
	s[ '-='         ] = c( '_handleDualisticOps',     2, 'r2l', MinusAssign );
	s[ '*='         ] = c( '_handleDualisticOps',     2, 'r2l', MultiplyAssign );
	s[ '/='         ] = c( '_handleDualisticOps',     2, 'r2l', DivideAssign );
	s[ '&='         ] = c( '_handleDualisticOps',     2, 'r2l', BitwiseAndAssign );
	s[ '|='         ] = c( '_handleDualisticOps',     2, 'r2l', BitwiseOrAssign );
	s[ '^='         ] = c( '_handleDualisticOps',     2, 'r2l', BitwiseXorAssign );
	s[ '%='         ] = c( '_handleDualisticOps',     2, 'r2l', RemainderAssign );
	s[ ','          ] = c( '_handleDualisticOps',     1, 'l2r', Comma );
	s[ ')'          ] = c( '_handlePass',             0 );
	s[ ':'          ] = c( '_handlePass',             0 );
	s[ ';'          ] = c( '_handlePass',            -1 );
	s[ 'of'         ] = c( '_handlePass',            -1 );
	s[ 'in'         ] = c( '_handlePass',            -1 );
	s[ '}'          ] = c( '_handlePass',            -1 );
	s[ 'else'       ] = c( '_handlePass',            -1 );

	return Object.freeze( s );
};

/*
| Phony handle denoting start of parsing on expression possiblity.
*/
def.staticLazy._parseExpressionStart = ( ) =>
	ParserHandle.Handler( '_handleParserError', 0 );

/*
| Phony handle denoting start of parsing on statement/expression possiblity
*/
def.staticLazy._parseStatementStart = ( ) =>
	ParserHandle.Handler( '_handleParserError', -1 );

/*
| Statement token specifications.
*/
def.staticLazy._statementHandles = ( ) =>
{
	const s = { };

	s[ 'async'    ] = ParserHandle.create( 'handler', '_handleFunctionDeclaration' );
	s[ 'break'    ] = ParserHandle.create( 'handler', '_handleBreak' );
	// FIXME continue
	s[ 'const'    ] =
	s[ 'let'      ] = ParserHandle.create( 'handler', '_handleLet', 'prec', 1.5 );
	s[ 'do'       ] = ParserHandle.create( 'handler', '_handleDo' );
	s[ 'if'       ] = ParserHandle.create( 'handler', '_handleIf' );
	s[ 'for'      ] = ParserHandle.create( 'handler', '_handleFor' );
	s[ 'function' ] = ParserHandle.create( 'handler', '_handleFunctionDeclaration' );
	// 1.5: stronger than comma, so multiple let definitions are not treated as comma operator
	s[ 'import'   ] = ParserHandle.create( 'handler', '_handleImport' );
	s[ 'return'   ] = ParserHandle.create( 'handler', '_handleReturn' );
	s[ 'switch'   ] = ParserHandle.create( 'handler', '_handleSwitch' );
	s[ 'try'      ] = ParserHandle.create( 'handler', '_handleTry' );
	s[ 'throw'    ] = ParserHandle.create( 'handler', '_handleThrow' );
	s[ 'while'    ] = ParserHandle.create( 'handler', '_handleWhile' );
	s[ '{'        ] = ParserHandle.create( 'handler', '_handleBlock' );

	return Object.freeze( s );
};

/*
| Ast nodes which ate their semicolon.
*/
def.staticLazy._noSemicolon = ( ) =>
{
	const set = new Set( );
	set.add( Block );
	set.add( DoWhile );
	set.add( For );
	set.add( ForIn );
	set.add( ForOf );
	set.add( Func );
	set.add( If );
	set.add( Switch );
	set.add( Try );
	set.add( While );
	return set;
};

/*
| Parses a token at current state (which has a pos from a tokenList).
|
| ~state:  current parser state
| ~handle: current operator handle
*/
def.static._parseToken =
	function( state, handle )
{
/**/if( CHECK && arguments.length !== 2 ) throw new Error( );

	if( !state.ast && state.current.ti2ctype !== Token )
	{
		// this is already a parsed astTree.
		state = state.advance( state.current );
	}
	else
	{
		const tokenHandle = Self._getHandle( state, handle.prec < 0 );
		// TODO needed?
		if( tokenHandle.handler === '_handlePass' ) return state;
		state = Self[ tokenHandle.handler ]( state, tokenHandle );
	}

	switch( state.ast.ti2ctype )
	{
		case Block:
		case DoWhile:
		case For:
		case ForIn:
		case ForOf:
		case If:
		case Switch:
		case Try:
		case While:
			return state;

		case Func:
			// only named functions stop parsing
			if( state.ast.name ) return state;
			break;
	}

	while( !state.reachedEnd )
	{
		const nextHandle = Self._getHandle( state, false );

		if(
			nextHandle.prec === undefined
			|| nextHandle.prec < handle.prec
			|| ( nextHandle.prec === handle.prec && handle.associativity === 'l2r' )
			|| nextHandle.handler === '_handlePass'
		) break;

		state = Self._parseToken( state, nextHandle );
	}

	return state;
};

/*
| Tokenizes an array.
|
| ~array: the array
*/
def.static._tokenizeArray =
	function( array )
{
	let tokens = ListParserToken.Empty;

	for( let arg of array )
	{
		if( arg === undefined ) continue;
		if( typeof( arg ) === 'string' )
		{
			tokens = tokens.appendList( Lexer.tokenize( arg ) );
		}
		else if( Array.isArray( arg ) )
		{
			tokens = tokens.appendList( Self._tokenizeArray( arg ) );
		}
		else
		{
			tokens = tokens.append( arg );
		}
	}

	return tokens;
};
