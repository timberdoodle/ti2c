/*
| A parser state.
*/
def.attributes =
{
	// current ast entity
	ast: { type: [ '< Ast/Type/Statement', '< Ast/Type/Expr', 'undefined' ] },

	// list of tokens to parse
	tokens: { type: 'list@<Parser/Tokens' },

	// current position in token list
	pos: { type: 'integer' }
};

import { Self as LexerToken } from '{ti2c:Lexer/Token}';

/*
| Advances the pos.
|
| ~ast: sets this ast value (maybe pass)
| ~expects: an array of token to expect after advancing.
*/
def.proto.advance =
	function( ast, ...expects )
{
	const next = this.create( 'ast', ast, 'pos', this.pos + 1 );
	if( expects.length > 0 ) next._expect( expects );
	return next;
};

/*
| The current token.
*/
def.lazy.current =
	function( )
{
	return(
		( this.pos < this.tokens.length )
		? this.tokens.get( this.pos )
		: undefined
	);
};

/*
| Expects token at current state.
|
| ~expects: expect these tokens.
*/
def.proto.expect =
	function( ...expects )
{
	this._expect( expects );
};

/*
| True if the types of next tokens match.
*/
def.proto.foresight =
	function( ...types )
{
	const tokens = this.tokens;
	let p = this.pos;
	if( p + types.length >= tokens.length ) return false;
	for( let type of types )
	{
		const token = tokens.get( ++p );
		if( token.ti2ctype !== LexerToken ) return false;
		if( token.type !== type ) return false;
	}
	return true;
};

/*
| The preview token.
*/
def.lazy.preview =
	function( )
{
	return(
		( this.pos + 1 < this.tokens.length )
		? this.tokens.get( this.pos + 1 )
		: undefined
	);
};

/*
| True if pos is at end of the token list.
*/
def.lazy.reachedEnd =
	function( )
{
	return this.pos >= this.tokens.length;
};

/*
| Stays on the pos, but changes ast state.
|
| ~ast: sets this ast value
*/
def.proto.stay =
	function( ast )
{
	return this.create( 'ast', ast );
};

/*
| Expects token at current state.
|
| ~expects: an array of tokens to expect
*/
def.proto._expect =
	function( expects )
{
	const elen = expects.length;
	if( elen === 0 ) return;

	let found = false;
	const cType = this.current.type;
	for( let e of expects )
	{
		if( cType === e )
		{
			found = true;
			break;
		}
	}

	if( !found )
	{
		this.preview.error( 'unexpected token' );
		throw new Error( );
	}
};
