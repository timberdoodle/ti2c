/*
| This is an auto generated file.
| Editing this might be rather futile.
*/

import { Self as tt__ti2c__Ast_Block } from '{{ti2c:Ast/Block}}';
import { Self as tt__ti2c__Ast_Break } from '{{ti2c:Ast/Break}}';
import { Self as tt__ti2c__Ast_Check } from '{{ti2c:Ast/Check}}';
import { Self as tt__ti2c__Ast_Comment } from '{{ti2c:Ast/Comment}}';
import { Self as tt__ti2c__Ast_Const } from '{{ti2c:Ast/Const}}';
import { Self as tt__ti2c__Ast_Continue } from '{{ti2c:Ast/Continue}}';
import { Self as tt__ti2c__Ast_DestructDecl } from '{{ti2c:Ast/DestructDecl}}';
import { Self as tt__ti2c__Ast_DoWhile } from '{{ti2c:Ast/DoWhile}}';
import { Self as tt__ti2c__Ast_For } from '{{ti2c:Ast/For}}';
import { Self as tt__ti2c__Ast_ForIn } from '{{ti2c:Ast/ForIn}}';
import { Self as tt__ti2c__Ast_ForOf } from '{{ti2c:Ast/ForOf}}';
import { Self as tt__ti2c__Ast_If } from '{{ti2c:Ast/If}}';
import { Self as tt__ti2c__Ast_Import } from '{{ti2c:Ast/Import}}';
import { Self as tt__ti2c__Ast_Let } from '{{ti2c:Ast/Let}}';
import { Self as tt__ti2c__Ast_Return } from '{{ti2c:Ast/Return}}';
import { Self as tt__ti2c__Ast_Sep } from '{{ti2c:Ast/Sep}}';
import { Self as tt__ti2c__Ast_Switch } from '{{ti2c:Ast/Switch}}';
import { Self as tt__ti2c__Ast_Throw } from '{{ti2c:Ast/Throw}}';
import { Self as tt__ti2c__Ast_Try } from '{{ti2c:Ast/Try}}';
import { Self as tt__ti2c__Ast_VarDec } from '{{ti2c:Ast/VarDec}}';
import { Self as tt__ti2c__Ast_While } from '{{ti2c:Ast/While}}';
import { Self as tt__ti2c__Ast_And } from '{{ti2c:Ast/And}}';
import { Self as tt__ti2c__Ast_ArrayLiteral } from '{{ti2c:Ast/ArrayLiteral}}';
import { Self as tt__ti2c__Ast_ArrowFunction } from '{{ti2c:Ast/ArrowFunction}}';
import { Self as tt__ti2c__Ast_Assign } from '{{ti2c:Ast/Assign}}';
import { Self as tt__ti2c__Ast_Await } from '{{ti2c:Ast/Await}}';
import { Self as tt__ti2c__Ast_BitwiseAnd } from '{{ti2c:Ast/BitwiseAnd}}';
import { Self as tt__ti2c__Ast_BitwiseAndAssign } from '{{ti2c:Ast/BitwiseAndAssign}}';
import { Self as tt__ti2c__Ast_BitwiseLeftShift } from '{{ti2c:Ast/BitwiseLeftShift}}';
import { Self as tt__ti2c__Ast_BitwiseNot } from '{{ti2c:Ast/BitwiseNot}}';
import { Self as tt__ti2c__Ast_BitwiseOr } from '{{ti2c:Ast/BitwiseOr}}';
import { Self as tt__ti2c__Ast_BitwiseOrAssign } from '{{ti2c:Ast/BitwiseOrAssign}}';
import { Self as tt__ti2c__Ast_BitwiseRightShift } from '{{ti2c:Ast/BitwiseRightShift}}';
import { Self as tt__ti2c__Ast_BitwiseUnsignedRightShift } from '{{ti2c:Ast/BitwiseUnsignedRightShift}}';
import { Self as tt__ti2c__Ast_BitwiseXor } from '{{ti2c:Ast/BitwiseXor}}';
import { Self as tt__ti2c__Ast_BitwiseXorAssign } from '{{ti2c:Ast/BitwiseXorAssign}}';
import { Self as tt__ti2c__Ast_Boolean } from '{{ti2c:Ast/Boolean}}';
import { Self as tt__ti2c__Ast_Call } from '{{ti2c:Ast/Call}}';
import { Self as tt__ti2c__Ast_Comma } from '{{ti2c:Ast/Comma}}';
import { Self as tt__ti2c__Ast_Condition } from '{{ti2c:Ast/Condition}}';
import { Self as tt__ti2c__Ast_ConditionalDot } from '{{ti2c:Ast/ConditionalDot}}';
import { Self as tt__ti2c__Ast_Delete } from '{{ti2c:Ast/Delete}}';
import { Self as tt__ti2c__Ast_Differs } from '{{ti2c:Ast/Differs}}';
import { Self as tt__ti2c__Ast_Divide } from '{{ti2c:Ast/Divide}}';
import { Self as tt__ti2c__Ast_DivideAssign } from '{{ti2c:Ast/DivideAssign}}';
import { Self as tt__ti2c__Ast_Dot } from '{{ti2c:Ast/Dot}}';
import { Self as tt__ti2c__Ast_Equals } from '{{ti2c:Ast/Equals}}';
import { Self as tt__ti2c__Ast_Func_Self } from '{{ti2c:Ast/Func/Self}}';
import { Self as tt__ti2c__Ast_GreaterOrEqual } from '{{ti2c:Ast/GreaterOrEqual}}';
import { Self as tt__ti2c__Ast_GreaterThan } from '{{ti2c:Ast/GreaterThan}}';
import { Self as tt__ti2c__Ast_Instanceof } from '{{ti2c:Ast/Instanceof}}';
import { Self as tt__ti2c__Ast_LessOrEqual } from '{{ti2c:Ast/LessOrEqual}}';
import { Self as tt__ti2c__Ast_LessThan } from '{{ti2c:Ast/LessThan}}';
import { Self as tt__ti2c__Ast_Member } from '{{ti2c:Ast/Member}}';
import { Self as tt__ti2c__Ast_Minus } from '{{ti2c:Ast/Minus}}';
import { Self as tt__ti2c__Ast_MinusAssign } from '{{ti2c:Ast/MinusAssign}}';
import { Self as tt__ti2c__Ast_Multiply } from '{{ti2c:Ast/Multiply}}';
import { Self as tt__ti2c__Ast_MultiplyAssign } from '{{ti2c:Ast/MultiplyAssign}}';
import { Self as tt__ti2c__Ast_Negate } from '{{ti2c:Ast/Negate}}';
import { Self as tt__ti2c__Ast_New } from '{{ti2c:Ast/New}}';
import { Self as tt__ti2c__Ast_Not } from '{{ti2c:Ast/Not}}';
import { Self as tt__ti2c__Ast_Null } from '{{ti2c:Ast/Null}}';
import { Self as tt__ti2c__Ast_NullishCoalescence } from '{{ti2c:Ast/NullishCoalescence}}';
import { Self as tt__ti2c__Ast_Number } from '{{ti2c:Ast/Number}}';
import { Self as tt__ti2c__Ast_ObjLiteral } from '{{ti2c:Ast/ObjLiteral}}';
import { Self as tt__ti2c__Ast_Or } from '{{ti2c:Ast/Or}}';
import { Self as tt__ti2c__Ast_Plus } from '{{ti2c:Ast/Plus}}';
import { Self as tt__ti2c__Ast_PlusAssign } from '{{ti2c:Ast/PlusAssign}}';
import { Self as tt__ti2c__Ast_PostDecrement } from '{{ti2c:Ast/PostDecrement}}';
import { Self as tt__ti2c__Ast_PostIncrement } from '{{ti2c:Ast/PostIncrement}}';
import { Self as tt__ti2c__Ast_PreDecrement } from '{{ti2c:Ast/PreDecrement}}';
import { Self as tt__ti2c__Ast_PreIncrement } from '{{ti2c:Ast/PreIncrement}}';
import { Self as tt__ti2c__Ast_Regex } from '{{ti2c:Ast/Regex}}';
import { Self as tt__ti2c__Ast_Remainder } from '{{ti2c:Ast/Remainder}}';
import { Self as tt__ti2c__Ast_RemainderAssign } from '{{ti2c:Ast/RemainderAssign}}';
import { Self as tt__ti2c__Ast_Spread } from '{{ti2c:Ast/Spread}}';
import { Self as tt__ti2c__Ast_String } from '{{ti2c:Ast/String}}';
import { Self as tt__ti2c__Ast_Typeof } from '{{ti2c:Ast/Typeof}}';
import { Self as tt__ti2c__Ast_Undefined } from '{{ti2c:Ast/Undefined}}';
import { Self as tt__ti2c__Ast_Var } from '{{ti2c:Ast/Var}}';
import { Self as tt__ti2c__Ast_Yield } from '{{ti2c:Ast/Yield}}';
import { Self as _$Q$_tt__listtt__$Z$_ti2c__Parser_Tokens } from '{{list@<ti2c:Parser/Tokens}}';

/*
| The interning map.
*/
let imap = new Map( );

/*
| Caching queue.
*/
let cSize = globalThis.TI2C_RETENTION ? 1024 : 0;
let cArray = cSize ? new Array( cSize ) : undefined;
let cPos = 0;

/*
| Timestamp since last turnaround
*/
let cTurn = cSize ? Date.now( ) : undefined;

/*
| Puts an entry in the caching queue.
*/
const cPut =
	function( o )
{
	cArray[ cPos ] = o;
	cPos = ++cPos % cSize;
	if( !cPos )
	{
		const now = Date.now( );
		if( now - cTurn < globalThis.TI2C_RETENTION )
		{
			cPos = cSize;
			cSize *= 2;
			cArray.length = cSize;
		}
		cTurn = now;
	}
};

/*
| Constructor.
*/
const Constructor =
	function( hash, v_ast, v_pos, v_tokens )
{
	this.__lazy = { };
	this.__hash = hash;
	this.ast = v_ast;
	this.pos = v_pos;
	this.tokens = v_tokens;
	Object.freeze( this );
};

/*
| In case of checking all unknown access is to be trapped.
*/
/**/if( CHECK )
/**/{
/**/const Trap = function( ) { };
/**/Trap.prototype = ti2c.trap;
/**/Constructor.prototype = new Trap( );
/**/}

/*
| Constructor prototype.
*/
const prototype = Self.prototype = Constructor.prototype;
Self.__hash = 1234971302;

/*
| Creates a new object.
*/
Self.create =
prototype.create =
	function( ...args )
{
	let v_ast;
	let v_pos;
	let v_tokens;
	if( this !== Self ) { v_ast = this.ast; v_pos = this.pos; v_tokens = this.tokens; }
	for( let a = 0, alen = args.length; a < alen; a += 2 )
	{
		let arg = args[ a + 1 ];
		switch( args[ a ] )
		{
			case 'ast':if( arg !== pass ) { v_ast = arg; }
				break;
			case 'pos':if( arg !== pass ) { v_pos = arg; }
				break;
			case 'tokens':if( arg !== pass ) { v_tokens = arg; }
				break;
			default :throw ( new Error( args[ a ] ) );
		}
	}
/**/if( CHECK )
/**/{
/**/	if(
/**/		v_ast !== undefined && v_ast.ti2ctype !== tt__ti2c__Ast_Block
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_Break
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_Check
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_Comment
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_Const
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_Continue
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_DestructDecl
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_DoWhile
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_For
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_ForIn
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_ForOf
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_If
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_Import
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_Let
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_Return
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_Sep
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_Switch
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_Throw
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_Try
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_VarDec
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_While
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_And
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_ArrayLiteral
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_ArrowFunction
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_Assign
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_Await
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_BitwiseAnd
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_BitwiseAndAssign
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_BitwiseLeftShift
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_BitwiseNot
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_BitwiseOr
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_BitwiseOrAssign
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_BitwiseRightShift
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_BitwiseUnsignedRightShift
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_BitwiseXor
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_BitwiseXorAssign
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_Boolean
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_Call
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_Comma
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_Condition
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_ConditionalDot
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_Delete
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_Differs
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_Divide
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_DivideAssign
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_Dot
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_Equals
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_Func_Self
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_GreaterOrEqual
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_GreaterThan
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_Instanceof
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_LessOrEqual
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_LessThan
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_Member
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_Minus
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_MinusAssign
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_Multiply
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_MultiplyAssign
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_Negate
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_New
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_Not
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_Null
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_NullishCoalescence
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_Number
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_ObjLiteral
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_Or
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_Plus
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_PlusAssign
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_PostDecrement
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_PostIncrement
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_PreDecrement
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_PreIncrement
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_Regex
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_Remainder
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_RemainderAssign
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_Spread
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_String
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_Typeof
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_Undefined
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_Var
/**/		&& v_ast.ti2ctype !== tt__ti2c__Ast_Yield
/**/	)
/**/	{
/**/		throw ( new Error( ) );
/**/	}
/**/	if(
/**/		typeof( v_pos ) !== 'number' || Number.isNaN( v_pos ) || Math.floor( v_pos ) !== v_pos
/**/	)
/**/	{
/**/		throw ( new Error( ) );
/**/	}
/**/	if( v_tokens.ti2ctype !== _$Q$_tt__listtt__$Z$_ti2c__Parser_Tokens )
/**/	{
/**/		throw ( new Error( ) );
/**/	}
/**/}
	const hash = ti2c._hashArgs( 1234971302, v_ast, v_pos, v_tokens );
	let ihash = hash;
	let uhash;
	let lhash;
	for( let wr = imap.get( hash ); wr; wr = imap.get( ihash += 4.76837158203125e-7 ) )
	{
		const e = wr.deref( );
		if( e )
		{
			lhash = ihash;
			if( v_ast === e.ast && v_pos === e.pos && v_tokens === e.tokens )
			{
				if( uhash !== undefined )
				{
					imap.set( ihash, imap.get( uhash ) );
					imap.set( uhash, wr );
				}
				return e;
			}
		}
		else
		{
			if( uhash === undefined ) { uhash = ihash; }
		}
	}
	if( uhash === undefined ) { uhash = ihash; if( lhash < uhash ) { lhash = uhash; } }
	if( lhash !== undefined )
	{
		lhash += 4.76837158203125e-7;
		while( imap.get( lhash ) ) { imap.set( lhash, undefined ); lhash += 4.76837158203125e-7; }
	}
	const newti2c = new Constructor( hash, v_ast, v_pos, v_tokens );
	imap.set( uhash, new WeakRef( newti2c ) );
	if( cSize ) { cPut( newti2c ); }
	return newti2c;
};

/*
| Type reflection.
*/
prototype.ti2ctype = Self;

/*
| Reflection for debugging.
*/
Self.__TI2C_NAME__ = prototype.__TI2C_NAME__ = 'ti2c:Parser/State';
