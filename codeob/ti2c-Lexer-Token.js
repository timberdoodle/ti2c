/*
| This is an auto generated file.
| Editing this might be rather futile.
*/


/*
| The interning map.
*/
let imap = new Map( );

/*
| Caching queue.
*/
let cSize = globalThis.TI2C_RETENTION ? 1024 : 0;
let cArray = cSize ? new Array( cSize ) : undefined;
let cPos = 0;

/*
| Timestamp since last turnaround
*/
let cTurn = cSize ? Date.now( ) : undefined;

/*
| Puts an entry in the caching queue.
*/
const cPut =
	function( o )
{
	cArray[ cPos ] = o;
	cPos = ++cPos % cSize;
	if( !cPos )
	{
		const now = Date.now( );
		if( now - cTurn < globalThis.TI2C_RETENTION )
		{
			cPos = cSize;
			cSize *= 2;
			cArray.length = cSize;
		}
		cTurn = now;
	}
};

/*
| Constructor.
*/
const Constructor =
	function( hash, v_column, v_line, v_lineNr, v_name, v_type, v_value )
{
	this.__lazy = { };
	this.__hash = hash;
	this.column = v_column;
	this.line = v_line;
	this.lineNr = v_lineNr;
	this.name = v_name;
	this.type = v_type;
	this.value = v_value;
	Object.freeze( this );
/**/if( CHECK ) { this._check( ); }
};

/*
| In case of checking all unknown access is to be trapped.
*/
/**/if( CHECK )
/**/{
/**/const Trap = function( ) { };
/**/Trap.prototype = ti2c.trap;
/**/Constructor.prototype = new Trap( );
/**/}

/*
| Constructor prototype.
*/
const prototype = Self.prototype = Constructor.prototype;
Self.__hash = 870567990;

/*
| Creates a new object.
*/
Self.create =
prototype.create =
	function( ...args )
{
	let v_column;
	let v_line;
	let v_lineNr;
	let v_name;
	let v_type;
	let v_value;
	if( this !== Self )
	{
		v_column = this.column;
		v_line = this.line;
		v_lineNr = this.lineNr;
		v_name = this.name;
		v_type = this.type;
		v_value = this.value;
	}
	for( let a = 0, alen = args.length; a < alen; a += 2 )
	{
		let arg = args[ a + 1 ];
		switch( args[ a ] )
		{
			case 'column':if( arg !== pass ) { v_column = arg; }
				break;
			case 'line':if( arg !== pass ) { v_line = arg; }
				break;
			case 'lineNr':if( arg !== pass ) { v_lineNr = arg; }
				break;
			case 'name':if( arg !== pass ) { v_name = arg; }
				break;
			case 'type':if( arg !== pass ) { v_type = arg; }
				break;
			case 'value':if( arg !== pass ) { v_value = arg; }
				break;
			default :throw ( new Error( args[ a ] ) );
		}
	}
/**/if( CHECK )
/**/{
/**/	if(
/**/		typeof( v_column ) !== 'number' || Number.isNaN( v_column )
/**/		|| Math.floor( v_column ) !== v_column
/**/	)
/**/	{
/**/		throw ( new Error( ) );
/**/	}
/**/	if( typeof( v_line ) !== 'string' ) { throw ( new Error( ) ); }
/**/	if(
/**/		typeof( v_lineNr ) !== 'number' || Number.isNaN( v_lineNr )
/**/		|| Math.floor( v_lineNr ) !== v_lineNr
/**/	)
/**/	{
/**/		throw ( new Error( ) );
/**/	}
/**/	if( typeof( v_name ) !== 'string' ) { throw ( new Error( ) ); }
/**/	if( typeof( v_type ) !== 'string' ) { throw ( new Error( ) ); }
/**/	if(
/**/		v_value !== undefined && ( typeof( v_value ) !== 'number' || Number.isNaN( v_value ) )
/**/		&& typeof( v_value ) !== 'string'
/**/	)
/**/	{
/**/		throw ( new Error( ) );
/**/	}
/**/}
	const hash = ti2c._hashArgs( 870567990, v_column, v_line, v_lineNr, v_name, v_type, v_value );
	let ihash = hash;
	let uhash;
	let lhash;
	for( let wr = imap.get( hash ); wr; wr = imap.get( ihash += 4.76837158203125e-7 ) )
	{
		const e = wr.deref( );
		if( e )
		{
			lhash = ihash;
			if(
				v_column === e.column && v_line === e.line && v_lineNr === e.lineNr
				&& v_name === e.name
				&& v_type === e.type
				&& v_value === e.value
			)
			{
				if( uhash !== undefined )
				{
					imap.set( ihash, imap.get( uhash ) );
					imap.set( uhash, wr );
				}
				return e;
			}
		}
		else
		{
			if( uhash === undefined ) { uhash = ihash; }
		}
	}
	if( uhash === undefined ) { uhash = ihash; if( lhash < uhash ) { lhash = uhash; } }
	if( lhash !== undefined )
	{
		lhash += 4.76837158203125e-7;
		while( imap.get( lhash ) ) { imap.set( lhash, undefined ); lhash += 4.76837158203125e-7; }
	}
	const newti2c = new Constructor( hash, v_column, v_line, v_lineNr, v_name, v_type, v_value );
	imap.set( uhash, new WeakRef( newti2c ) );
	if( cSize ) { cPut( newti2c ); }
	return newti2c;
};

/*
| Type reflection.
*/
prototype.ti2ctype = Self;

/*
| Reflection for debugging.
*/
Self.__TI2C_NAME__ = prototype.__TI2C_NAME__ = 'ti2c:Lexer/Token';
