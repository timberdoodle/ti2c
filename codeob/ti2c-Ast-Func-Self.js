/*
| This is an auto generated file.
| Editing this might be rather futile.
*/

import { Self as _$Q$_tt__listtt__ti2c__Ast_Func_Arg } from '{{list@ti2c:Ast/Func/Arg}}';
import { Self as tt__ti2c__Ast_Block } from '{{ti2c:Ast/Block}}';

/*
| The interning map.
*/
let imap = new Map( );

/*
| Caching queue.
*/
let cSize = globalThis.TI2C_RETENTION ? 1024 : 0;
let cArray = cSize ? new Array( cSize ) : undefined;
let cPos = 0;

/*
| Timestamp since last turnaround
*/
let cTurn = cSize ? Date.now( ) : undefined;

/*
| Puts an entry in the caching queue.
*/
const cPut =
	function( o )
{
	cArray[ cPos ] = o;
	cPos = ++cPos % cSize;
	if( !cPos )
	{
		const now = Date.now( );
		if( now - cTurn < globalThis.TI2C_RETENTION )
		{
			cPos = cSize;
			cSize *= 2;
			cArray.length = cSize;
		}
		cTurn = now;
	}
};

/*
| Constructor.
*/
const Constructor =
	function( hash, v_args, v_body, v_isAsync, v_isGenerator, v_name )
{
	this.__hash = hash;
	this.args = v_args;
	this.body = v_body;
	this.isAsync = v_isAsync;
	this.isGenerator = v_isGenerator;
	this.name = v_name;
	Object.freeze( this );
};

/*
| In case of checking all unknown access is to be trapped.
*/
/**/if( CHECK )
/**/{
/**/const Trap = function( ) { };
/**/Trap.prototype = ti2c.trap;
/**/Constructor.prototype = new Trap( );
/**/}

/*
| Constructor prototype.
*/
const prototype = Self.prototype = Constructor.prototype;
Self.__hash = 1124834196;

/*
| Creates a new object.
*/
Self.create =
prototype.create =
	function( ...args )
{
	let v_args;
	let v_body;
	let v_isAsync;
	let v_isGenerator;
	let v_name;
	if( this !== Self )
	{
		v_args = this.args;
		v_body = this.body;
		v_isAsync = this.isAsync;
		v_isGenerator = this.isGenerator;
		v_name = this.name;
	}
	for( let a = 0, alen = args.length; a < alen; a += 2 )
	{
		let arg = args[ a + 1 ];
		switch( args[ a ] )
		{
			case 'args':if( arg !== pass ) { v_args = arg; }
				break;
			case 'body':if( arg !== pass ) { v_body = arg; }
				break;
			case 'isAsync':if( arg !== pass ) { v_isAsync = arg; }
				break;
			case 'isGenerator':if( arg !== pass ) { v_isGenerator = arg; }
				break;
			case 'name':if( arg !== pass ) { v_name = arg; }
				break;
			default :throw ( new Error( args[ a ] ) );
		}
	}
	if( v_args === undefined ) { v_args = _$Q$_tt__listtt__ti2c__Ast_Func_Arg.Empty; }
	if( v_isAsync === undefined ) { v_isAsync = false; }
	if( v_isGenerator === undefined ) { v_isGenerator = false; }
/**/if( CHECK )
/**/{
/**/	if( v_args.ti2ctype !== _$Q$_tt__listtt__ti2c__Ast_Func_Arg ) { throw ( new Error( ) ); }
/**/	if( v_body !== undefined && v_body.ti2ctype !== tt__ti2c__Ast_Block )
/**/	{
/**/		throw ( new Error( ) );
/**/	}
/**/	if( typeof( v_isAsync ) !== 'boolean' ) { throw ( new Error( ) ); }
/**/	if( typeof( v_isGenerator ) !== 'boolean' ) { throw ( new Error( ) ); }
/**/	if( v_name !== undefined && typeof( v_name ) !== 'string' ) { throw ( new Error( ) ); }
/**/}
	const hash = ti2c._hashArgs( 1124834196, v_args, v_body, v_isAsync, v_isGenerator, v_name );
	let ihash = hash;
	let uhash;
	let lhash;
	for( let wr = imap.get( hash ); wr; wr = imap.get( ihash += 4.76837158203125e-7 ) )
	{
		const e = wr.deref( );
		if( e )
		{
			lhash = ihash;
			if(
				v_args === e.args && v_body === e.body && v_isAsync === e.isAsync
				&& v_isGenerator === e.isGenerator
				&& v_name === e.name
			)
			{
				if( uhash !== undefined )
				{
					imap.set( ihash, imap.get( uhash ) );
					imap.set( uhash, wr );
				}
				return e;
			}
		}
		else
		{
			if( uhash === undefined ) { uhash = ihash; }
		}
	}
	if( uhash === undefined ) { uhash = ihash; if( lhash < uhash ) { lhash = uhash; } }
	if( lhash !== undefined )
	{
		lhash += 4.76837158203125e-7;
		while( imap.get( lhash ) ) { imap.set( lhash, undefined ); lhash += 4.76837158203125e-7; }
	}
	const newti2c = new Constructor( hash, v_args, v_body, v_isAsync, v_isGenerator, v_name );
	imap.set( uhash, new WeakRef( newti2c ) );
	if( cSize ) { cPut( newti2c ); }
	return newti2c;
};

/*
| Type reflection.
*/
prototype.ti2ctype = Self;

/*
| Reflection for debugging.
*/
Self.__TI2C_NAME__ = prototype.__TI2C_NAME__ = 'ti2c:Ast/Func/Self';
