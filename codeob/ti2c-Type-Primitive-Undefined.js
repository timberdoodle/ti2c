/*
| This is an auto generated file.
| Editing this might be rather futile.
*/


/*
| Constructor.
*/
const Constructor = function( hash ) { this.__hash = hash; Object.freeze( this ); };

/*
| In case of checking all unknown access is to be trapped.
*/
/**/if( CHECK )
/**/{
/**/const Trap = function( ) { };
/**/Trap.prototype = ti2c.trap;
/**/Constructor.prototype = new Trap( );
/**/}

/*
| Constructor prototype.
*/
const prototype = Self.prototype = Constructor.prototype;
Self.__hash = -81964636;

/*
| Singleton.
*/
let _singleton;
ti2c._proto.lazyStaticValue( Self, 'singleton', function( ) { return Self._create( ); } );

/*
| Creates a new object.
*/
Self._create =
prototype._create =
	function( ...args )
{
	const hash = -81964636;
	if( !_singleton ) { _singleton = new Constructor( hash ); }
	return _singleton;
};

/*
| Type reflection.
*/
prototype.ti2ctype = Self;

/*
| Reflection for debugging.
*/
Self.__TI2C_NAME__ = prototype.__TI2C_NAME__ = 'ti2c:Type/Primitive/Undefined';
