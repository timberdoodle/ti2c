/*
| This is an auto generated file.
| Editing this might be rather futile.
*/

import { Self as tt__ti2c__Lexer_Token } from '{{ti2c:Lexer/Token}}';

/*
| The interning map.
*/
let imap = new Map( );

/*
| Caching queue.
*/
let cSize = globalThis.TI2C_RETENTION ? 1024 : 0;
let cArray = cSize ? new Array( cSize ) : undefined;
let cPos = 0;

/*
| Timestamp since last turnaround
*/
let cTurn = cSize ? Date.now( ) : undefined;

/*
| Puts an entry in the caching queue.
*/
const cPut =
	function( o )
{
	cArray[ cPos ] = o;
	cPos = ++cPos % cSize;
	if( !cPos )
	{
		const now = Date.now( );
		if( now - cTurn < globalThis.TI2C_RETENTION )
		{
			cPos = cSize;
			cSize *= 2;
			cArray.length = cSize;
		}
		cTurn = now;
	}
};

/*
| Constructor.
*/
const Constructor =
	function( hash, v_name, v_token )
{
	this.__lazy = { };
	this.__hash = hash;
	this.name = v_name;
	this.token = v_token;
	Object.freeze( this );
};

/*
| In case of checking all unknown access is to be trapped.
*/
/**/if( CHECK )
/**/{
/**/const Trap = function( ) { };
/**/Trap.prototype = ti2c.trap;
/**/Constructor.prototype = new Trap( );
/**/}

/*
| Constructor prototype.
*/
const prototype = Self.prototype = Constructor.prototype;
Self.__hash = -1757122545;

/*
| Creates a new object.
*/
Self.create =
prototype.create =
	function( ...args )
{
	let v_name;
	let v_token;
	if( this !== Self ) { v_name = this.name; v_token = this.token; }
	for( let a = 0, alen = args.length; a < alen; a += 2 )
	{
		let arg = args[ a + 1 ];
		switch( args[ a ] )
		{
			case 'name':if( arg !== pass ) { v_name = arg; }
				break;
			case 'token':if( arg !== pass ) { v_token = arg; }
				break;
			default :throw ( new Error( args[ a ] ) );
		}
	}
/**/if( CHECK )
/**/{
/**/	if( v_name !== undefined && typeof( v_name ) !== 'string' ) { throw ( new Error( ) ); }
/**/	if( v_token.ti2ctype !== tt__ti2c__Lexer_Token ) { throw ( new Error( ) ); }
/**/}
	const hash = ti2c._hashArgs( -1757122545, v_name, v_token );
	let ihash = hash;
	let uhash;
	let lhash;
	for( let wr = imap.get( hash ); wr; wr = imap.get( ihash += 4.76837158203125e-7 ) )
	{
		const e = wr.deref( );
		if( e )
		{
			lhash = ihash;
			if( v_name === e.name && v_token === e.token )
			{
				if( uhash !== undefined )
				{
					imap.set( ihash, imap.get( uhash ) );
					imap.set( uhash, wr );
				}
				return e;
			}
		}
		else
		{
			if( uhash === undefined ) { uhash = ihash; }
		}
	}
	if( uhash === undefined ) { uhash = ihash; if( lhash < uhash ) { lhash = uhash; } }
	if( lhash !== undefined )
	{
		lhash += 4.76837158203125e-7;
		while( imap.get( lhash ) ) { imap.set( lhash, undefined ); lhash += 4.76837158203125e-7; }
	}
	const newti2c = new Constructor( hash, v_name, v_token );
	imap.set( uhash, new WeakRef( newti2c ) );
	if( cSize ) { cPut( newti2c ); }
	return newti2c;
};

/*
| Creates a new object from json.
*/
Self.FromJson =
	function( json )
{
/**/if( CHECK ) { if( arguments.length !== 1 ) { throw ( new Error( ) ); } }
	let v_name;
	let v_token;
	for( let name in json )
	{
		const arg = json[ name ];
		switch( name )
		{
			case '$type':if( arg !== 'ti2c:SMap/Node' ) { throw ( new Error( ) ); }
				break;
		}
	}
	const hash = ti2c._hashArgs( -1757122545, v_name, v_token );
	let ihash = hash;
	let uhash;
	let lhash;
	for( let wr = imap.get( hash ); wr; wr = imap.get( ihash += 4.76837158203125e-7 ) )
	{
		const e = wr.deref( );
		if( e )
		{
			lhash = ihash;
			if( v_name === e.name && v_token === e.token )
			{
				if( uhash !== undefined )
				{
					imap.set( ihash, imap.get( uhash ) );
					imap.set( uhash, wr );
				}
				return e;
			}
		}
		else
		{
			if( uhash === undefined ) { uhash = ihash; }
		}
	}
	if( uhash === undefined ) { uhash = ihash; if( lhash < uhash ) { lhash = uhash; } }
	if( lhash !== undefined )
	{
		lhash += 4.76837158203125e-7;
		while( imap.get( lhash ) ) { imap.set( lhash, undefined ); lhash += 4.76837158203125e-7; }
	}
	const newtim = new Constructor( hash, v_name, v_token );
	imap.set( uhash, new WeakRef( newtim ) );
	return newtim;
};

/*
| Type reflection.
*/
prototype.ti2ctype = Self;

/*
| Reflection for debugging.
*/
Self.__TI2C_NAME__ = prototype.__TI2C_NAME__ = 'ti2c:SMap/Node';

/*
| Stable jsonfy
*/
ti2c._proto.lazyFunction(
	prototype,
	'jsonfy',
	function( spacing )
{
	let i0space, i1space;
	if( spacing === undefined )
	{
		i0space = i1space = '';
	}
	else
	{
		if( typeof( spacing ) === 'string' )
		{
			i0space = '\n';
			i1space = i0space + spacing;
		}
		else
		{
			i0space = '\n' + spacing.level;
			i1space = i0space + spacing.step;
		}
	}
	const colon = i1space !== '' ? ': ' : ':';
	let r = '{' + i1space + '"$type"' + colon + '"ti2c:SMap/Node"';
	r += i0space + '}';
	return r;
}
);

/*
| Json type identifier
*/
Self.$type = prototype.$type = 'ti2c:SMap/Node';
Self.$fromJsonArgs = prototype.$fromJsonArgs = [ ];
