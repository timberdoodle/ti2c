/*
| This is an auto generated file.
| Editing this might be rather futile.
*/


/*
| The interning map.
*/
let imap = new Map( );

/*
| Caching queue.
*/
let cSize = globalThis.TI2C_RETENTION ? 1024 : 0;
let cArray = cSize ? new Array( cSize ) : undefined;
let cPos = 0;

/*
| Timestamp since last turnaround
*/
let cTurn = cSize ? Date.now( ) : undefined;

/*
| Puts an entry in the caching queue.
*/
const cPut =
	function( o )
{
	cArray[ cPos ] = o;
	cPos = ++cPos % cSize;
	if( !cPos )
	{
		const now = Date.now( );
		if( now - cTurn < globalThis.TI2C_RETENTION )
		{
			cPos = cSize;
			cSize *= 2;
			cArray.length = cSize;
		}
		cTurn = now;
	}
};

/*
| Constructor.
*/
const Constructor =
	function( hash, v_associativity, v_astCreator, v_handler, v_prec )
{
	this.__hash = hash;
	this.associativity = v_associativity;
	this.astCreator = v_astCreator;
	this.handler = v_handler;
	this.prec = v_prec;
	Object.freeze( this );
/**/if( CHECK ) { this._check( ); }
};

/*
| In case of checking all unknown access is to be trapped.
*/
/**/if( CHECK )
/**/{
/**/const Trap = function( ) { };
/**/Trap.prototype = ti2c.trap;
/**/Constructor.prototype = new Trap( );
/**/}

/*
| Constructor prototype.
*/
const prototype = Self.prototype = Constructor.prototype;
Self.__hash = 146575033;

/*
| Creates a new object.
*/
Self.create =
prototype.create =
	function( ...args )
{
	let v_associativity;
	let v_astCreator;
	let v_handler;
	let v_prec;
	if( this !== Self )
	{
		v_associativity = this.associativity;
		v_astCreator = this.astCreator;
		v_handler = this.handler;
		v_prec = this.prec;
	}
	for( let a = 0, alen = args.length; a < alen; a += 2 )
	{
		let arg = args[ a + 1 ];
		switch( args[ a ] )
		{
			case 'associativity':if( arg !== pass ) { v_associativity = arg; }
				break;
			case 'astCreator':if( arg !== pass ) { v_astCreator = arg; }
				break;
			case 'handler':if( arg !== pass ) { v_handler = arg; }
				break;
			case 'prec':if( arg !== pass ) { v_prec = arg; }
				break;
			default :throw ( new Error( args[ a ] ) );
		}
	}
	if( v_associativity === undefined ) { v_associativity = 'n/a'; }
/**/if( CHECK )
/**/{
/**/	if( typeof( v_associativity ) !== 'string' ) { throw ( new Error( ) ); }
/**/	if( v_astCreator !== undefined && typeof( v_astCreator ) !== 'object' )
/**/	{
/**/		throw ( new Error( ) );
/**/	}
/**/	if( typeof( v_handler ) !== 'string' ) { throw ( new Error( ) ); }
/**/	if( v_prec !== undefined && ( typeof( v_prec ) !== 'number' || Number.isNaN( v_prec ) ) )
/**/	{
/**/		throw ( new Error( ) );
/**/	}
/**/}
	const hash = ti2c._hashArgs( 146575033, v_associativity, v_astCreator, v_handler, v_prec );
	let ihash = hash;
	let uhash;
	let lhash;
	for( let wr = imap.get( hash ); wr; wr = imap.get( ihash += 4.76837158203125e-7 ) )
	{
		const e = wr.deref( );
		if( e )
		{
			lhash = ihash;
			if(
				v_associativity === e.associativity && v_astCreator === e.astCreator
				&& v_handler === e.handler
				&& v_prec === e.prec
			)
			{
				if( uhash !== undefined )
				{
					imap.set( ihash, imap.get( uhash ) );
					imap.set( uhash, wr );
				}
				return e;
			}
		}
		else
		{
			if( uhash === undefined ) { uhash = ihash; }
		}
	}
	if( uhash === undefined ) { uhash = ihash; if( lhash < uhash ) { lhash = uhash; } }
	if( lhash !== undefined )
	{
		lhash += 4.76837158203125e-7;
		while( imap.get( lhash ) ) { imap.set( lhash, undefined ); lhash += 4.76837158203125e-7; }
	}
	const newti2c = new Constructor( hash, v_associativity, v_astCreator, v_handler, v_prec );
	imap.set( uhash, new WeakRef( newti2c ) );
	if( cSize ) { cPut( newti2c ); }
	return newti2c;
};

/*
| Type reflection.
*/
prototype.ti2ctype = Self;

/*
| Reflection for debugging.
*/
Self.__TI2C_NAME__ = prototype.__TI2C_NAME__ = 'ti2c:Parser/Handle';
