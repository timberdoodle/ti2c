/*
| This is an auto generated file.
| Editing this might be rather futile.
*/

import { Self as _$Q$_tt__liststring } from '{{list@string}}';

/*
| The interning map.
*/
let imap = new Map( );

/*
| Caching queue.
*/
let cSize = globalThis.TI2C_RETENTION ? 1024 : 0;
let cArray = cSize ? new Array( cSize ) : undefined;
let cPos = 0;

/*
| Timestamp since last turnaround
*/
let cTurn = cSize ? Date.now( ) : undefined;

/*
| Puts an entry in the caching queue.
*/
const cPut =
	function( o )
{
	cArray[ cPos ] = o;
	cPos = ++cPos % cSize;
	if( !cPos )
	{
		const now = Date.now( );
		if( now - cTurn < globalThis.TI2C_RETENTION )
		{
			cPos = cSize;
			cSize *= 2;
			cArray.length = cSize;
		}
		cTurn = now;
	}
};

/*
| Constructor.
*/
const Constructor =
	function( hash, set )
{
	this.__lazy = { };
	this.__hash = hash;
	this._set = set;
	Object.freeze( this );
	Object.freeze( set );
};

/*
| In case of checking all unknown access is to be trapped.
*/
/**/if( CHECK )
/**/{
/**/const Trap = function( ) { };
/**/Trap.prototype = ti2c.trap;
/**/Constructor.prototype = new Trap( );
/**/}

/*
| Constructor prototype.
*/
const prototype = Self.prototype = Constructor.prototype;
Self.__hash = -459224041;

/*
| Creates a new object.
*/
Self.create =
prototype.create =
	function( ...args )
{
	let set;
	let setDup;
	if( this !== Self )
	{
		set = this._set;
		setDup = false;
	}
	else
	{
		set = new Set( );
		setDup = true;
	}
	for( let a = 0, alen = args.length; a < alen; a += 2 )
	{
		let arg = args[ a + 1 ];
		switch( args[ a ] )
		{
			case 'set:add':if( !setDup ) { set = new Set( set ); setDup = true; }
				set.add( arg, args[ a + 1 ] );
				break;
			case 'set:init':if( CHECK ) { if( !( arg instanceof Set ) ) { throw ( new Error( ) ); } }
				set = arg;
				setDup = true;
				break;
			case 'set:remove':if( !setDup ) { set = new Set( set ); setDup = true; }
				set.delete( arg );
				break;
			default :throw ( new Error( args[ a ] ) );
		}
	}
/**/if( CHECK )
/**/{
/**/	for( let v of set ) { if( typeof( v ) !== 'string' ) { throw ( new Error( ) ); } }
/**/}
	const hash = ti2c._hashSet( -459224041, set );
	let ihash = hash;
	let uhash;
	let lhash;
	for( let wr = imap.get( hash ); wr; wr = imap.get( ihash += 4.76837158203125e-7 ) )
	{
		const e = wr.deref( );
		if( e )
		{
			lhash = ihash;
			const eset = e._set;
			if( set.size !== eset.size ) { continue; }
			let eq = true;
			for( let e of set ) { if( !eset.has( e ) ) { eq = false; break; } }
			if( eq )
			{
				if( uhash !== undefined )
				{
					imap.set( ihash, imap.get( uhash ) );
					imap.set( uhash, wr );
				}
				return e;
			}
		}
		else
		{
			if( uhash === undefined ) { uhash = ihash; }
		}
	}
	if( uhash === undefined ) { uhash = ihash; if( lhash < uhash ) { lhash = uhash; } }
	if( lhash !== undefined )
	{
		lhash += 4.76837158203125e-7;
		while( imap.get( lhash ) ) { imap.set( lhash, undefined ); lhash += 4.76837158203125e-7; }
	}
	const newti2c = new Constructor( hash, set );
	imap.set( uhash, new WeakRef( newti2c ) );
	if( cSize ) { cPut( newti2c ); }
	return newti2c;
};

/*
| Creates a new object from json.
*/
Self.FromJson =
	function( json )
{
/**/if( CHECK ) { if( arguments.length !== 1 ) { throw ( new Error( ) ); } }
	let jset;
	let set;
	for( let name in json )
	{
		const arg = json[ name ];
		switch( name )
		{
			case '$type':if( arg !== 'set@string' ) { throw ( new Error( ) ); }
				break;
			case 'set':jset = arg;
				break;
			default :throw ( new Error( ) );
		}
	}
	if( !jset ) { throw ( new Error( ) ); }
	set = new Set( );
	for( let e of jset )
	{
		if( typeof( e ) === 'string' ) { set.add( e ); continue; }
		throw ( new Error( ) );
	}
	for( let v of set ) { if( typeof( v ) !== 'string' ) { throw ( new Error( ) ); } }
	const hash = ti2c._hashSet( -459224041, set );
	let ihash = hash;
	let uhash;
	let lhash;
	for( let wr = imap.get( hash ); wr; wr = imap.get( ihash += 4.76837158203125e-7 ) )
	{
		const e = wr.deref( );
		if( e )
		{
			lhash = ihash;
			const eset = e._set;
			if( set.size !== eset.size ) { continue; }
			let eq = true;
			for( let e of set ) { if( !eset.has( e ) ) { eq = false; break; } }
			if( eq )
			{
				if( uhash !== undefined )
				{
					imap.set( ihash, imap.get( uhash ) );
					imap.set( uhash, wr );
				}
				return e;
			}
		}
		else
		{
			if( uhash === undefined ) { uhash = ihash; }
		}
	}
	if( uhash === undefined ) { uhash = ihash; if( lhash < uhash ) { lhash = uhash; } }
	if( lhash !== undefined )
	{
		lhash += 4.76837158203125e-7;
		while( imap.get( lhash ) ) { imap.set( lhash, undefined ); lhash += 4.76837158203125e-7; }
	}
	const newtim = new Constructor( hash, set );
	imap.set( uhash, new WeakRef( newtim ) );
	return newtim;
};

/*
| Type reflection.
*/
prototype.ti2ctype = Self;

/*
| Reflection for debugging.
*/
Self.__TI2C_NAME__ = prototype.__TI2C_NAME__ = 'set@string';

/*
| Returns the set with one element added.
*/
prototype.add = ti2c._proto.setAdd;

/*
| Returns the set with another set added.
*/
prototype.addSet = ti2c._proto.setAddSet;

/*
| Returns a clone primitive.
*/
prototype.clone = function( ) { return new Set( this._set ); };

/*
| Returns true if the set has an element.
*/
prototype.has = ti2c._proto.setHas;

/*
| Returns the set with one element removed.
*/
prototype.remove = ti2c._proto.setRemove;

/*
| Returns the size of the set.
*/
ti2c._proto.lazyValue( prototype, 'size', ti2c._proto.setSize );

/*
| Returns the one and only element or the set if size != 1.
*/
ti2c._proto.lazyValue( prototype, 'trivial', ti2c._proto.setTrivial );

/*
| Forwards the iterator.
*/
prototype[ Symbol.iterator ] = function( ) { return this._set[ Symbol.iterator ]( ); };

/*
| Creates the set with direct elements.
*/
Self.Elements = function( ) { return Self.create( 'set:init', new Set( arguments ) ); };

/*
| Creates an empty set of this type.
*/
ti2c._proto.lazyStaticValue( Self, 'Empty', function( ) { return Self.create( ); } );

/*
| Turns the set into a sorted list.
*/
ti2c._proto.lazyValue(
	prototype,
	'list',
	function( )
{
	const a = Array.from( this._set );
	a.sort( );
	return _$Q$_tt__liststring.Array( a );
}
);

/*
| Stable jsonfy
*/
ti2c._proto.lazyFunction(
	prototype,
	'jsonfy',
	function( spacing )
{
	let i0space, i1space;
	let i2space;
	if( spacing === undefined )
	{
		i0space = i1space = '';
		i2space = '';
	}
	else
	{
		if( typeof( spacing ) === 'string' )
		{
			i0space = '\n';
			i1space = i0space + spacing;
			i2space = i1space + spacing;
		}
		else
		{
			i0space = '\n' + spacing.level;
			i1space = i0space + spacing.step;
			i2space = i1space + spacing.step;
		}
	}
	const colon = i1space !== '' ? ': ' : ':';
	let r = '{' + i1space + '"$type"' + colon + '"set@string",' + i1space + '"set"' + colon;
	if( this.size === 0 ) { return r + '[ ]' + i0space + '}'; }
	const set = this._set;
	const jstrs = [ ];
	for( let val of set ) { jstrs.push( JSON.stringify( val ) ); }
	jstrs.sort( );
	r += '[' + i2space;
	r += jstrs.join( ',' + i2space );
	r += i1space + ']' + i0space + '}';
	return r;
}
);

/*
| Json type identifier
*/
Self.$type = prototype.$type = 'set@string';
Self.$fromJsonArgs = prototype.$fromJsonArgs = [ ];
