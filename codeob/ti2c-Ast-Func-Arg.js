/*
| This is an auto generated file.
| Editing this might be rather futile.
*/


/*
| The interning map.
*/
let imap = new Map( );

/*
| Caching queue.
*/
let cSize = globalThis.TI2C_RETENTION ? 1024 : 0;
let cArray = cSize ? new Array( cSize ) : undefined;
let cPos = 0;

/*
| Timestamp since last turnaround
*/
let cTurn = cSize ? Date.now( ) : undefined;

/*
| Puts an entry in the caching queue.
*/
const cPut =
	function( o )
{
	cArray[ cPos ] = o;
	cPos = ++cPos % cSize;
	if( !cPos )
	{
		const now = Date.now( );
		if( now - cTurn < globalThis.TI2C_RETENTION )
		{
			cPos = cSize;
			cSize *= 2;
			cArray.length = cSize;
		}
		cTurn = now;
	}
};

/*
| Constructor.
*/
const Constructor =
	function( hash, v_comment, v_name, v_rest )
{
	this.__hash = hash;
	this.comment = v_comment;
	this.name = v_name;
	this.rest = v_rest;
	Object.freeze( this );
};

/*
| In case of checking all unknown access is to be trapped.
*/
/**/if( CHECK )
/**/{
/**/const Trap = function( ) { };
/**/Trap.prototype = ti2c.trap;
/**/Constructor.prototype = new Trap( );
/**/}

/*
| Constructor prototype.
*/
const prototype = Self.prototype = Constructor.prototype;
Self.__hash = 318580053;

/*
| Creates a new object.
*/
Self.create =
prototype.create =
	function( ...args )
{
	let v_comment;
	let v_name;
	let v_rest;
	if( this !== Self ) { v_comment = this.comment; v_name = this.name; v_rest = this.rest; }
	for( let a = 0, alen = args.length; a < alen; a += 2 )
	{
		let arg = args[ a + 1 ];
		switch( args[ a ] )
		{
			case 'comment':if( arg !== pass ) { v_comment = arg; }
				break;
			case 'name':if( arg !== pass ) { v_name = arg; }
				break;
			case 'rest':if( arg !== pass ) { v_rest = arg; }
				break;
			default :throw ( new Error( args[ a ] ) );
		}
	}
	if( v_rest === undefined ) { v_rest = false; }
/**/if( CHECK )
/**/{
/**/	if( v_comment !== undefined && typeof( v_comment ) !== 'string' )
/**/	{
/**/		throw ( new Error( ) );
/**/	}
/**/	if( v_name !== undefined && typeof( v_name ) !== 'string' ) { throw ( new Error( ) ); }
/**/	if( typeof( v_rest ) !== 'boolean' ) { throw ( new Error( ) ); }
/**/}
	const hash = ti2c._hashArgs( 318580053, v_comment, v_name, v_rest );
	let ihash = hash;
	let uhash;
	let lhash;
	for( let wr = imap.get( hash ); wr; wr = imap.get( ihash += 4.76837158203125e-7 ) )
	{
		const e = wr.deref( );
		if( e )
		{
			lhash = ihash;
			if( v_comment === e.comment && v_name === e.name && v_rest === e.rest )
			{
				if( uhash !== undefined )
				{
					imap.set( ihash, imap.get( uhash ) );
					imap.set( uhash, wr );
				}
				return e;
			}
		}
		else
		{
			if( uhash === undefined ) { uhash = ihash; }
		}
	}
	if( uhash === undefined ) { uhash = ihash; if( lhash < uhash ) { lhash = uhash; } }
	if( lhash !== undefined )
	{
		lhash += 4.76837158203125e-7;
		while( imap.get( lhash ) ) { imap.set( lhash, undefined ); lhash += 4.76837158203125e-7; }
	}
	const newti2c = new Constructor( hash, v_comment, v_name, v_rest );
	imap.set( uhash, new WeakRef( newti2c ) );
	if( cSize ) { cPut( newti2c ); }
	return newti2c;
};

/*
| Type reflection.
*/
prototype.ti2ctype = Self;

/*
| Reflection for debugging.
*/
Self.__TI2C_NAME__ = prototype.__TI2C_NAME__ = 'ti2c:Ast/Func/Arg';
