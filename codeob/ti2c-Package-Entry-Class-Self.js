/*
| This is an auto generated file.
| Editing this might be rather futile.
*/

import { Self as _$Q$_tt__liststring } from '{{list@string}}';
import { Self as tt__ti2c__Path_Self } from '{{ti2c:Path/Self}}';
import { Self as tt__ti2c__Package_Entry_Class_Alike_Group } from '{{ti2c:Package/Entry/Class/Alike/Group}}';
import { Self as tt__ti2c__Package_Entry_Class_Attribute_Group } from '{{ti2c:Package/Entry/Class/Attribute/Group}}';
import { Self as tt__ti2c__Package_Entry_Class_Self } from '{{ti2c:Package/Entry/Class/Self}}';
import { Self as tt__ti2c__Package_Entry_Collection_Group } from '{{ti2c:Package/Entry/Collection/Group}}';
import { Self as tt__ti2c__Package_Entry_Collection_List } from '{{ti2c:Package/Entry/Collection/List}}';
import { Self as tt__ti2c__Package_Entry_Collection_Set } from '{{ti2c:Package/Entry/Collection/Set}}';
import { Self as tt__ti2c__Package_Entry_Collection_Twig } from '{{ti2c:Package/Entry/Collection/Twig}}';
import { Self as tt__ti2c__Package_Manager } from '{{ti2c:Package/Manager}}';
import { Self as tt__ti2c__Ouroboros_Root } from '{{ti2c:Ouroboros/Root}}';
import { Self as tt__ti2c__Type_Ti2c_Class } from '{{ti2c:Type/Ti2c/Class}}';

/*
| The interning map.
*/
let imap = new Map( );

/*
| Caching queue.
*/
let cSize = globalThis.TI2C_RETENTION ? 1024 : 0;
let cArray = cSize ? new Array( cSize ) : undefined;
let cPos = 0;

/*
| Timestamp since last turnaround
*/
let cTurn = cSize ? Date.now( ) : undefined;

/*
| Puts an entry in the caching queue.
*/
const cPut =
	function( o )
{
	cArray[ cPos ] = o;
	cPos = ++cPos % cSize;
	if( !cPos )
	{
		const now = Date.now( );
		if( now - cTurn < globalThis.TI2C_RETENTION )
		{
			cPos = cSize;
			cSize *= 2;
			cArray.length = cSize;
		}
		cTurn = now;
	}
};

/*
| Constructor.
*/
const Constructor =
	function(
		hash,
		v__coreEntry,
		v__fromJsonArgsOverride,
		v_aPathCodeGen,
		v_aPathDef,
		v_abstract,
		v_alikes,
		v_attributes,
		v_check,
		v_creator,
		v_customFromJson,
		v_customJsonfy,
		v_entryExtend,
		v_extendCollection,
		v_hasLazy,
		v_jsonName,
		v_manager,
		v_setGlobal,
		v_singleton,
		v_type
	)
{
	this.__lazy = { };
	this.__hash = hash;
	this._coreEntry = v__coreEntry;
	this._fromJsonArgsOverride = v__fromJsonArgsOverride;
	this.aPathCodeGen = v_aPathCodeGen;
	this.aPathDef = v_aPathDef;
	this.abstract = v_abstract;
	this.alikes = v_alikes;
	this.attributes = v_attributes;
	this.check = v_check;
	this.creator = v_creator;
	this.customFromJson = v_customFromJson;
	this.customJsonfy = v_customJsonfy;
	this.entryExtend = v_entryExtend;
	this.extendCollection = v_extendCollection;
	this.hasLazy = v_hasLazy;
	this.jsonName = v_jsonName;
	this.manager = v_manager;
	this.setGlobal = v_setGlobal;
	this.singleton = v_singleton;
	this.type = v_type;
	Object.freeze( this );
};

/*
| In case of checking all unknown access is to be trapped.
*/
/**/if( CHECK )
/**/{
/**/const Trap = function( ) { };
/**/Trap.prototype = ti2c.trap;
/**/Constructor.prototype = new Trap( );
/**/}

/*
| Constructor prototype.
*/
const prototype = Self.prototype = Constructor.prototype;
Self.__hash = -1398943180;

/*
| Creates a new object.
*/
Self.create =
prototype.create =
	function( ...args )
{
	let v__coreEntry;
	let v__fromJsonArgsOverride;
	let v_aPathCodeGen;
	let v_aPathDef;
	let v_abstract;
	let v_alikes;
	let v_attributes;
	let v_check;
	let v_creator;
	let v_customFromJson;
	let v_customJsonfy;
	let v_entryExtend;
	let v_extendCollection;
	let v_hasLazy;
	let v_jsonName;
	let v_manager;
	let v_setGlobal;
	let v_singleton;
	let v_type;
	if( this !== Self )
	{
		v__coreEntry = this._coreEntry;
		v__fromJsonArgsOverride = this._fromJsonArgsOverride;
		v_aPathCodeGen = this.aPathCodeGen;
		v_aPathDef = this.aPathDef;
		v_abstract = this.abstract;
		v_alikes = this.alikes;
		v_attributes = this.attributes;
		v_check = this.check;
		v_creator = this.creator;
		v_customFromJson = this.customFromJson;
		v_customJsonfy = this.customJsonfy;
		v_entryExtend = this.entryExtend;
		v_extendCollection = this.extendCollection;
		v_hasLazy = this.hasLazy;
		v_jsonName = this.jsonName;
		v_manager = this.manager;
		v_setGlobal = this.setGlobal;
		v_singleton = this.singleton;
		v_type = this.type;
	}
	for( let a = 0, alen = args.length; a < alen; a += 2 )
	{
		let arg = args[ a + 1 ];
		switch( args[ a ] )
		{
			case '_coreEntry':if( arg !== pass ) { v__coreEntry = arg; }
				break;
			case '_fromJsonArgsOverride':if( arg !== pass ) { v__fromJsonArgsOverride = arg; }
				break;
			case 'aPathCodeGen':if( arg !== pass ) { v_aPathCodeGen = arg; }
				break;
			case 'aPathDef':if( arg !== pass ) { v_aPathDef = arg; }
				break;
			case 'abstract':if( arg !== pass ) { v_abstract = arg; }
				break;
			case 'alikes':if( arg !== pass ) { v_alikes = arg; }
				break;
			case 'attributes':if( arg !== pass ) { v_attributes = arg; }
				break;
			case 'check':if( arg !== pass ) { v_check = arg; }
				break;
			case 'creator':if( arg !== pass ) { v_creator = arg; }
				break;
			case 'customFromJson':if( arg !== pass ) { v_customFromJson = arg; }
				break;
			case 'customJsonfy':if( arg !== pass ) { v_customJsonfy = arg; }
				break;
			case 'entryExtend':if( arg !== pass ) { v_entryExtend = arg; }
				break;
			case 'extendCollection':if( arg !== pass ) { v_extendCollection = arg; }
				break;
			case 'hasLazy':if( arg !== pass ) { v_hasLazy = arg; }
				break;
			case 'jsonName':if( arg !== pass ) { v_jsonName = arg; }
				break;
			case 'manager':if( arg !== pass ) { v_manager = arg; }
				break;
			case 'setGlobal':if( arg !== pass ) { v_setGlobal = arg; }
				break;
			case 'singleton':if( arg !== pass ) { v_singleton = arg; }
				break;
			case 'type':if( arg !== pass ) { v_type = arg; }
				break;
			default :throw ( new Error( args[ a ] ) );
		}
	}
/**/if( CHECK )
/**/{
/**/	if( v__coreEntry !== undefined && typeof( v__coreEntry ) !== 'object' )
/**/	{
/**/		throw ( new Error( ) );
/**/	}
/**/	if(
/**/		v__fromJsonArgsOverride !== undefined
/**/		&& v__fromJsonArgsOverride.ti2ctype !== _$Q$_tt__liststring
/**/	)
/**/	{
/**/		throw ( new Error( ) );
/**/	}
/**/	if( v_aPathCodeGen.ti2ctype !== tt__ti2c__Path_Self ) { throw ( new Error( ) ); }
/**/	if( v_aPathDef.ti2ctype !== tt__ti2c__Path_Self ) { throw ( new Error( ) ); }
/**/	if( typeof( v_abstract ) !== 'boolean' ) { throw ( new Error( ) ); }
/**/	if(
/**/		v_alikes !== undefined
/**/		&& v_alikes.ti2ctype !== tt__ti2c__Package_Entry_Class_Alike_Group
/**/	)
/**/	{
/**/		throw ( new Error( ) );
/**/	}
/**/	if( v_attributes.ti2ctype !== tt__ti2c__Package_Entry_Class_Attribute_Group )
/**/	{
/**/		throw ( new Error( ) );
/**/	}
/**/	if( typeof( v_check ) !== 'boolean' ) { throw ( new Error( ) ); }
/**/	if( typeof( v_creator ) !== 'string' ) { throw ( new Error( ) ); }
/**/	if( typeof( v_customFromJson ) !== 'boolean' ) { throw ( new Error( ) ); }
/**/	if( typeof( v_customJsonfy ) !== 'boolean' ) { throw ( new Error( ) ); }
/**/	if(
/**/		v_entryExtend !== undefined
/**/		&& v_entryExtend.ti2ctype !== tt__ti2c__Package_Entry_Class_Self
/**/		&& v_entryExtend.ti2ctype !== tt__ti2c__Package_Entry_Collection_Group
/**/		&& v_entryExtend.ti2ctype !== tt__ti2c__Package_Entry_Collection_List
/**/		&& v_entryExtend.ti2ctype !== tt__ti2c__Package_Entry_Collection_Set
/**/		&& v_entryExtend.ti2ctype !== tt__ti2c__Package_Entry_Collection_Twig
/**/	)
/**/	{
/**/		throw ( new Error( ) );
/**/	}
/**/	if(
/**/		v_extendCollection !== undefined
/**/		&& v_extendCollection.ti2ctype !== tt__ti2c__Package_Entry_Collection_Group
/**/		&& v_extendCollection.ti2ctype !== tt__ti2c__Package_Entry_Collection_List
/**/		&& v_extendCollection.ti2ctype !== tt__ti2c__Package_Entry_Collection_Set
/**/		&& v_extendCollection.ti2ctype !== tt__ti2c__Package_Entry_Collection_Twig
/**/	)
/**/	{
/**/		throw ( new Error( ) );
/**/	}
/**/	if( typeof( v_hasLazy ) !== 'boolean' ) { throw ( new Error( ) ); }
/**/	if( v_jsonName !== undefined && typeof( v_jsonName ) !== 'string' )
/**/	{
/**/		throw ( new Error( ) );
/**/	}
/**/	if(
/**/		v_manager.ti2ctype !== tt__ti2c__Package_Manager
/**/		&& v_manager.ti2ctype !== tt__ti2c__Ouroboros_Root
/**/	)
/**/	{
/**/		throw ( new Error( ) );
/**/	}
/**/	if( v_setGlobal !== undefined && typeof( v_setGlobal ) !== 'string' )
/**/	{
/**/		throw ( new Error( ) );
/**/	}
/**/	if( typeof( v_singleton ) !== 'boolean' ) { throw ( new Error( ) ); }
/**/	if( v_type.ti2ctype !== tt__ti2c__Type_Ti2c_Class ) { throw ( new Error( ) ); }
/**/}
	const hash =
		ti2c._hashArgs(
			-1398943180,
			v__coreEntry,
			v__fromJsonArgsOverride,
			v_aPathCodeGen,
			v_aPathDef,
			v_abstract,
			v_alikes,
			v_attributes,
			v_check,
			v_creator,
			v_customFromJson,
			v_customJsonfy,
			v_entryExtend,
			v_extendCollection,
			v_hasLazy,
			v_jsonName,
			v_manager,
			v_setGlobal,
			v_singleton,
			v_type
		);
	let ihash = hash;
	let uhash;
	let lhash;
	for( let wr = imap.get( hash ); wr; wr = imap.get( ihash += 4.76837158203125e-7 ) )
	{
		const e = wr.deref( );
		if( e )
		{
			lhash = ihash;
			if(
				v__coreEntry === e._coreEntry
				&& v__fromJsonArgsOverride === e._fromJsonArgsOverride
				&& v_aPathCodeGen === e.aPathCodeGen
				&& v_aPathDef === e.aPathDef
				&& v_abstract === e.abstract
				&& v_alikes === e.alikes
				&& v_attributes === e.attributes
				&& v_check === e.check
				&& v_creator === e.creator
				&& v_customFromJson === e.customFromJson
				&& v_customJsonfy === e.customJsonfy
				&& v_entryExtend === e.entryExtend
				&& v_extendCollection === e.extendCollection
				&& v_hasLazy === e.hasLazy
				&& v_jsonName === e.jsonName
				&& v_manager === e.manager
				&& v_setGlobal === e.setGlobal
				&& v_singleton === e.singleton
				&& v_type === e.type
			)
			{
				if( uhash !== undefined )
				{
					imap.set( ihash, imap.get( uhash ) );
					imap.set( uhash, wr );
				}
				return e;
			}
		}
		else
		{
			if( uhash === undefined ) { uhash = ihash; }
		}
	}
	if( uhash === undefined ) { uhash = ihash; if( lhash < uhash ) { lhash = uhash; } }
	if( lhash !== undefined )
	{
		lhash += 4.76837158203125e-7;
		while( imap.get( lhash ) ) { imap.set( lhash, undefined ); lhash += 4.76837158203125e-7; }
	}
	const newti2c =
		new Constructor(
			hash,
			v__coreEntry,
			v__fromJsonArgsOverride,
			v_aPathCodeGen,
			v_aPathDef,
			v_abstract,
			v_alikes,
			v_attributes,
			v_check,
			v_creator,
			v_customFromJson,
			v_customJsonfy,
			v_entryExtend,
			v_extendCollection,
			v_hasLazy,
			v_jsonName,
			v_manager,
			v_setGlobal,
			v_singleton,
			v_type
		);
	imap.set( uhash, new WeakRef( newti2c ) );
	if( cSize ) { cPut( newti2c ); }
	return newti2c;
};

/*
| Type reflection.
*/
prototype.ti2ctype = Self;

/*
| Reflection for debugging.
*/
Self.__TI2C_NAME__ = prototype.__TI2C_NAME__ = 'ti2c:Package/Entry/Class/Self';
