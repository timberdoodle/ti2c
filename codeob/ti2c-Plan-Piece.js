/*
| This is an auto generated file.
| Editing this might be rather futile.
*/

import { Self as tt__group$QC$_function__tt__ti2c__Plan_Piece_$D$ } from '{{group@(function,ti2c:Plan/Piece)}}';
import { Self as _$Q$_tt__groupprotean } from '{{group@protean}}';

/*
| The interning map.
*/
let imap = new Map( );

/*
| Caching queue.
*/
let cSize = globalThis.TI2C_RETENTION ? 1024 : 0;
let cArray = cSize ? new Array( cSize ) : undefined;
let cPos = 0;

/*
| Timestamp since last turnaround
*/
let cTurn = cSize ? Date.now( ) : undefined;

/*
| Puts an entry in the caching queue.
*/
const cPut =
	function( o )
{
	cArray[ cPos ] = o;
	cPos = ++cPos % cSize;
	if( !cPos )
	{
		const now = Date.now( );
		if( now - cTurn < globalThis.TI2C_RETENTION )
		{
			cPos = cSize;
			cSize *= 2;
			cArray.length = cSize;
		}
		cTurn = now;
	}
};

/*
| Constructor.
*/
const Constructor =
	function( hash, v_at, v_key, v_name, v_subs, v_types )
{
	this.__lazy = { };
	this.__hash = hash;
	this.at = v_at;
	this.key = v_key;
	this.name = v_name;
	this.subs = v_subs;
	this.types = v_types;
	Object.freeze( this );
};

/*
| In case of checking all unknown access is to be trapped.
*/
/**/if( CHECK )
/**/{
/**/const Trap = function( ) { };
/**/Trap.prototype = ti2c.trap;
/**/Constructor.prototype = new Trap( );
/**/}

/*
| Constructor prototype.
*/
const prototype = Self.prototype = Constructor.prototype;
Self.__hash = 985334628;

/*
| Creates a new object.
*/
Self.create =
prototype.create =
	function( ...args )
{
	let v_at;
	let v_key;
	let v_name;
	let v_subs;
	let v_types;
	if( this !== Self )
	{
		v_at = this.at;
		v_key = this.key;
		v_name = this.name;
		v_subs = this.subs;
		v_types = this.types;
	}
	for( let a = 0, alen = args.length; a < alen; a += 2 )
	{
		let arg = args[ a + 1 ];
		switch( args[ a ] )
		{
			case 'at':if( arg !== pass ) { v_at = arg; }
				break;
			case 'key':if( arg !== pass ) { v_key = arg; }
				break;
			case 'name':if( arg !== pass ) { v_name = arg; }
				break;
			case 'subs':if( arg !== pass ) { v_subs = arg; }
				break;
			case 'types':if( arg !== pass ) { v_types = arg; }
				break;
			default :throw ( new Error( args[ a ] ) );
		}
	}
	if( v_at === undefined ) { v_at = false; }
	if( v_key === undefined ) { v_key = false; }
/**/if( CHECK )
/**/{
/**/	if( typeof( v_at ) !== 'boolean' ) { throw ( new Error( ) ); }
/**/	if( typeof( v_key ) !== 'boolean' ) { throw ( new Error( ) ); }
/**/	if( typeof( v_name ) !== 'string' ) { throw ( new Error( ) ); }
/**/	if(
/**/		v_subs !== undefined
/**/		&& v_subs.ti2ctype !== tt__group$QC$_function__tt__ti2c__Plan_Piece_$D$
/**/	)
/**/	{
/**/		throw ( new Error( ) );
/**/	}
/**/	if( v_types !== undefined && v_types.ti2ctype !== _$Q$_tt__groupprotean )
/**/	{
/**/		throw ( new Error( ) );
/**/	}
/**/}
	const hash = ti2c._hashArgs( 985334628, v_at, v_key, v_name, v_subs, v_types );
	let ihash = hash;
	let uhash;
	let lhash;
	for( let wr = imap.get( hash ); wr; wr = imap.get( ihash += 4.76837158203125e-7 ) )
	{
		const e = wr.deref( );
		if( e )
		{
			lhash = ihash;
			if(
				v_at === e.at && v_key === e.key && v_name === e.name && v_subs === e.subs
				&& v_types === e.types
			)
			{
				if( uhash !== undefined )
				{
					imap.set( ihash, imap.get( uhash ) );
					imap.set( uhash, wr );
				}
				return e;
			}
		}
		else
		{
			if( uhash === undefined ) { uhash = ihash; }
		}
	}
	if( uhash === undefined ) { uhash = ihash; if( lhash < uhash ) { lhash = uhash; } }
	if( lhash !== undefined )
	{
		lhash += 4.76837158203125e-7;
		while( imap.get( lhash ) ) { imap.set( lhash, undefined ); lhash += 4.76837158203125e-7; }
	}
	const newti2c = new Constructor( hash, v_at, v_key, v_name, v_subs, v_types );
	imap.set( uhash, new WeakRef( newti2c ) );
	if( cSize ) { cPut( newti2c ); }
	return newti2c;
};

/*
| Creates a new object from json.
*/
Self.FromJson =
	function( json )
{
/**/if( CHECK ) { if( arguments.length !== 1 ) { throw ( new Error( ) ); } }
	let v_at;
	let v_key;
	let v_name;
	let v_subs;
	let v_types;
	for( let name in json )
	{
		const arg = json[ name ];
		switch( name )
		{
			case '$type':if( arg !== 'ti2c:Plan/Piece' ) { throw ( new Error( ) ); }
				break;
		}
	}
	if( v_at === undefined ) { v_at = false; }
	if( v_key === undefined ) { v_key = false; }
	const hash = ti2c._hashArgs( 985334628, v_at, v_key, v_name, v_subs, v_types );
	let ihash = hash;
	let uhash;
	let lhash;
	for( let wr = imap.get( hash ); wr; wr = imap.get( ihash += 4.76837158203125e-7 ) )
	{
		const e = wr.deref( );
		if( e )
		{
			lhash = ihash;
			if(
				v_at === e.at && v_key === e.key && v_name === e.name && v_subs === e.subs
				&& v_types === e.types
			)
			{
				if( uhash !== undefined )
				{
					imap.set( ihash, imap.get( uhash ) );
					imap.set( uhash, wr );
				}
				return e;
			}
		}
		else
		{
			if( uhash === undefined ) { uhash = ihash; }
		}
	}
	if( uhash === undefined ) { uhash = ihash; if( lhash < uhash ) { lhash = uhash; } }
	if( lhash !== undefined )
	{
		lhash += 4.76837158203125e-7;
		while( imap.get( lhash ) ) { imap.set( lhash, undefined ); lhash += 4.76837158203125e-7; }
	}
	const newtim = new Constructor( hash, v_at, v_key, v_name, v_subs, v_types );
	imap.set( uhash, new WeakRef( newtim ) );
	return newtim;
};

/*
| Type reflection.
*/
prototype.ti2ctype = Self;

/*
| Reflection for debugging.
*/
Self.__TI2C_NAME__ = prototype.__TI2C_NAME__ = 'ti2c:Plan/Piece';

/*
| Stable jsonfy
*/
ti2c._proto.lazyFunction(
	prototype,
	'jsonfy',
	function( spacing )
{
	let i0space, i1space;
	if( spacing === undefined )
	{
		i0space = i1space = '';
	}
	else
	{
		if( typeof( spacing ) === 'string' )
		{
			i0space = '\n';
			i1space = i0space + spacing;
		}
		else
		{
			i0space = '\n' + spacing.level;
			i1space = i0space + spacing.step;
		}
	}
	const colon = i1space !== '' ? ': ' : ':';
	let r = '{' + i1space + '"$type"' + colon + '"ti2c:Plan/Piece"';
	r += i0space + '}';
	return r;
}
);

/*
| Json type identifier
*/
Self.$type = prototype.$type = 'ti2c:Plan/Piece';
Self.$fromJsonArgs = prototype.$fromJsonArgs = [ ];
