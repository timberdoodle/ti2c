/*
| This is an auto generated file.
| Editing this might be rather futile.
*/

import { Self as tt__ti2c__Package_Exports } from '{{ti2c:Package/Exports}}';
import { Self as tt__ti2c__Path_Self } from '{{ti2c:Path/Self}}';

/*
| The interning map.
*/
let imap = new Map( );

/*
| Caching queue.
*/
let cSize = globalThis.TI2C_RETENTION ? 1024 : 0;
let cArray = cSize ? new Array( cSize ) : undefined;
let cPos = 0;

/*
| Timestamp since last turnaround
*/
let cTurn = cSize ? Date.now( ) : undefined;

/*
| Puts an entry in the caching queue.
*/
const cPut =
	function( o )
{
	cArray[ cPos ] = o;
	cPos = ++cPos % cSize;
	if( !cPos )
	{
		const now = Date.now( );
		if( now - cTurn < globalThis.TI2C_RETENTION )
		{
			cPos = cSize;
			cSize *= 2;
			cArray.length = cSize;
		}
		cTurn = now;
	}
};

/*
| Constructor.
*/
const Constructor =
	function( hash, v_exports, v_name, v_rootDir, v_srcDir )
{
	this.__hash = hash;
	this.exports = v_exports;
	this.name = v_name;
	this.rootDir = v_rootDir;
	this.srcDir = v_srcDir;
	Object.freeze( this );
};

/*
| In case of checking all unknown access is to be trapped.
*/
/**/if( CHECK )
/**/{
/**/const Trap = function( ) { };
/**/Trap.prototype = ti2c.trap;
/**/Constructor.prototype = new Trap( );
/**/}

/*
| Constructor prototype.
*/
const prototype = Self.prototype = Constructor.prototype;
Self.__hash = 1843832534;

/*
| Creates a new object.
*/
Self.create =
prototype.create =
	function( ...args )
{
	let v_exports;
	let v_name;
	let v_rootDir;
	let v_srcDir;
	if( this !== Self )
	{
		v_exports = this.exports;
		v_name = this.name;
		v_rootDir = this.rootDir;
		v_srcDir = this.srcDir;
	}
	for( let a = 0, alen = args.length; a < alen; a += 2 )
	{
		let arg = args[ a + 1 ];
		switch( args[ a ] )
		{
			case 'exports':if( arg !== pass ) { v_exports = arg; }
				break;
			case 'name':if( arg !== pass ) { v_name = arg; }
				break;
			case 'rootDir':if( arg !== pass ) { v_rootDir = arg; }
				break;
			case 'srcDir':if( arg !== pass ) { v_srcDir = arg; }
				break;
			default :throw ( new Error( args[ a ] ) );
		}
	}
/**/if( CHECK )
/**/{
/**/	if( v_exports !== undefined && v_exports.ti2ctype !== tt__ti2c__Package_Exports )
/**/	{
/**/		throw ( new Error( ) );
/**/	}
/**/	if( typeof( v_name ) !== 'string' ) { throw ( new Error( ) ); }
/**/	if( v_rootDir.ti2ctype !== tt__ti2c__Path_Self ) { throw ( new Error( ) ); }
/**/	if( v_srcDir.ti2ctype !== tt__ti2c__Path_Self ) { throw ( new Error( ) ); }
/**/}
	const hash = ti2c._hashArgs( 1843832534, v_exports, v_name, v_rootDir, v_srcDir );
	let ihash = hash;
	let uhash;
	let lhash;
	for( let wr = imap.get( hash ); wr; wr = imap.get( ihash += 4.76837158203125e-7 ) )
	{
		const e = wr.deref( );
		if( e )
		{
			lhash = ihash;
			if(
				v_exports === e.exports && v_name === e.name && v_rootDir === e.rootDir
				&& v_srcDir === e.srcDir
			)
			{
				if( uhash !== undefined )
				{
					imap.set( ihash, imap.get( uhash ) );
					imap.set( uhash, wr );
				}
				return e;
			}
		}
		else
		{
			if( uhash === undefined ) { uhash = ihash; }
		}
	}
	if( uhash === undefined ) { uhash = ihash; if( lhash < uhash ) { lhash = uhash; } }
	if( lhash !== undefined )
	{
		lhash += 4.76837158203125e-7;
		while( imap.get( lhash ) ) { imap.set( lhash, undefined ); lhash += 4.76837158203125e-7; }
	}
	const newti2c = new Constructor( hash, v_exports, v_name, v_rootDir, v_srcDir );
	imap.set( uhash, new WeakRef( newti2c ) );
	if( cSize ) { cPut( newti2c ); }
	return newti2c;
};

/*
| Type reflection.
*/
prototype.ti2ctype = Self;

/*
| Reflection for debugging.
*/
Self.__TI2C_NAME__ = prototype.__TI2C_NAME__ = 'ti2c:Package/Self';
