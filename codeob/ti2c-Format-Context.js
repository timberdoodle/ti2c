/*
| This is an auto generated file.
| Editing this might be rather futile.
*/


/*
| The interning map.
*/
let imap = new Map( );

/*
| Caching queue.
*/
let cSize = globalThis.TI2C_RETENTION ? 1024 : 0;
let cArray = cSize ? new Array( cSize ) : undefined;
let cPos = 0;

/*
| Timestamp since last turnaround
*/
let cTurn = cSize ? Date.now( ) : undefined;

/*
| Puts an entry in the caching queue.
*/
const cPut =
	function( o )
{
	cArray[ cPos ] = o;
	cPos = ++cPos % cSize;
	if( !cPos )
	{
		const now = Date.now( );
		if( now - cTurn < globalThis.TI2C_RETENTION )
		{
			cPos = cSize;
			cSize *= 2;
			cArray.length = cSize;
		}
		cTurn = now;
	}
};

/*
| Constructor.
*/
const Constructor =
	function( hash, v_check, v_indent, v_inline )
{
	this.__lazy = { };
	this.__hash = hash;
	this.check = v_check;
	this.indent = v_indent;
	this.inline = v_inline;
	Object.freeze( this );
};

/*
| In case of checking all unknown access is to be trapped.
*/
/**/if( CHECK )
/**/{
/**/const Trap = function( ) { };
/**/Trap.prototype = ti2c.trap;
/**/Constructor.prototype = new Trap( );
/**/}

/*
| Constructor prototype.
*/
const prototype = Self.prototype = Constructor.prototype;
Self.__hash = -2024592727;

/*
| Creates a new object.
*/
Self.create =
prototype.create =
	function( ...args )
{
	let v_check;
	let v_indent;
	let v_inline;
	if( this !== Self ) { v_check = this.check; v_indent = this.indent; v_inline = this.inline; }
	for( let a = 0, alen = args.length; a < alen; a += 2 )
	{
		let arg = args[ a + 1 ];
		switch( args[ a ] )
		{
			case 'check':if( arg !== pass ) { v_check = arg; }
				break;
			case 'indent':if( arg !== pass ) { v_indent = arg; }
				break;
			case 'inline':if( arg !== pass ) { v_inline = arg; }
				break;
			default :throw ( new Error( args[ a ] ) );
		}
	}
	if( v_check === undefined ) { v_check = false; }
	if( v_indent === undefined ) { v_indent = 0; }
	if( v_inline === undefined ) { v_inline = false; }
/**/if( CHECK )
/**/{
/**/	if( typeof( v_check ) !== 'boolean' ) { throw ( new Error( ) ); }
/**/	if(
/**/		typeof( v_indent ) !== 'number' || Number.isNaN( v_indent )
/**/		|| Math.floor( v_indent ) !== v_indent
/**/	)
/**/	{
/**/		throw ( new Error( ) );
/**/	}
/**/	if( typeof( v_inline ) !== 'boolean' ) { throw ( new Error( ) ); }
/**/}
	const hash = ti2c._hashArgs( -2024592727, v_check, v_indent, v_inline );
	let ihash = hash;
	let uhash;
	let lhash;
	for( let wr = imap.get( hash ); wr; wr = imap.get( ihash += 4.76837158203125e-7 ) )
	{
		const e = wr.deref( );
		if( e )
		{
			lhash = ihash;
			if( v_check === e.check && v_indent === e.indent && v_inline === e.inline )
			{
				if( uhash !== undefined )
				{
					imap.set( ihash, imap.get( uhash ) );
					imap.set( uhash, wr );
				}
				return e;
			}
		}
		else
		{
			if( uhash === undefined ) { uhash = ihash; }
		}
	}
	if( uhash === undefined ) { uhash = ihash; if( lhash < uhash ) { lhash = uhash; } }
	if( lhash !== undefined )
	{
		lhash += 4.76837158203125e-7;
		while( imap.get( lhash ) ) { imap.set( lhash, undefined ); lhash += 4.76837158203125e-7; }
	}
	const newti2c = new Constructor( hash, v_check, v_indent, v_inline );
	imap.set( uhash, new WeakRef( newti2c ) );
	if( cSize ) { cPut( newti2c ); }
	return newti2c;
};

/*
| Type reflection.
*/
prototype.ti2ctype = Self;

/*
| Reflection for debugging.
*/
Self.__TI2C_NAME__ = prototype.__TI2C_NAME__ = 'ti2c:Format/Context';
