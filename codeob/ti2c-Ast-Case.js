/*
| This is an auto generated file.
| Editing this might be rather futile.
*/

import { Self as tt__ti2c__Ast_Block } from '{{ti2c:Ast/Block}}';
import { Self as _$Q$_tt__listtt__$Z$_ti2c__Ast_Type_Expr } from '{{list@<ti2c:Ast/Type/Expr}}';

/*
| The interning map.
*/
let imap = new Map( );

/*
| Caching queue.
*/
let cSize = globalThis.TI2C_RETENTION ? 1024 : 0;
let cArray = cSize ? new Array( cSize ) : undefined;
let cPos = 0;

/*
| Timestamp since last turnaround
*/
let cTurn = cSize ? Date.now( ) : undefined;

/*
| Puts an entry in the caching queue.
*/
const cPut =
	function( o )
{
	cArray[ cPos ] = o;
	cPos = ++cPos % cSize;
	if( !cPos )
	{
		const now = Date.now( );
		if( now - cTurn < globalThis.TI2C_RETENTION )
		{
			cPos = cSize;
			cSize *= 2;
			cArray.length = cSize;
		}
		cTurn = now;
	}
};

/*
| Constructor.
*/
const Constructor =
	function( hash, v_block, v_values )
{
	this.__hash = hash;
	this.block = v_block;
	this.values = v_values;
	Object.freeze( this );
};

/*
| In case of checking all unknown access is to be trapped.
*/
/**/if( CHECK )
/**/{
/**/const Trap = function( ) { };
/**/Trap.prototype = ti2c.trap;
/**/Constructor.prototype = new Trap( );
/**/}

/*
| Constructor prototype.
*/
const prototype = Self.prototype = Constructor.prototype;
Self.__hash = 1502045076;

/*
| Creates a new object.
*/
Self.create =
prototype.create =
	function( ...args )
{
	let v_block;
	let v_values;
	if( this !== Self ) { v_block = this.block; v_values = this.values; }
	for( let a = 0, alen = args.length; a < alen; a += 2 )
	{
		let arg = args[ a + 1 ];
		switch( args[ a ] )
		{
			case 'block':if( arg !== pass ) { v_block = arg; }
				break;
			case 'values':if( arg !== pass ) { v_values = arg; }
				break;
			default :throw ( new Error( args[ a ] ) );
		}
	}
	if( v_values === undefined ) { v_values = _$Q$_tt__listtt__$Z$_ti2c__Ast_Type_Expr.Empty; }
/**/if( CHECK )
/**/{
/**/	if( v_block.ti2ctype !== tt__ti2c__Ast_Block ) { throw ( new Error( ) ); }
/**/	if( v_values.ti2ctype !== _$Q$_tt__listtt__$Z$_ti2c__Ast_Type_Expr )
/**/	{
/**/		throw ( new Error( ) );
/**/	}
/**/}
	const hash = ti2c._hashArgs( 1502045076, v_block, v_values );
	let ihash = hash;
	let uhash;
	let lhash;
	for( let wr = imap.get( hash ); wr; wr = imap.get( ihash += 4.76837158203125e-7 ) )
	{
		const e = wr.deref( );
		if( e )
		{
			lhash = ihash;
			if( v_block === e.block && v_values === e.values )
			{
				if( uhash !== undefined )
				{
					imap.set( ihash, imap.get( uhash ) );
					imap.set( uhash, wr );
				}
				return e;
			}
		}
		else
		{
			if( uhash === undefined ) { uhash = ihash; }
		}
	}
	if( uhash === undefined ) { uhash = ihash; if( lhash < uhash ) { lhash = uhash; } }
	if( lhash !== undefined )
	{
		lhash += 4.76837158203125e-7;
		while( imap.get( lhash ) ) { imap.set( lhash, undefined ); lhash += 4.76837158203125e-7; }
	}
	const newti2c = new Constructor( hash, v_block, v_values );
	imap.set( uhash, new WeakRef( newti2c ) );
	if( cSize ) { cPut( newti2c ); }
	return newti2c;
};

/*
| Type reflection.
*/
prototype.ti2ctype = Self;

/*
| Reflection for debugging.
*/
Self.__TI2C_NAME__ = prototype.__TI2C_NAME__ = 'ti2c:Ast/Case';
