/*
| This is an auto generated file.
| Editing this might be rather futile.
*/


/*
| The interning map.
*/
let imap = new Map( );

/*
| Caching queue.
*/
let cSize = globalThis.TI2C_RETENTION ? 1024 : 0;
let cArray = cSize ? new Array( cSize ) : undefined;
let cPos = 0;

/*
| Timestamp since last turnaround
*/
let cTurn = cSize ? Date.now( ) : undefined;

/*
| Puts an entry in the caching queue.
*/
const cPut =
	function( o )
{
	cArray[ cPos ] = o;
	cPos = ++cPos % cSize;
	if( !cPos )
	{
		const now = Date.now( );
		if( now - cTurn < globalThis.TI2C_RETENTION )
		{
			cPos = cSize;
			cSize *= 2;
			cArray.length = cSize;
		}
		cTurn = now;
	}
};

/*
| Constructor.
*/
const Constructor =
	function( hash, v_aliasName, v_exportName, v_moduleName )
{
	this.__hash = hash;
	this.aliasName = v_aliasName;
	this.exportName = v_exportName;
	this.moduleName = v_moduleName;
	Object.freeze( this );
};

/*
| In case of checking all unknown access is to be trapped.
*/
/**/if( CHECK )
/**/{
/**/const Trap = function( ) { };
/**/Trap.prototype = ti2c.trap;
/**/Constructor.prototype = new Trap( );
/**/}

/*
| Constructor prototype.
*/
const prototype = Self.prototype = Constructor.prototype;
Self.__hash = 1771802775;

/*
| Creates a new object.
*/
Self.create =
prototype.create =
	function( ...args )
{
	let v_aliasName;
	let v_exportName;
	let v_moduleName;
	if( this !== Self )
	{
		v_aliasName = this.aliasName;
		v_exportName = this.exportName;
		v_moduleName = this.moduleName;
	}
	for( let a = 0, alen = args.length; a < alen; a += 2 )
	{
		let arg = args[ a + 1 ];
		switch( args[ a ] )
		{
			case 'aliasName':if( arg !== pass ) { v_aliasName = arg; }
				break;
			case 'exportName':if( arg !== pass ) { v_exportName = arg; }
				break;
			case 'moduleName':if( arg !== pass ) { v_moduleName = arg; }
				break;
			default :throw ( new Error( args[ a ] ) );
		}
	}
/**/if( CHECK )
/**/{
/**/	if( typeof( v_aliasName ) !== 'string' ) { throw ( new Error( ) ); }
/**/	if( typeof( v_exportName ) !== 'string' ) { throw ( new Error( ) ); }
/**/	if( typeof( v_moduleName ) !== 'string' ) { throw ( new Error( ) ); }
/**/}
	const hash = ti2c._hashArgs( 1771802775, v_aliasName, v_exportName, v_moduleName );
	let ihash = hash;
	let uhash;
	let lhash;
	for( let wr = imap.get( hash ); wr; wr = imap.get( ihash += 4.76837158203125e-7 ) )
	{
		const e = wr.deref( );
		if( e )
		{
			lhash = ihash;
			if(
				v_aliasName === e.aliasName && v_exportName === e.exportName
				&& v_moduleName === e.moduleName
			)
			{
				if( uhash !== undefined )
				{
					imap.set( ihash, imap.get( uhash ) );
					imap.set( uhash, wr );
				}
				return e;
			}
		}
		else
		{
			if( uhash === undefined ) { uhash = ihash; }
		}
	}
	if( uhash === undefined ) { uhash = ihash; if( lhash < uhash ) { lhash = uhash; } }
	if( lhash !== undefined )
	{
		lhash += 4.76837158203125e-7;
		while( imap.get( lhash ) ) { imap.set( lhash, undefined ); lhash += 4.76837158203125e-7; }
	}
	const newti2c = new Constructor( hash, v_aliasName, v_exportName, v_moduleName );
	imap.set( uhash, new WeakRef( newti2c ) );
	if( cSize ) { cPut( newti2c ); }
	return newti2c;
};

/*
| Type reflection.
*/
prototype.ti2ctype = Self;

/*
| Reflection for debugging.
*/
Self.__TI2C_NAME__ = prototype.__TI2C_NAME__ = 'ti2c:Ast/Import';
