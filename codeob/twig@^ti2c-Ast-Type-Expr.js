/*
| This is an auto generated file.
| Editing this might be rather futile.
*/

import { Self as tt__ti2c__Ast_And } from '{{ti2c:Ast/And}}';
import { Self as tt__ti2c__Ast_ArrayLiteral } from '{{ti2c:Ast/ArrayLiteral}}';
import { Self as tt__ti2c__Ast_ArrowFunction } from '{{ti2c:Ast/ArrowFunction}}';
import { Self as tt__ti2c__Ast_Assign } from '{{ti2c:Ast/Assign}}';
import { Self as tt__ti2c__Ast_Await } from '{{ti2c:Ast/Await}}';
import { Self as tt__ti2c__Ast_BitwiseAnd } from '{{ti2c:Ast/BitwiseAnd}}';
import { Self as tt__ti2c__Ast_BitwiseAndAssign } from '{{ti2c:Ast/BitwiseAndAssign}}';
import { Self as tt__ti2c__Ast_BitwiseLeftShift } from '{{ti2c:Ast/BitwiseLeftShift}}';
import { Self as tt__ti2c__Ast_BitwiseNot } from '{{ti2c:Ast/BitwiseNot}}';
import { Self as tt__ti2c__Ast_BitwiseOr } from '{{ti2c:Ast/BitwiseOr}}';
import { Self as tt__ti2c__Ast_BitwiseOrAssign } from '{{ti2c:Ast/BitwiseOrAssign}}';
import { Self as tt__ti2c__Ast_BitwiseRightShift } from '{{ti2c:Ast/BitwiseRightShift}}';
import { Self as tt__ti2c__Ast_BitwiseUnsignedRightShift } from '{{ti2c:Ast/BitwiseUnsignedRightShift}}';
import { Self as tt__ti2c__Ast_BitwiseXor } from '{{ti2c:Ast/BitwiseXor}}';
import { Self as tt__ti2c__Ast_BitwiseXorAssign } from '{{ti2c:Ast/BitwiseXorAssign}}';
import { Self as tt__ti2c__Ast_Boolean } from '{{ti2c:Ast/Boolean}}';
import { Self as tt__ti2c__Ast_Call } from '{{ti2c:Ast/Call}}';
import { Self as tt__ti2c__Ast_Comma } from '{{ti2c:Ast/Comma}}';
import { Self as tt__ti2c__Ast_Condition } from '{{ti2c:Ast/Condition}}';
import { Self as tt__ti2c__Ast_ConditionalDot } from '{{ti2c:Ast/ConditionalDot}}';
import { Self as tt__ti2c__Ast_Delete } from '{{ti2c:Ast/Delete}}';
import { Self as tt__ti2c__Ast_Differs } from '{{ti2c:Ast/Differs}}';
import { Self as tt__ti2c__Ast_Divide } from '{{ti2c:Ast/Divide}}';
import { Self as tt__ti2c__Ast_DivideAssign } from '{{ti2c:Ast/DivideAssign}}';
import { Self as tt__ti2c__Ast_Dot } from '{{ti2c:Ast/Dot}}';
import { Self as tt__ti2c__Ast_Equals } from '{{ti2c:Ast/Equals}}';
import { Self as tt__ti2c__Ast_Func_Self } from '{{ti2c:Ast/Func/Self}}';
import { Self as tt__ti2c__Ast_GreaterOrEqual } from '{{ti2c:Ast/GreaterOrEqual}}';
import { Self as tt__ti2c__Ast_GreaterThan } from '{{ti2c:Ast/GreaterThan}}';
import { Self as tt__ti2c__Ast_Instanceof } from '{{ti2c:Ast/Instanceof}}';
import { Self as tt__ti2c__Ast_LessOrEqual } from '{{ti2c:Ast/LessOrEqual}}';
import { Self as tt__ti2c__Ast_LessThan } from '{{ti2c:Ast/LessThan}}';
import { Self as tt__ti2c__Ast_Member } from '{{ti2c:Ast/Member}}';
import { Self as tt__ti2c__Ast_Minus } from '{{ti2c:Ast/Minus}}';
import { Self as tt__ti2c__Ast_MinusAssign } from '{{ti2c:Ast/MinusAssign}}';
import { Self as tt__ti2c__Ast_Multiply } from '{{ti2c:Ast/Multiply}}';
import { Self as tt__ti2c__Ast_MultiplyAssign } from '{{ti2c:Ast/MultiplyAssign}}';
import { Self as tt__ti2c__Ast_Negate } from '{{ti2c:Ast/Negate}}';
import { Self as tt__ti2c__Ast_New } from '{{ti2c:Ast/New}}';
import { Self as tt__ti2c__Ast_Not } from '{{ti2c:Ast/Not}}';
import { Self as tt__ti2c__Ast_Null } from '{{ti2c:Ast/Null}}';
import { Self as tt__ti2c__Ast_NullishCoalescence } from '{{ti2c:Ast/NullishCoalescence}}';
import { Self as tt__ti2c__Ast_Number } from '{{ti2c:Ast/Number}}';
import { Self as tt__ti2c__Ast_ObjLiteral } from '{{ti2c:Ast/ObjLiteral}}';
import { Self as tt__ti2c__Ast_Or } from '{{ti2c:Ast/Or}}';
import { Self as tt__ti2c__Ast_Plus } from '{{ti2c:Ast/Plus}}';
import { Self as tt__ti2c__Ast_PlusAssign } from '{{ti2c:Ast/PlusAssign}}';
import { Self as tt__ti2c__Ast_PostDecrement } from '{{ti2c:Ast/PostDecrement}}';
import { Self as tt__ti2c__Ast_PostIncrement } from '{{ti2c:Ast/PostIncrement}}';
import { Self as tt__ti2c__Ast_PreDecrement } from '{{ti2c:Ast/PreDecrement}}';
import { Self as tt__ti2c__Ast_PreIncrement } from '{{ti2c:Ast/PreIncrement}}';
import { Self as tt__ti2c__Ast_Regex } from '{{ti2c:Ast/Regex}}';
import { Self as tt__ti2c__Ast_Remainder } from '{{ti2c:Ast/Remainder}}';
import { Self as tt__ti2c__Ast_RemainderAssign } from '{{ti2c:Ast/RemainderAssign}}';
import { Self as tt__ti2c__Ast_Spread } from '{{ti2c:Ast/Spread}}';
import { Self as tt__ti2c__Ast_String } from '{{ti2c:Ast/String}}';
import { Self as tt__ti2c__Ast_Typeof } from '{{ti2c:Ast/Typeof}}';
import { Self as tt__ti2c__Ast_Undefined } from '{{ti2c:Ast/Undefined}}';
import { Self as tt__ti2c__Ast_Var } from '{{ti2c:Ast/Var}}';
import { Self as tt__ti2c__Ast_Yield } from '{{ti2c:Ast/Yield}}';

/*
| The interning map.
*/
let imap = new Map( );

/*
| Caching queue.
*/
let cSize = globalThis.TI2C_RETENTION ? 1024 : 0;
let cArray = cSize ? new Array( cSize ) : undefined;
let cPos = 0;

/*
| Timestamp since last turnaround
*/
let cTurn = cSize ? Date.now( ) : undefined;

/*
| Puts an entry in the caching queue.
*/
const cPut =
	function( o )
{
	cArray[ cPos ] = o;
	cPos = ++cPos % cSize;
	if( !cPos )
	{
		const now = Date.now( );
		if( now - cTurn < globalThis.TI2C_RETENTION )
		{
			cPos = cSize;
			cSize *= 2;
			cArray.length = cSize;
		}
		cTurn = now;
	}
};

/*
| Constructor.
*/
const Constructor =
	function( hash, keys, twig )
{
	this.__lazy = { };
	this.__hash = hash;
	this._twig = twig;
	this.keys = keys;
	Object.freeze( this );
	Object.freeze( twig );
	Object.freeze( keys );
};

/*
| In case of checking all unknown access is to be trapped.
*/
/**/if( CHECK )
/**/{
/**/const Trap = function( ) { };
/**/Trap.prototype = ti2c.trap;
/**/Constructor.prototype = new Trap( );
/**/}

/*
| Constructor prototype.
*/
const prototype = Self.prototype = Constructor.prototype;
Self.__hash = 480650326;

/*
| Creates a new object.
*/
Self.create =
prototype.create =
	function( ...args )
{
	let key;
	let keys;
	let rank;
	let twig;
	let twigDup;
	if( this !== Self )
	{
		twig = this._twig;
		keys = this.keys;
		twigDup = false;
	}
	else
	{
		twig = { };
		keys = [ ];
		twigDup = true;
	}
	for( let a = 0, alen = args.length; a < alen; a += 2 )
	{
		let arg = args[ a + 1 ];
		switch( args[ a ] )
		{
			case 'twig:add':if( twigDup !== true )
				{
					twig = ti2c.copy( twig );
					keys = keys.slice( );
					twigDup = true;
				}
				key = arg;
				arg = args[ ++a + 1 ];
				if( twig[ key ] !== undefined ) { throw ( new Error( ) ); }
				twig[ key ] = arg;
				keys.push( key );
				break;
			case 'twig:init':twigDup = true;
				twig = arg;
				keys = args[ ++a + 1 ];
/**/			if( CHECK )
/**/			{
/**/				if( !Array.isArray( keys ) ) { throw ( new Error( ) ); }
/**/				if( Object.keys( twig ).length !== keys.length ) { throw ( new Error( ) ); }
/**/				for( let key of keys )
/**/				{
/**/					if( twig[ key ] === undefined ) { throw ( new Error( ) ); }
/**/				}
/**/			}
				break;
			case 'twig:insert':if( twigDup !== true )
				{
					twig = ti2c.copy( twig );
					keys = keys.slice( );
					twigDup = true;
				}
				key = arg;
				rank = args[ a + 2 ];
				arg = args[ a + 3 ];
				a += 2;
				if( twig[ key ] !== undefined ) { throw ( new Error( ) ); }
				if( rank < 0 || rank > keys.length ) { throw ( new Error( ) ); }
				twig[ key ] = arg;
				keys.splice( rank, 0, key );
				break;
			case 'twig:remove':if( twigDup !== true )
				{
					twig = ti2c.copy( twig );
					keys = keys.slice( );
					twigDup = true;
				}
				if( twig[ arg ] === undefined ) { throw ( new Error( ) ); }
				delete twig[ arg ];
				keys.splice( keys.indexOf( arg ), 1 );
				break;
			case 'twig:set+':if( twigDup !== true )
				{
					twig = ti2c.copy( twig );
					keys = keys.slice( );
					twigDup = true;
				}
				key = arg;
				arg = args[ ++a + 1 ];
				if( twig[ key ] === undefined ) { keys.push( key ); }
				twig[ key ] = arg;
				break;
			case 'twig:set':if( twigDup !== true )
				{
					twig = ti2c.copy( twig );
					keys = keys.slice( );
					twigDup = true;
				}
				key = arg;
				arg = args[ ++a + 1 ];
				if( twig[ key ] === undefined ) { throw ( new Error( ) ); }
				twig[ key ] = arg;
				break;
			default :throw ( new Error( args[ a ] ) );
		}
	}
/**/if( CHECK )
/**/{
/**/	for( let key of keys )
/**/	{
/**/		const o = twig[ key ];
/**/		if(
/**/			o.ti2ctype !== tt__ti2c__Ast_And && o.ti2ctype !== tt__ti2c__Ast_ArrayLiteral
/**/			&& o.ti2ctype !== tt__ti2c__Ast_ArrowFunction
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Assign
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Await
/**/			&& o.ti2ctype !== tt__ti2c__Ast_BitwiseAnd
/**/			&& o.ti2ctype !== tt__ti2c__Ast_BitwiseAndAssign
/**/			&& o.ti2ctype !== tt__ti2c__Ast_BitwiseLeftShift
/**/			&& o.ti2ctype !== tt__ti2c__Ast_BitwiseNot
/**/			&& o.ti2ctype !== tt__ti2c__Ast_BitwiseOr
/**/			&& o.ti2ctype !== tt__ti2c__Ast_BitwiseOrAssign
/**/			&& o.ti2ctype !== tt__ti2c__Ast_BitwiseRightShift
/**/			&& o.ti2ctype !== tt__ti2c__Ast_BitwiseUnsignedRightShift
/**/			&& o.ti2ctype !== tt__ti2c__Ast_BitwiseXor
/**/			&& o.ti2ctype !== tt__ti2c__Ast_BitwiseXorAssign
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Boolean
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Call
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Comma
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Condition
/**/			&& o.ti2ctype !== tt__ti2c__Ast_ConditionalDot
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Delete
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Differs
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Divide
/**/			&& o.ti2ctype !== tt__ti2c__Ast_DivideAssign
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Dot
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Equals
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Func_Self
/**/			&& o.ti2ctype !== tt__ti2c__Ast_GreaterOrEqual
/**/			&& o.ti2ctype !== tt__ti2c__Ast_GreaterThan
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Instanceof
/**/			&& o.ti2ctype !== tt__ti2c__Ast_LessOrEqual
/**/			&& o.ti2ctype !== tt__ti2c__Ast_LessThan
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Member
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Minus
/**/			&& o.ti2ctype !== tt__ti2c__Ast_MinusAssign
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Multiply
/**/			&& o.ti2ctype !== tt__ti2c__Ast_MultiplyAssign
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Negate
/**/			&& o.ti2ctype !== tt__ti2c__Ast_New
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Not
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Null
/**/			&& o.ti2ctype !== tt__ti2c__Ast_NullishCoalescence
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Number
/**/			&& o.ti2ctype !== tt__ti2c__Ast_ObjLiteral
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Or
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Plus
/**/			&& o.ti2ctype !== tt__ti2c__Ast_PlusAssign
/**/			&& o.ti2ctype !== tt__ti2c__Ast_PostDecrement
/**/			&& o.ti2ctype !== tt__ti2c__Ast_PostIncrement
/**/			&& o.ti2ctype !== tt__ti2c__Ast_PreDecrement
/**/			&& o.ti2ctype !== tt__ti2c__Ast_PreIncrement
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Regex
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Remainder
/**/			&& o.ti2ctype !== tt__ti2c__Ast_RemainderAssign
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Spread
/**/			&& o.ti2ctype !== tt__ti2c__Ast_String
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Typeof
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Undefined
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Var
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Yield
/**/		)
/**/		{
/**/			throw ( new Error( ) );
/**/		}
/**/	}
/**/}
	const hash = ti2c._hashKeys( 480650326, keys, twig );
	let ihash = hash;
	let uhash;
	let lhash;
	for( let wr = imap.get( hash ); wr; wr = imap.get( ihash += 4.76837158203125e-7 ) )
	{
		const e = wr.deref( );
		if( e )
		{
			lhash = ihash;
			const len = keys.length;
			const ekeys = e.keys;
			const etwig = e._twig;
			if( keys.length !== ekeys.length ) { continue; }
			let eq = true;
			for( let a = 0; a < len; a++ )
			{
				const key = keys[ a ];
				if( key !== ekeys[ a ] || twig[ key ] !== etwig[ key ] ) { eq = false; break; }
			}
			if( eq )
			{
				if( uhash !== undefined )
				{
					imap.set( ihash, imap.get( uhash ) );
					imap.set( uhash, wr );
				}
				return e;
			}
		}
		else
		{
			if( uhash === undefined ) { uhash = ihash; }
		}
	}
	if( uhash === undefined ) { uhash = ihash; if( lhash < uhash ) { lhash = uhash; } }
	if( lhash !== undefined )
	{
		lhash += 4.76837158203125e-7;
		while( imap.get( lhash ) ) { imap.set( lhash, undefined ); lhash += 4.76837158203125e-7; }
	}
	const newti2c = new Constructor( hash, keys, twig );
	imap.set( uhash, new WeakRef( newti2c ) );
	if( cSize ) { cPut( newti2c ); }
	return newti2c;
};

/*
| Type reflection.
*/
prototype.ti2ctype = Self;

/*
| Reflection for debugging.
*/
Self.__TI2C_NAME__ = prototype.__TI2C_NAME__ = 'twig@<ti2c:Ast/Type/Expr';

/*
| Returns the element at rank.
*/
prototype.atRank = ti2c._proto.twigAtRank;

/*
| Returns the element by key.
*/
prototype.get = ti2c._proto.twigGet;

/*
| Returns the key at a rank.
*/
prototype.getKey = ti2c._proto.twigGetKey;

/*
| Returns the length of the twig.
*/
ti2c._proto.lazyValue( prototype, 'length', ti2c._proto.twigLength );

/*
| Returns the rank of the key.
*/
ti2c._proto.lazyFunction( prototype, 'rankOf', ti2c._proto.twigRankOf );

/*
| Returns the twig with the element at key set.
*/
prototype.set = ti2c._proto.twigSet;

/*
| Iterates over the twig.
*/
prototype[ Symbol.iterator ] =
	function*( ) { for( let a = 0, al = this.length; a < al; a++ ) { yield this.atRank( a ); } };

/*
| Reverse iterates over the twig.
*/
prototype.reverse =
	function*( ) { for( let a = this.length - 1; a >= 0; a-- ) { yield this.atRank( a ); } };

/*
| Creates an empty group of this type.
*/
ti2c._proto.lazyStaticValue( Self, 'Empty', function( ) { return Self.create( ); } );

/*
| Grows the twig.
*/
Self.Grow =
	function( ...args )
{
	let ranks = [ ];
	let twig = { };
	for( let a = 0, alen = args.length; a < alen; a += 2 )
	{
		const key = args[ a ];
		ranks.push( key );
		twig[ key ] = args[ a + 1 ];
	}
	return Self.create( 'twig:init', twig, ranks );
};
