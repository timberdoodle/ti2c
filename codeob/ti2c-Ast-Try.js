/*
| This is an auto generated file.
| Editing this might be rather futile.
*/

import { Self as tt__ti2c__Ast_Block } from '{{ti2c:Ast/Block}}';
import { Self as tt__ti2c__Ast_Var } from '{{ti2c:Ast/Var}}';

/*
| The interning map.
*/
let imap = new Map( );

/*
| Caching queue.
*/
let cSize = globalThis.TI2C_RETENTION ? 1024 : 0;
let cArray = cSize ? new Array( cSize ) : undefined;
let cPos = 0;

/*
| Timestamp since last turnaround
*/
let cTurn = cSize ? Date.now( ) : undefined;

/*
| Puts an entry in the caching queue.
*/
const cPut =
	function( o )
{
	cArray[ cPos ] = o;
	cPos = ++cPos % cSize;
	if( !cPos )
	{
		const now = Date.now( );
		if( now - cTurn < globalThis.TI2C_RETENTION )
		{
			cPos = cSize;
			cSize *= 2;
			cArray.length = cSize;
		}
		cTurn = now;
	}
};

/*
| Constructor.
*/
const Constructor =
	function( hash, v_catchStatement, v_exceptionVar, v_tryStatement )
{
	this.__hash = hash;
	this.catchStatement = v_catchStatement;
	this.exceptionVar = v_exceptionVar;
	this.tryStatement = v_tryStatement;
	Object.freeze( this );
};

/*
| In case of checking all unknown access is to be trapped.
*/
/**/if( CHECK )
/**/{
/**/const Trap = function( ) { };
/**/Trap.prototype = ti2c.trap;
/**/Constructor.prototype = new Trap( );
/**/}

/*
| Constructor prototype.
*/
const prototype = Self.prototype = Constructor.prototype;
Self.__hash = -2050631931;

/*
| Creates a new object.
*/
Self.create =
prototype.create =
	function( ...args )
{
	let v_catchStatement;
	let v_exceptionVar;
	let v_tryStatement;
	if( this !== Self )
	{
		v_catchStatement = this.catchStatement;
		v_exceptionVar = this.exceptionVar;
		v_tryStatement = this.tryStatement;
	}
	for( let a = 0, alen = args.length; a < alen; a += 2 )
	{
		let arg = args[ a + 1 ];
		switch( args[ a ] )
		{
			case 'catchStatement':if( arg !== pass ) { v_catchStatement = arg; }
				break;
			case 'exceptionVar':if( arg !== pass ) { v_exceptionVar = arg; }
				break;
			case 'tryStatement':if( arg !== pass ) { v_tryStatement = arg; }
				break;
			default :throw ( new Error( args[ a ] ) );
		}
	}
/**/if( CHECK )
/**/{
/**/	if( v_catchStatement.ti2ctype !== tt__ti2c__Ast_Block ) { throw ( new Error( ) ); }
/**/	if( v_exceptionVar.ti2ctype !== tt__ti2c__Ast_Var ) { throw ( new Error( ) ); }
/**/	if( v_tryStatement.ti2ctype !== tt__ti2c__Ast_Block ) { throw ( new Error( ) ); }
/**/}
	const hash = ti2c._hashArgs( -2050631931, v_catchStatement, v_exceptionVar, v_tryStatement );
	let ihash = hash;
	let uhash;
	let lhash;
	for( let wr = imap.get( hash ); wr; wr = imap.get( ihash += 4.76837158203125e-7 ) )
	{
		const e = wr.deref( );
		if( e )
		{
			lhash = ihash;
			if(
				v_catchStatement === e.catchStatement && v_exceptionVar === e.exceptionVar
				&& v_tryStatement === e.tryStatement
			)
			{
				if( uhash !== undefined )
				{
					imap.set( ihash, imap.get( uhash ) );
					imap.set( uhash, wr );
				}
				return e;
			}
		}
		else
		{
			if( uhash === undefined ) { uhash = ihash; }
		}
	}
	if( uhash === undefined ) { uhash = ihash; if( lhash < uhash ) { lhash = uhash; } }
	if( lhash !== undefined )
	{
		lhash += 4.76837158203125e-7;
		while( imap.get( lhash ) ) { imap.set( lhash, undefined ); lhash += 4.76837158203125e-7; }
	}
	const newti2c = new Constructor( hash, v_catchStatement, v_exceptionVar, v_tryStatement );
	imap.set( uhash, new WeakRef( newti2c ) );
	if( cSize ) { cPut( newti2c ); }
	return newti2c;
};

/*
| Type reflection.
*/
prototype.ti2ctype = Self;

/*
| Reflection for debugging.
*/
Self.__TI2C_NAME__ = prototype.__TI2C_NAME__ = 'ti2c:Ast/Try';
