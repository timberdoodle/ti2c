/*
| This is an auto generated file.
| Editing this might be rather futile.
*/

import { Self as tt__ti2c__Ast_Block } from '{{ti2c:Ast/Block}}';
import { Self as tt__ti2c__Ast_Break } from '{{ti2c:Ast/Break}}';
import { Self as tt__ti2c__Ast_Check } from '{{ti2c:Ast/Check}}';
import { Self as tt__ti2c__Ast_Comment } from '{{ti2c:Ast/Comment}}';
import { Self as tt__ti2c__Ast_Const } from '{{ti2c:Ast/Const}}';
import { Self as tt__ti2c__Ast_Continue } from '{{ti2c:Ast/Continue}}';
import { Self as tt__ti2c__Ast_DestructDecl } from '{{ti2c:Ast/DestructDecl}}';
import { Self as tt__ti2c__Ast_DoWhile } from '{{ti2c:Ast/DoWhile}}';
import { Self as tt__ti2c__Ast_For } from '{{ti2c:Ast/For}}';
import { Self as tt__ti2c__Ast_ForIn } from '{{ti2c:Ast/ForIn}}';
import { Self as tt__ti2c__Ast_ForOf } from '{{ti2c:Ast/ForOf}}';
import { Self as tt__ti2c__Ast_If } from '{{ti2c:Ast/If}}';
import { Self as tt__ti2c__Ast_Import } from '{{ti2c:Ast/Import}}';
import { Self as tt__ti2c__Ast_Let } from '{{ti2c:Ast/Let}}';
import { Self as tt__ti2c__Ast_Return } from '{{ti2c:Ast/Return}}';
import { Self as tt__ti2c__Ast_Sep } from '{{ti2c:Ast/Sep}}';
import { Self as tt__ti2c__Ast_Switch } from '{{ti2c:Ast/Switch}}';
import { Self as tt__ti2c__Ast_Throw } from '{{ti2c:Ast/Throw}}';
import { Self as tt__ti2c__Ast_Try } from '{{ti2c:Ast/Try}}';
import { Self as tt__ti2c__Ast_VarDec } from '{{ti2c:Ast/VarDec}}';
import { Self as tt__ti2c__Ast_While } from '{{ti2c:Ast/While}}';
import { Self as tt__ti2c__Ast_And } from '{{ti2c:Ast/And}}';
import { Self as tt__ti2c__Ast_ArrayLiteral } from '{{ti2c:Ast/ArrayLiteral}}';
import { Self as tt__ti2c__Ast_ArrowFunction } from '{{ti2c:Ast/ArrowFunction}}';
import { Self as tt__ti2c__Ast_Assign } from '{{ti2c:Ast/Assign}}';
import { Self as tt__ti2c__Ast_Await } from '{{ti2c:Ast/Await}}';
import { Self as tt__ti2c__Ast_BitwiseAnd } from '{{ti2c:Ast/BitwiseAnd}}';
import { Self as tt__ti2c__Ast_BitwiseAndAssign } from '{{ti2c:Ast/BitwiseAndAssign}}';
import { Self as tt__ti2c__Ast_BitwiseLeftShift } from '{{ti2c:Ast/BitwiseLeftShift}}';
import { Self as tt__ti2c__Ast_BitwiseNot } from '{{ti2c:Ast/BitwiseNot}}';
import { Self as tt__ti2c__Ast_BitwiseOr } from '{{ti2c:Ast/BitwiseOr}}';
import { Self as tt__ti2c__Ast_BitwiseOrAssign } from '{{ti2c:Ast/BitwiseOrAssign}}';
import { Self as tt__ti2c__Ast_BitwiseRightShift } from '{{ti2c:Ast/BitwiseRightShift}}';
import { Self as tt__ti2c__Ast_BitwiseUnsignedRightShift } from '{{ti2c:Ast/BitwiseUnsignedRightShift}}';
import { Self as tt__ti2c__Ast_BitwiseXor } from '{{ti2c:Ast/BitwiseXor}}';
import { Self as tt__ti2c__Ast_BitwiseXorAssign } from '{{ti2c:Ast/BitwiseXorAssign}}';
import { Self as tt__ti2c__Ast_Boolean } from '{{ti2c:Ast/Boolean}}';
import { Self as tt__ti2c__Ast_Call } from '{{ti2c:Ast/Call}}';
import { Self as tt__ti2c__Ast_Comma } from '{{ti2c:Ast/Comma}}';
import { Self as tt__ti2c__Ast_Condition } from '{{ti2c:Ast/Condition}}';
import { Self as tt__ti2c__Ast_ConditionalDot } from '{{ti2c:Ast/ConditionalDot}}';
import { Self as tt__ti2c__Ast_Delete } from '{{ti2c:Ast/Delete}}';
import { Self as tt__ti2c__Ast_Differs } from '{{ti2c:Ast/Differs}}';
import { Self as tt__ti2c__Ast_Divide } from '{{ti2c:Ast/Divide}}';
import { Self as tt__ti2c__Ast_DivideAssign } from '{{ti2c:Ast/DivideAssign}}';
import { Self as tt__ti2c__Ast_Dot } from '{{ti2c:Ast/Dot}}';
import { Self as tt__ti2c__Ast_Equals } from '{{ti2c:Ast/Equals}}';
import { Self as tt__ti2c__Ast_Func_Self } from '{{ti2c:Ast/Func/Self}}';
import { Self as tt__ti2c__Ast_GreaterOrEqual } from '{{ti2c:Ast/GreaterOrEqual}}';
import { Self as tt__ti2c__Ast_GreaterThan } from '{{ti2c:Ast/GreaterThan}}';
import { Self as tt__ti2c__Ast_Instanceof } from '{{ti2c:Ast/Instanceof}}';
import { Self as tt__ti2c__Ast_LessOrEqual } from '{{ti2c:Ast/LessOrEqual}}';
import { Self as tt__ti2c__Ast_LessThan } from '{{ti2c:Ast/LessThan}}';
import { Self as tt__ti2c__Ast_Member } from '{{ti2c:Ast/Member}}';
import { Self as tt__ti2c__Ast_Minus } from '{{ti2c:Ast/Minus}}';
import { Self as tt__ti2c__Ast_MinusAssign } from '{{ti2c:Ast/MinusAssign}}';
import { Self as tt__ti2c__Ast_Multiply } from '{{ti2c:Ast/Multiply}}';
import { Self as tt__ti2c__Ast_MultiplyAssign } from '{{ti2c:Ast/MultiplyAssign}}';
import { Self as tt__ti2c__Ast_Negate } from '{{ti2c:Ast/Negate}}';
import { Self as tt__ti2c__Ast_New } from '{{ti2c:Ast/New}}';
import { Self as tt__ti2c__Ast_Not } from '{{ti2c:Ast/Not}}';
import { Self as tt__ti2c__Ast_Null } from '{{ti2c:Ast/Null}}';
import { Self as tt__ti2c__Ast_NullishCoalescence } from '{{ti2c:Ast/NullishCoalescence}}';
import { Self as tt__ti2c__Ast_Number } from '{{ti2c:Ast/Number}}';
import { Self as tt__ti2c__Ast_ObjLiteral } from '{{ti2c:Ast/ObjLiteral}}';
import { Self as tt__ti2c__Ast_Or } from '{{ti2c:Ast/Or}}';
import { Self as tt__ti2c__Ast_Plus } from '{{ti2c:Ast/Plus}}';
import { Self as tt__ti2c__Ast_PlusAssign } from '{{ti2c:Ast/PlusAssign}}';
import { Self as tt__ti2c__Ast_PostDecrement } from '{{ti2c:Ast/PostDecrement}}';
import { Self as tt__ti2c__Ast_PostIncrement } from '{{ti2c:Ast/PostIncrement}}';
import { Self as tt__ti2c__Ast_PreDecrement } from '{{ti2c:Ast/PreDecrement}}';
import { Self as tt__ti2c__Ast_PreIncrement } from '{{ti2c:Ast/PreIncrement}}';
import { Self as tt__ti2c__Ast_Regex } from '{{ti2c:Ast/Regex}}';
import { Self as tt__ti2c__Ast_Remainder } from '{{ti2c:Ast/Remainder}}';
import { Self as tt__ti2c__Ast_RemainderAssign } from '{{ti2c:Ast/RemainderAssign}}';
import { Self as tt__ti2c__Ast_Spread } from '{{ti2c:Ast/Spread}}';
import { Self as tt__ti2c__Ast_String } from '{{ti2c:Ast/String}}';
import { Self as tt__ti2c__Ast_Typeof } from '{{ti2c:Ast/Typeof}}';
import { Self as tt__ti2c__Ast_Undefined } from '{{ti2c:Ast/Undefined}}';
import { Self as tt__ti2c__Ast_Var } from '{{ti2c:Ast/Var}}';
import { Self as tt__ti2c__Ast_Yield } from '{{ti2c:Ast/Yield}}';

/*
| The interning map.
*/
let imap = new Map( );

/*
| Caching queue.
*/
let cSize = globalThis.TI2C_RETENTION ? 1024 : 0;
let cArray = cSize ? new Array( cSize ) : undefined;
let cPos = 0;

/*
| Timestamp since last turnaround
*/
let cTurn = cSize ? Date.now( ) : undefined;

/*
| Puts an entry in the caching queue.
*/
const cPut =
	function( o )
{
	cArray[ cPos ] = o;
	cPos = ++cPos % cSize;
	if( !cPos )
	{
		const now = Date.now( );
		if( now - cTurn < globalThis.TI2C_RETENTION )
		{
			cPos = cSize;
			cSize *= 2;
			cArray.length = cSize;
		}
		cTurn = now;
	}
};

/*
| Constructor.
*/
const Constructor =
	function( hash, list )
{
	this.__lazy = { };
	this.__hash = hash;
	this._list = list;
	Object.freeze( this );
	Object.freeze( list );
};

/*
| In case of checking all unknown access is to be trapped.
*/
/**/if( CHECK )
/**/{
/**/const Trap = function( ) { };
/**/Trap.prototype = ti2c.trap;
/**/Constructor.prototype = new Trap( );
/**/}

/*
| Constructor prototype.
*/
const prototype = Self.prototype = Constructor.prototype;
Self.__hash = -984515051;

/*
| Creates a new object.
*/
Self.create =
prototype.create =
	function( ...args )
{
	let list;
	let listDup;
	if( this !== Self )
	{
		list = this._list;
		listDup = false;
	}
	else
	{
		list = [ ];
		listDup = true;
	}
	for( let a = 0, alen = args.length; a < alen; a += 2 )
	{
		let arg = args[ a + 1 ];
		switch( args[ a ] )
		{
			case 'list:init':if( Array.isArray( arg ) )
				{
					list = arg;
					listDup = true;
				}
				else
				{
					list = arg._list;
					listDup = false;
				}
				break;
			case 'list:append':if( !listDup ) { list = list.slice( ); listDup = true; }
				list.push( arg );
				break;
			case 'list:insert':if( !listDup ) { list = list.slice( ); listDup = true; }
				list.splice( arg, 0, args[ ++a + 1 ] );
				break;
			case 'list:remove':if( !listDup ) { list = list.slice( ); listDup = true; }
				list.splice( arg, 1 );
				break;
			case 'list:set':if( !listDup ) { list = list.slice( ); listDup = true; }
				list[ arg ] = args[ ++a + 1 ];
				break;
			default :throw ( new Error( args[ a ] ) );
		}
	}
/**/if( CHECK )
/**/{
/**/	for( let o of list )
/**/	{
/**/		if(
/**/			o.ti2ctype !== tt__ti2c__Ast_Block && o.ti2ctype !== tt__ti2c__Ast_Break
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Check
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Comment
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Const
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Continue
/**/			&& o.ti2ctype !== tt__ti2c__Ast_DestructDecl
/**/			&& o.ti2ctype !== tt__ti2c__Ast_DoWhile
/**/			&& o.ti2ctype !== tt__ti2c__Ast_For
/**/			&& o.ti2ctype !== tt__ti2c__Ast_ForIn
/**/			&& o.ti2ctype !== tt__ti2c__Ast_ForOf
/**/			&& o.ti2ctype !== tt__ti2c__Ast_If
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Import
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Let
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Return
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Sep
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Switch
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Throw
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Try
/**/			&& o.ti2ctype !== tt__ti2c__Ast_VarDec
/**/			&& o.ti2ctype !== tt__ti2c__Ast_While
/**/			&& o.ti2ctype !== tt__ti2c__Ast_And
/**/			&& o.ti2ctype !== tt__ti2c__Ast_ArrayLiteral
/**/			&& o.ti2ctype !== tt__ti2c__Ast_ArrowFunction
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Assign
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Await
/**/			&& o.ti2ctype !== tt__ti2c__Ast_BitwiseAnd
/**/			&& o.ti2ctype !== tt__ti2c__Ast_BitwiseAndAssign
/**/			&& o.ti2ctype !== tt__ti2c__Ast_BitwiseLeftShift
/**/			&& o.ti2ctype !== tt__ti2c__Ast_BitwiseNot
/**/			&& o.ti2ctype !== tt__ti2c__Ast_BitwiseOr
/**/			&& o.ti2ctype !== tt__ti2c__Ast_BitwiseOrAssign
/**/			&& o.ti2ctype !== tt__ti2c__Ast_BitwiseRightShift
/**/			&& o.ti2ctype !== tt__ti2c__Ast_BitwiseUnsignedRightShift
/**/			&& o.ti2ctype !== tt__ti2c__Ast_BitwiseXor
/**/			&& o.ti2ctype !== tt__ti2c__Ast_BitwiseXorAssign
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Boolean
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Call
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Comma
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Condition
/**/			&& o.ti2ctype !== tt__ti2c__Ast_ConditionalDot
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Delete
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Differs
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Divide
/**/			&& o.ti2ctype !== tt__ti2c__Ast_DivideAssign
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Dot
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Equals
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Func_Self
/**/			&& o.ti2ctype !== tt__ti2c__Ast_GreaterOrEqual
/**/			&& o.ti2ctype !== tt__ti2c__Ast_GreaterThan
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Instanceof
/**/			&& o.ti2ctype !== tt__ti2c__Ast_LessOrEqual
/**/			&& o.ti2ctype !== tt__ti2c__Ast_LessThan
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Member
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Minus
/**/			&& o.ti2ctype !== tt__ti2c__Ast_MinusAssign
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Multiply
/**/			&& o.ti2ctype !== tt__ti2c__Ast_MultiplyAssign
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Negate
/**/			&& o.ti2ctype !== tt__ti2c__Ast_New
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Not
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Null
/**/			&& o.ti2ctype !== tt__ti2c__Ast_NullishCoalescence
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Number
/**/			&& o.ti2ctype !== tt__ti2c__Ast_ObjLiteral
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Or
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Plus
/**/			&& o.ti2ctype !== tt__ti2c__Ast_PlusAssign
/**/			&& o.ti2ctype !== tt__ti2c__Ast_PostDecrement
/**/			&& o.ti2ctype !== tt__ti2c__Ast_PostIncrement
/**/			&& o.ti2ctype !== tt__ti2c__Ast_PreDecrement
/**/			&& o.ti2ctype !== tt__ti2c__Ast_PreIncrement
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Regex
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Remainder
/**/			&& o.ti2ctype !== tt__ti2c__Ast_RemainderAssign
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Spread
/**/			&& o.ti2ctype !== tt__ti2c__Ast_String
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Typeof
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Undefined
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Var
/**/			&& o.ti2ctype !== tt__ti2c__Ast_Yield
/**/		)
/**/		{
/**/			throw ( new Error( ) );
/**/		}
/**/	}
/**/}
	const len = list.length;
	const hash = ti2c._hashIterable( -984515051, list );
	let ihash = hash;
	let uhash;
	let lhash;
	for( let wr = imap.get( hash ); wr; wr = imap.get( ihash += 4.76837158203125e-7 ) )
	{
		const e = wr.deref( );
		if( e )
		{
			lhash = ihash;
			const elist = e._list;
			if( len !== elist.length ) { continue; }
			let eq = true;
			for( let a = 0; a < len; a++ )
			{
				if( list[ a ] !== elist[ a ] ) { eq = false; break; }
			}
			if( eq )
			{
				if( uhash !== undefined )
				{
					imap.set( ihash, imap.get( uhash ) );
					imap.set( uhash, wr );
				}
				return e;
			}
		}
		else
		{
			if( uhash === undefined ) { uhash = ihash; }
		}
	}
	if( uhash === undefined ) { uhash = ihash; if( lhash < uhash ) { lhash = uhash; } }
	if( lhash !== undefined )
	{
		lhash += 4.76837158203125e-7;
		while( imap.get( lhash ) ) { imap.set( lhash, undefined ); lhash += 4.76837158203125e-7; }
	}
	const newti2c = new Constructor( hash, list );
	imap.set( uhash, new WeakRef( newti2c ) );
	if( cSize ) { cPut( newti2c ); }
	return newti2c;
};

/*
| Type reflection.
*/
prototype.ti2ctype = Self;

/*
| Reflection for debugging.
*/
Self.__TI2C_NAME__ =
prototype.__TI2C_NAME__ = 'list@(<ti2c:Ast/Type/Statement,<ti2c:Ast/Type/Expr)';

/*
| Returns the list with an element appended.
*/
prototype.append = ti2c._proto.listAppend;

/*
| Returns the list with another list appended.
*/
prototype.appendList = ti2c._proto.listAppendList;

/*
| Returns an array clone of the list.
*/
prototype.clone = ti2c._proto.listClone;

/*
| Returns the first element of the list.
*/
ti2c._proto.lazyValue( prototype, 'first', ti2c._proto.listFirst );

/*
| Returns one element from the list.
*/
prototype.get = ti2c._proto.listGet;

/*
| Returns the list with one element inserted.
*/
prototype.insert = ti2c._proto.listInsert;

/*
| Returns the last element of the list.
*/
ti2c._proto.lazyValue( prototype, 'last', ti2c._proto.listLast );

/*
| Returns the length of the list.
*/
ti2c._proto.lazyValue( prototype, 'length', ti2c._proto.listLength );

/*
| Returns the list with one element removed.
*/
prototype.remove = ti2c._proto.listRemove;

/*
| Returns the list with one element set.
*/
prototype.set = ti2c._proto.listSet;

/*
| Returns a slice from the list.
*/
prototype.slice = ti2c._proto.listSlice;

/*
| Returns a sorted list.
*/
prototype.sort = ti2c._proto.listSort;

/*
| Forwards the iterator.
*/
prototype[ Symbol.iterator ] = function( ) { return this._list[ Symbol.iterator ]( ); };

/*
| Reverse iterates over the list.
*/
prototype.reverse =
	function*( ) { for( let a = this.length - 1; a >= 0; a-- ) { yield this._list[ a ]; } };

/*
| Creates the list from an Array.
*/
Self.Array = function( array ) { return Self.create( 'list:init', array ); };

/*
| Creates the list with direct elements.
*/
Self.Elements =
	function( ) { return Self.create( 'list:init', Array.prototype.slice.call( arguments ) ); };

/*
| Creates an empty list of this type.
*/
ti2c._proto.lazyStaticValue( Self, 'Empty', function( ) { return Self.create( ); } );
