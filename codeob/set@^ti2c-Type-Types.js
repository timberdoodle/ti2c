/*
| This is an auto generated file.
| Editing this might be rather futile.
*/

import { Self as tt__ti2c__Type_InFile } from '{{ti2c:Type/InFile}}';
import { Self as tt__ti2c__Type_Primitive_Boolean } from '{{ti2c:Type/Primitive/Boolean}}';
import { Self as tt__ti2c__Type_Primitive_Date } from '{{ti2c:Type/Primitive/Date}}';
import { Self as tt__ti2c__Type_Primitive_Function } from '{{ti2c:Type/Primitive/Function}}';
import { Self as tt__ti2c__Type_Primitive_Integer } from '{{ti2c:Type/Primitive/Integer}}';
import { Self as tt__ti2c__Type_Primitive_Null } from '{{ti2c:Type/Primitive/Null}}';
import { Self as tt__ti2c__Type_Primitive_Number } from '{{ti2c:Type/Primitive/Number}}';
import { Self as tt__ti2c__Type_Primitive_Protean } from '{{ti2c:Type/Primitive/Protean}}';
import { Self as tt__ti2c__Type_Primitive_Undefined } from '{{ti2c:Type/Primitive/Undefined}}';
import { Self as tt__ti2c__Type_Primitive_String } from '{{ti2c:Type/Primitive/String}}';
import { Self as tt__ti2c__Type_Ti2c_Arbitrary } from '{{ti2c:Type/Ti2c/Arbitrary}}';
import { Self as tt__ti2c__Type_Ti2c_Class } from '{{ti2c:Type/Ti2c/Class}}';
import { Self as tt__ti2c__Type_Ti2c_Collection_Group } from '{{ti2c:Type/Ti2c/Collection/Group}}';
import { Self as tt__ti2c__Type_Ti2c_Collection_List } from '{{ti2c:Type/Ti2c/Collection/List}}';
import { Self as tt__ti2c__Type_Ti2c_Collection_Set } from '{{ti2c:Type/Ti2c/Collection/Set}}';
import { Self as tt__ti2c__Type_Ti2c_Collection_Twig } from '{{ti2c:Type/Ti2c/Collection/Twig}}';

/*
| The interning map.
*/
let imap = new Map( );

/*
| Caching queue.
*/
let cSize = globalThis.TI2C_RETENTION ? 1024 : 0;
let cArray = cSize ? new Array( cSize ) : undefined;
let cPos = 0;

/*
| Timestamp since last turnaround
*/
let cTurn = cSize ? Date.now( ) : undefined;

/*
| Puts an entry in the caching queue.
*/
const cPut =
	function( o )
{
	cArray[ cPos ] = o;
	cPos = ++cPos % cSize;
	if( !cPos )
	{
		const now = Date.now( );
		if( now - cTurn < globalThis.TI2C_RETENTION )
		{
			cPos = cSize;
			cSize *= 2;
			cArray.length = cSize;
		}
		cTurn = now;
	}
};

/*
| Constructor.
*/
const Constructor =
	function( hash, set )
{
	this.__lazy = { };
	this.__hash = hash;
	this._set = set;
	Object.freeze( this );
	Object.freeze( set );
};

/*
| In case of checking all unknown access is to be trapped.
*/
/**/if( CHECK )
/**/{
/**/const Trap = function( ) { };
/**/Trap.prototype = ti2c.trap;
/**/Constructor.prototype = new Trap( );
/**/}

/*
| Constructor prototype.
*/
const prototype = Self.prototype = Constructor.prototype;
Self.__hash = 1289335941;

/*
| Creates a new object.
*/
Self.create =
prototype.create =
	function( ...args )
{
	let set;
	let setDup;
	if( this !== Self )
	{
		set = this._set;
		setDup = false;
	}
	else
	{
		set = new Set( );
		setDup = true;
	}
	for( let a = 0, alen = args.length; a < alen; a += 2 )
	{
		let arg = args[ a + 1 ];
		switch( args[ a ] )
		{
			case 'set:add':if( !setDup ) { set = new Set( set ); setDup = true; }
				set.add( arg, args[ a + 1 ] );
				break;
			case 'set:init':if( CHECK ) { if( !( arg instanceof Set ) ) { throw ( new Error( ) ); } }
				set = arg;
				setDup = true;
				break;
			case 'set:remove':if( !setDup ) { set = new Set( set ); setDup = true; }
				set.delete( arg );
				break;
			default :throw ( new Error( args[ a ] ) );
		}
	}
/**/if( CHECK )
/**/{
/**/	for( let v of set )
/**/	{
/**/		if(
/**/			v.ti2ctype !== tt__ti2c__Type_InFile
/**/			&& v.ti2ctype !== tt__ti2c__Type_Primitive_Boolean
/**/			&& v.ti2ctype !== tt__ti2c__Type_Primitive_Date
/**/			&& v.ti2ctype !== tt__ti2c__Type_Primitive_Function
/**/			&& v.ti2ctype !== tt__ti2c__Type_Primitive_Integer
/**/			&& v.ti2ctype !== tt__ti2c__Type_Primitive_Null
/**/			&& v.ti2ctype !== tt__ti2c__Type_Primitive_Number
/**/			&& v.ti2ctype !== tt__ti2c__Type_Primitive_Protean
/**/			&& v.ti2ctype !== tt__ti2c__Type_Primitive_Undefined
/**/			&& v.ti2ctype !== tt__ti2c__Type_Primitive_String
/**/			&& v.ti2ctype !== tt__ti2c__Type_Ti2c_Arbitrary
/**/			&& v.ti2ctype !== tt__ti2c__Type_Ti2c_Class
/**/			&& v.ti2ctype !== tt__ti2c__Type_Ti2c_Collection_Group
/**/			&& v.ti2ctype !== tt__ti2c__Type_Ti2c_Collection_List
/**/			&& v.ti2ctype !== tt__ti2c__Type_Ti2c_Collection_Set
/**/			&& v.ti2ctype !== tt__ti2c__Type_Ti2c_Collection_Twig
/**/		)
/**/		{
/**/			throw ( new Error( ) );
/**/		}
/**/	}
/**/}
	const hash = ti2c._hashSet( 1289335941, set );
	let ihash = hash;
	let uhash;
	let lhash;
	for( let wr = imap.get( hash ); wr; wr = imap.get( ihash += 4.76837158203125e-7 ) )
	{
		const e = wr.deref( );
		if( e )
		{
			lhash = ihash;
			const eset = e._set;
			if( set.size !== eset.size ) { continue; }
			let eq = true;
			for( let e of set ) { if( !eset.has( e ) ) { eq = false; break; } }
			if( eq )
			{
				if( uhash !== undefined )
				{
					imap.set( ihash, imap.get( uhash ) );
					imap.set( uhash, wr );
				}
				return e;
			}
		}
		else
		{
			if( uhash === undefined ) { uhash = ihash; }
		}
	}
	if( uhash === undefined ) { uhash = ihash; if( lhash < uhash ) { lhash = uhash; } }
	if( lhash !== undefined )
	{
		lhash += 4.76837158203125e-7;
		while( imap.get( lhash ) ) { imap.set( lhash, undefined ); lhash += 4.76837158203125e-7; }
	}
	const newti2c = new Constructor( hash, set );
	imap.set( uhash, new WeakRef( newti2c ) );
	if( cSize ) { cPut( newti2c ); }
	return newti2c;
};

/*
| Type reflection.
*/
prototype.ti2ctype = Self;

/*
| Reflection for debugging.
*/
Self.__TI2C_NAME__ = prototype.__TI2C_NAME__ = 'set@<ti2c:Type/Types';

/*
| Returns the set with one element added.
*/
prototype.add = ti2c._proto.setAdd;

/*
| Returns the set with another set added.
*/
prototype.addSet = ti2c._proto.setAddSet;

/*
| Returns a clone primitive.
*/
prototype.clone = function( ) { return new Set( this._set ); };

/*
| Returns true if the set has an element.
*/
prototype.has = ti2c._proto.setHas;

/*
| Returns the set with one element removed.
*/
prototype.remove = ti2c._proto.setRemove;

/*
| Returns the size of the set.
*/
ti2c._proto.lazyValue( prototype, 'size', ti2c._proto.setSize );

/*
| Returns the one and only element or the set if size != 1.
*/
ti2c._proto.lazyValue( prototype, 'trivial', ti2c._proto.setTrivial );

/*
| Forwards the iterator.
*/
prototype[ Symbol.iterator ] = function( ) { return this._set[ Symbol.iterator ]( ); };

/*
| Creates the set with direct elements.
*/
Self.Elements = function( ) { return Self.create( 'set:init', new Set( arguments ) ); };

/*
| Creates an empty set of this type.
*/
ti2c._proto.lazyStaticValue( Self, 'Empty', function( ) { return Self.create( ); } );
