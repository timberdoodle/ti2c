/*
| This is an auto generated file.
| Editing this might be rather futile.
*/

import { Self as tt__ti2c__Path_Self } from '{{ti2c:Path/Self}}';
import { Self as tt__ti2c__Package_Manager } from '{{ti2c:Package/Manager}}';
import { Self as tt__ti2c__Ouroboros_Root } from '{{ti2c:Ouroboros/Root}}';
import { Self as tt__ti2c__Type_Ti2c_Collection_Group } from '{{ti2c:Type/Ti2c/Collection/Group}}';
import { Self as tt__ti2c__Type_Set } from '{{ti2c:Type/Set}}';

/*
| The interning map.
*/
let imap = new Map( );

/*
| Caching queue.
*/
let cSize = globalThis.TI2C_RETENTION ? 1024 : 0;
let cArray = cSize ? new Array( cSize ) : undefined;
let cPos = 0;

/*
| Timestamp since last turnaround
*/
let cTurn = cSize ? Date.now( ) : undefined;

/*
| Puts an entry in the caching queue.
*/
const cPut =
	function( o )
{
	cArray[ cPos ] = o;
	cPos = ++cPos % cSize;
	if( !cPos )
	{
		const now = Date.now( );
		if( now - cTurn < globalThis.TI2C_RETENTION )
		{
			cPos = cSize;
			cSize *= 2;
			cArray.length = cSize;
		}
		cTurn = now;
	}
};

/*
| Constructor.
*/
const Constructor =
	function(
		hash,
		v__coreEntry,
		v_aPathCodeGen,
		v_jsonName,
		v_manager,
		v_type,
		v_typeItemsExpanded
	)
{
	this.__lazy = { };
	this.__hash = hash;
	this._coreEntry = v__coreEntry;
	this.aPathCodeGen = v_aPathCodeGen;
	this.jsonName = v_jsonName;
	this.manager = v_manager;
	this.type = v_type;
	this.typeItemsExpanded = v_typeItemsExpanded;
	Object.freeze( this );
};

/*
| In case of checking all unknown access is to be trapped.
*/
/**/if( CHECK )
/**/{
/**/const Trap = function( ) { };
/**/Trap.prototype = ti2c.trap;
/**/Constructor.prototype = new Trap( );
/**/}

/*
| Constructor prototype.
*/
const prototype = Self.prototype = Constructor.prototype;
Self.__hash = -104062830;

/*
| Creates a new object.
*/
Self.create =
prototype.create =
	function( ...args )
{
	let v__coreEntry;
	let v_aPathCodeGen;
	let v_jsonName;
	let v_manager;
	let v_type;
	let v_typeItemsExpanded;
	if( this !== Self )
	{
		v__coreEntry = this._coreEntry;
		v_aPathCodeGen = this.aPathCodeGen;
		v_jsonName = this.jsonName;
		v_manager = this.manager;
		v_type = this.type;
		v_typeItemsExpanded = this.typeItemsExpanded;
	}
	for( let a = 0, alen = args.length; a < alen; a += 2 )
	{
		let arg = args[ a + 1 ];
		switch( args[ a ] )
		{
			case '_coreEntry':if( arg !== pass ) { v__coreEntry = arg; }
				break;
			case 'aPathCodeGen':if( arg !== pass ) { v_aPathCodeGen = arg; }
				break;
			case 'jsonName':if( arg !== pass ) { v_jsonName = arg; }
				break;
			case 'manager':if( arg !== pass ) { v_manager = arg; }
				break;
			case 'type':if( arg !== pass ) { v_type = arg; }
				break;
			case 'typeItemsExpanded':if( arg !== pass ) { v_typeItemsExpanded = arg; }
				break;
			default :throw ( new Error( args[ a ] ) );
		}
	}
/**/if( CHECK )
/**/{
/**/	if( v__coreEntry !== undefined && typeof( v__coreEntry ) !== 'object' )
/**/	{
/**/		throw ( new Error( ) );
/**/	}
/**/	if( v_aPathCodeGen.ti2ctype !== tt__ti2c__Path_Self ) { throw ( new Error( ) ); }
/**/	if( typeof( v_jsonName ) !== 'string' ) { throw ( new Error( ) ); }
/**/	if(
/**/		v_manager.ti2ctype !== tt__ti2c__Package_Manager
/**/		&& v_manager.ti2ctype !== tt__ti2c__Ouroboros_Root
/**/	)
/**/	{
/**/		throw ( new Error( ) );
/**/	}
/**/	if( v_type.ti2ctype !== tt__ti2c__Type_Ti2c_Collection_Group ) { throw ( new Error( ) ); }
/**/	if( v_typeItemsExpanded.ti2ctype !== tt__ti2c__Type_Set ) { throw ( new Error( ) ); }
/**/}
	const hash =
		ti2c._hashArgs(
			-104062830,
			v__coreEntry,
			v_aPathCodeGen,
			v_jsonName,
			v_manager,
			v_type,
			v_typeItemsExpanded
		);
	let ihash = hash;
	let uhash;
	let lhash;
	for( let wr = imap.get( hash ); wr; wr = imap.get( ihash += 4.76837158203125e-7 ) )
	{
		const e = wr.deref( );
		if( e )
		{
			lhash = ihash;
			if(
				v__coreEntry === e._coreEntry && v_aPathCodeGen === e.aPathCodeGen
				&& v_jsonName === e.jsonName
				&& v_manager === e.manager
				&& v_type === e.type
				&& v_typeItemsExpanded === e.typeItemsExpanded
			)
			{
				if( uhash !== undefined )
				{
					imap.set( ihash, imap.get( uhash ) );
					imap.set( uhash, wr );
				}
				return e;
			}
		}
		else
		{
			if( uhash === undefined ) { uhash = ihash; }
		}
	}
	if( uhash === undefined ) { uhash = ihash; if( lhash < uhash ) { lhash = uhash; } }
	if( lhash !== undefined )
	{
		lhash += 4.76837158203125e-7;
		while( imap.get( lhash ) ) { imap.set( lhash, undefined ); lhash += 4.76837158203125e-7; }
	}
	const newti2c =
		new Constructor(
			hash,
			v__coreEntry,
			v_aPathCodeGen,
			v_jsonName,
			v_manager,
			v_type,
			v_typeItemsExpanded
		);
	imap.set( uhash, new WeakRef( newti2c ) );
	if( cSize ) { cPut( newti2c ); }
	return newti2c;
};

/*
| Type reflection.
*/
prototype.ti2ctype = Self;

/*
| Reflection for debugging.
*/
Self.__TI2C_NAME__ = prototype.__TI2C_NAME__ = 'ti2c:Package/Entry/Collection/Group';
