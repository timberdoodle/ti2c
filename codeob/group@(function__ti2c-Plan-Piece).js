/*
| This is an auto generated file.
| Editing this might be rather futile.
*/

import { Self as tt__ti2c__Plan_Piece } from '{{ti2c:Plan/Piece}}';
import { Self as tt__ti2c__Jsonfy_Spacing } from '{{ti2c:Jsonfy/Spacing}}';

/*
| The interning map.
*/
let imap = new Map( );

/*
| Caching queue.
*/
let cSize = globalThis.TI2C_RETENTION ? 1024 : 0;
let cArray = cSize ? new Array( cSize ) : undefined;
let cPos = 0;

/*
| Timestamp since last turnaround
*/
let cTurn = cSize ? Date.now( ) : undefined;

/*
| Puts an entry in the caching queue.
*/
const cPut =
	function( o )
{
	cArray[ cPos ] = o;
	cPos = ++cPos % cSize;
	if( !cPos )
	{
		const now = Date.now( );
		if( now - cTurn < globalThis.TI2C_RETENTION )
		{
			cPos = cSize;
			cSize *= 2;
			cArray.length = cSize;
		}
		cTurn = now;
	}
};

/*
| Constructor.
*/
const Constructor =
	function( hash, group, keys )
{
	this.__lazy = { };
	this.__hash = hash;
	this._group = group;
	this.keys = keys;
	Object.freeze( this );
	Object.freeze( group );
	Object.freeze( keys );
};

/*
| In case of checking all unknown access is to be trapped.
*/
/**/if( CHECK )
/**/{
/**/const Trap = function( ) { };
/**/Trap.prototype = ti2c.trap;
/**/Constructor.prototype = new Trap( );
/**/}

/*
| Constructor prototype.
*/
const prototype = Self.prototype = Constructor.prototype;
Self.__hash = -1084839804;

/*
| Creates a new object.
*/
Self.create =
prototype.create =
	function( ...args )
{
	let group;
	let groupDup;
	if( this !== Self )
	{
		group = this._group;
		groupDup = false;
	}
	else
	{
		group = { };
		groupDup = true;
	}
	for( let a = 0, alen = args.length; a < alen; a += 2 )
	{
		let arg = args[ a + 1 ];
		switch( args[ a ] )
		{
			case 'group:init':group = arg;
				groupDup = true;
				break;
			case 'group:set':if( !groupDup ) { group = ti2c.copy( group ); groupDup = true; }
				group[ arg ] = args[ ++a + 1 ];
				break;
			case 'group:remove':if( !groupDup ) { group = ti2c.copy( group ); groupDup = true; }
				delete group[ arg ];
				break;
			default :throw ( new Error( args[ a ] ) );
		}
	}
/**/if( CHECK )
/**/{
/**/	for( let k in group )
/**/	{
/**/		const o = group[ k ];
/**/		if( typeof( o ) !== 'function' && o.ti2ctype !== tt__ti2c__Plan_Piece )
/**/		{
/**/			throw ( new Error( ) );
/**/		}
/**/	}
/**/}
	const keys = Object.freeze( Object.keys( group ).sort( ) );
	const hash = ti2c._hashKeys( -1084839804, keys, group );
	let ihash = hash;
	let uhash;
	let lhash;
	for( let wr = imap.get( hash ); wr; wr = imap.get( ihash += 4.76837158203125e-7 ) )
	{
		const e = wr.deref( );
		if( e )
		{
			lhash = ihash;
			const ekeys = e.keys;
			const len = keys.length;
			const egroup = e._group;
			if( keys.length !== ekeys.length ) { continue; }
			let eq = true;
			for( let a = 0; a < len; a++ )
			{
				const key = keys[ a ];
				if( key !== ekeys[ a ] || group[ key ] !== egroup[ key ] ) { eq = false; break; }
			}
			if( eq )
			{
				if( uhash !== undefined )
				{
					imap.set( ihash, imap.get( uhash ) );
					imap.set( uhash, wr );
				}
				return e;
			}
		}
		else
		{
			if( uhash === undefined ) { uhash = ihash; }
		}
	}
	if( uhash === undefined ) { uhash = ihash; if( lhash < uhash ) { lhash = uhash; } }
	if( lhash !== undefined )
	{
		lhash += 4.76837158203125e-7;
		while( imap.get( lhash ) ) { imap.set( lhash, undefined ); lhash += 4.76837158203125e-7; }
	}
	const newti2c = new Constructor( hash, group, keys );
	imap.set( uhash, new WeakRef( newti2c ) );
	if( cSize ) { cPut( newti2c ); }
	return newti2c;
};

/*
| Creates a new object from json.
*/
Self.FromJson =
	function( json )
{
/**/if( CHECK ) { if( arguments.length !== 1 ) { throw ( new Error( ) ); } }
	let group;
	let jgroup;
	for( let name in json )
	{
		const arg = json[ name ];
		switch( name )
		{
			case '$type':if( arg !== 'group@(function,ti2c:Plan/Piece)' ) { throw ( new Error( ) ); }
				break;
			case 'group':jgroup = arg;
				break;
		}
	}
	if( !jgroup ) { throw ( new Error( ) ); }
	group = { };
	for( let k in jgroup )
	{
		switch( jgroup[ k ].$type )
		{
			case 'ti2c:Plan/Piece':group[ k ] = tt__ti2c__Plan_Piece.FromJson( jgroup[ k ] );
				break;
			default :throw ( new Error( ) );
		}
	}
	for( let k in group )
	{
		const o = group[ k ];
		if( typeof( o ) !== 'function' && o.ti2ctype !== tt__ti2c__Plan_Piece )
		{
			throw ( new Error( ) );
		}
	}
	const keys = Object.freeze( Object.keys( group ).sort( ) );
	const hash = ti2c._hashKeys( -1084839804, keys, group );
	let ihash = hash;
	let uhash;
	let lhash;
	for( let wr = imap.get( hash ); wr; wr = imap.get( ihash += 4.76837158203125e-7 ) )
	{
		const e = wr.deref( );
		if( e )
		{
			lhash = ihash;
			const ekeys = e.keys;
			const len = keys.length;
			const egroup = e._group;
			if( keys.length !== ekeys.length ) { continue; }
			let eq = true;
			for( let a = 0; a < len; a++ )
			{
				const key = keys[ a ];
				if( key !== ekeys[ a ] || group[ key ] !== egroup[ key ] ) { eq = false; break; }
			}
			if( eq )
			{
				if( uhash !== undefined )
				{
					imap.set( ihash, imap.get( uhash ) );
					imap.set( uhash, wr );
				}
				return e;
			}
		}
		else
		{
			if( uhash === undefined ) { uhash = ihash; }
		}
	}
	if( uhash === undefined ) { uhash = ihash; if( lhash < uhash ) { lhash = uhash; } }
	if( lhash !== undefined )
	{
		lhash += 4.76837158203125e-7;
		while( imap.get( lhash ) ) { imap.set( lhash, undefined ); lhash += 4.76837158203125e-7; }
	}
	const newtim = new Constructor( hash, group, keys );
	imap.set( uhash, new WeakRef( newtim ) );
	return newtim;
};

/*
| Type reflection.
*/
prototype.ti2ctype = Self;

/*
| Reflection for debugging.
*/
Self.__TI2C_NAME__ = prototype.__TI2C_NAME__ = 'group@(function,ti2c:Plan/Piece)';

/*
| Returns the group with another group added,
| overwriting collisions.
*/
prototype.addGroup = ti2c._proto.groupAddGroup;

/*
| Gets one element from the group.
*/
prototype.get = ti2c._proto.groupGet;

/*
| Returns the group with one element removed.
*/
prototype.remove = ti2c._proto.groupRemove;

/*
| Returns the group with one element set.
*/
prototype.set = ti2c._proto.groupSet;

/*
| Returns the size of the group.
*/
ti2c._proto.lazyValue( prototype, 'size', ti2c._proto.groupSize );

/*
| Iterates over the group by sorted keys.
*/
prototype[ Symbol.iterator ] =
	function*( ) { for( let key of this.keys ) { yield this.get( key ); } };

/*
| Creates an empty group of this type.
*/
ti2c._proto.lazyStaticValue( Self, 'Empty', function( ) { return Self.create( ); } );
Self.Table = function( group ) { return Self.create( 'group:init', group ); };

/*
| Stable jsonfy
*/
ti2c._proto.lazyFunction(
	prototype,
	'jsonfy',
	function( spacing )
{
	let i0space, i1space;
	let i2space;
	let iSpacing;
	if( spacing === undefined )
	{
		i0space = i1space = '';
		i2space = '';
	}
	else
	{
		if( typeof( spacing ) === 'string' )
		{
			i0space = '\n';
			i1space = '\n' + spacing;
			i2space = i1space + spacing;
			iSpacing = tt__ti2c__Jsonfy_Spacing.Start2( spacing );
		}
		else
		{
			i0space = '\n' + spacing.level;
			i1space = i0space + spacing.step;
			i2space = i1space + spacing.step;
			iSpacing = spacing.Inc2;
		}
	}
	const colon = i1space !== '' ? ': ' : ':';
	let r =
		'{' + i1space + '"$type"' + colon + '"group@(function,ti2c:Plan/Piece)",' + i1space
		+ '"group"'
		+ colon;
	r += '{';
	const keys = this.keys;
	const group = this._group;
	let first = true;
	for( let key of keys )
	{
		const val = group[ key ];
		if( !first ) { r += ','; } else { first = false; }
		r += i2space + '"' + key + '"' + colon;
		r += typeof( val ) === 'object' ? val.jsonfy( iSpacing ) : JSON.stringify( val );
	}
	r += i1space + '}' + i0space + '}';
	return r;
}
);

/*
| Json type identifier
*/
Self.$type = prototype.$type = 'group@(function,ti2c:Plan/Piece)';
Self.$fromJsonArgs = prototype.$fromJsonArgs = [ ];
