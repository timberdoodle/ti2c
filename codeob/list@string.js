/*
| This is an auto generated file.
| Editing this might be rather futile.
*/


/*
| The interning map.
*/
let imap = new Map( );

/*
| Caching queue.
*/
let cSize = globalThis.TI2C_RETENTION ? 1024 : 0;
let cArray = cSize ? new Array( cSize ) : undefined;
let cPos = 0;

/*
| Timestamp since last turnaround
*/
let cTurn = cSize ? Date.now( ) : undefined;

/*
| Puts an entry in the caching queue.
*/
const cPut =
	function( o )
{
	cArray[ cPos ] = o;
	cPos = ++cPos % cSize;
	if( !cPos )
	{
		const now = Date.now( );
		if( now - cTurn < globalThis.TI2C_RETENTION )
		{
			cPos = cSize;
			cSize *= 2;
			cArray.length = cSize;
		}
		cTurn = now;
	}
};

/*
| Constructor.
*/
const Constructor =
	function( hash, list )
{
	this.__lazy = { };
	this.__hash = hash;
	this._list = list;
	Object.freeze( this );
	Object.freeze( list );
};

/*
| In case of checking all unknown access is to be trapped.
*/
/**/if( CHECK )
/**/{
/**/const Trap = function( ) { };
/**/Trap.prototype = ti2c.trap;
/**/Constructor.prototype = new Trap( );
/**/}

/*
| Constructor prototype.
*/
const prototype = Self.prototype = Constructor.prototype;
Self.__hash = -2076116883;

/*
| Creates a new object.
*/
Self.create =
prototype.create =
	function( ...args )
{
	let list;
	let listDup;
	if( this !== Self )
	{
		list = this._list;
		listDup = false;
	}
	else
	{
		list = [ ];
		listDup = true;
	}
	for( let a = 0, alen = args.length; a < alen; a += 2 )
	{
		let arg = args[ a + 1 ];
		switch( args[ a ] )
		{
			case 'list:init':if( Array.isArray( arg ) )
				{
					list = arg;
					listDup = true;
				}
				else
				{
					list = arg._list;
					listDup = false;
				}
				break;
			case 'list:append':if( !listDup ) { list = list.slice( ); listDup = true; }
				list.push( arg );
				break;
			case 'list:insert':if( !listDup ) { list = list.slice( ); listDup = true; }
				list.splice( arg, 0, args[ ++a + 1 ] );
				break;
			case 'list:remove':if( !listDup ) { list = list.slice( ); listDup = true; }
				list.splice( arg, 1 );
				break;
			case 'list:set':if( !listDup ) { list = list.slice( ); listDup = true; }
				list[ arg ] = args[ ++a + 1 ];
				break;
			default :throw ( new Error( args[ a ] ) );
		}
	}
/**/if( CHECK )
/**/{
/**/	for( let o of list ) { if( typeof( o ) !== 'string' ) { throw ( new Error( ) ); } }
/**/}
	const len = list.length;
	const hash = ti2c._hashIterable( -2076116883, list );
	let ihash = hash;
	let uhash;
	let lhash;
	for( let wr = imap.get( hash ); wr; wr = imap.get( ihash += 4.76837158203125e-7 ) )
	{
		const e = wr.deref( );
		if( e )
		{
			lhash = ihash;
			const elist = e._list;
			if( len !== elist.length ) { continue; }
			let eq = true;
			for( let a = 0; a < len; a++ )
			{
				if( list[ a ] !== elist[ a ] ) { eq = false; break; }
			}
			if( eq )
			{
				if( uhash !== undefined )
				{
					imap.set( ihash, imap.get( uhash ) );
					imap.set( uhash, wr );
				}
				return e;
			}
		}
		else
		{
			if( uhash === undefined ) { uhash = ihash; }
		}
	}
	if( uhash === undefined ) { uhash = ihash; if( lhash < uhash ) { lhash = uhash; } }
	if( lhash !== undefined )
	{
		lhash += 4.76837158203125e-7;
		while( imap.get( lhash ) ) { imap.set( lhash, undefined ); lhash += 4.76837158203125e-7; }
	}
	const newti2c = new Constructor( hash, list );
	imap.set( uhash, new WeakRef( newti2c ) );
	if( cSize ) { cPut( newti2c ); }
	return newti2c;
};

/*
| Creates a new object from json.
*/
Self.FromJson =
	function( json )
{
/**/if( CHECK ) { if( arguments.length !== 1 ) { throw ( new Error( ) ); } }
	let jlist;
	let list;
	for( let name in json )
	{
		const arg = json[ name ];
		switch( name )
		{
			case '$type':if( arg !== 'list@string' ) { throw ( new Error( ) ); }
				break;
			case 'list':jlist = arg;
				break;
			default :throw ( new Error( ) );
		}
	}
	if( !jlist ) { throw ( new Error( ) ); }
	list = jlist;
	for( let o of list ) { if( typeof( o ) !== 'string' ) { throw ( new Error( ) ); } }
	const len = list.length;
	const hash = ti2c._hashIterable( -2076116883, list );
	let ihash = hash;
	let uhash;
	let lhash;
	for( let wr = imap.get( hash ); wr; wr = imap.get( ihash += 4.76837158203125e-7 ) )
	{
		const e = wr.deref( );
		if( e )
		{
			lhash = ihash;
			const elist = e._list;
			if( len !== elist.length ) { continue; }
			let eq = true;
			for( let a = 0; a < len; a++ )
			{
				if( list[ a ] !== elist[ a ] ) { eq = false; break; }
			}
			if( eq )
			{
				if( uhash !== undefined )
				{
					imap.set( ihash, imap.get( uhash ) );
					imap.set( uhash, wr );
				}
				return e;
			}
		}
		else
		{
			if( uhash === undefined ) { uhash = ihash; }
		}
	}
	if( uhash === undefined ) { uhash = ihash; if( lhash < uhash ) { lhash = uhash; } }
	if( lhash !== undefined )
	{
		lhash += 4.76837158203125e-7;
		while( imap.get( lhash ) ) { imap.set( lhash, undefined ); lhash += 4.76837158203125e-7; }
	}
	const newtim = new Constructor( hash, list );
	imap.set( uhash, new WeakRef( newtim ) );
	return newtim;
};

/*
| Type reflection.
*/
prototype.ti2ctype = Self;

/*
| Reflection for debugging.
*/
Self.__TI2C_NAME__ = prototype.__TI2C_NAME__ = 'list@string';

/*
| Returns the list with an element appended.
*/
prototype.append = ti2c._proto.listAppend;

/*
| Returns the list with another list appended.
*/
prototype.appendList = ti2c._proto.listAppendList;

/*
| Returns an array clone of the list.
*/
prototype.clone = ti2c._proto.listClone;

/*
| Returns the first element of the list.
*/
ti2c._proto.lazyValue( prototype, 'first', ti2c._proto.listFirst );

/*
| Returns one element from the list.
*/
prototype.get = ti2c._proto.listGet;

/*
| Returns the list with one element inserted.
*/
prototype.insert = ti2c._proto.listInsert;

/*
| Returns the last element of the list.
*/
ti2c._proto.lazyValue( prototype, 'last', ti2c._proto.listLast );

/*
| Returns the length of the list.
*/
ti2c._proto.lazyValue( prototype, 'length', ti2c._proto.listLength );

/*
| Returns the list with one element removed.
*/
prototype.remove = ti2c._proto.listRemove;

/*
| Returns the list with one element set.
*/
prototype.set = ti2c._proto.listSet;

/*
| Returns a slice from the list.
*/
prototype.slice = ti2c._proto.listSlice;

/*
| Returns a sorted list.
*/
prototype.sort = ti2c._proto.listSort;

/*
| Forwards the iterator.
*/
prototype[ Symbol.iterator ] = function( ) { return this._list[ Symbol.iterator ]( ); };

/*
| Reverse iterates over the list.
*/
prototype.reverse =
	function*( ) { for( let a = this.length - 1; a >= 0; a-- ) { yield this._list[ a ]; } };

/*
| Creates the list from an Array.
*/
Self.Array = function( array ) { return Self.create( 'list:init', array ); };

/*
| Creates the list with direct elements.
*/
Self.Elements =
	function( ) { return Self.create( 'list:init', Array.prototype.slice.call( arguments ) ); };

/*
| Creates an empty list of this type.
*/
ti2c._proto.lazyStaticValue( Self, 'Empty', function( ) { return Self.create( ); } );

/*
| Joins the string list into one string
*/
ti2c._proto.lazyFunction( prototype, 'join', function( sep ) { return this._list.join( sep ); } );

/*
| Stable jsonfy
*/
ti2c._proto.lazyFunction(
	prototype,
	'jsonfy',
	function( spacing )
{
	let i0space, i1space;
	let i2space;
	if( spacing === undefined )
	{
		i0space = i1space = '';
		i2space = '';
	}
	else
	{
		if( typeof( spacing ) === 'string' )
		{
			i0space = '\n';
			i1space = i0space + spacing;
			i2space = i1space + spacing;
		}
		else
		{
			i0space = '\n' + spacing.level;
			i1space = i0space + spacing.step;
			i2space = i1space + spacing.step;
		}
	}
	const colon = i1space !== '' ? ': ' : ':';
	let r = '{' + i1space + '"$type"' + colon + '"list@string",' + i1space + '"list"' + colon;
	if( this.length === 0 ) { return r + '[ ]' + i0space + '}'; }
	r += '[' + i2space;
	let first = true;
	for( let val of this._list )
	{
		if( !first ) { r += ',' + i2space; } else { first = false; }
		r += JSON.stringify( val );
	}
	r += i1space + ']' + i0space + '}';
	return r;
}
);

/*
| Json type identifier
*/
Self.$type = prototype.$type = 'list@string';
Self.$fromJsonArgs = prototype.$fromJsonArgs = [ ];
